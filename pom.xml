<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.robot.challenge</groupId>
	<artifactId>rc-webapp</artifactId>
	<version>1.1.0-SNAPSHOT</version>
	<packaging>war</packaging>

	<properties>
		<maven.build.timestamp.format>yyyyMMddHHmmss</maven.build.timestamp.format>
		<timestamp>${maven.build.timestamp}</timestamp>

		<java.version>1.8</java.version>
		<javaee7.version>1.0.3.Final</javaee7.version>


		<wildfly.version>10.1.0.Final</wildfly.version>

		<!-- Custom authentication mechanism -->
		<picketbox.version>4.9.6.Final</picketbox.version>

		<!-- 3rd party-->
		<owner.version>1.0.9</owner.version>
		<javaslang.version>2.0.0-RC4</javaslang.version>
		<velocity.version>1.7</velocity.version>

		<!-- Arquillian -->
		<arquillian.extension.rest.version>1.0.0.Alpha4</arquillian.extension.rest.version>
		<arquillian.suite.extension>1.1.2</arquillian.suite.extension>
		<shrinkwrap.version>2.2.2</shrinkwrap.version>

		<!-- Arquillian Java8 dependency -->
		<javassist.version>3.20.0-GA</javassist.version>

		<!-- LOGGING -->
		<slf4j.version>1.7.12</slf4j.version>
		<logback.version>1.1.3</logback.version>

		<!-- TEST -->
		<junit.version>4.12</junit.version>
		<rest.assured.version>2.7.0</rest.assured.version>
		<fest.assert.version>1.4</fest.assert.version>
	</properties>

	<scm>
		<connection>scm:git:git@bitbucket.org:robotchallenge2016/rc-webapp.git</connection>
		<developerConnection>scm:git:git@bitbucket.org:robotchallenge2016/rc-webapp.git</developerConnection>
		<url>https://bitbucket.org/robotchallenge2016/rc-webapp</url>
		<tag>HEAD</tag>
	</scm>

	<distributionManagement>
		<repository>
			<id>releases</id>
			<url>http://localhost:8081/nexus/content/repositories/releases</url>
		</repository>
		<snapshotRepository>
			<id>snapshots</id>
			<url>http://localhost:8081/nexus/content/repositories/snapshots</url>
		</snapshotRepository>
	</distributionManagement>

	<dependencyManagement>
		<dependencies>
			<!-- Contains versions of artifacts in Wildfly -->
			<dependency>
				<groupId>org.wildfly</groupId>
				<artifactId>wildfly-parent</artifactId>
				<version>${wildfly.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<repositories>
		<repository>
			<id>central</id>
			<url>https://repo1.maven.org/maven2/</url>
		</repository>
		<repository>
			<id>jboss-releases</id>
			<url>https://repository.jboss.org/nexus/content/repositories/releases/</url>
		</repository>
		<repository>
			<id>jboss-3rd-party-releases</id>
			<url>https://repository.jboss.org/nexus/content/repositories/thirdparty-releases/</url>
		</repository>
	</repositories>

	<dependencies>
		<!-- Contains dependencies on APIs -->
		<dependency>
			<groupId>org.jboss.spec</groupId>
			<artifactId>jboss-javaee-7.0</artifactId>
			<version>${javaee7.version}</version>
			<type>pom</type>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.sun.mail</groupId>
			<artifactId>javax.mail</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Logging API -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- RESTEasy -->
		<dependency>
			<groupId>org.jboss.resteasy</groupId>
			<artifactId>resteasy-jaxrs</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.resteasy</groupId>
			<artifactId>resteasy-validator-provider-11</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.resteasy</groupId>
			<artifactId>resteasy-multipart-provider</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.resteasy</groupId>
			<artifactId>resteasy-jackson2-provider</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Custom authentication mechanism -->
		<dependency>
			<groupId>org.picketbox</groupId>
			<artifactId>picketbox</artifactId>
			<version>${picketbox.version}</version>
            <scope>provided</scope>
		</dependency>

		<!-- FasterXML -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-json-provider</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- 3rd party -->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.javaslang</groupId>
			<artifactId>javaslang</artifactId>
			<version>${javaslang.version}</version>
		</dependency>

		<dependency>
			<groupId>org.aeonbits.owner</groupId>
			<artifactId>owner-java8</artifactId>
			<version>${owner.version}</version>
		</dependency>

		<dependency>
			<groupId>org.apache.velocity</groupId>
			<artifactId>velocity</artifactId>
			<version>${velocity.version}</version>
		</dependency>


		<!-- Arquillian -->
		<dependency>
			<groupId>org.wildfly.arquillian</groupId>
			<artifactId>wildfly-arquillian-container-managed</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.arquillian.junit</groupId>
			<artifactId>arquillian-junit-container</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.arquillian.protocol</groupId>
			<artifactId>arquillian-protocol-servlet</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Arquillian Java8 dependency -->
		<dependency>
			<groupId>org.javassist</groupId>
			<artifactId>javassist</artifactId>
			<version>${javassist.version}</version>
			<scope>test</scope>
		</dependency>
		<!-- Arquillian Maven dependency -->
		<dependency>
			<groupId>org.apache.maven</groupId>
			<artifactId>maven-aether-provider</artifactId>
			<version>3.2.5</version>
			<scope>test</scope>
		</dependency>

		<!-- Arquillian extension -->
		<dependency>
			<groupId>org.jboss.arquillian.extension</groupId>
			<artifactId>arquillian-rest-client-api</artifactId>
			<version>${arquillian.extension.rest.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.arquillian.extension</groupId>
			<artifactId>arquillian-rest-client-impl-3x</artifactId>
			<version>${arquillian.extension.rest.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.eu.ingwar.tools</groupId>
			<artifactId>arquillian-suite-extension</artifactId>
			<version>${arquillian.suite.extension}</version>
			<scope>test</scope>
		</dependency>

		<!-- Testing frameworks -->
		<dependency>
			<groupId>com.jayway.restassured</groupId>
			<artifactId>rest-assured</artifactId>
			<version>${rest.assured.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>

        <dependency>
            <groupId>org.easytesting</groupId>
            <artifactId>fest-assert</artifactId>
            <version>1.4</version>
            <scope>test</scope>
        </dependency>

		<!-- Shrinkwrap -->
		<dependency>
			<groupId>org.jboss.shrinkwrap.resolver</groupId>
			<artifactId>shrinkwrap-resolver-api-maven</artifactId>
			<version>${shrinkwrap.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.shrinkwrap.resolver</groupId>
			<artifactId>shrinkwrap-resolver-impl-maven</artifactId>
			<version>${shrinkwrap.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- Logging -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<!--<scope>test</scope>-->
		</dependency>

		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>${logback.version}</version>
			<!--<scope>test</scope>-->
		</dependency>

		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-core</artifactId>
			<version>${logback.version}</version>
			<!--<scope>test</scope>-->
		</dependency>

		<!--<dependency>-->
			<!--<groupId>org.slf4j</groupId>-->
			<!--<artifactId>jul-to-slf4j</artifactId>-->
			<!--<version>1.7.7</version>-->
		<!--</dependency>-->

	</dependencies>

	<profiles>
		<profile>
			<id>development</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<build>
				<resources>
					<resource>
						<directory>src/main/resources</directory>
						<excludes>
							<exclude>imports/TOURNAMENT.sql</exclude>
							<exclude>imports/ROBOT_SUBTYPE.sql</exclude>
							<exclude>imports/COMPETITION_SUBTYPE.sql</exclude>
						</excludes>
					</resource>
				</resources>
			</build>
		</profile>
		<profile>
			<id>release</id>
			<build>
				<resources>
					<resource>
						<directory>src/main/resources</directory>
						<includes>
							<include>**/**</include>
						</includes>
						<excludes>
							<exclude>dev-imports/*.sql</exclude>
						</excludes>
					</resource>
				</resources>
			</build>
		</profile>
	</profiles>

	<build>
		<finalName>${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<artifactId>maven-deploy-plugin</artifactId>
				<version>2.8.1</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>2.5.2</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.2</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.6</version>
				<executions>
					<execution>
						<id>1</id>
						<phase>compile</phase>
						<goals>
							<goal>manifest</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<!-- web.xml is not mandatory since JavaEE 5 -->
					<failOnMissingWebXml>false</failOnMissingWebXml>
					<archive>
						<manifest>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
						</manifest>
						<manifestEntries>
							<Build-time>${maven.build.timestamp}</Build-time>
							<SCM-branch>${scmBranch}</SCM-branch>
							<SCM-details>${scmBranch} - ${buildNumber}</SCM-details>
							<Docker-tag>${docker.tag}</Docker-tag>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>buildnumber-maven-plugin</artifactId>
				<version>1.3</version>
				<configuration>
					<shortRevisionLength>5</shortRevisionLength>
				</configuration>
				<executions>
					<execution>
						<phase>validate</phase>
						<goals>
							<goal>create</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

</project>
