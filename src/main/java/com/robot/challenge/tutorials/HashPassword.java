package com.robot.challenge.tutorials;

import com.robot.challenge.infrastructure.component.SecretDigester;
import com.robot.challenge.infrastructure.component.SecretGenerator;

public class HashPassword {
    
    public static void main(String[] args) {
        for(int i = 0; i < 25; ++i) {
            String secret = SecretGenerator.INSTANCE.generate();
            System.out.println(String.format("%s: %s", secret, SecretDigester.INSTANCE.hashSecret(secret)));
        }
    }
}