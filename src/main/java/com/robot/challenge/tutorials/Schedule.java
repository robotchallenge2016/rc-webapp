package com.robot.challenge.tutorials;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Schedule {

    public static void main(String[] args) {
        Schedule schedule = new Schedule();
        schedule.run();
    }

    public void run() {
        List<Integer> input = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        int matchDaysCount = input.size() - 1;
        int matchesInMatchDay = input.size() / 2;

        List<MatchDay> matchDays = new ArrayList<>(matchDaysCount);

        MatchDay firstRound = new MatchDay(matchesInMatchDay);
        matchDays.add(firstRound);
        for(int matchCounter = 0; matchCounter < matchesInMatchDay; ++matchCounter) {
            Match match = new Match(input.get(matchCounter), input.get(input.size() - 1 - matchCounter));
            firstRound.addMatch(match);
        }

        MatchDay baseMatchDay = firstRound;
        for(int matchDay = 0; matchDay < (matchDaysCount - 1) / 2; ++matchDay) {
            MatchDay evenRound = new MatchDay(matchesInMatchDay);
            evenRound.addMatch(new Match(baseMatchDay.awayOfMatchDay(0), baseMatchDay.awayOfMatchDay(matchesInMatchDay - 1)));
            for(int matchCounter = 1; matchCounter < matchesInMatchDay - 1; ++matchCounter) {
                Match match = new Match(baseMatchDay.awayOfMatchDay(matchesInMatchDay - matchCounter - 1), baseMatchDay.homeOfMatchDay(matchesInMatchDay - matchCounter));
                evenRound.addMatch(match);
            }
            evenRound.addMatch(new Match(baseMatchDay.homeOfMatchDay(0), baseMatchDay.homeOfMatchDay(1)));

            MatchDay oddRound = new MatchDay(matchesInMatchDay);
            oddRound.addMatch(new Match(evenRound.awayOfMatchDay(matchesInMatchDay - 1), evenRound.homeOfMatchDay(0)));
            for(int matchCounter = 1; matchCounter < matchesInMatchDay - 1; ++matchCounter) {
                Match match = new Match(evenRound.awayOfMatchDay(matchesInMatchDay - matchCounter - 1), evenRound.homeOfMatchDay(matchesInMatchDay - matchCounter));
                oddRound.addMatch(match);
            }
            oddRound.addMatch(new Match(evenRound.awayOfMatchDay(0), evenRound.homeOfMatchDay(1)));

            matchDays.add(evenRound);
            matchDays.add(oddRound);
            baseMatchDay = oddRound;
        }

        for(int matchDayCounter = 0; matchDayCounter < matchDaysCount; ++matchDayCounter) {
            System.out.println(String.format("Match: %d\n%s", matchDayCounter + 1, matchDays.get(matchDayCounter)));
        }

    }

    class MatchDay {
        private final List<Match> matches;

        MatchDay(int matchesInMatchDay) {
            matches = new ArrayList<>(matchesInMatchDay);
        }

        void addMatch(Match match) {
            matches.add(match);
        }

        Integer homeOfMatchDay(int matchDayNumber) {
            return matches.get(matchDayNumber).getHome();
        }

        Integer awayOfMatchDay(int matchDayNumber) {
            return matches.get(matchDayNumber).getAway();
        }

        @Override
        public String toString() {
            return String.join("\n", matches.stream().map(Match::toString).collect(Collectors.toList()));
        }
    }

    class Match {
        private final Integer home;
        private final Integer away;

        Match(Integer home, Integer away) {
            this.home = home;
            this.away = away;
        }

        public Integer getHome() {
            return home;
        }

        public Integer getAway() {
            return away;
        }

        @Override
        public String toString() {
            return home + " vs " + away;
        }
    }
}
