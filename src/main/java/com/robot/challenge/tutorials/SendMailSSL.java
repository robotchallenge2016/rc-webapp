package com.robot.challenge.tutorials;

import com.robot.challenge.infrastructure.service.MailAuthenticationProperties;
import com.robot.challenge.infrastructure.service.MailProperties;
import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SendMailSSL {
	private static final Logger LOG = LoggerFactory.getLogger(SendMailSSL.class);

	public static void main(String[] args) {
		LOG.info("Preparing mail settings");

		Map<String, String> bridge = new HashMap<>();

		MailProperties mailProperties = ConfigFactory.create(MailProperties.class);
		LOG.info(mailProperties.host());

		mailProperties.fill(bridge);

		MailAuthenticationProperties authenticationProperties = ConfigFactory.create(MailAuthenticationProperties.class);

		Properties properties = new Properties();
		properties.putAll(bridge);

//		Properties props = new Properties();
//		props.put("mail.smtp.host", "tl.krakow.pl");
//		props.put("mail.smtp.socketFactory.port", "465");
//		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//		props.put("mail.smtp.starttls.enable", "true");
//		props.put("mail.smtp.ssl.enable", "true");
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.port", "465");
//		props.put("mail.debug", "true");

		LOG.info("Preparing mail authentication");
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(authenticationProperties.login(),authenticationProperties.password());
				}
			});

		try {

			String text = "Dear Mail Crawler,<ul><li>item1</li><li>item2</li></ul>\n\n No spam to my email, please!";
			LOG.info("Preparing mail message");
			Message message = new MimeMessage(session);
			message.setContent(text, "text/html; charset=utf-8");
			message.setFrom(new InternetAddress("mklys@tl.krakow.pl"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("kmaci3k@gmail.com"));
			message.setSubject("Testing Subject");
//			message.setText("Dear Mail Crawler,<ul><li>item1</li><li>item2</li></ul>\n\n No spam to my email, please!");

			LOG.info("Sending...");
			Transport.send(message);

			LOG.info("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}