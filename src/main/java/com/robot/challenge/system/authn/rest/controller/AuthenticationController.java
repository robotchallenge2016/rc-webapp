package com.robot.challenge.system.authn.rest.controller;

import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.system.authn.rest.model.IntrospectionModel;
import com.robot.challenge.system.authn.rest.resource.AuthenticationResource;
import com.robot.challenge.system.authn.service.IntrospectionService;
import com.robot.challenge.system.rest.resource.SystemResource;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.repository.TournamentFinder;
import com.robot.challenge.tournament.rest.model.TournamentDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.security.Principal;

@Path(SystemResource.SYSTEM_PREFIX + AuthenticationResource.AUTHN_PREFIX)
public class AuthenticationController implements AuthenticationResource {

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticationController.class);
    @Inject
    private IntrospectionService introspectionService;
    @Inject
    private Principal principal;
    @Inject
    private TournamentFinder tournamentFinder;

    @Override
    public Response login() {
        LOG.info("{} already authenticated", principal.getName());
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response logout(@Context HttpServletRequest request) {
        LOG.info("Logging out {}", principal.getName());
        request.getSession().invalidate();
        try {
            request.logout();
            return Response.status(Response.Status.OK).build();
        } catch (ServletException ex) {
            LOG.error(ex.getMessage(), ex);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @Override
    public Response introspection(@Context HttpServletRequest request) {
        LOG.info("Introspecting: {}", principal.getName());
        Tournament tournament = tournamentFinder.retrieveByDomain(TournamentDomain.of(request))
                .orElseThrow(DataAccessException::entityNotFound);
        return introspectionService.introspection(tournament)
                .map(systemUser -> Response.status(Response.Status.OK).entity(IntrospectionModel.of(systemUser)).build())
                .orElseGet(() -> Response.status(Response.Status.UNAUTHORIZED).build());
    }
}
