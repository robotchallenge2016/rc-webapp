package com.robot.challenge.system.authn.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.system.dictionary.rest.model.SystemRoleModel;
import com.robot.challenge.system.user.domain.SystemUser;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IntrospectionModel {
    @JsonProperty
    private String guid;
    @JsonProperty
    private String login;
    @JsonProperty
    private String firstName;
    @JsonProperty
    private String lastName;
    @JsonProperty
    private SystemRoleModel systemRole;

    private IntrospectionModel() {

    }

    public String getGuid() {
        return guid;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public SystemRoleModel getSystemRole() {
        return systemRole;
    }

    public static IntrospectionModel of(SystemUser systemUser) {
        IntrospectionModel model = new IntrospectionModel();
        model.guid = systemUser.getGuid().getValue();
        model.login = systemUser.getLogin();
        model.firstName = systemUser.getFirstName();
        model.lastName = systemUser.getLastName();
        model.systemRole = SystemRoleModel.from(systemUser.getSystemRole());

        return model;
    }
}
