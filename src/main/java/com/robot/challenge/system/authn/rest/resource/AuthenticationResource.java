package com.robot.challenge.system.authn.rest.resource;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

public interface AuthenticationResource {

    String AUTHN_PREFIX = "/authn";
    String LOGIN_ENDPOINT = "/login/j_security_check";
    String LOGOUT_ENDPOINT = "/logout";
    String INTROSPECTION_ENDPOINT = "/introspection";

    @POST
    @Path(LOGIN_ENDPOINT)
    @PermitAll
    Response login();

    @POST
    @Path(LOGOUT_ENDPOINT)
    @PermitAll
    Response logout(@Context HttpServletRequest request);

    @GET
    @Path(INTROSPECTION_ENDPOINT)
    @PermitAll
    Response introspection(@Context HttpServletRequest request);
}
