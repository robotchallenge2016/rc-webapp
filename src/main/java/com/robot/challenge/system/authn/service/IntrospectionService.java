package com.robot.challenge.system.authn.service;

import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.system.user.repository.SystemUserFinder;
import com.robot.challenge.tournament.domain.Tournament;

import javax.inject.Inject;
import java.security.Principal;
import java.util.Optional;

public class IntrospectionService {

    private final Principal principal;
    private final SystemUserFinder systemUserFinder;

    @Inject
    public IntrospectionService(Principal principal, SystemUserFinder systemUserFinder) {
        this.principal = principal;
        this.systemUserFinder = systemUserFinder;
    }

    public Optional<SystemUser> introspection(Tournament tournament) {
        return systemUserFinder.retrieveByLogin(principal.getName(), tournament);
    }
}
