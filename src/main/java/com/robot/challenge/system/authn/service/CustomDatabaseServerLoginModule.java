package com.robot.challenge.system.authn.service;

import com.robot.challenge.tournament.rest.model.TournamentDomain;
import org.jboss.security.auth.spi.DatabaseServerLoginModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

public class CustomDatabaseServerLoginModule extends DatabaseServerLoginModule {

    private static final Logger LOG = LoggerFactory.getLogger(CustomDatabaseServerLoginModule.class);
    private static final String query =
            "SELECT SYSTEM_USER.LOGIN FROM SYSTEM_USER " +
            "LEFT JOIN TOURNAMENT ON SYSTEM_USER.TOURNAMENT_ID = TOURNAMENT.ID " +
            "WHERE SYSTEM_USER.LOGIN = ? AND TOURNAMENT.DOMAIN = ?";

    @Override
    public boolean login() throws LoginException {
        Optional<String> maybeUsername = maybeUsername();
        Optional<TournamentDomain> maybeDomain = domain();
        if(maybeUsername.isPresent() && maybeDomain.isPresent()) {
            String username = maybeUsername.get();
            TournamentDomain domain = maybeDomain.get();

            if(systemUserExistsInDomain(username, domain)) {
                return super.login();
            } else {
                LOG.warn("SystemUser({}) does not exist in tournament({})", username, domain.getDomain());
                return false;
            }
        }
        LOG.warn("Problem with login input data (principal={}, domain={})", maybeUsername, maybeDomain);
        return false;
    }

    private Optional<TournamentDomain> domain() {
        String WEB_REQUEST_KEY = "javax.servlet.http.HttpServletRequest";
        try {
            Object maybeRequest = PolicyContext.getContext(WEB_REQUEST_KEY);
            HttpServletRequest request = (HttpServletRequest) maybeRequest;
            return Optional.of(TournamentDomain.of(request));
        } catch (PolicyContextException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return Optional.empty();
    }

    private Optional<String> maybeUsername() {
        try {
            String[] usernameAndPassword = getUsernameAndPassword();
            if(usernameAndPassword.length > 0) {
                return Optional.of(usernameAndPassword[0]);
            }
        } catch (LoginException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return Optional.empty();
    }

    private Optional<DataSource> loadDataSource() {
        try {
            InitialContext ctx = new InitialContext();
            return Optional.of(DataSource.class.cast(ctx.lookup(dsJndiName))) ;
        } catch (NamingException ex) {
            LOG.error(ex.getMessage(), ex);
            return Optional.empty();
        }
    }

    private boolean systemUserExistsInDomain(String username, TournamentDomain domain) {
        return loadDataSource()
                .map(dataSource -> executeQuery(dataSource, username, domain))
                .orElse(false);
    }

    private boolean executeQuery(DataSource dataSource, String username, TournamentDomain domain) {
        try(Connection connection = dataSource.getConnection()) {
            try(PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, username);
                statement.setString(2, domain.getDomain());

                return statement.executeQuery().next();
            }
        } catch (SQLException ex) {
            LOG.error(ex.getMessage(), ex);
            return false;
        }
    }
}
