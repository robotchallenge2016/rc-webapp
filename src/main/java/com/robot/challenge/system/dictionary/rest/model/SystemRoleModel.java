package com.robot.challenge.system.dictionary.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.system.dictionary.domain.SystemRole;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by mklys on 05/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SystemRoleModel implements GloballyUniqueModel, VersionedModel {

    @JsonProperty
    private String guid;
    @JsonProperty
    private Integer priority;
    @JsonProperty
    private String name;
    @JsonProperty
    private Long version;

    @Override
    public String getGuid() {
        return guid;
    }

    public Integer getPriority() {
        return priority;
    }

    public String getName() {
        return name;
    }

    public Long getVersion() {
        return version;
    }

    private SystemRoleModel() {

    }

    public static SystemRoleModel from(SystemRole systemRole) {
        SystemRoleModel model = new SystemRoleModel();
        model.guid = systemRole.getGuid().getValue();
        model.priority = systemRole.getPriority();
        model.name = systemRole.getName();
        model.version = systemRole.getVersion();

        return model;
    }
}
