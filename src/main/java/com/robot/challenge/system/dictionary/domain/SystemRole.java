package com.robot.challenge.system.dictionary.domain;

import com.robot.challenge.infrastructure.domain.Guid;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "SYSTEM_ROLE", indexes = { @Index(name = "system_role_idx", columnList = "guid") })
public class SystemRole {

    public static final String ADMIN_VALUE = "ADMIN";
    public static final String USER_VALUE = "USER";
    public static final String COMPETITOR_VALUE = "COMPETITOR";

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "system_role_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "PRIORITY", unique = true, nullable = false, updatable = false)
    private Integer priority;
    @Column(name = "NAME", updatable = true, nullable = false, unique = false)
    private String name;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    protected SystemRole() {

    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public Integer getPriority() {
        return priority;
    }

    public String getName() {
        return name;
    }

    public Long getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SystemRole that = (SystemRole) o;

        if (!id.equals(that.id)) return false;
        if (!guid.equals(that.guid)) return false;
        if (!priority.equals(that.priority)) return false;
        if (!name.equals(that.name)) return false;
        return version.equals(that.version);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + guid.hashCode();
        result = 31 * result + priority.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }
}
