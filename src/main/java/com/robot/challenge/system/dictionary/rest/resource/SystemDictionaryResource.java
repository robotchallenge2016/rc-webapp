package com.robot.challenge.system.dictionary.rest.resource;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by mklys on 04/01/16.
 */
public interface SystemDictionaryResource {

    String DICTIONARY_PREFIX = "/dictionary";
    String SYSTEM_ROLE_ENDPOINT = DICTIONARY_PREFIX + "/role/all";

    @GET
    @Path(SYSTEM_ROLE_ENDPOINT)
    @Produces("application/json")
    @PermitAll
    Response roleDictionary();
}
