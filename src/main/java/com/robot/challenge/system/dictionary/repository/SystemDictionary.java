package com.robot.challenge.system.dictionary.repository;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * Created by mklys on 05/01/16.
 */
@Stateless
public class SystemDictionary {
    private static final Logger LOG = LoggerFactory.getLogger(SystemDictionary.class);

    @PersistenceContext(name = "PostgresqlPersistenceUnit")
    private EntityManager em;

    public List<SystemRole> retrieveRoles() {
        LOG.info("Retrieving all Roles");
        TypedQuery<SystemRole> q = em.createQuery("SELECT r FROM SystemRole r order by r.priority", SystemRole.class);
        return q.getResultList();
    }

    public Optional<SystemRole> retrieveRoleByGuid(Guid guid) {
        TypedQuery<SystemRole> q = em.createQuery("SELECT r FROM SystemRole r WHERE r.guid = :guid", SystemRole.class);
        q.setParameter("guid", guid);

        try {
            return Optional.of(q.getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

    public Optional<SystemRole> retrieveRoleByName(String name) {
        TypedQuery<SystemRole> q = em.createQuery("SELECT r FROM SystemRole r WHERE r.name = :name", SystemRole.class);
        q.setParameter("name", name);

        try {
            return Optional.of(q.getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }
}
