package com.robot.challenge.system.dictionary.rest.controller;

import com.robot.challenge.system.dictionary.repository.SystemDictionary;
import com.robot.challenge.system.dictionary.rest.model.SystemRoleModel;
import com.robot.challenge.system.dictionary.rest.resource.SystemDictionaryResource;
import com.robot.challenge.system.rest.resource.SystemResource;
import com.robot.challenge.tournament.rest.resource.TournamentEditionResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + SystemResource.SYSTEM_PREFIX)
public class SystemDictionaryController implements SystemDictionaryResource {

    @Inject
    private SystemDictionary systemDictionary;

    @Override
    public Response roleDictionary() {
        return Response.status(Response.Status.OK).entity(systemDictionary.retrieveRoles().stream().map(SystemRoleModel::from).collect(Collectors.toList())).build();
    }
}
