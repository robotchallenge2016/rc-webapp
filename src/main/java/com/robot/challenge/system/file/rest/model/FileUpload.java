package com.robot.challenge.system.file.rest.model;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MultivaluedMap;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class FileUpload {

	private static final Logger LOG = LoggerFactory.getLogger(FileUpload.class);
	private static final int EOF = -1;

	private String name;
	private String extension;
	private byte[] content;

	FileUpload(String name, String extension, byte[] content) {
		this.name = name;
		this.extension = extension;
		this.content = content;
	}

	public byte[] getContent() {
		return content;
	}

	public String getName() {
		return name;
	}

	public String getExtension() {
		return extension;
	}

	public static FileUpload from(MultipartFormDataInput input) {
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("uploadedFile");

		for (InputPart inputPart : inputParts) {
			try {
				String fileName = getFileName(inputPart.getHeaders());
				String[] splitted = fileName.split("\\.");
				String name = splitted[0];
				String extension = splitted.length > 1 ? splitted[1] : "";

				InputStream istream = inputPart.getBody(InputStream.class, null);

				ByteArrayOutputStream output = new ByteArrayOutputStream();
				byte buffer[] = new byte[1024];
				int n = 0;
				while (EOF != (n = istream.read(buffer))) {
					output.write(buffer, 0, n);
				}

				byte[] content = output.toByteArray();

				return new FileUpload(name, extension, content);
			} catch (IOException ex) {
				LOG.error(ex.getMessage(), ex);
			}
		}
		return null;
	}

	private static String getFileName(MultivaluedMap<String, String> header) {
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				return name[1].trim().replaceAll("\"", "");
			}
		}
		return "unknown";
	}
}