package com.robot.challenge.system.file.service;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.system.file.domain.SystemFileAware;
import com.robot.challenge.system.file.rest.model.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;

/**
 * Created by mklys on 06/02/16.
 */
public class SystemFileService {

    private static final Logger LOG = LoggerFactory.getLogger(SystemFileService.class);
    private final String fileStorageDirectory = "/data";

    void removeOldFile(Path fullFilename) {
        if(Files.exists(fullFilename)) {
            try {
                Files.delete(fullFilename);
            } catch (IOException ex) {
                LOG.warn(ex.getMessage(), ex);
            }
        }
    }

    void saveToDisk(Path fullFilename, FileUpload fileUpload) {
        try {
            Files.write(fullFilename, fileUpload.getContent());
        } catch (IOException ex) {
            LOG.warn(ex.getMessage(), ex);
        }
    }

    @Transactional
    public <T extends SystemFileAware & GloballyUnique> void addSystemFile(Guid guid, FileUpload file, JpaRepository<T> repository, JpaFinder<T> finder) {
        T entity = finder.retrieveByGuid(guid).orElseThrow(DataAccessException::entityNotFound);

        Path fullFileName = produceFullFileName(entity, file);
        entity.getFullFilename().ifPresent(this::removeOldFile);
        saveToDisk(fullFileName, file);

        entity.setFullFilename(fullFileName);
        repository.update(entity);
    }

    private <T extends SystemFileAware & GloballyUnique> Path produceFullFileName(T entity, FileUpload file) {
        String fileName = String.format("%s-%s-%s.%s",
                entity.getClass().getSimpleName().toLowerCase(),
                entity.getGuid().getValue(),
                Instant.now().getEpochSecond(),
                file.getExtension());
        return Paths.get(fileStorageDirectory).resolve(fileName);
    }
}
