package com.robot.challenge.system.file.domain;

import java.nio.file.Path;
import java.util.Optional;

/**
 * Created by mklys on 07/02/16.
 */
public interface SystemFileAware {
    Optional<Path> getFullFilename();
    void setFullFilename(Path fullFileName);
}
