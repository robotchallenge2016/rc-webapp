package com.robot.challenge.system.file.rest.resource;

import com.robot.challenge.system.dictionary.domain.SystemRole;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.annotation.security.RolesAllowed;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface SystemFileResource {

    String UPLOAD_IMAGE = "{guid}/file/upload";

    @POST
    @Path(UPLOAD_IMAGE)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response uploadImage(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                         @PathParam("guid") @NotNull @Size(min = 1) String guid,
                         MultipartFormDataInput input);
}
