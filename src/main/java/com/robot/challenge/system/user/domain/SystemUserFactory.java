package com.robot.challenge.system.user.domain;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.domain.Tournament;

public class SystemUserFactory extends Factory<SystemUser> {

    public SystemUserFactory() {
        super(SystemUser.class);
    }

    public SystemUser create(Tournament tournament, SystemRole systemRole) {
        return new SystemUser(Guid.generate(), systemRole, tournament);
    }
}
