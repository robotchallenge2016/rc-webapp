package com.robot.challenge.system.user.rest.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.system.dictionary.rest.model.SystemRoleModel;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.registration.domain.RegistrationStatus;

/**
 * Created by mklys on 04/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BriefSystemUserModel {
    private String guid;
    private String login;
    private SystemRoleModel systemRole;
    private boolean active;

    private BriefSystemUserModel() {

    }

    public String getGuid() {
        return guid;
    }

    public String getLogin() {
        return login;
    }

    public SystemRoleModel getSystemRole() {
        return systemRole;
    }

    public boolean isActive() {
        return active;
    }

    public static BriefSystemUserModel from(SystemUser systemUser) {
        BriefSystemUserModel model = new BriefSystemUserModel();
        model.guid = systemUser.getGuid().getValue();
        model.login = systemUser.getLogin();
        model.systemRole = SystemRoleModel.from(systemUser.getSystemRole());
        model.active = systemUser.getRegistrationInfo().getStatus() == RegistrationStatus.ACTIVE;

        return model;
    }
}
