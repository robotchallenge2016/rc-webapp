package com.robot.challenge.system.user.repository;

import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.system.user.domain.SystemUserFactory;
import com.robot.challenge.tournament.domain.Tournament;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SystemUserFinder extends JpaFinder<SystemUser> {

    private static final Logger LOG = LoggerFactory.getLogger(SystemUserFinder.class);

    @Inject
    public SystemUserFinder(SystemUserFactory factory) {
        super(factory);
    }

    public Optional<SystemUser> retrieveByLogin(String login, Tournament tournament) {
        LOG.info("Retrieving {} by login", SystemUser.class);
        return single(entityManager().createQuery("SELECT u FROM SystemUser u " +
                "LEFT JOIN FETCH u.systemRole ur " +
                "WHERE u.login = :login AND " +
                "u.tournament = :tournament", SystemUser.class)
                .setParameter("login", login)
                .setParameter("tournament", tournament));
    }

    public List<SystemUser> retrieveAll(Tournament tournament) {
        LOG.info("Retrieving all {}", SystemUser.class);
        return list(entityManager().createQuery("SELECT u FROM SystemUser u " +
                "LEFT JOIN FETCH u.systemRole ur " +
                "WHERE u.systemRole.name IN :roles AND " +
                "u.tournament = :tournament " +
                "ORDER BY u.login", SystemUser.class)
                .setParameter("roles", Arrays.asList(SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE))
                .setParameter("tournament", tournament));
    }
}
