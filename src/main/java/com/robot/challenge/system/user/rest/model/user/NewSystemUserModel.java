
package com.robot.challenge.system.user.rest.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.infrastructure.domain.Guid;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewSystemUserModel {

    @NotNull
    @Size(min = 1)
    private String login;
    @NotNull
    @Size(min = 1)
    private String firstName;
    @NotNull
    @Size(min = 1)
    private String lastName;
    @NotNull
    @Size(min = 1)
    private String password;
    @NotNull
    @Size(min = 1)
    @JsonProperty
    private String systemRoleGuid;

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }
    @JsonIgnore
    public Guid getSystemRoleGuid() {
        return Guid.from(systemRoleGuid);
    }

    public static NewSystemUserModelBuilder builder() {
        return new NewSystemUserModelBuilder();
    }

    public static class NewSystemUserModelBuilder {

        private final NewSystemUserModel model = new NewSystemUserModel();

        private NewSystemUserModelBuilder() {

        }

        public NewSystemUserModelBuilder setLogin(String login) {
            model.login = login;
            return this;
        }

        public NewSystemUserModelBuilder setFirstName(String firstName) {
            model.firstName = firstName;
            return this;
        }

        public NewSystemUserModelBuilder setLastName(String lastName) {
            model.lastName = lastName;
            return this;
        }

        public NewSystemUserModelBuilder setPassword(String password) {
            model.password = password;
            return this;
        }

        public NewSystemUserModelBuilder setSystemRoleGuid(String systemRoleGuid) {
            model.systemRoleGuid = systemRoleGuid;
            return this;
        }

        public NewSystemUserModel build() {
            return model;
        }
    }
}
