package com.robot.challenge.system.user.domain;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.infrastructure.component.SecretDigester;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.registration.domain.RegistrationInfo;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "SYSTEM_USER", indexes = { @Index(name = "system_user_idx", columnList = "guid") })
public class SystemUser implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "system_user_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "LOGIN", unique = true, nullable = false, updatable = false)
    private String login;
    @Column(name = "PASSWORD", nullable = false)
    private String password;
    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;
    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;
    @ManyToOne(optional = false)
    @JoinColumn(name = "SYSTEM_ROLE_ID", nullable = false)
    private SystemRole systemRole;
    @Embedded
    private RegistrationInfo registrationInfo;
    @Column(name = "CREATE_DATE_TIME", nullable = false, updatable = false)
    private Date createDateTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE_TIME")
    private Date updateDateTime;
    @ManyToOne(optional = false)
    @JoinColumn(name = "TOURNAMENT_ID", nullable = false, updatable = false)
    private Tournament tournament;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    private SystemUser() {

    }

    public SystemUser(Guid guid, SystemRole systemRole, Tournament tournament) {
        this.guid = guid;
        this.systemRole = systemRole;
        this.registrationInfo = new RegistrationInfo();
        this.createDateTime = Date.from(ZonedDateTime.now(ZoneId.of("UTC")).toInstant());
        this.tournament = tournament;
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public String getLogin() {
        return login;
    }

    public SystemUser setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public SystemUser setPassword(String password) {
        this.password = SecretDigester.INSTANCE.hashSecret(password);
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public SystemUser setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public SystemUser setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public SystemRole getSystemRole() {
        return systemRole;
    }

    public SystemUser setSystemRole(SystemRole systemRole) {
        this.systemRole = systemRole;
        return this;
    }

    public RegistrationInfo getRegistrationInfo() {
        return registrationInfo;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime.toInstant().atZone(ZoneId.of("UTC"));
    }

    public Optional<ZonedDateTime> getUpdateDateTime() {
        if(updateDateTime != null) {
            return Optional.of(updateDateTime.toInstant().atZone(ZoneId.of("UTC")));
        }
        return Optional.empty();
    }

    public SystemUser setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = Date.from(updateDateTime.toInstant());
        return this;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public Long getVersion() {
        return version;
    }

    public SystemUser setVersion(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SystemUser that = (SystemUser) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (guid != null ? !guid.equals(that.guid) : that.guid != null) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (systemRole != null ? !systemRole.equals(that.systemRole) : that.systemRole != null) return false;
        if (registrationInfo != null ? !registrationInfo.equals(that.registrationInfo) : that.registrationInfo != null)
            return false;
        return !(version != null ? !version.equals(that.version) : that.version != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (guid != null ? guid.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (systemRole != null ? systemRole.hashCode() : 0);
        result = 31 * result + (registrationInfo != null ? registrationInfo.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SystemUser{" +
                "registrationInfo=" + registrationInfo +
                ", systemRole=" + systemRole +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", id=" + id +
                ", guid=" + guid +
                ", login='" + login + '\'' +
                '}';
    }
}
