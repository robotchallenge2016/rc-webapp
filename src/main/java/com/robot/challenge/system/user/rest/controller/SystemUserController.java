package com.robot.challenge.system.user.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.system.rest.resource.SystemResource;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.system.user.repository.SystemUserFinder;
import com.robot.challenge.system.user.repository.SystemUserRepository;
import com.robot.challenge.system.user.rest.model.user.BriefSystemUserModel;
import com.robot.challenge.system.user.rest.model.user.ExistingSystemUserModel;
import com.robot.challenge.system.user.rest.model.user.NewSystemUserModel;
import com.robot.challenge.system.user.rest.resource.SystemUserResource;
import com.robot.challenge.system.user.service.SystemUserService;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + SystemResource.SYSTEM_PREFIX + SystemUserResource.USER_PREFIX)
public class SystemUserController implements SystemUserResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private SystemUserService service;
    @Inject
    private SystemUserFinder systemUserFinder;
    @Inject
    private SystemUserRepository systemUserRepository;

    public Response create(@NotNull @Size(min = 1) String editionGuid, @Valid NewSystemUserModel systemUser) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        SystemUser createdSystemUser = service.create(systemUser, tournament);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdSystemUser)).build();
    }

    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid,
                           @Valid ExistingSystemUserModel systemUser) {
        service.update(Guid.from(guid), systemUser);
        return Response.status(Response.Status.OK).build();
    }

    public Response delete(@NotNull @Size(min = 1) String editionGuid, @NotNull @Size(min = 1) String guid) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        service.delete(Guid.from(guid), tournament);
        return Response.status(Response.Status.OK).build();
    }

    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid, @NotNull @Size(min = 1) String guid) {
        SystemUser systemUser = systemUserFinder.retrieveByGuid(Guid.from(guid)).orElseThrow(DataAccessException::entityNotFound);
        return Response.status(Response.Status.OK).entity(ExistingSystemUserModel.from(systemUser)).build();
    }

    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        List<BriefSystemUserModel> entity = systemUserFinder.retrieveAll(tournament)
                .stream()
                .map(BriefSystemUserModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(entity).build();
    }

    @Override
    public Response loginExists(@NotNull @Size(min = 1) String editionGuid, @NotNull @Size(min = 1) String login) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        return systemUserFinder.retrieveByLogin(login, tournament)
                .map(r -> Response.status(Response.Status.OK))
                .orElse(Response.status(Response.Status.BAD_REQUEST))
                .build();
    }

    @Override
    public Response loginIsFree(@NotNull @Size(min = 1) String editionGuid, @NotNull @Size(min = 1) String login) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        return systemUserFinder.retrieveByLogin(login, tournament)
                .map(r -> Response.status(Response.Status.BAD_REQUEST))
                .orElse(Response.status(Response.Status.OK))
                .build();
    }

    @Override
    public Response markAsVerified(@NotNull @Size(min = 1) String editionGuid, @NotNull @Size(min = 1) String guid) {
        SystemUser competitorToVerify = systemUserFinder.retrieveByGuid(Guid.from(guid)).orElseThrow(DataAccessException::entityNotFound);
        competitorToVerify.getRegistrationInfo().markAsVerified();
        systemUserRepository.update(competitorToVerify);
        return Response.ok().build();
    }

    @Override
    public Response markAsUnverified(@NotNull @Size(min = 1) String editionGuid, @NotNull @Size(min = 1) String guid) {
        SystemUser competitorToVerify = systemUserFinder.retrieveByGuid(Guid.from(guid)).orElseThrow(DataAccessException::entityNotFound);
        competitorToVerify.getRegistrationInfo().markAsUnverified();
        systemUserRepository.update(competitorToVerify);
        return Response.ok().build();
    }
}
