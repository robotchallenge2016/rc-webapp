package com.robot.challenge.system.user.rest.resource;

import com.robot.challenge.infrastructure.rest.resource.CrudResource;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.system.user.rest.model.user.ExistingSystemUserModel;
import com.robot.challenge.system.user.rest.model.user.NewSystemUserModel;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface SystemUserResource extends CrudResource<NewSystemUserModel, ExistingSystemUserModel> {

    String USER_PREFIX = "/user";
    String CREATE = "/";
    String UPDATE = "/guid/{guid}";
    String DELETE = "/guid/{guid}";
    String RETRIEVE_SINGLE = "/guid/{guid}";
    String RETRIEVE_ALL = "/all";
    String USER_LOGIN_EXISTS = "/login/{login}/exists";
    String USER_LOGIN_IS_FREE = "/login/{login}/free";
    String USER_MARKED_AS_VERIFIED = "/guid/{guid}/verified";
    String USER_MARKED_AS_UNVERIFIED = "/guid/{guid}/unverified";

    @POST
    @Path(CREATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid NewSystemUserModel user);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingSystemUserModel user);

    @DELETE
    @Path(DELETE)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response retrieveAll(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);

    @GET
    @Path(USER_LOGIN_EXISTS)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response loginExists(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                         @PathParam("login") @NotNull @Size(min = 1) String login);

    @GET
    @Path(USER_LOGIN_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response loginIsFree(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                         @PathParam("login") @NotNull @Size(min = 1) String login);

    @POST
    @Path(USER_MARKED_AS_VERIFIED)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response markAsVerified(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @POST
    @Path(USER_MARKED_AS_UNVERIFIED)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response markAsUnverified(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                              @PathParam("guid") @NotNull @Size(min = 1) String guid);
}
