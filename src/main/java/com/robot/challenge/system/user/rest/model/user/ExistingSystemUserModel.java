package com.robot.challenge.system.user.rest.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.system.user.domain.SystemUser;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ExistingSystemUserModel implements GloballyUniqueModel, VersionedModel {

    private String guid;
    @NotNull
    @Size(min = 1)
    private String login;
    @NotNull
    @Size(min = 1)
    private String firstName;
    @NotNull
    @Size(min = 1)
    private String lastName;

    private String password;
    @NotNull
    @Size(min = 1)
    @JsonProperty("systemRoleGuid")
    private String systemRoleGuid;
    @NotNull
    @Min(value = 0L)
    private Long version;

    public String getGuid() {
        return guid;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }
    @JsonIgnore
    public Optional<String> getPassword() {
        return Optional.ofNullable(password);
    }
    @JsonIgnore
    public Guid getSystemRoleGuid() {
        return Guid.from(systemRoleGuid);
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingSystemUserModel from(SystemUser entity) {
        ExistingSystemUserModel model = new ExistingSystemUserModel();
        model.guid = entity.getGuid().getValue();
        model.login = entity.getLogin();
        model.firstName = entity.getFirstName();
        model.lastName = entity.getLastName();
        model.systemRoleGuid = entity.getSystemRole().getGuid().getValue();
        model.version = entity.getVersion();

        return model;
    }

    public static ExistingSystemUserModelBuilder builder() {
        return new ExistingSystemUserModelBuilder();
    }

    public static class ExistingSystemUserModelBuilder {

        private final ExistingSystemUserModel model = new ExistingSystemUserModel();

        private ExistingSystemUserModelBuilder() {

        }

        public ExistingSystemUserModelBuilder setLogin(String login) {
            model.login = login;
            return this;
        }

        public ExistingSystemUserModelBuilder setFirstName(String firstName) {
            model.firstName = firstName;
            return this;
        }

        public ExistingSystemUserModelBuilder setLastName(String lastName) {
            model.lastName = lastName;
            return this;
        }

        public ExistingSystemUserModelBuilder setPassword(String password) {
            model.password = password;
            return this;
        }

        public ExistingSystemUserModelBuilder setSystemRoleGuid(String systemRoleGuid) {
            model.systemRoleGuid = systemRoleGuid;
            return this;
        }

        public ExistingSystemUserModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingSystemUserModel build() {
            return model;
        }
    }    
}
