package com.robot.challenge.system.user.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class SystemUserException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        STILL_CONNECTED_TO_ROBOTS("User has still assigned robots"),
        LOGIN_ALREADY_EXISTS("Login already exists");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public SystemUserException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static SystemUserException loginAlreadyExists() {
        return new SystemUserException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.LOGIN_ALREADY_EXISTS,
                Cause.LOGIN_ALREADY_EXISTS.getErrorDescription());
    }

    public static SystemUserException stillConntectedToRobots() {
        return new SystemUserException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.STILL_CONNECTED_TO_ROBOTS,
                Cause.STILL_CONNECTED_TO_ROBOTS.getErrorDescription());
    }
}
