package com.robot.challenge.system.user.repository;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.system.user.domain.SystemUser;

import javax.ejb.Stateless;

@Stateless
public class SystemUserRepository extends JpaRepository<SystemUser> {

    public SystemUserRepository() {
        super(SystemUser.class);
    }
}
