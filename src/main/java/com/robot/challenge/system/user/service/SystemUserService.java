package com.robot.challenge.system.user.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.system.dictionary.repository.SystemDictionary;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.system.user.domain.SystemUserFactory;
import com.robot.challenge.system.user.exception.SystemUserException;
import com.robot.challenge.system.user.repository.SystemUserFinder;
import com.robot.challenge.system.user.repository.SystemUserRepository;
import com.robot.challenge.system.user.rest.model.user.ExistingSystemUserModel;
import com.robot.challenge.system.user.rest.model.user.NewSystemUserModel;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.competitor.service.CompetitorService;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.rest.model.NewTournamentOwnerModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

public class SystemUserService {

    private static final Logger LOG = LoggerFactory.getLogger(SystemUserService.class);

    private final CompetitorService competitorService;
    private final SystemUserFactory systemUserFactory;
    private final SystemUserRepository systemUserRepository;
    private final SystemUserFinder systemUserFinder;
    private final SystemDictionary systemDictionary;
    private final RobotFinder robotFinder;

    @Inject
    public SystemUserService(CompetitorService competitorService, SystemUserFactory systemUserFactory,
                             SystemUserRepository systemUserRepository, SystemUserFinder systemUserFinder,
                             SystemDictionary systemDictionary, RobotFinder robotFinder) {
        this.competitorService = competitorService;
        this.systemUserFactory = systemUserFactory;
        this.systemUserRepository = systemUserRepository;
        this.systemUserFinder = systemUserFinder;
        this.systemDictionary = systemDictionary;
        this.robotFinder = robotFinder;
    }

    private void validateLogin(String login, Tournament tournament) {
        systemUserFinder.retrieveByLogin(login, tournament).ifPresent(systemUser -> {
            throw SystemUserException.loginAlreadyExists();
        });
    }

    public SystemUser create(NewTournamentOwnerModel tournamentOwner, Tournament tournament) {
        LOG.info("Creating {} [login={}]", SystemUser.class.getCanonicalName(), tournamentOwner.getLogin());
        validateLogin(tournamentOwner.getLogin(), tournament);

        SystemRole systemRole = systemDictionary.retrieveRoleByName(SystemRole.ADMIN_VALUE)
                .orElseThrow(DataAccessException::entityNotFound);

        SystemUser systemUser = systemUserFactory.create(tournament, systemRole);

        systemUser.setLogin(tournamentOwner.getLogin())
                .setFirstName(tournamentOwner.getFirstName())
                .setLastName(tournamentOwner.getLastName())
                .setPassword(tournamentOwner.getPassword());

        competitorService.create(systemUser);
        return systemUser;
    }

    public SystemUser create(NewSystemUserModel systemUserModel, Tournament tournament) {
        LOG.info("Creating {} [login={}]", SystemUser.class.getCanonicalName(), systemUserModel.getLogin());
        validateLogin(systemUserModel.getLogin(), tournament);

        Guid systemRoleGuid = systemUserModel.getSystemRoleGuid();
        SystemRole systemRole = systemDictionary.retrieveRoleByGuid(systemUserModel.getSystemRoleGuid())
                .orElseThrow(() -> DataAccessException.entityNotFound(SystemRole.class, systemRoleGuid));

        SystemUser systemUser = systemUserFactory.create(tournament, systemRole);

        systemUser.setLogin(systemUserModel.getLogin())
                .setFirstName(systemUserModel.getFirstName())
                .setLastName(systemUserModel.getLastName())
                .setPassword(systemUserModel.getPassword());

        competitorService.create(systemUser);
        return systemUser;
    }

    public void update(Guid guid, ExistingSystemUserModel systemUserModel) {
        LOG.info("Updating {} [login={}, {}]", SystemUser.class.getCanonicalName(), systemUserModel.getLogin(), guid);

        SystemUser systemUser = systemUserFinder.retrieveByGuid(guid)
                .orElseThrow(() -> DataAccessException.entityNotFound(SystemUser.class, guid));

        Guid systemRoleGuid = systemUserModel.getSystemRoleGuid();
        SystemRole systemRole = systemDictionary.retrieveRoleByGuid(systemRoleGuid)
                .orElseThrow(() -> DataAccessException.entityNotFound(SystemRole.class, systemRoleGuid));

        systemUser.setLogin(systemUserModel.getLogin())
                .setFirstName(systemUserModel.getFirstName())
                .setLastName(systemUserModel.getLastName())
                .setSystemRole(systemRole)
                .setUpdateDateTime(ZonedDateTime.now(ZoneId.of("UTC")))
                .setVersion(systemUserModel.getVersion());
        systemUserModel.getPassword().ifPresent(systemUser::setPassword);

        systemUserRepository.update(systemUser);
    }

    private void checkIfHasRobots(Guid guid, Tournament tournament) {
        List<Robot> robots = robotFinder.retrieveByCompetitor(guid, tournament);
        if(!robots.isEmpty()) {
            throw SystemUserException.stillConntectedToRobots();
        }
    }

    public void delete(Guid guid, Tournament tournament) {
        LOG.info("Deleting {} [{}]", SystemUser.class.getCanonicalName(), guid);
        checkIfHasRobots(guid, tournament);
        competitorService.delete(guid, tournament);
    }
}
