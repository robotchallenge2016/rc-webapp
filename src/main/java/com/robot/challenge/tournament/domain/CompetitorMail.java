package com.robot.challenge.tournament.domain;

import com.robot.challenge.infrastructure.service.RcMail;
import com.robot.challenge.tournament.competitor.domain.Competitor;

/**
 * Created by mklys on 30/09/16.
 */
public class CompetitorMail extends RcMail {

    private final String subject;
    private final String content;

    private CompetitorMail(Competitor competitor, String subject, String content) {
        super(competitor);
        this.subject = subject;
        this.content = content;
    }

    @Override
    protected String getSubject() {
        return subject;
    }

    @Override
    protected String getContent() {
        return content;
    }

    public static CompetitorMail of(Competitor competitor, String title, String content) {
        return new CompetitorMail(competitor, title, content);
    }
}
