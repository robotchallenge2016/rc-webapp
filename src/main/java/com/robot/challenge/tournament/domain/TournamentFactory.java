package com.robot.challenge.tournament.domain;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;

public class TournamentFactory extends Factory<Tournament> {

    public TournamentFactory() {
        super(Tournament.class);
    }

    public Tournament create(String name, String shortName, String domain) {
        return new Tournament(Guid.generate(), name, shortName, domain);
    }
}
