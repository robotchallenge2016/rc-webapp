package com.robot.challenge.tournament.domain;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;

import javax.inject.Inject;

public class TournamentEditionFactory extends Factory<TournamentEdition> {

    @Inject
    public TournamentEditionFactory() {
        super(TournamentEdition.class);
    }

    public TournamentEdition create(String name, Tournament tournament) {
        TournamentEdition edition = new TournamentEdition(Guid.generate(), name, tournament);
        tournament.addEdition(edition);
        return edition;
    }
}
