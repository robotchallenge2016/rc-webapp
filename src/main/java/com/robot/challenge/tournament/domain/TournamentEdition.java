package com.robot.challenge.tournament.domain;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "TOURNAMENT_EDITION", indexes = { @Index(name = "tournament_edition_idx", columnList = "guid") })
public class TournamentEdition implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "tournament_edition_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "SUFFIX", nullable = false)
    private String suffix;
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE", nullable = false, updatable = false)
    private Date createDate;
    @Column(name = "REGISTRATION_OPENED", nullable = false)
    private boolean registrationOpened;
    @Column(name = "TOURNAMENT_STARTED", nullable = false)
    private boolean tournamentStarted;
    @Column(name = "MAX_LINEFOLLOWER_PER_COMPETITOR", nullable = false)
    private int maxLineFollowerPerCompetitor;
    @Column(name = "MAX_SUMO_PER_COMPETITOR", nullable = false)
    private int maxSumoPerCompetitor;
    @Column(name = "MAX_FREESTYLE_PER_COMPETITOR", nullable = false)
    private int maxFreestylePerCompetitor;
    @ManyToOne(optional = false)
    @JoinColumn(name = "TOURNAMENT_ID")
    private Tournament tournament;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    private TournamentEdition() {

    }

    TournamentEdition(Guid guid, String suffix, Tournament tournament) {
        this.guid = guid;
        this.suffix = suffix;
        this.createDate = Date.from(Instant.now());
        this.tournament = tournament;
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public String getSuffix() {
        return suffix;
    }

    public TournamentEdition setSuffix(String name) {
        this.suffix = name;
        return this;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public boolean isRegistrationOpened() {
        return registrationOpened;
    }

    public TournamentEdition setRegistrationOpened(boolean registrationOpened) {
        this.registrationOpened = registrationOpened;
        return this;
    }

    public boolean isTournamentStarted() {
        return tournamentStarted;
    }

    public TournamentEdition setTournamentStarted(boolean tournamentStarted) {
        this.tournamentStarted = tournamentStarted;
        return this;
    }

    public int getMaxLineFollowerPerCompetitor() {
        return maxLineFollowerPerCompetitor;
    }

    public TournamentEdition setMaxLineFollowerPerCompetitor(int maxLinefollowerPerCompetitor) {
        this.maxLineFollowerPerCompetitor = maxLinefollowerPerCompetitor;
        return this;
    }

    public int getMaxSumoPerCompetitor() {
        return maxSumoPerCompetitor;
    }

    public TournamentEdition setMaxSumoPerCompetitor(int maxSumoPerCompetitor) {
        this.maxSumoPerCompetitor = maxSumoPerCompetitor;
        return this;
    }

    public int getMaxFreestylePerCompetitor() {
        return maxFreestylePerCompetitor;
    }

    public TournamentEdition setMaxFreestylePerCompetitor(int maxFreestylePerCompetitor) {
        this.maxFreestylePerCompetitor = maxFreestylePerCompetitor;
        return this;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public Long getVersion() {
        return version;
    }

    public TournamentEdition setVersion(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TournamentEdition edition = (TournamentEdition) o;

        if (registrationOpened != edition.registrationOpened) return false;
        if (tournamentStarted != edition.tournamentStarted) return false;
        if (maxLineFollowerPerCompetitor != edition.maxLineFollowerPerCompetitor) return false;
        if (maxSumoPerCompetitor != edition.maxSumoPerCompetitor) return false;
        if (maxFreestylePerCompetitor != edition.maxFreestylePerCompetitor) return false;
        if (!id.equals(edition.id)) return false;
        if (!guid.equals(edition.guid)) return false;
        if (suffix != null ? !suffix.equals(edition.suffix) : edition.suffix != null) return false;
        if (!createDate.equals(edition.createDate)) return false;
        if (!tournament.equals(edition.tournament)) return false;
        return version != null ? version.equals(edition.version) : edition.version == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + guid.hashCode();
        result = 31 * result + (suffix != null ? suffix.hashCode() : 0);
        result = 31 * result + createDate.hashCode();
        result = 31 * result + (registrationOpened ? 1 : 0);
        result = 31 * result + (tournamentStarted ? 1 : 0);
        result = 31 * result + maxLineFollowerPerCompetitor;
        result = 31 * result + maxSumoPerCompetitor;
        result = 31 * result + maxFreestylePerCompetitor;
        result = 31 * result + tournament.hashCode();
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
