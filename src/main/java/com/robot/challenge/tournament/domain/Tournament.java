package com.robot.challenge.tournament.domain;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "TOURNAMENT", indexes = { @Index(name = "tournament_idx", columnList = "guid") })
public class Tournament implements GloballyUnique {

    public static final String TOURNAMENT_GUID = "76aadcb2-9eb2-4ec0-9310-4b206e70daef";

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "tournament_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "SHORT_NAME", nullable = false)
    private String shortName;
    @Column(name = "DOMAIN", nullable = false, unique = true)
    private String domain;
    @OrderBy("createDate DESC")
    @OneToMany(mappedBy = "tournament", fetch = FetchType.EAGER)
    private List<TournamentEdition> editions;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    private Tournament() {

    }

    Tournament(Guid guid, String name, String shortName, String domain) {
        this.guid = guid;
        this.name = name;
        this.shortName = shortName;
        this.domain = domain;
        this.editions = Collections.emptyList();
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public Tournament setName(String name) {
        this.name = name;
        return this;
    }

    public String getShortName() {
        return shortName;
    }

    public Tournament setShortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public String getDomain() {
        return domain;
    }

    public Tournament setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public Tournament addEdition(TournamentEdition edition) {
        this.editions.add(edition);
        return this;
    }

    public List<TournamentEdition> getEditions() {
        return Collections.unmodifiableList(editions);
    }

    public Optional<TournamentEdition> getLatestEdition() {
        return editions.stream().findFirst();
    }

    public Long getVersion() {
        return version;
    }

    public Tournament setVersion(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tournament that = (Tournament) o;

        if (!id.equals(that.id)) return false;
        if (!guid.equals(that.guid)) return false;
        if (!name.equals(that.name)) return false;
        if (!domain.equals(that.domain)) return false;
        if (editions != null ? !editions.equals(that.editions) : that.editions != null) return false;
        return version != null ? version.equals(that.version) : that.version == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + guid.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + domain.hashCode();
        result = 31 * result + (editions != null ? editions.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
