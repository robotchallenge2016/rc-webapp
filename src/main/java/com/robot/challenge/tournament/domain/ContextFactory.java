package com.robot.challenge.tournament.domain;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.repository.TournamentEditionFinder;
import com.robot.challenge.tournament.repository.TournamentFinder;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContextFactory {

    private final TournamentFinder tournamentFinder;
    private final TournamentEditionFinder editionFinder;

    @Inject
    public ContextFactory(TournamentFinder tournamentFinder, TournamentEditionFinder editionFinder) {
        this.tournamentFinder = tournamentFinder;
        this.editionFinder = editionFinder;
    }

    public Tournament getTournamentContext(@NotNull @Size(min = 1) String rawTournamentGuid) {
        Guid tournamentGuid = Guid.from(rawTournamentGuid);
        return tournamentFinder.retrieveByGuid(Guid.from(rawTournamentGuid))
                .orElseThrow(() -> DataAccessException.entityNotFound(Tournament.class, tournamentGuid));
    }

    public TournamentEdition getEditionContext(@NotNull @Size(min = 1) String rawEditionGuid) {
        Guid editionGuid = Guid.from(rawEditionGuid);
        return editionFinder.retrieveByGuid(editionGuid)
                .orElseThrow(() -> DataAccessException.entityNotFound(Tournament.class, editionGuid));
    }
}
