package com.robot.challenge.tournament.competition.domain.competition;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SumoRules {

    @Column(name = "REQUIRED_NUMBER_OF_WINS", nullable = false)
    private Integer requiredNumberOfWins;
    @Column(name = "BIG_WIN_THRESHOLD", nullable = false)
    private Integer bigWinThreshold;
    @Column(name = "BIG_WIN_POINTS", nullable = false)
    private Integer bigWin;
    @Column(name = "BIG_LOST_POINTS", nullable = false)
    private Integer bigLost;
    @Column(name = "SMALL_WIN_POINTS", nullable = false)
    private Integer smallWin;
    @Column(name = "SMALL_LOST_POINTS", nullable = false)
    private Integer smallLost;

    protected SumoRules() {

    }

    protected SumoRules(Integer requiredNumberOfWins, Integer bigWinThreshold, Integer bigWin, Integer bigLost, Integer smallWin, Integer smallLost) {
        this.requiredNumberOfWins = requiredNumberOfWins;
        this.bigWinThreshold = bigWinThreshold;
        this.bigWin = bigWin;
        this.bigLost = bigLost;
        this.smallWin = smallWin;
        this.smallLost = smallLost;
    }

    public static SumoRules from(Integer requiredNumberOfWins, Integer bigWinThreshold, Integer bigWin, Integer bigLose, Integer smallWin, Integer smallLose) {
        return new SumoRules(requiredNumberOfWins, bigWinThreshold, bigWin, bigLose, smallWin, smallLose);
    }

    public Integer getRequiredNumberOfWins() {
        return requiredNumberOfWins;
    }

    public Integer getBigWinThreshold() {
        return bigWinThreshold;
    }

    public Integer getBigWin() {
        return bigWin;
    }

    public Integer getBigLost() {
        return bigLost;
    }

    public Integer getSmallWin() {
        return smallWin;
    }

    public Integer getSmallLost() {
        return smallLost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SumoRules that = (SumoRules) o;

        if (!bigWinThreshold.equals(that.bigWinThreshold)) return false;
        if (!bigWin.equals(that.bigWin)) return false;
        if (!bigLost.equals(that.bigLost)) return false;
        if (!smallWin.equals(that.smallWin)) return false;
        return smallLost.equals(that.smallLost);

    }

    @Override
    public int hashCode() {
        int result = bigWinThreshold.hashCode();
        result = 31 * result + bigWin.hashCode();
        result = 31 * result + bigLost.hashCode();
        result = 31 * result + smallWin.hashCode();
        result = 31 * result + smallLost.hashCode();
        return result;
    }
}
