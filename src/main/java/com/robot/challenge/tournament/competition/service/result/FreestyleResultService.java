package com.robot.challenge.tournament.competition.service.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.system.authn.service.IntrospectionService;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetition;
import com.robot.challenge.tournament.competition.domain.result.FreestyleResult;
import com.robot.challenge.tournament.competition.domain.result.FreestyleResultFactory;
import com.robot.challenge.tournament.competition.exception.CompetitionException;
import com.robot.challenge.tournament.competition.repository.FreestyleCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.result.FreestyleResultFinder;
import com.robot.challenge.tournament.competition.repository.result.FreestyleResultRepository;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.ExistingFreestyleResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.NewFreestyleResultModel;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class FreestyleResultService {

    private final FreestyleResultFactory resultFactory;
    private final FreestyleResultRepository resultRepository;
    private final FreestyleResultFinder resultFinder;
    private final FreestyleCompetitionFinder competitionFinder;
    private final RobotFinder robotFinder;
    private final IntrospectionService introspectionService;

    @Inject
    public FreestyleResultService(FreestyleResultFactory resultFactory, FreestyleResultRepository resultRepository,
                                  FreestyleResultFinder resultFinder, FreestyleCompetitionFinder competitionFinder,
                                  RobotFinder robotFinder, IntrospectionService introspectionService) {
        this.resultFactory = resultFactory;
        this.resultRepository = resultRepository;
        this.resultFinder = resultFinder;
        this.competitionFinder = competitionFinder;
        this.robotFinder = robotFinder;
        this.introspectionService = introspectionService;
    }

    private void checkIfCompetitionIsStarted(Competition competition) {
        if(competition.isFinished()) {
            throw CompetitionException.competitionFinished();
        }
        if(!competition.isStarted()) {
            throw CompetitionException.competitionNotStartedYet();
        }
    }

    public FreestyleResult create(NewFreestyleResultModel resultModel, TournamentEdition edition) {
        SystemUser author = introspectionService.introspection(edition.getTournament())
                .orElseThrow(DataAccessException::entityNotFound);
        FreestyleCompetition competition = competitionFinder.retrieveByGuid(Guid.from(resultModel.getCompetitionGuid()))
                .orElseThrow(DataAccessException::entityNotFound);
        checkIfCompetitionIsStarted(competition);

        Robot robot = robotFinder.retrieveByGuid(Guid.from(resultModel.getRobotGuid()))
                .orElseThrow(DataAccessException::entityNotFound);
        FreestyleResult result = resultFactory.create(author, competition, robot, resultModel.getPlace().getValue());

        resultRepository.create(result);

        return result;
    }

    public void update(Guid guid, ExistingFreestyleResultModel resultModel, TournamentEdition edition) {
        SystemUser editor = introspectionService.introspection(edition.getTournament())
                .orElseThrow(DataAccessException::entityNotFound);
        FreestyleResult result = resultFinder.retrieveByGuid(guid).orElseThrow(DataAccessException::entityNotFound)
                .setPlace(resultModel.getPlace().getValue())
                .setEditor(editor)
                .setUpdateDateTime(ZonedDateTime.now(ZoneId.of("UTC")))
                .setVersion(resultModel.getVersion());

        checkIfCompetitionIsStarted(result.getCompetition());

        resultRepository.update(result);
    }

    @Transactional
    public void delete(Guid guid) {
        resultFinder.retrieveByGuid(guid).map(entity -> resultRepository.delete(entity.getId())).orElseThrow(DataAccessException::entityNotFound);
    }
}
