package com.robot.challenge.tournament.competition.rest.controller.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResult;
import com.robot.challenge.tournament.competition.repository.CompetitionRepository;
import com.robot.challenge.tournament.competition.repository.result.LineFollowerResultFinder;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.BriefLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.ExistingLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.NewLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.resource.result.LineFollowerResultResource;
import com.robot.challenge.tournament.competition.rest.resource.result.ResultResource;
import com.robot.challenge.tournament.competition.service.result.LineFollowerResultService;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + ResultResource.RESULT_CONTEXT + LineFollowerResultResource.DEFAULT_LINEFOLLOWER_RESULT_PREFIX)
public class LineFollowerResultController implements LineFollowerResultResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private LineFollowerResultService resultService;
    @Inject
    private CompetitionRepository competitionRepository;
    @Inject
    private LineFollowerResultFinder resultFinder;

    @Override
    public Response create(@NotNull @Size(min = 1) String editionGuid,
                           @Valid NewLineFollowerResultModel resultModel) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        LineFollowerResult createdResult = resultService.create(resultModel, edition);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdResult)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid,
                           @Valid ExistingLineFollowerResultModel resultModel) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        resultService.update(Guid.from(guid), resultModel, edition);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid) {
        resultService.delete(Guid.from(guid));
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        ExistingLineFollowerResultModel result = ExistingLineFollowerResultModel.from(resultFinder.retrieveByGuid(Guid.from(guid))
                .orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid) {
        List<BriefLineFollowerResultModel> result = resultFinder.retrieveAll().stream()
                .map(BriefLineFollowerResultModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByCompetition(@NotNull @Size(min = 1) String editionGuid,
                                          @NotNull @Size(min = 1) String guid) {
        List<BriefLineFollowerResultModel> result = resultFinder.retrieveByCompetition(Guid.from(guid)).stream()
                .map(BriefLineFollowerResultModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByRobot(@NotNull @Size(min = 1) String editionGuid,
                                    @NotNull @Size(min = 1) String robotGuid) {
        List<BriefLineFollowerResultModel> result = resultFinder.retrieveAllByRobot(Guid.from(robotGuid)).stream()
                .map(BriefLineFollowerResultModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }
}
