package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.competition.repository.result.SumoResultRepository;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.inject.Inject;
import java.util.function.Function;

public class SumoMatchFactory extends Factory<SumoMatch> {
    private final SumoResultFactory sumoResultFactory;
    private final SumoResultRepository resultRepository;

    @Inject
    public SumoMatchFactory(SumoResultFactory sumoResultFactory,
                            SumoResultRepository resultRepository) {
        super(SumoMatch.class);
        this.sumoResultFactory = sumoResultFactory;
        this.resultRepository = resultRepository;
    }

    @Override
    public Function<SumoMatch, SumoMatch> decorator() {
        return (sumoMatch -> sumoMatch
                .setResultFactory(sumoResultFactory)
                .setResultRepository(resultRepository));
    }

    public SumoMatch create(SumoMatchDay matchDay, Robot homeRobot, Robot awayRobot) {
        return decorator().apply(new SumoMatch(Guid.generate(), matchDay, homeRobot, awayRobot));
    }
}
