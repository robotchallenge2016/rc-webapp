package com.robot.challenge.tournament.competition.domain.competition.freestyle;

public enum FreestylePlace {
    FIRST,
    SECOND,
    THIRD
}
