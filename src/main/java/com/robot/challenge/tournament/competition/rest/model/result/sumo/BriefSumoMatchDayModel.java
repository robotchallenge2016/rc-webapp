package com.robot.challenge.tournament.competition.rest.model.result.sumo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competition.domain.result.SumoMatchDay;

import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BriefSumoMatchDayModel {

    private String guid;
    private List<BriefSumoMatchModel> matches;

    public String getGuid() {
        return guid;
    }

    public List<BriefSumoMatchModel> getMatches() {
        return matches;
    }

    public static BriefSumoMatchDayModel from(SumoMatchDay matchDay) {
        BriefSumoMatchDayModel model = new BriefSumoMatchDayModel();
        model.guid = matchDay.getGuid().getValue();
        model.matches = matchDay.getMatches().stream().map(BriefSumoMatchModel::from).collect(Collectors.toList());

        return model;
    }
}
