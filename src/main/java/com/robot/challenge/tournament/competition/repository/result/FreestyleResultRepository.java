package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competition.domain.result.FreestyleResult;

import javax.ejb.Stateless;

@Stateless
public class FreestyleResultRepository extends JpaRepository<FreestyleResult> {

    public FreestyleResultRepository() {
        super(FreestyleResult.class);
    }
}
