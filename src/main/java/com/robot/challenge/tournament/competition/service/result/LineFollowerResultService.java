package com.robot.challenge.tournament.competition.service.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.system.authn.service.IntrospectionService;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetition;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResult;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResultFactory;
import com.robot.challenge.tournament.competition.exception.CompetitionException;
import com.robot.challenge.tournament.competition.repository.LineFollowerCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.result.LineFollowerResultFinder;
import com.robot.challenge.tournament.competition.repository.result.LineFollowerResultRepository;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.ExistingLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.NewLineFollowerResultModel;
import com.robot.challenge.tournament.competition.service.FreestyleCompetitionService;
import com.robot.challenge.tournament.competition.service.result.exception.LineFollowerResultException;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.domain.TournamentEdition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class LineFollowerResultService {

    private static final Logger LOG = LoggerFactory.getLogger(FreestyleCompetitionService.class);

    private final LineFollowerResultFactory resultFactory;
    private final LineFollowerResultRepository resultRepository;
    private final LineFollowerResultFinder resultFinder;
    private final LineFollowerCompetitionFinder competitionFinder;
    private final RobotFinder robotFinder;
    private final IntrospectionService introspectionService;

    @Inject
    public LineFollowerResultService(LineFollowerResultFactory resultFactory, LineFollowerResultRepository resultRepository,
                                     LineFollowerResultFinder resultFinder, LineFollowerCompetitionFinder competitionFinder,
                                     RobotFinder robotFinder, IntrospectionService introspectionService) {
        this.resultFactory = resultFactory;
        this.resultRepository = resultRepository;
        this.resultFinder = resultFinder;
        this.competitionFinder = competitionFinder;
        this.robotFinder = robotFinder;
        this.introspectionService = introspectionService;
    }

    private void checkMaxLineFollowerRuns(Robot robot, LineFollowerCompetition competition) {
        Long numberOfResults = resultFinder.countResultsOfRobotInCompetition(robot.getGuid(), competition.getGuid());
        if(numberOfResults >= competition.getMaxNumberOfRuns()) {
            LOG.warn("Too many results for robot {} in competition {}", robot.getGuid(), competition.getGuid());
            throw LineFollowerResultException.tooManyResultPerRobotInCompetition();
        }
    }

    private void checkIfCompetitionIsStarted(Competition competition) {
        if(competition.isFinished()) {
            LOG.warn("Competition {} is already finished", competition.getGuid());
            throw CompetitionException.competitionFinished();
        }
        if(!competition.isStarted()) {
            LOG.warn("Competition {} is not started yet finished");
            throw CompetitionException.competitionNotStartedYet();
        }
    }

    public LineFollowerResult create(NewLineFollowerResultModel resultModel, TournamentEdition edition) {
        SystemUser author = introspectionService.introspection(edition.getTournament())
                .orElseThrow(DataAccessException::entityNotFound);
        LineFollowerCompetition competition = competitionFinder.retrieveByGuid(Guid.from(resultModel.getCompetitionGuid()))
                .orElseThrow(DataAccessException::entityNotFound);
        checkIfCompetitionIsStarted(competition);

        Robot robot = robotFinder.retrieveByGuid(Guid.from(resultModel.getRobotGuid()))
                .orElseThrow(DataAccessException::entityNotFound);
        checkMaxLineFollowerRuns(robot, competition);

        LineFollowerResult result = resultFactory.create(author, competition, robot, resultModel.getResult());
        resultRepository.create(result);

        return result;
    }

    public void update(Guid guid, ExistingLineFollowerResultModel resultModel, TournamentEdition edition) {
        SystemUser editor = introspectionService.introspection(edition.getTournament())
                .orElseThrow(DataAccessException::entityNotFound);
        LineFollowerResult result = resultFinder.retrieveByGuid(guid).orElseThrow(DataAccessException::entityNotFound)
                .setResult(resultModel.getResult())
                .setEditor(editor)
                .setUpdateDateTime(ZonedDateTime.now(ZoneId.of("UTC")))
                .setVersion(resultModel.getVersion());

        checkIfCompetitionIsStarted(result.getCompetition());

        resultRepository.update(result);
    }

    @Transactional
    public void delete(Guid guid) {
        resultFinder.retrieveByGuid(guid).map(entity -> resultRepository.delete(entity.getId())).orElseThrow(DataAccessException::entityNotFound);
    }
}
