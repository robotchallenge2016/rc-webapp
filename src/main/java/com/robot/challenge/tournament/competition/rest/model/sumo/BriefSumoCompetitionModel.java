package com.robot.challenge.tournament.competition.rest.model.sumo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competition.domain.competition.CompetitionStatus;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.dictionary.rest.model.CompetitionSubtypeModel;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BriefSumoCompetitionModel {
    private String guid;
    private String name;
    private Integer requiredNumberOfWins;
    private Integer bigWinThreshold;
    private Integer bigWinPoints;
    private Integer bigLostPoints;
    private Integer smallWinPoints;
    private Integer smallLostPoints;
    private CompetitionStatus status;
    private CompetitionSubtypeModel competitionSubtype;

    private BriefSumoCompetitionModel() {

    }

    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public Integer getRequiredNumberOfWins() {
        return requiredNumberOfWins;
    }

    public Integer getBigWinThreshold() {
        return bigWinThreshold;
    }

    public Integer getBigWinPoints() {
        return bigWinPoints;
    }

    public Integer getBigLostPoints() {
        return bigLostPoints;
    }

    public Integer getSmallWinPoints() {
        return smallWinPoints;
    }

    public Integer getSmallLostPoints() {
        return smallLostPoints;
    }

    public CompetitionStatus getStatus() {
        return status;
    }

    public CompetitionSubtypeModel getCompetitionSubtype() {
        return competitionSubtype;
    }

    public static BriefSumoCompetitionModel from(SumoCompetition competition) {
        BriefSumoCompetitionModel model = new BriefSumoCompetitionModel();
        model.guid = competition.getGuid().getValue();
        model.name = competition.getName();
        model.competitionSubtype = CompetitionSubtypeModel.from(competition.getCompetitionSubtype());
        model.requiredNumberOfWins = competition.getRules().getRequiredNumberOfWins();
        model.bigWinThreshold = competition.getRules().getBigWinThreshold();
        model.bigWinPoints = competition.getRules().getBigWin();
        model.bigLostPoints = competition.getRules().getBigLost();
        model.smallWinPoints = competition.getRules().getSmallWin();
        model.smallLostPoints = competition.getRules().getSmallLost();
        model.status = competition.getStatus();

        return model;
    }
}
