package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competition.domain.result.SumoMatch;
import com.robot.challenge.tournament.competition.domain.result.SumoMatchFactory;
import com.robot.challenge.tournament.competition.domain.result.SumoRanking;
import com.robot.challenge.tournament.competition.repository.SumoCompetitionFinder;
import com.robot.challenge.tournament.competitor.domain.Robot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Collections.sort;

public class SumoMatchFinder extends JpaFinder<SumoMatch> {
    private static final Logger LOG = LoggerFactory.getLogger(SumoMatchFinder.class);

    private final SumoCompetitionFinder competitionFinder;

    @Inject
    public SumoMatchFinder(SumoMatchFactory factory, SumoCompetitionFinder competitionFinder) {
        super(factory);
        this.competitionFinder = competitionFinder;
    }

    public List<SumoMatch> retrieveAllByCompetition(@NotNull Guid competitionGuid) {
        LOG.info("Rertieving all {} by competition", SumoMatch.class.getName());
        TypedQuery<SumoMatch> q = entityManager()
                .createQuery("SELECT sm FROM SumoMatch sm WHERE sm.matchDay.competition.guid = :guid ORDER BY sm.matchDay.number", SumoMatch.class);
        q.setParameter("guid", competitionGuid);

        return q.getResultList();
    }

    public List<SumoMatch> retrieveAllByRobot(@NotNull Guid robotGuid) {
        LOG.info("Rertieving all {} by robot", SumoMatch.class.getName());
        TypedQuery<SumoMatch> q = entityManager().createQuery("SELECT sm FROM SumoMatch sm WHERE " +
                "sm.homeRobot.guid = :guid" +
                " OR " +
                "sm.awayRobot.guid = :guid" +
                " ORDER BY sm.matchDay.number", SumoMatch.class);
        q.setParameter("guid", robotGuid);

        return q.getResultList();
    }

    public List<SumoRanking> getRanking(@NotNull Guid competitionGuid) {
        LOG.info("Retrieving {} ranking", SumoCompetition.class.getName());
        TypedQuery<SumoMatch> q = entityManager()
                .createQuery("SELECT sm FROM SumoMatch sm WHERE sm.matchDay.competition.guid = :guid AND sm.results IS NOT EMPTY", SumoMatch.class);
        q.setParameter("guid", competitionGuid);

        SumoCompetition competition = competitionFinder.retrieveByGuid(competitionGuid).orElseThrow(DataAccessException::entityNotFound);

        List<SumoMatch> matches = q.getResultList();
        Map<Robot, SumoRanking> generalRanking = new HashMap<>();
        for(SumoMatch match : matches) {
            SumoRanking homeRobotRanking = createAndAddToRanking(match.getHomeRobot(), competition, generalRanking);
            SumoRanking awayRobotRanking = createAndAddToRanking(match.getAwayRobot(), competition, generalRanking);

            homeRobotRanking.addMatchResult(match);
            awayRobotRanking.addMatchResult(match);
        }

        List<SumoRanking> sortedRanking = generalRanking.entrySet().stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
        sort(sortedRanking, Collections.reverseOrder());
        return sortedRanking;
    }

    private SumoRanking createAndAddToRanking(Robot robot, SumoCompetition competition, Map<Robot, SumoRanking> generalRanking) {
        SumoRanking ranking = Optional.ofNullable(generalRanking.get(robot))
                .orElseGet(() -> new SumoRanking(competition.getRules(), robot));
        generalRanking.put(robot, ranking);

        return ranking;
    }
}
