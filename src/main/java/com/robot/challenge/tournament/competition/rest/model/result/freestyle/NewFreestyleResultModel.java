package com.robot.challenge.tournament.competition.rest.model.result.freestyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewFreestyleResultModel {

    @NotNull
    @Size(min = 1)
    private String competitionGuid;
    @NotNull
    @Size(min = 1)
    private String robotGuid;
    @NotNull
    private FreestylePlaceModel place;

    public String getCompetitionGuid() {
        return competitionGuid;
    }

    public String getRobotGuid() {
        return robotGuid;
    }

    public FreestylePlaceModel getPlace() {
        return place;
    }

    public static NewFreestyleResultModelBuilder builder() {
        return new NewFreestyleResultModelBuilder();
    }

    public static class NewFreestyleResultModelBuilder {

        private final NewFreestyleResultModel model = new NewFreestyleResultModel();

        public NewFreestyleResultModelBuilder setResult(FreestylePlaceModel place) {
            model.place = place;
            return this;
        }

        public NewFreestyleResultModelBuilder setCompetitionGuid(String competitionGuid) {
            model.competitionGuid = competitionGuid;
            return this;
        }

        public NewFreestyleResultModelBuilder setRobotGuid(String robotGuid) {
            model.robotGuid = robotGuid;
            return this;
        }

        public NewFreestyleResultModelBuilder setPlace(FreestylePlaceModel place) {
            model.place = place;
            return this;
        }

        public NewFreestyleResultModel build() {
            return model;
        }
    }
}
