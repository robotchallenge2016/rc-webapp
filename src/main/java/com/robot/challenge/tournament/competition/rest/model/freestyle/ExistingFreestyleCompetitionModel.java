package com.robot.challenge.tournament.competition.rest.model.freestyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competition.domain.competition.CompetitionStatus;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetition;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingFreestyleCompetitionModel implements GloballyUniqueModel, VersionedModel {
    @NotNull
    @Size(min = 1)
    private String name;
    private String guid;
    @NotNull
    @Size(min = 1)
    private String competitionSubtypeGuid;
    private List<String> robotGuids = new ArrayList<>();
    @NotNull
    private Integer firstPlacePoints;
    @NotNull
    private Integer secondPlacePoints;
    @NotNull
    private Integer thirdPlacePoints;
    private CompetitionStatus status;
    @NotNull
    @Min(value = 0L)
    private Long version;

    ExistingFreestyleCompetitionModel() {

    }

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public String getCompetitionSubtypeGuid() {
        return competitionSubtypeGuid;
    }

    public List<String> getRobotGuids() {
        return robotGuids;
    }

    public Integer getFirstPlacePoints() {
        return firstPlacePoints;
    }

    public Integer getSecondPlacePoints() {
        return secondPlacePoints;
    }

    public Integer getThirdPlacePoints() {
        return thirdPlacePoints;
    }

    public CompetitionStatus getStatus() {
        return status;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingFreestyleCompetitionModel from(FreestyleCompetition competition) {
        ExistingFreestyleCompetitionModel model = new ExistingFreestyleCompetitionModel();
        model.name = competition.getName();
        model.guid = competition.getGuid().getValue();
        model.competitionSubtypeGuid = competition.getCompetitionSubtype().getGuid().getValue();
        model.robotGuids.addAll(competition.getRobots().stream().map(Robot::getGuid).map(Guid::getValue).collect(Collectors.toList()));
        model.firstPlacePoints = competition.getPoints().getFirstPlace();
        model.secondPlacePoints = competition.getPoints().getSecondPlace();
        model.thirdPlacePoints = competition.getPoints().getThirdPlace();
        model.status = competition.getStatus();
        model.version = competition.getVersion();

        return model;
    }

    public static ExistingCompetitionModelBuilder builder() {
        return new ExistingCompetitionModelBuilder();
    }

    public static class ExistingCompetitionModelBuilder {

        private final ExistingFreestyleCompetitionModel model = new ExistingFreestyleCompetitionModel();

        public ExistingCompetitionModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public ExistingCompetitionModelBuilder setType(String competitionSubtypeGuid) {
            model.competitionSubtypeGuid = competitionSubtypeGuid;
            return this;
        }

        public ExistingCompetitionModelBuilder addRobot(String guid) {
            model.robotGuids.add(guid);
            return this;
        }

        public ExistingCompetitionModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingCompetitionModelBuilder setFirstPlacePoints(Integer firstPlacePoints) {
            model.firstPlacePoints = firstPlacePoints;
            return this;
        }

        public ExistingCompetitionModelBuilder setSecondPlacePoints(Integer secondPlacePoints) {
            model.secondPlacePoints = secondPlacePoints;
            return this;
        }

        public ExistingCompetitionModelBuilder setThirdPlacePoints(Integer thirdPlacePoints) {
            model.thirdPlacePoints = thirdPlacePoints;
            return this;
        }

        public ExistingFreestyleCompetitionModel build() {
            return model;
        }
    }    
}
