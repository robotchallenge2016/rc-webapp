package com.robot.challenge.tournament.competition.rest.model.result.linefollower;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.system.user.rest.model.user.BriefSystemUserModel;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResult;
import com.robot.challenge.tournament.competition.rest.model.linefollower.BriefLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BriefLineFollowerResultModel {

    private String guid;
    private Long result;
    private BriefLineFollowerCompetitionModel competition;
    private BriefRobotModel robot;
    private BriefSystemUserModel author;
    private BriefSystemUserModel editor;

    private BriefLineFollowerResultModel() {

    }

    public String getGuid() {
        return guid;
    }

    public Long getResult() {
        return result;
    }

    public BriefLineFollowerCompetitionModel getCompetition() {
        return competition;
    }

    public BriefRobotModel getRobot() {
        return robot;
    }

    public BriefSystemUserModel getAuthor() {
        return author;
    }

    public BriefSystemUserModel getEditor() {
        return editor;
    }

    public static BriefLineFollowerResultModel from(LineFollowerResult result) {
        BriefLineFollowerResultModel model = new BriefLineFollowerResultModel();
        model.guid = result.getGuid().getValue();
        model.result = result.getResult();
        model.competition = BriefLineFollowerCompetitionModel.from(result.getCompetition());
        model.robot = BriefRobotModel.from(result.getRobot());
        model.author = BriefSystemUserModel.from(result.getAuthor());
        result.getEditor().ifPresent(editor -> model.editor = BriefSystemUserModel.from(editor));

        return model;
    }
}
