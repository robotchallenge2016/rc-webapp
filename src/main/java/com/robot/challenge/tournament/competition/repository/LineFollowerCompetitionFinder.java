package com.robot.challenge.tournament.competition.repository;

import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetition;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetitionFactory;

import javax.inject.Inject;

public class LineFollowerCompetitionFinder extends CompetitionFinder<LineFollowerCompetition> {

    @Inject
    public LineFollowerCompetitionFinder(LineFollowerCompetitionFactory factory) {
        super(factory);
    }
}
