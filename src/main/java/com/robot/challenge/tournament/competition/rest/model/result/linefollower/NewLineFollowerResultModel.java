package com.robot.challenge.tournament.competition.rest.model.result.linefollower;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewLineFollowerResultModel {

    @NotNull
    @Min(value = 0L)
    private Long result;
    @NotNull
    @Size(min = 1)
    private String competitionGuid;
    @NotNull
    @Size(min = 1)
    private String robotGuid;

    public Long getResult() {
        return result;
    }

    public String getCompetitionGuid() {
        return competitionGuid;
    }

    public String getRobotGuid() {
        return robotGuid;
    }

    public static NewLineFollowerResultModelBuilder builder() {
        return new NewLineFollowerResultModelBuilder();
    }

    public static class NewLineFollowerResultModelBuilder {

        private final NewLineFollowerResultModel model = new NewLineFollowerResultModel();

        public NewLineFollowerResultModelBuilder setResult(Long result) {
            model.result = result;
            return this;
        }

        public NewLineFollowerResultModelBuilder setCompetitionGuid(String competitionGuid) {
            model.competitionGuid = competitionGuid;
            return this;
        }

        public NewLineFollowerResultModelBuilder setRobotGuid(String robotGuid) {
            model.robotGuid = robotGuid;
            return this;
        }

        public NewLineFollowerResultModel build() {
            return model;
        }
    }
}
