package com.robot.challenge.tournament.competition.rest.model.result.sumo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competition.domain.result.SumoResult;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BriefSumoResultModel {

    private String guid;
    private BriefRobotModel winner;
    private BriefRobotModel looser;

    public String getGuid() {
        return guid;
    }

    public BriefRobotModel getWinner() {
        return winner;
    }

    public BriefRobotModel getLooser() {
        return looser;
    }

    public static BriefSumoResultModel from(SumoResult sumoResult) {
        BriefSumoResultModel model = new BriefSumoResultModel();
        model.guid = sumoResult.getGuid().getValue();
        model.winner = BriefRobotModel.from(sumoResult.getWinner());
        model.looser = BriefRobotModel.from(sumoResult.getLooser());

        return model;
    }
}
