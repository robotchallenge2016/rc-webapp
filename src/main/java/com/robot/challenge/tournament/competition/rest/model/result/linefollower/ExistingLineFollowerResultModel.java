package com.robot.challenge.tournament.competition.rest.model.result.linefollower;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResult;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingLineFollowerResultModel implements GloballyUniqueModel, VersionedModel {

    @NotNull
    @Min(value = 0L)
    private Long result;
    private String guid;
    private String competitionGuid;
    private String robotGuid;
    @NotNull
    @Min(value = 0L)
    private Long version;

    ExistingLineFollowerResultModel() {

    }

    public Long getResult() {
        return result;
    }

    @Override
    public String getGuid() {
        return guid;
    }

    public String getCompetitionGuid() {
        return competitionGuid;
    }

    public String getRobotGuid() {
        return robotGuid;
    }

    @Override
    public Long getVersion() {
        return version;
    }

    public static ExistingLineFollowerResultModel from(LineFollowerResult result) {
        ExistingLineFollowerResultModel model = new ExistingLineFollowerResultModel();
        model.result = result.getResult();
        model.guid = result.getGuid().getValue();
        model.competitionGuid = result.getCompetition().getGuid().getValue();
        model.robotGuid = result.getRobot().getGuid().getValue();
        model.version = result.getVersion();

        return model;
    }

    public static ExistingLinefollowerResultModelBuilder builder() {
        return new ExistingLinefollowerResultModelBuilder();
    }

    public static class ExistingLinefollowerResultModelBuilder {

        private final ExistingLineFollowerResultModel model = new ExistingLineFollowerResultModel();

        public ExistingLinefollowerResultModelBuilder setResult(Long result) {
            model.result = result;
            return this;
        }

        public ExistingLinefollowerResultModelBuilder setCompetitionGuid(String competitionGuid) {
            model.competitionGuid = competitionGuid;
            return this;
        }

        public ExistingLinefollowerResultModelBuilder setRobotGuid(String robotGuid) {
            model.robotGuid = robotGuid;
            return this;
        }

        public ExistingLinefollowerResultModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingLineFollowerResultModel build() {
            return model;
        }
    }
}
