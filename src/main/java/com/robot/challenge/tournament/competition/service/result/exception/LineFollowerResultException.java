package com.robot.challenge.tournament.competition.service.result.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class LineFollowerResultException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        TOO_MANY_RESULTS_PER_ROBOT_IN_COMPETITION("Too many results per robot in competition");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public LineFollowerResultException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static LineFollowerResultException tooManyResultPerRobotInCompetition() {
        return new LineFollowerResultException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.TOO_MANY_RESULTS_PER_ROBOT_IN_COMPETITION,
                Cause.TOO_MANY_RESULTS_PER_ROBOT_IN_COMPETITION.getErrorDescription());
    }
}
