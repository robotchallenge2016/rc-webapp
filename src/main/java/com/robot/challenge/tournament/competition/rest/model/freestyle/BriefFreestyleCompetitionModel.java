package com.robot.challenge.tournament.competition.rest.model.freestyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competition.domain.competition.CompetitionStatus;
import com.robot.challenge.tournament.dictionary.rest.model.CompetitionSubtypeModel;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BriefFreestyleCompetitionModel {
    private String guid;
    private String name;
    private CompetitionStatus status;
    private CompetitionSubtypeModel competitionSubtype;

    private BriefFreestyleCompetitionModel() {

    }

    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public CompetitionStatus getStatus() {
        return status;
    }

    public CompetitionSubtypeModel getCompetitionSubtype() {
        return competitionSubtype;
    }

    public static BriefFreestyleCompetitionModel from(Competition competition) {
        BriefFreestyleCompetitionModel model = new BriefFreestyleCompetitionModel();
        model.guid = competition.getGuid().getValue();
        model.name = competition.getName();
        model.status = competition.getStatus();
        model.competitionSubtype = CompetitionSubtypeModel.from(competition.getCompetitionSubtype());

        return model;
    }
}
