package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "SUMO_RESULT", indexes = { @Index(name = "sumo_result_idx", columnList = "guid") })
public class SumoResult {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "sumo_result_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @ManyToOne(optional = false)
    @JoinColumn(name = "AUTHOR_ID", nullable = false, updatable = false)
    private SystemUser author;
    @ManyToOne(optional = false)
    @JoinColumn(name = "WINNER_ID", nullable = false, updatable = false)
    private Robot winner;
    @ManyToOne(optional = false)
    @JoinColumn(name = "LOOSER_ID", nullable = false, updatable = false)
    private Robot looser;
    @ManyToOne(optional = false)
    @JoinColumn(name = "MATCH_ID", nullable = false, updatable = false)
    private SumoMatch match;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    private SumoResult() {

    }

    SumoResult(Guid guid, SystemUser author, SumoMatch match, Robot winner, Robot looser) {
        this.guid = guid;
        this.author = author;
        this.match = match;
        this.winner = winner;
        this.looser = looser;
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public SystemUser getAuthor() {
        return author;
    }

    public Robot getWinner() {
        return winner;
    }

    public Robot getLooser() {
        return looser;
    }

    public SumoMatch getMatch() {
        return match;
    }

    public Long getVersion() {
        return version;
    }
}
