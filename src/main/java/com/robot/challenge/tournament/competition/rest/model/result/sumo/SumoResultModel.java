package com.robot.challenge.tournament.competition.rest.model.result.sumo;

import com.robot.challenge.tournament.competition.domain.result.SumoResult;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SumoResultModel {

    private String guid;
    @NotNull
    @Size(min = 1)
    private String winnerGuid;
    @NotNull
    @Size(min = 1)
    private String looserGuid;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getWinnerGuid() {
        return winnerGuid;
    }

    public void setWinnerGuid(String winnerGuid) {
        this.winnerGuid = winnerGuid;
    }

    public String getLooserGuid() {
        return looserGuid;
    }

    public void setLooserGuid(String looserGuid) {
        this.looserGuid = looserGuid;
    }

    public static SumoResultModel from(SumoResult result) {
        SumoResultModel model = new SumoResultModel();
        model.guid = result.getGuid().getValue();
        model.winnerGuid = result.getWinner().getGuid().getValue();
        model.looserGuid = result.getLooser().getGuid().getValue();

        return model;
    }

    public static SumoResultModelBuilder builder() {
        return new SumoResultModelBuilder();
    }

    static class SumoResultModelBuilder {

        private final SumoResultModel model = new SumoResultModel();

        public SumoResultModelBuilder guid(String guid) {
            model.guid = guid;
            return this;
        }

        public SumoResultModelBuilder winnerGuid(String winnerGuid) {
            model.winnerGuid = winnerGuid;
            return this;
        }

        public SumoResultModelBuilder looserGuid(String looserGuid) {
            model.looserGuid = looserGuid;
            return this;
        }
    }
}

