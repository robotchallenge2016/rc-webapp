package com.robot.challenge.tournament.competition.rest.controller.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competition.repository.result.SumoMatchFinder;
import com.robot.challenge.tournament.competition.rest.model.result.sumo.BriefSumoMatchModel;
import com.robot.challenge.tournament.competition.rest.model.result.sumo.ExistingSumoMatchModel;
import com.robot.challenge.tournament.competition.rest.resource.result.ResultResource;
import com.robot.challenge.tournament.competition.rest.resource.result.SumoMatchResource;
import com.robot.challenge.tournament.competition.service.result.SumoMatchService;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + ResultResource.RESULT_CONTEXT + SumoMatchResource.DEFAULT_SUMO_RESULT_PREFIX)
public class SumoMatchController implements SumoMatchResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private SumoMatchService matchService;
    @Inject
    private SumoMatchFinder sumoMatchFinder;

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid,
                           @Valid ExistingSumoMatchModel model) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        matchService.updateMatch(Guid.from(guid), model, edition);
        return Response.ok().build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        ExistingSumoMatchModel matchModel = ExistingSumoMatchModel.from(sumoMatchFinder.retrieveByGuid(Guid.from(guid))
                .orElseThrow(DataAccessException::entityNotFound));
        return Response.ok(matchModel).build();
    }

    @Override
    public Response retrieveAllByCompetition(@NotNull @Size(min = 1) String editionGuid,
                                             @NotNull @Size(min = 1) String competitionGuid) {
        List<BriefSumoMatchModel> matches = sumoMatchFinder.retrieveAllByCompetition(Guid.from(competitionGuid)).stream()
                .map(BriefSumoMatchModel::from)
                .collect(Collectors.toList());
        return Response.ok(matches).build();
    }

    @Override
    public Response retrieveByRobot(@NotNull @Size(min = 1) String editionGuid,
                                    @NotNull @Size(min = 1) String robotGuid) {
        List<BriefSumoMatchModel> matches = sumoMatchFinder.retrieveAllByRobot(Guid.from(robotGuid)).stream()
                .map(BriefSumoMatchModel::from)
                .collect(Collectors.toList());
        return Response.ok(matches).build();
    }
}
