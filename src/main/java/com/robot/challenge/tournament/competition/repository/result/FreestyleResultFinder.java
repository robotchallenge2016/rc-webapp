package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetition;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePlace;
import com.robot.challenge.tournament.competition.domain.result.FreestyleRanking;
import com.robot.challenge.tournament.competition.domain.result.FreestyleResult;
import com.robot.challenge.tournament.competition.domain.result.FreestyleResultFactory;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResult;
import com.robot.challenge.tournament.competition.repository.FreestyleCompetitionFinder;
import com.robot.challenge.tournament.competitor.domain.Robot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class FreestyleResultFinder extends JpaFinder<FreestyleResult> {
    private static final Logger LOG = LoggerFactory.getLogger(FreestyleResultFinder.class);

    private final FreestyleCompetitionFinder competitionFinder;

    @Inject
    public FreestyleResultFinder(FreestyleResultFactory factory, FreestyleCompetitionFinder competitionFinder) {
        super(factory);
        this.competitionFinder = competitionFinder;
    }

    public List<FreestyleResult> retrieveAll() {
        LOG.info("Retrieving all {}", FreestyleResult.class.getName());
        TypedQuery<FreestyleResult> q = entityManager().createQuery("SELECT r FROM FreestyleResult r ORDER BY r.place", FreestyleResult.class);
        return q.getResultList();
    }

    public List<FreestyleResult> retrieveByCompetition(Guid competitionGid) {
        LOG.info("Retrieving all {}", LineFollowerResult.class.getName());
        TypedQuery<FreestyleResult> q = entityManager().createQuery(
                "SELECT r FROM FreestyleResult r WHERE r.competition.guid = :guid ORDER BY r.place",
                FreestyleResult.class);
        q.setParameter("guid", competitionGid);
        return q.getResultList();
    }

    public List<FreestyleResult> retrieveAllByRobot(@NotNull Guid robotGuid) {
        LOG.info("Rertieving all {} by robot", FreestyleResult.class.getName());
        TypedQuery<FreestyleResult> q = entityManager().createQuery("SELECT fsr FROM FreestyleResult fsr WHERE " +
                "fsr.robot.guid = :guid" +
                " ORDER BY fsr.place", FreestyleResult.class);
        q.setParameter("guid", robotGuid);

        return q.getResultList();
    }

    public List<FreestyleRanking> getRanking(Guid competitionGuid) {
        LOG.info("Retrieving {} ranking", FreestyleCompetition.class.getName());
        TypedQuery<Object[]> q = entityManager().createQuery(
                "SELECT fr_robot, fr.place, COUNT(fr.place) FROM FreestyleResult fr JOIN fr.robot fr_robot " +
                        "WHERE fr.competition.guid = :guid " +
                        "GROUP BY fr_robot, fr.place " +
                        "ORDER BY fr_robot.id",
                Object[].class);
        q.setParameter("guid", competitionGuid);

        FreestyleCompetition competition = competitionFinder.retrieveByGuid(competitionGuid)
                .orElseThrow(DataAccessException::entityNotFound);
        List<Object[]> results = q.getResultList();
        Map<Robot, FreestyleRanking> generalRanking = new HashMap<>();
        for(Object[] result : results) {
            Robot robot = (Robot) result[0];
            FreestylePlace place = (FreestylePlace) result[1];
            Long placeCount = (Long) result[2];

            FreestyleRanking ranking = createAndAddToRanking(robot, competition, generalRanking);
            ranking.addResult(place, placeCount.intValue());
        }
        List<FreestyleRanking> ranking = generalRanking.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
        Collections.sort(ranking, Collections.reverseOrder());
        return ranking;
    }

    private FreestyleRanking createAndAddToRanking(Robot robot, FreestyleCompetition competition, Map<Robot, FreestyleRanking> generalRanking) {
        FreestyleRanking ranking = Optional.ofNullable(generalRanking.get(robot))
                .orElseGet(() -> new FreestyleRanking(competition.getPoints(), robot));
        generalRanking.put(robot, ranking);

        return ranking;
    }
}
