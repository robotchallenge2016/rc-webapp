package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competition.domain.result.SumoMatch;

import javax.ejb.Stateless;

@Stateless
public class SumoMatchRepository extends JpaRepository<SumoMatch> {

    public SumoMatchRepository() {
        super(SumoMatch.class);
    }
}
