package com.robot.challenge.tournament.competition.domain.competition;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.repository.TournamentDictionary;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.inject.Inject;

public class LineFollowerCompetitionFactory extends Factory<LineFollowerCompetition> {

    private final TournamentDictionary tournamentDictionary;

    @Inject
    public LineFollowerCompetitionFactory(TournamentDictionary tournamentDictionary) {
        super(LineFollowerCompetition.class);
        this.tournamentDictionary = tournamentDictionary;
    }

    public LineFollowerCompetition create(Guid competitionSubtypeGuid,
                                          TournamentEdition edition) {
        CompetitionSubtype competitionSubtype = tournamentDictionary
                .retrieveCompetitionSubtypeByGuidAndCompetitionType(competitionSubtypeGuid, CompetitionType.LINEFOLLOWER)
                .orElseThrow(DataAccessException::entityNotFound);
        return new LineFollowerCompetition(Guid.generate(), competitionSubtype, edition);
    }
}
