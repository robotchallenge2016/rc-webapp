package com.robot.challenge.tournament.competition.rest.model.result.sumo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competition.domain.result.SumoRanking;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SumoRankingModel {

    private BriefRobotModel robot;
    private int fightsWon;
    private int played;
    private int win;
    private int lost;
    private int points;

    public BriefRobotModel getRobot() {
        return robot;
    }

    public int getFightsWon() {
        return fightsWon;
    }

    public int getPlayed() {
        return played;
    }

    public int getWin() {
        return win;
    }

    public int getLost() {
        return lost;
    }

    public int getPoints() {
        return points;
    }

    public static SumoRankingModel from(SumoRanking ranking) {
        SumoRankingModel model = new SumoRankingModel();
        model.fightsWon = ranking.getFightsWon();
        model.played = ranking.getPlayed();
        model.robot = BriefRobotModel.from(ranking.getRobot());
        model.win = ranking.getWin();
        model.lost = ranking.getLost();
        model.points = ranking.getPoints();

        return model;
    }
}
