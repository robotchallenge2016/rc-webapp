package com.robot.challenge.tournament.competition.rest.controller.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.competition.repository.result.FreestyleResultFinder;
import com.robot.challenge.tournament.competition.repository.result.LineFollowerResultFinder;
import com.robot.challenge.tournament.competition.repository.result.SumoMatchFinder;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.FreestyleRankingModel;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.BriefLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.sumo.SumoRankingModel;
import com.robot.challenge.tournament.competition.rest.resource.result.RankingResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + RankingResource.RANKING_CONTEXT)
public class RankingController implements RankingResource {

    @Inject
    private LineFollowerResultFinder lineFollowerRankingFinder;
    @Inject
    private SumoMatchFinder sumoMatchFinder;
    @Inject
    private FreestyleResultFinder freestyleResultFinder;

    public Response lineFollowerRanking(@NotNull @Size(min = 1) String editionGuid,
                                        @NotNull @Size(min = 1) String guid) {
        List<BriefLineFollowerResultModel> ranking = lineFollowerRankingFinder.getRanking(Guid.from(guid)).stream()
                .map(BriefLineFollowerResultModel::from)
                .collect(Collectors.toList());
        return Response.ok(ranking).build();
    }

    public Response sumoRanking(@NotNull @Size(min = 1) String editionGuid,
                                @NotNull @Size(min = 1) String guid) {
        List<SumoRankingModel> ranking = sumoMatchFinder.getRanking(Guid.from(guid)).stream()
                .map(SumoRankingModel::from)
                .collect(Collectors.toList());
        return Response.ok(ranking).build();
    }

    public Response freestyleRanking(@NotNull @Size(min = 1) String editionGuid,
                                     @NotNull @Size(min = 1) String guid) {
        List<FreestyleRankingModel> ranking = freestyleResultFinder.getRanking(Guid.from(guid)).stream()
                .map(FreestyleRankingModel::from)
                .collect(Collectors.toList());
        return Response.ok(ranking).build();
    }
}
