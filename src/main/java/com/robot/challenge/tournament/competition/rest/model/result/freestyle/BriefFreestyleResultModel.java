package com.robot.challenge.tournament.competition.rest.model.result.freestyle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.system.user.rest.model.user.BriefSystemUserModel;
import com.robot.challenge.tournament.competition.domain.result.FreestyleResult;
import com.robot.challenge.tournament.competition.rest.model.linefollower.BriefLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BriefFreestyleResultModel {

    private String guid;
    private FreestylePlaceModel place;
    private BriefLineFollowerCompetitionModel competition;
    private BriefRobotModel robot;
    private BriefSystemUserModel author;
    private BriefSystemUserModel editor;

    private BriefFreestyleResultModel() {

    }

    public String getGuid() {
        return guid;
    }

    public FreestylePlaceModel getPlace() {
        return place;
    }

    public BriefLineFollowerCompetitionModel getCompetition() {
        return competition;
    }

    public BriefRobotModel getRobot() {
        return robot;
    }

    public BriefSystemUserModel getAuthor() {
        return author;
    }

    public BriefSystemUserModel getEditor() {
        return editor;
    }

    public static BriefFreestyleResultModel from(FreestyleResult result) {
        BriefFreestyleResultModel model = new BriefFreestyleResultModel();
        model.guid = result.getGuid().getValue();
        model.place = FreestylePlaceModel.from(result.getPlace());
        model.competition = BriefLineFollowerCompetitionModel.from(result.getCompetition());
        model.robot = BriefRobotModel.from(result.getRobot());
        model.author = BriefSystemUserModel.from(result.getAuthor());
        result.getEditor().ifPresent(editor -> model.editor = BriefSystemUserModel.from(editor));

        return model;
    }
}
