package com.robot.challenge.tournament.competition.domain.competition.freestyle;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FreestylePoints {
    @Column(name = "FIRST_PLACE_POINTS")
    private Integer firstPlace;
    @Column(name = "SECOND_PLACE_POINTS")
    private Integer secondPlace;
    @Column(name = "THIRD_PLACE_POINTS")
    private Integer thirdPlace;

    protected FreestylePoints() {
    }

    protected FreestylePoints(Integer firstPlace, Integer secondPlace, Integer thirdPlace) {
        this.firstPlace = firstPlace;
        this.secondPlace = secondPlace;
        this.thirdPlace = thirdPlace;
    }

    public static FreestylePoints from(Integer firstPlacePoints, Integer secondPlacePoints, Integer thirdPlacePoints) {
        return new FreestylePoints(firstPlacePoints, secondPlacePoints, thirdPlacePoints);
    }

    public Integer getFirstPlace() {
        return firstPlace;
    }

    public Integer getSecondPlace() {
        return secondPlace;
    }

    public Integer getThirdPlace() {
        return thirdPlace;
    }

    public Integer getPointsForPlace(FreestylePlace place) {
        switch (place) {
            case FIRST:
                return firstPlace;
            case SECOND:
                return secondPlace;
            case THIRD:
                return thirdPlace;
            default:
                return 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FreestylePoints that = (FreestylePoints) o;

        if (!firstPlace.equals(that.firstPlace)) return false;
        if (!secondPlace.equals(that.secondPlace)) return false;
        return thirdPlace.equals(that.thirdPlace);

    }

    @Override
    public int hashCode() {
        int result = firstPlace.hashCode();
        result = 31 * result + secondPlace.hashCode();
        result = 31 * result + thirdPlace.hashCode();
        return result;
    }
}
