package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetition;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResult;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResultFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.List;

public class LineFollowerResultFinder extends JpaFinder<LineFollowerResult> {
    private static final Logger LOG = LoggerFactory.getLogger(LineFollowerResultFinder.class);

    @Inject
    protected LineFollowerResultFinder(LineFollowerResultFactory factory) {
        super(factory);
    }

    public List<LineFollowerResult> retrieveAll() {
        LOG.info("Retrieving all {}", LineFollowerResult.class.getName());
        TypedQuery<LineFollowerResult> q = entityManager().createQuery("SELECT r FROM LineFollowerResult r ORDER BY r.result", LineFollowerResult.class);
        return q.getResultList();
    }

    public List<LineFollowerResult> retrieveByCompetition(Guid competitionGuid) {
        LOG.info("Retrieving all {}", LineFollowerResult.class.getName());
        TypedQuery<LineFollowerResult> q = entityManager().createQuery(
                "SELECT r FROM LineFollowerResult r WHERE r.competition.guid = :guid ORDER BY r.result",
                LineFollowerResult.class);
        q.setParameter("guid", competitionGuid);
        return q.getResultList();
    }

    public Long countResultsOfRobotInCompetition(Guid robotGuid, Guid competitionGuid) {
        LOG.info("Counting results of Robot in competition");
        TypedQuery<Long> q = entityManager().createQuery(
                "SELECT COUNT(r) FROM LineFollowerResult r WHERE r.competition.guid = :competitionGuid AND r.robot.guid = :robotGuid",
                Long.class);
        q.setParameter("competitionGuid", competitionGuid);
        q.setParameter("robotGuid", robotGuid);
        return q.getSingleResult();
    }

    public List<LineFollowerResult> retrieveAllByRobot(@NotNull Guid robotGuid) {
        LOG.info("Rertieving all {} by robot", LineFollowerResult.class.getName());
        TypedQuery<LineFollowerResult> q = entityManager().createQuery("SELECT lfr FROM LineFollowerResult lfr WHERE " +
                "lfr.robot.guid = :guid" +
                " ORDER BY lfr.result", LineFollowerResult.class);
        q.setParameter("guid", robotGuid);

        return q.getResultList();
    }

    public List<LineFollowerResult> getRanking(Guid competitionGuid) {
        LOG.info("Retrieving {} ranking", LineFollowerCompetition.class.getName());
        TypedQuery<LineFollowerResult> q = entityManager().createQuery(
                "SELECT lfr FROM LineFollowerResult lfr " +
                        "WHERE lfr.competition.guid = :guid AND " +
                        "lfr.robot IN " +
                        "(SELECT sublfr.robot FROM LineFollowerResult sublfr GROUP BY sublfr.robot HAVING MIN(sublfr.result) = lfr.result) " +
                        "ORDER BY lfr.result", LineFollowerResult.class);
        q.setParameter("guid", competitionGuid);

        return q.getResultList();
    }
}
