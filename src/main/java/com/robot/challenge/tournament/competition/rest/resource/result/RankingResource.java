package com.robot.challenge.tournament.competition.rest.resource.result;

import javax.annotation.security.PermitAll;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface RankingResource {
    String RANKING_CONTEXT = "/ranking";

    String DEFAULT_LINEFOLLOWER_RANKING = "/linefollower/guid/{guid}";
    String DEFAULT_SUMO_RANKING = "/sumo/guid/{guid}";
    String DEFAULT_FREESTYLE_RANKING = "/freestyle/guid/{guid}";

    @GET
    @Path(DEFAULT_LINEFOLLOWER_RANKING)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response lineFollowerRanking(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                 @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(DEFAULT_SUMO_RANKING)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response sumoRanking(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                         @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(DEFAULT_FREESTYLE_RANKING)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response freestyleRanking(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                              @PathParam("guid") @NotNull @Size(min = 1) String guid);
}
