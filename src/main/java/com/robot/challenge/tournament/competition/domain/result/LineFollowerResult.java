package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

/**
 * Created by mklys on 27/01/16.
 */
@Entity
@Table(name = "LINEFOLLOWER_RESULT", indexes = { @Index(name = "linefollower_result_idx", columnList = "guid") })
public class LineFollowerResult implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "linefollower_result_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "RESULT", nullable = false)
    private Long result;
    @ManyToOne(optional = false)
    @JoinColumn(name = "AUTHOR_ID", nullable = false, updatable = false)
    private SystemUser author;
    @ManyToOne(optional = false)
    @JoinColumn(name = "COMPETITION_ID", nullable = false, updatable = false)
    private Competition competition;
    @ManyToOne(optional = false)
    @JoinColumn(name = "ROBOT_ID", nullable = false, updatable = false)
    private Robot robot;
    @ManyToOne
    @JoinColumn(name = "EDITOR_ID")
    private SystemUser editor;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE_TIME", nullable = false, updatable = false)
    private Date createDateTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE_TIME")
    private Date updateDateTime;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    private LineFollowerResult() {

    }

    public LineFollowerResult(Guid guid, SystemUser author, Competition competition, Robot robot, Long result) {
        this.guid = guid;
        this.author = author;
        this.competition = competition;
        this.robot = robot;
        this.result = result;
        this.createDateTime = Date.from(ZonedDateTime.now(ZoneId.of("UTC")).toInstant());
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public Long getResult() {
        return result;
    }

    public LineFollowerResult setResult(Long result) {
        this.result = result;
        return this;
    }

    public SystemUser getAuthor() {
        return author;
    }

    public Competition getCompetition() {
        return competition;
    }

    public Robot getRobot() {
        return robot;
    }

    public Optional<SystemUser> getEditor() {
        return Optional.ofNullable(editor);
    }

    public LineFollowerResult setEditor(SystemUser editor) {
        this.editor = editor;
        return this;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime.toInstant().atZone(ZoneId.of("UTC"));
    }

    public Optional<ZonedDateTime> getUpdateDateTime() {
        if(updateDateTime != null) {
            return Optional.of(updateDateTime.toInstant().atZone(ZoneId.of("UTC")));
        }
        return Optional.empty();
    }

    public LineFollowerResult setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = Date.from(updateDateTime.toInstant());
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public LineFollowerResult setVersion(Long version) {
        this.version = version;
        return this;
    }
}
