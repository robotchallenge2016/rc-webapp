package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;

import javax.inject.Inject;
import java.util.function.Function;

public class SumoMatchDayFactory extends Factory<SumoMatchDay> {
    private final SumoMatchFactory sumoMatchFactory;

    @Inject
    public SumoMatchDayFactory(SumoMatchFactory sumoMatchFactory) {
        super(SumoMatchDay.class);
        this.sumoMatchFactory = sumoMatchFactory;
    }

    @Override
    public Function<SumoMatchDay, SumoMatchDay> decorator() {
        return (matchDay -> matchDay.setMatchFactory(sumoMatchFactory));
    }

    public SumoMatchDay create(Integer number, SumoCompetition competition) {
        return decorator().apply(new SumoMatchDay(Guid.generate(), number, competition));
    }
}
