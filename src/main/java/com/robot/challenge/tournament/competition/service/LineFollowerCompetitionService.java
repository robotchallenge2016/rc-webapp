package com.robot.challenge.tournament.competition.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetition;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetitionFactory;
import com.robot.challenge.tournament.competition.exception.CompetitionException;
import com.robot.challenge.tournament.competition.repository.CompetitionRepository;
import com.robot.challenge.tournament.competition.repository.LineFollowerCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.result.LineFollowerResultFinder;
import com.robot.challenge.tournament.competition.repository.result.LineFollowerResultRepository;
import com.robot.challenge.tournament.competition.rest.model.linefollower.ExistingLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.linefollower.NewLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.repository.TournamentDictionary;
import com.robot.challenge.tournament.domain.TournamentEdition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class LineFollowerCompetitionService {

    private static final Logger LOG = LoggerFactory.getLogger(LineFollowerCompetitionService.class);

    private final LineFollowerCompetitionFactory competitionFactory;
    private final CompetitionRepository competitionRepository;
    private final LineFollowerCompetitionFinder competitionFinder;
    private final RobotFinder robotFinder;
    private final LineFollowerResultFinder lineFollowerResultFinder;
    private final LineFollowerResultRepository lineFollowerResultRepository;
    private final TournamentDictionary tournamentDictionary;

    @Inject
    public LineFollowerCompetitionService(LineFollowerCompetitionFactory competitionFactory, CompetitionRepository competitionRepository,
                                          LineFollowerCompetitionFinder competitionFinder, RobotFinder robotFinder,
                                          LineFollowerResultFinder lineFollowerResultFinder,
                                          LineFollowerResultRepository lineFollowerResultRepository,
                                          TournamentDictionary tournamentDictionary) {
        this.competitionFactory = competitionFactory;
        this.competitionRepository = competitionRepository;
        this.competitionFinder = competitionFinder;
        this.robotFinder = robotFinder;
        this.lineFollowerResultFinder = lineFollowerResultFinder;
        this.lineFollowerResultRepository = lineFollowerResultRepository;
        this.tournamentDictionary = tournamentDictionary;
    }

    private void validateName(String name, TournamentEdition edition) {
        competitionFinder.retrieveByName(name, edition).ifPresent(systemUser -> {
            throw CompetitionException.nameAlreadyExists();
        });
    }

    public LineFollowerCompetition create(NewLineFollowerCompetitionModel model, TournamentEdition edition) {
        LOG.info("Creating {} [name={}]", LineFollowerCompetition.class.getCanonicalName(), model.getName());
        validateName(model.getName(), edition);
        
        List<Robot> robots = Collections.emptyList();
        if(!model.getRobotGuids().isEmpty()) {
            List<Guid> robotGuids = model.getRobotGuids()
                    .stream()
                    .map(Guid::from)
                    .collect(Collectors.toList());
            robots = robotFinder.retrieveByGuids(robotGuids, edition);
        }
        LineFollowerCompetition competition = (LineFollowerCompetition)
                competitionFactory.create(Guid.from(model.getCompetitionSubtypeGuid()), edition)
                .setMaxNumberOfRuns(model.getMaxNumberOfRuns())
                .setName(model.getName())
                .replaceListOfRobots(robots);

        competitionRepository.create(competition);

        return competition;
    }

    public void update(Guid guid, ExistingLineFollowerCompetitionModel model, TournamentEdition edition) {
        LOG.info("Updating {} [name={}, {}]", LineFollowerCompetition.class.getCanonicalName(), model.getName(), guid);
        CompetitionSubtype competitionSubtype = tournamentDictionary
                .retrieveCompetitionSubtypeByGuidAndCompetitionType(Guid.from(model.getCompetitionSubtypeGuid()), CompetitionType.LINEFOLLOWER)
                .orElseThrow(DataAccessException::entityNotFound);
        List<Robot> robots = Collections.emptyList();
        if(!model.getRobotGuids().isEmpty()) {
            List<Guid> robotGuids = model.getRobotGuids()
                    .stream()
                    .map(Guid::from)
                    .collect(Collectors.toList());
            robots = robotFinder.retrieveByGuids(robotGuids, edition);
        }
        Competition competition = competitionFinder.retrieveByGuid(guid, Boolean.TRUE).orElseThrow(DataAccessException::entityNotFound)
                .setMaxNumberOfRuns(model.getMaxNumberOfRuns())
                .setName(model.getName())
                .setCompetitionSubtype(competitionSubtype)
                .replaceListOfRobots(robots)
                .setVersion(model.getVersion());

        if(!competition.getName().equals(model.getName())) {
            validateName(model.getName(), edition);
        }

        competitionRepository.update(competition);
    }

    public void addRobotToCompetition(LineFollowerCompetition competition, Robot robot) {
        if(!competition.getRobots().contains(robot)) {
            competition.addRobot(robot);
            competitionRepository.update(competition);
        }
    }

    @Transactional
    public void delete(Guid guid) {
        LOG.info("Deleting {} [{}]", LineFollowerCompetition.class.getCanonicalName(), guid);
        lineFollowerResultFinder.retrieveByCompetition(guid).forEach(result -> lineFollowerResultRepository.delete(result.getId()));
        competitionFinder.retrieveByGuid(guid).map(entity -> competitionRepository.delete(entity.getId())).orElseThrow(DataAccessException::entityNotFound);
    }

    public void startCompetition(Guid guid, TournamentEdition edition) {
        if(!edition.isTournamentStarted()) {
            throw CompetitionException.tournamentNotOpened();
        }

        LOG.info("Starting {} [{}]", LineFollowerCompetition.class.getCanonicalName(), guid);
        LineFollowerCompetition lineFollowerCompetition = competitionFinder.retrieveByGuid(guid, true)
                .orElseThrow(DataAccessException::entityNotFound);

        lineFollowerCompetition.start();
        competitionRepository.update(lineFollowerCompetition);
    }

    public void stopCompetition(Guid guid) {
        LOG.info("Stopping {} [{}]", LineFollowerCompetition.class.getCanonicalName(), guid);
        LineFollowerCompetition lineFollowerCompetition = competitionFinder.retrieveByGuid(guid, true)
                .orElseThrow(DataAccessException::entityNotFound);

        lineFollowerCompetition.finish();
        competitionRepository.update(lineFollowerCompetition);
    }
}
