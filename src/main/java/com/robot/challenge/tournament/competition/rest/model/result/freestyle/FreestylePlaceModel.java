package com.robot.challenge.tournament.competition.rest.model.result.freestyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePlace;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FreestylePlaceModel {

    @NotNull
    @Size(min = 1)
    @JsonProperty
    private FreestylePlace value;

    private FreestylePlaceModel() {

    }

    public FreestylePlace getValue() {
        return value;
    }

    public static FreestylePlaceModel from(FreestylePlace freestylePlace) {
        FreestylePlaceModel model = new FreestylePlaceModel();
        model.value = freestylePlace;

        return model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FreestylePlaceModel that = (FreestylePlaceModel) o;

        return Objects.equals(value, that.value);

    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}