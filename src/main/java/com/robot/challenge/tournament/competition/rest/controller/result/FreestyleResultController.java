package com.robot.challenge.tournament.competition.rest.controller.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.domain.result.FreestyleResult;
import com.robot.challenge.tournament.competition.repository.CompetitionRepository;
import com.robot.challenge.tournament.competition.repository.result.FreestyleResultFinder;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.BriefFreestyleResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.ExistingFreestyleResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.NewFreestyleResultModel;
import com.robot.challenge.tournament.competition.rest.resource.result.FreestyleResultResource;
import com.robot.challenge.tournament.competition.rest.resource.result.ResultResource;
import com.robot.challenge.tournament.competition.service.result.FreestyleResultService;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + ResultResource.RESULT_CONTEXT + FreestyleResultResource.DEFAULT_FREESTYLE_RESULT_PREFIX)
public class FreestyleResultController implements FreestyleResultResource {

    @Inject
    private CompetitionRepository competitionRepository;
    @Inject
    private ContextFactory contextFactory;
    @Inject
    private FreestyleResultService resultService;
    @Inject
    private FreestyleResultFinder resultFinder;

    @Override
    public Response create(@NotNull @Size(min = 1) String editionGuid,
                           @Valid NewFreestyleResultModel resultModel) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        FreestyleResult createdResult = resultService.create(resultModel, edition);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdResult)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid,
                           @Valid ExistingFreestyleResultModel resultModel) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        resultService.update(Guid.from(guid), resultModel, edition);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid) {
        resultService.delete(Guid.from(guid));
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        ExistingFreestyleResultModel result = ExistingFreestyleResultModel.from(resultFinder.retrieveByGuid(Guid.from(guid))
                .orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid) {
        List<BriefFreestyleResultModel> result = resultFinder.retrieveAll().stream()
                .map(BriefFreestyleResultModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByCompetition(@NotNull @Size(min = 1) String editionGuid,
                                          @NotNull @Size(min = 1) String competitionGuid) {
        List<BriefFreestyleResultModel> result = resultFinder.retrieveByCompetition(Guid.from(competitionGuid)).stream()
                .map(BriefFreestyleResultModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByRobot(@NotNull @Size(min = 1) String editionGuid,
                                    @NotNull @Size(min = 1) String robotGuid) {
        List<BriefFreestyleResultModel> result = resultFinder.retrieveAllByRobot(Guid.from(robotGuid)).stream()
                .map(BriefFreestyleResultModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }
}
