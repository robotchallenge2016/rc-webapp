package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competition.domain.competition.SumoRules;
import com.robot.challenge.tournament.competition.repository.result.SumoResultRepository;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "SUMO_MATCH", indexes = { @Index(name = "sumo_match_idx", columnList = "guid") })
public class SumoMatch {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "sumo_match_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @ManyToOne(optional = false)
    @JoinColumn(name = "HOME_ROBOT_ID", nullable = false, updatable = false)
    private Robot homeRobot;
    @ManyToOne(optional = false)
    @JoinColumn(name = "AWAY_ROBOT_ID", nullable = false, updatable = false)
    private Robot awayRobot;
    @ManyToOne(optional = false)
    @JoinColumn(name = "MATCH_DAY_ID", nullable = false, updatable = false)
    private SumoMatchDay matchDay;
    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<SumoResult> results;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;
    @Transient
    private SumoResultFactory resultFactory;
    @Transient
    private SumoResultRepository resultRepository;

    protected SumoMatch() {

    }

    SumoMatch(Guid guid, SumoMatchDay matchDay, Robot homeRobot, Robot awayRobot) {
        this.guid = guid;
        this.matchDay = matchDay;
        this.homeRobot = homeRobot;
        this.awayRobot = awayRobot;
        this.results = new ArrayList<>();
    }

    public void addFightResult(Robot winner, Robot looser, SystemUser author) {
        results.add(resultFactory.create(this, winner, looser, author));
    }

    @Transactional
    public void removeAllFightResults() {
        results.forEach(result -> resultRepository.delete(result.getId()));
        results.clear();
    }

    public void removeFightResult(Guid guid) {
        Optional<SumoResult> maybeSumoResult = results.stream().filter(sumoResult -> sumoResult.getGuid().equals(guid)).findFirst();
        maybeSumoResult.ifPresent(results::remove);
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public SumoMatchDay getMatchDay() {
        return matchDay;
    }

    public Robot getHomeRobot() {
        return homeRobot;
    }

    public Robot getAwayRobot() {
        return awayRobot;
    }

    public Optional<Robot> getWinner(SumoRules rules) {
        if(results.isEmpty()) {
            return Optional.empty();
        }
        Long homeRobotPoints = results.stream().filter(result -> result.getWinner().equals(homeRobot)).count();
        Long awayRobotPoints = results.stream().filter(result -> result.getWinner().equals(awayRobot)).count();
        if(homeRobotPoints.intValue() != rules.getRequiredNumberOfWins() && awayRobotPoints.intValue() != rules.getRequiredNumberOfWins()) {
            return Optional.empty();
        }
        if(homeRobotPoints > awayRobotPoints) {
            return Optional.of(homeRobot);
        }
        return Optional.of(awayRobot);
    }

    public Optional<Robot> getLooser(SumoRules rules) {
        if(results.isEmpty()) {
            return Optional.empty();
        }
        Long homeRobotPoints = results.stream().filter(result -> result.getWinner().equals(homeRobot)).count();
        Long awayRobotPoints = results.stream().filter(result -> result.getWinner().equals(awayRobot)).count();
        if(homeRobotPoints.intValue() != rules.getRequiredNumberOfWins() && awayRobotPoints.intValue() != rules.getRequiredNumberOfWins()) {
            return Optional.empty();
        }
        if(homeRobotPoints < awayRobotPoints) {
            return Optional.of(homeRobot);
        }
        return Optional.of(awayRobot);
    }

    public boolean hasRequiredNumberOfWins(SumoRules rules) {
        return numberOfWinnerWonFights(rules).equals(rules.getRequiredNumberOfWins());
    }
    
    public Integer numberOfWinnerWonFights(SumoRules rules) {
        return getWinner(rules)
                .map(winner -> Long.valueOf(results.stream().filter(result -> result.getWinner().equals(winner)).count()).intValue())
                .orElse(0);
    }

    public Integer numberOfLoserWonFights(SumoRules rules) {
        return getLooser(rules)
                .map(looser -> Long.valueOf(results.stream().filter(result -> result.getWinner().equals(looser)).count()).intValue())
                .orElse(0);
    }

    public List<SumoResult> getResults() {
        return Collections.unmodifiableList(results);
    }

    public Long getVersion() {
        return version;
    }

    public SumoMatch setVersion(Long version) {
        this.version = version;
        return this;
    }

    SumoMatch setResultFactory(SumoResultFactory resultFactory) {
        this.resultFactory = resultFactory;
        return this;
    }

    SumoMatch setResultRepository(SumoResultRepository resultRepository) {
        this.resultRepository = resultRepository;
        return this;
    }
}
