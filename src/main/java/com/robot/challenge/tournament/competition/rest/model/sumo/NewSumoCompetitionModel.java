package com.robot.challenge.tournament.competition.rest.model.sumo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewSumoCompetitionModel {

    @NotNull
    @Size(min = 1)
    private String name;
    @NotNull
    @Size(min = 1)
    private String competitionSubtypeGuid;
    @NotNull
    @Min(value = 0)
    private Integer requiredNumberOfWins;
    @NotNull
    @Min(value = 0)
    private Integer bigWinThreshold;
    @NotNull
    private Integer bigWinPoints;
    @NotNull
    private Integer bigLostPoints;
    @NotNull
    private Integer smallWinPoints;
    @NotNull
    private Integer smallLostPoints;
    private boolean secondLeg;

    private List<String> robotGuids = new ArrayList<>();

    public String getName() {
        return name;
    }

    public String getCompetitionSubtypeGuid() {
        return competitionSubtypeGuid;
    }

    public Integer getRequiredNumberOfWins() {
        return requiredNumberOfWins;
    }

    public Integer getBigWinThreshold() {
        return bigWinThreshold;
    }

    public Integer getBigWinPoints() {
        return bigWinPoints;
    }

    public Integer getBigLostPoints() {
        return bigLostPoints;
    }

    public Integer getSmallWinPoints() {
        return smallWinPoints;
    }

    public Integer getSmallLostPoints() {
        return smallLostPoints;
    }

    public boolean isSecondLeg() {
        return secondLeg;
    }

    public List<String> getRobotGuids() {
        return robotGuids;
    }

    public static NewCompetitionModelBuilder builder() {
        return new NewCompetitionModelBuilder();
    }

    public static class NewCompetitionModelBuilder {

        private final NewSumoCompetitionModel model = new NewSumoCompetitionModel();

        public NewCompetitionModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public NewCompetitionModelBuilder setCompetitionSubtypeGuid(String competitionTypeGuid) {
            model.competitionSubtypeGuid = competitionTypeGuid;
            return this;
        }

        public NewCompetitionModelBuilder addRobot(String guid) {
            model.robotGuids.add(guid);
            return this;
        }

        public NewCompetitionModelBuilder requiredNumberOfWins(Integer requiredNumberOfWins) {
            model.requiredNumberOfWins = requiredNumberOfWins;
            return this;
        }

        public NewCompetitionModelBuilder bigWinThreshold(Integer threshold) {
            model.bigWinThreshold = threshold;
            return this;
        }

        public NewCompetitionModelBuilder bigWinPoints(Integer points) {
            model.bigWinPoints = points;
            return this;
        }

        public NewCompetitionModelBuilder bigLosePoints(Integer points) {
            model.bigLostPoints = points;
            return this;
        }

        public NewCompetitionModelBuilder smallWinPoints(Integer points) {
            model.smallWinPoints = points;
            return this;
        }

        public NewCompetitionModelBuilder smallLosePoints(Integer points) {
            model.smallLostPoints = points;
            return this;
        }

        public NewCompetitionModelBuilder rematch(boolean rematch) {
            model.secondLeg = rematch;
            return this;
        }

        public NewSumoCompetitionModel build() {
            return model;
        }
    }
}
