package com.robot.challenge.tournament.competition.service.result.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class SumoMatchException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        NOT_ENOUGH_RESULTS("Match contains different number of winner results than competition allows"),
        THE_SAME_NUMBER_OF_MATCHES("Match contains the same number of matches of home and away robot");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public SumoMatchException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static SumoMatchException notEnoughResults() {
        return new SumoMatchException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.NOT_ENOUGH_RESULTS,
                Cause.NOT_ENOUGH_RESULTS.getErrorDescription());
    }

    public static SumoMatchException theSameNumberOfMatches() {
        return new SumoMatchException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.THE_SAME_NUMBER_OF_MATCHES,
                Cause.THE_SAME_NUMBER_OF_MATCHES.getErrorDescription());
    }
}
