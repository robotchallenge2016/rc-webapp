package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SUMO_MATCH_DAY", indexes = { @Index(name = "sumo_match_day_idx", columnList = "guid") })
public class SumoMatchDay implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "sumo_match_day_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "NUMBER", nullable = false)
    private Integer number;
    @ManyToOne(optional = false)
    @JoinColumn(name = "SUMO_COMPETITION_ID", nullable = false, updatable = false)
    private SumoCompetition competition;
    @OneToMany(mappedBy = "matchDay", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<SumoMatch> matches = new ArrayList<>();
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;
    @Transient
    private SumoMatchFactory matchFactory;

    protected SumoMatchDay() {
    }

    protected SumoMatchDay(Guid guid, Integer number, SumoCompetition competition) {
        this.guid = guid;
        this.number = number;
        this.competition = competition;
    }

    SumoMatchDay setMatchFactory(SumoMatchFactory matchFactory) {
        this.matchFactory = matchFactory;
        return this;
    }

    public Long getId() {
        return id;
    }

    @Override
    public Guid getGuid() {
        return guid;
    }

    public Integer getNumber() {
        return number;
    }

    public SumoCompetition getCompetition() {
        return competition;
    }

    public void addMatch(Robot home, Robot away) {
        SumoMatch match = matchFactory.create(this, home, away);
        matches.add(match);
    }

    public void createSecondLeg(SumoMatchDay secondLegMatchDay) {
        for(SumoMatch firstLeg : matches) {
            secondLegMatchDay.addMatch(firstLeg.getAwayRobot(), firstLeg.getHomeRobot());
        }
    }

    public void removeFakeMatch(Robot fakeRobot) {
        for(int i = matches.size() - 1; i >= 0; --i) {
            SumoMatch match = matches.get(i);
            if(match.getHomeRobot().equals(fakeRobot) || match.getAwayRobot().equals(fakeRobot)) {
                matches.remove(match);
            }
        }
    }

    public List<SumoMatch> getMatches() {
        return matches;
    }

    public Robot homeOfMatchDay(int matchDayNumber) {
        return matches.get(matchDayNumber).getHomeRobot();
    }

    public Robot awayOfMatchDay(int matchDayNumber) {
        return matches.get(matchDayNumber).getAwayRobot();
    }

    public Long getVersion() {
        return version;
    }

    public SumoMatchDay setVersion(Long version) {
        this.version = version;
        return this;
    }
}
