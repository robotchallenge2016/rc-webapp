package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.inject.Inject;

public class LineFollowerResultFactory extends Factory<LineFollowerResult> {

    @Inject
    public LineFollowerResultFactory() {
        super(LineFollowerResult.class);
    }

    public LineFollowerResult create(SystemUser author, Competition competition, Robot robot, Long result) {
        return new LineFollowerResult(Guid.generate(), author, competition, robot, result);
    }
}
