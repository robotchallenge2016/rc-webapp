package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competition.domain.result.SumoMatchDay;

import javax.ejb.Stateless;

@Stateless
public class SumoMatchDayRepository extends JpaRepository<SumoMatchDay> {

    public SumoMatchDayRepository() {
        super(SumoMatchDay.class);
    }
}
