package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetition;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePlace;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.inject.Inject;

public class FreestyleResultFactory extends Factory<FreestyleResult> {

    @Inject
    public FreestyleResultFactory() {
        super(FreestyleResult.class);
    }

    public FreestyleResult create(SystemUser author, FreestyleCompetition competition, Robot robot, FreestylePlace place) {
        return new FreestyleResult(Guid.generate(), author, competition, robot, place);
    }
}
