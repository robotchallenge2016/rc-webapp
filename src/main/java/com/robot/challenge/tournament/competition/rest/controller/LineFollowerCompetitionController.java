package com.robot.challenge.tournament.competition.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetition;
import com.robot.challenge.tournament.competition.repository.LineFollowerCompetitionFinder;
import com.robot.challenge.tournament.competition.rest.model.linefollower.BriefLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.linefollower.ExistingLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.linefollower.NewLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competition.rest.resource.CompetitionResource;
import com.robot.challenge.tournament.competition.rest.resource.LineFollowerCompetitionResource;
import com.robot.challenge.tournament.competition.service.LineFollowerCompetitionService;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + CompetitionResource.DEFAULT_COMPETITION_PREFIX + LineFollowerCompetitionResource.DEFAULT_LINEFOLLOWER_COMPETITION_PREFIX)
public class LineFollowerCompetitionController implements LineFollowerCompetitionResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private LineFollowerCompetitionService competitionService;
    @Inject
    private LineFollowerCompetitionFinder competitionFinder;

    @Override
    public Response create(@NotNull @Size(min = 1) String editionGuid,
                           @Valid NewLineFollowerCompetitionModel competition) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        LineFollowerCompetition createdCompetition = competitionService.create(competition, edition);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdCompetition)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid,
                           @Valid ExistingLineFollowerCompetitionModel competition) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        competitionService.update(Guid.from(guid), competition, edition);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid) {
        competitionService.delete(Guid.from(guid));
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        ExistingLineFollowerCompetitionModel result = ExistingLineFollowerCompetitionModel
                .from(competitionFinder.retrieveByGuid(Guid.from(guid), true)
                        .orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefLineFollowerCompetitionModel> result = competitionFinder.retrieveAll(edition)
                .stream()
                .map(BriefLineFollowerCompetitionModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByCompetitor(@NotNull @Size(min = 1) String editionGuid,
                                         @NotNull @Size(min = 1) String guid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefLineFollowerCompetitionModel> result = competitionFinder
                .retrieveByCompetitor(Guid.from(guid), true, edition)
                .stream()
                .map(BriefLineFollowerCompetitionModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response searchMatchingAnyParam(@NotNull @Size(min = 1) String editionGuid,
                                           @NotNull @Size(min = 1) String competitionGuid,
                                           @NotNull @Size(min = 1) String search) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefRobotModel> result = competitionFinder
                .retrieveRobotsInCompetitionMatchingAnyParam(Guid.from(competitionGuid), search, edition)
                .map(Competition::getRobots).orElseGet(Collections::emptyList)
                .stream()
                .map(BriefRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response nameExists(@NotNull @Size(min = 1) String editionGuid,
                               @NotNull @Size(min = 1) String name) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        return competitionFinder.retrieveByName(name, edition)
                .map(r -> Response.status(Response.Status.OK))
                .orElse(Response.status(Response.Status.BAD_REQUEST)).build();
    }

    @Override
    public Response nameIsFree(@NotNull @Size(min = 1) String editionGuid,
                               @NotNull @Size(min = 1) String name) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        return competitionFinder.retrieveByName(name, edition)
                .map(r -> Response.status(Response.Status.BAD_REQUEST))
                .orElse(Response.status(Response.Status.OK)).build();
    }

    @Override
    public Response startCompetition(@NotNull @Size(min = 1) String editionGuid,
                                     @NotNull @Size(min = 1) String guid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        competitionService.startCompetition(Guid.from(guid), edition);
        return Response.ok().build();
    }

    @Override
    public Response stopCompetition(@NotNull @Size(min = 1) String editionGuid,
                                    @NotNull @Size(min = 1) String guid) {
        competitionService.stopCompetition(Guid.from(guid));
        return Response.ok().build();
    }
}
