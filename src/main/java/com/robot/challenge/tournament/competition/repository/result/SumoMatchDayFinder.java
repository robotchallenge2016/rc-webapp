package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.competition.domain.result.SumoMatchDay;
import com.robot.challenge.tournament.competition.domain.result.SumoMatchDayFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import java.util.List;

public class SumoMatchDayFinder extends JpaFinder<SumoMatchDay> {
    private static final Logger LOG = LoggerFactory.getLogger(SumoMatchDayFinder.class);

    @Inject
    public SumoMatchDayFinder(SumoMatchDayFactory factory) {
        super(factory);
    }

    public List<SumoMatchDay> retrieveByCompetiton(Guid competitionGuid) {
        LOG.info("Retrieving {} by Competition GUID", SumoMatchDay.class.getName());
        TypedQuery<SumoMatchDay> q = entityManager().createQuery("SELECT smd FROM SumoMatchDay smd WHERE smd.competition.guid = :guid", SumoMatchDay.class);
        q.setParameter("guid", competitionGuid);

        return q.getResultList();
    }
}
