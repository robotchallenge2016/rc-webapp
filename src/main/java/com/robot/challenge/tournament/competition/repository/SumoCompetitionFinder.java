package com.robot.challenge.tournament.competition.repository;

import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetitionFactory;

import javax.inject.Inject;

public class SumoCompetitionFinder extends CompetitionFinder<SumoCompetition> {

    @Inject
    public SumoCompetitionFinder(SumoCompetitionFactory factory) {
        super(factory);
    }
}
