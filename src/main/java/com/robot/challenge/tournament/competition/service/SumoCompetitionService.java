package com.robot.challenge.tournament.competition.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetitionFactory;
import com.robot.challenge.tournament.competition.domain.competition.SumoRules;
import com.robot.challenge.tournament.competition.exception.CompetitionException;
import com.robot.challenge.tournament.competition.repository.CompetitionRepository;
import com.robot.challenge.tournament.competition.repository.SumoCompetitionFinder;
import com.robot.challenge.tournament.competition.rest.model.sumo.ExistingSumoCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.sumo.NewSumoCompetitionModel;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.repository.TournamentDictionary;
import com.robot.challenge.tournament.domain.TournamentEdition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SumoCompetitionService {

    private static final Logger LOG = LoggerFactory.getLogger(SumoCompetitionService.class);

    private final SumoCompetitionFactory competitionFactory;
    private final CompetitionRepository competitionRepository;
    private final SumoCompetitionFinder competitionFinder;
    private final SumoScheduleService scheduleGenerator;
    private final RobotFinder robotFinder;
    private final TournamentDictionary tournamentDictionary;

    @Inject
    public SumoCompetitionService(SumoCompetitionFactory competitionFactory, CompetitionRepository competitionRepository,
                                  SumoCompetitionFinder competitionFinder, SumoScheduleService scheduleGenerator,
                                  RobotFinder robotFinder, TournamentDictionary tournamentDictionary) {
        this.competitionFactory = competitionFactory;
        this.competitionRepository = competitionRepository;
        this.competitionFinder = competitionFinder;
        this.scheduleGenerator = scheduleGenerator;
        this.robotFinder = robotFinder;
        this.tournamentDictionary = tournamentDictionary;
    }

    private void validateName(String name, TournamentEdition edition) {
        competitionFinder.retrieveByName(name, edition).ifPresent(systemUser -> {
            throw CompetitionException.nameAlreadyExists();
        });
    }

    public SumoCompetition create(NewSumoCompetitionModel model, TournamentEdition edition) {
        LOG.info("Creating {} [name={}]", SumoCompetition.class.getCanonicalName(), model.getName());
        validateName(model.getName(), edition);

        List<Robot> robots = Collections.emptyList();
        if(!model.getRobotGuids().isEmpty()) {
            List<Guid> robotGuids = model.getRobotGuids()
                    .stream()
                    .map(Guid::from)
                    .collect(Collectors.toList());
            robots = robotFinder.retrieveByGuids(robotGuids, edition);
        }
        SumoRules rules = SumoRules.from(
                model.getRequiredNumberOfWins(), model.getBigWinThreshold(),
                model.getBigWinPoints(), model.getBigLostPoints(),
                model.getSmallWinPoints(), model.getSmallLostPoints());
        SumoCompetition competition = (SumoCompetition)
                competitionFactory.create(
                        Guid.from(model.getCompetitionSubtypeGuid()),
                        rules,
                        edition)
                .setSecondLeg(model.isSecondLeg())
                .setName(model.getName())
                .replaceListOfRobots(robots);

        competitionRepository.create(competition);

        return competition;
    }

    public void update(Guid guid, ExistingSumoCompetitionModel model, TournamentEdition edition) {
        LOG.info("Updating {} [name={}, {}]", SumoCompetition.class.getCanonicalName(), model.getName(), guid);
        CompetitionSubtype competitionSubtype = tournamentDictionary
                .retrieveCompetitionSubtypeByGuidAndCompetitionType(Guid.from(model.getCompetitionSubtypeGuid()), CompetitionType.SUMO)
                .orElseThrow(DataAccessException::entityNotFound);

        List<Robot> robots = Collections.emptyList();
        if(!model.getRobotGuids().isEmpty()) {
            List<Guid> robotGuids = model.getRobotGuids()
                    .stream()
                    .map(Guid::from)
                    .collect(Collectors.toList());
            robots = robotFinder.retrieveByGuids(robotGuids, edition);
        }
        SumoCompetition competition = (SumoCompetition)
                competitionFinder.retrieveByGuid(guid, Boolean.TRUE).orElseThrow(DataAccessException::entityNotFound)
                .setPoints(SumoRules.from(model.getRequiredNumberOfWins(), model.getBigWinThreshold(),
                                            model.getBigWinPoints(), model.getBigLostPoints(),
                                            model.getSmallWinPoints(), model.getSmallLostPoints()))
                .setSecondLeg(model.isSecondLeg())
                .setName(model.getName())
                .setCompetitionSubtype(competitionSubtype)
                .replaceListOfRobots(robots)
                .setVersion(model.getVersion());

        if(!competition.getName().equals(model.getName())) {
            validateName(model.getName(), edition);
        }

        competitionRepository.update(competition);
    }

    public void addRobotToCompetition(SumoCompetition competition, Robot robot) {
        if(!competition.getRobots().contains(robot)) {
            competition.addRobot(robot);
            competitionRepository.update(competition);
        }
    }

    @Transactional
    public void delete(Guid guid) {
        LOG.info("Deleting {} [{}]", SumoCompetition.class.getCanonicalName(), guid);
        SumoCompetition competition = competitionFinder.retrieveByGuid(guid).orElseThrow(DataAccessException::entityNotFound);
        scheduleGenerator.deleteSchedule(competition);
        competitionRepository.delete(competition.getId());
    }

    public void startCompetition(Guid guid, TournamentEdition edition) {
        if(!edition.isTournamentStarted()) {
            throw CompetitionException.tournamentNotOpened();
        }

        LOG.info("Starting {} [{}]", SumoCompetition.class.getCanonicalName(), guid);
        SumoCompetition sumoCompetition = competitionFinder.retrieveByGuid(guid, true).orElseThrow(DataAccessException::entityNotFound);
        sumoCompetition.start();
        competitionRepository.update(sumoCompetition);
    }

    public void stopCompetition(Guid guid) {
        LOG.info("Stopping {} [{}]", SumoCompetition.class.getCanonicalName(), guid);
        SumoCompetition sumoCompetition = competitionFinder.retrieveByGuid(guid, true).orElseThrow(DataAccessException::entityNotFound);
        sumoCompetition.finish();
        competitionRepository.update(sumoCompetition);
    }
}
