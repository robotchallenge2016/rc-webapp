package com.robot.challenge.tournament.competition.rest.model.freestyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewFreestyleCompetitionModel {

    @NotNull
    @Size(min = 1)
    private String name;
    @NotNull
    @Size(min = 1)
    private String competitionSubtypeGuid;
    private List<String> robotGuids = new ArrayList<>();
    @NotNull
    private Integer firstPlacePoints;
    @NotNull
    private Integer secondPlacePoints;
    @NotNull
    private Integer thirdPlacePoints;

    public String getName() {
        return name;
    }

    public String getCompetitionSubtypeGuid() {
        return competitionSubtypeGuid;
    }

    public List<String> getRobotGuids() {
        return robotGuids;
    }

    public Integer getFirstPlacePoints() {
        return firstPlacePoints;
    }

    public Integer getSecondPlacePoints() {
        return secondPlacePoints;
    }

    public Integer getThirdPlacePoints() {
        return thirdPlacePoints;
    }

    public static NewCompetitionModelBuilder builder() {
        return new NewCompetitionModelBuilder();
    }

    public static class NewCompetitionModelBuilder {

        private final NewFreestyleCompetitionModel model = new NewFreestyleCompetitionModel();

        public NewCompetitionModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public NewCompetitionModelBuilder setCompetitionSubtypeGuid(String competitionSubtypeGuid) {
            model.competitionSubtypeGuid = competitionSubtypeGuid;
            return this;
        }

        public NewCompetitionModelBuilder addRobot(String guid) {
            model.robotGuids.add(guid);
            return this;
        }

        public NewCompetitionModelBuilder setFirstPlacePoints(Integer firstPlacePoints) {
            model.firstPlacePoints = firstPlacePoints;
            return this;
        }

        public NewCompetitionModelBuilder setSecondPlacePoints(Integer secondPlacePoints) {
            model.secondPlacePoints = secondPlacePoints;
            return this;
        }

        public NewCompetitionModelBuilder setThirdPlacePoints(Integer thirdPlacePoints) {
            model.thirdPlacePoints = thirdPlacePoints;
            return this;
        }

        public NewFreestyleCompetitionModel build() {
            return model;
        }
    }
}
