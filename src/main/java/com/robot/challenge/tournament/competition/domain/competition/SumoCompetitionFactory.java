package com.robot.challenge.tournament.competition.domain.competition;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.repository.TournamentDictionary;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.inject.Inject;

public class SumoCompetitionFactory extends Factory<SumoCompetition> {
    private final TournamentDictionary tournamentDictionary;

    @Inject
    public SumoCompetitionFactory(TournamentDictionary tournamentDictionary) {
        super(SumoCompetition.class);
        this.tournamentDictionary = tournamentDictionary;
    }

    public SumoCompetition create(Guid competitionSubtypeGuid,
                                  SumoRules sumoRules,
                                  TournamentEdition edition) {
        CompetitionSubtype competitionSubtype = tournamentDictionary
                .retrieveCompetitionSubtypeByGuidAndCompetitionType(competitionSubtypeGuid, CompetitionType.SUMO)
                .orElseThrow(DataAccessException::entityNotFound);
        return new SumoCompetition(Guid.generate(), competitionSubtype, sumoRules, edition);
    }
}
