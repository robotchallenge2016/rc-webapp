package com.robot.challenge.tournament.competition.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetition;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetition;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetitionFactory;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePoints;
import com.robot.challenge.tournament.competition.exception.CompetitionException;
import com.robot.challenge.tournament.competition.repository.CompetitionRepository;
import com.robot.challenge.tournament.competition.repository.FreestyleCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.result.FreestyleResultFinder;
import com.robot.challenge.tournament.competition.repository.result.FreestyleResultRepository;
import com.robot.challenge.tournament.competition.rest.model.freestyle.ExistingFreestyleCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.freestyle.NewFreestyleCompetitionModel;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.repository.TournamentDictionary;
import com.robot.challenge.tournament.domain.TournamentEdition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FreestyleCompetitionService {

    private static final Logger LOG = LoggerFactory.getLogger(FreestyleCompetitionService.class);

    private final FreestyleCompetitionFactory competitionFactory;
    private final CompetitionRepository competitionRepository;
    private final FreestyleCompetitionFinder competitionFinder;
    private final RobotFinder robotFinder;
    private final FreestyleResultFinder freestyleResultFinder;
    private final FreestyleResultRepository freestyleResultRepository;
    private final TournamentDictionary tournamentDictionary;

    @Inject
    public FreestyleCompetitionService(FreestyleCompetitionFactory competitionFactory, CompetitionRepository competitionRepository,
                                       FreestyleCompetitionFinder competitionFinder, RobotFinder robotFinder,
                                       FreestyleResultFinder freestyleResultFinder,
                                       FreestyleResultRepository freestyleResultRepository,
                                       TournamentDictionary tournamentDictionary) {
        this.competitionFactory = competitionFactory;
        this.competitionRepository = competitionRepository;
        this.competitionFinder = competitionFinder;
        this.robotFinder = robotFinder;
        this.freestyleResultFinder = freestyleResultFinder;
        this.freestyleResultRepository = freestyleResultRepository;
        this.tournamentDictionary = tournamentDictionary;
    }

    private void validateName(String name, TournamentEdition edition) {
        competitionFinder.retrieveByName(name, edition).ifPresent(systemUser -> {
            throw CompetitionException.nameAlreadyExists();
        });
    }

    public FreestyleCompetition create(NewFreestyleCompetitionModel competitionModel, TournamentEdition edition) {
        LOG.info("Creating {} [name={}]", FreestyleCompetition.class.getCanonicalName(), competitionModel.getName());
        validateName(competitionModel.getName(), edition);

        List<Robot> robots = Collections.emptyList();
        if(!competitionModel.getRobotGuids().isEmpty()) {
            List<Guid> robotGuids = competitionModel.getRobotGuids()
                    .stream()
                    .map(Guid::from)
                    .collect(Collectors.toList());
            robots = robotFinder.retrieveByGuids(robotGuids, edition);
        }
        FreestylePoints points = FreestylePoints.from(
                competitionModel.getFirstPlacePoints(),
                competitionModel.getSecondPlacePoints(),
                competitionModel.getThirdPlacePoints());
        FreestyleCompetition competition = (FreestyleCompetition)
                competitionFactory.create(
                        Guid.from(competitionModel.getCompetitionSubtypeGuid()),
                        points,
                        edition)
                .setName(competitionModel.getName())
                .replaceListOfRobots(robots);

        competitionRepository.create(competition);

        return competition;
    }

    public void update(Guid guid, ExistingFreestyleCompetitionModel competitionModel, TournamentEdition edition) {
        LOG.info("Updating {} [name={}, {}]", FreestyleCompetition.class.getCanonicalName(), competitionModel.getName(), guid);
        CompetitionSubtype competitionSubtype = tournamentDictionary
                .retrieveCompetitionSubtypeByGuidAndCompetitionType(Guid.from(competitionModel.getCompetitionSubtypeGuid()), CompetitionType.FREESTYLE)
                .orElseThrow(DataAccessException::entityNotFound);

        List<Robot> robots = Collections.emptyList();
        if(!competitionModel.getRobotGuids().isEmpty()) {
            List<Guid> robotGuids = competitionModel.getRobotGuids()
                    .stream()
                    .map(Guid::from)
                    .collect(Collectors.toList());
            robots = robotFinder.retrieveByGuids(robotGuids, edition);
        }
        FreestyleCompetition competition = (FreestyleCompetition)
                competitionFinder.retrieveByGuid(guid, true).orElseThrow(DataAccessException::entityNotFound)
                .setPoints(FreestylePoints.from(competitionModel.getFirstPlacePoints(), competitionModel.getSecondPlacePoints(), competitionModel.getThirdPlacePoints()))
                .setName(competitionModel.getName())
                .setCompetitionSubtype(competitionSubtype)
                .replaceListOfRobots(robots)
                .setVersion(competitionModel.getVersion());

        if(!competition.getName().equals(competitionModel.getName())) {
            validateName(competitionModel.getName(), edition);
        }

        competitionRepository.update(competition);
    }

    public void addRobotToCompetition(FreestyleCompetition competition, Robot robot) {
        if(!competition.getRobots().contains(robot)) {
            competition.addRobot(robot);
            competitionRepository.update(competition);
        }
    }

    @Transactional
    public void delete(Guid guid) {
        LOG.info("Deleting {} [{}]", FreestyleCompetition.class.getCanonicalName(), guid);
        freestyleResultFinder.retrieveByCompetition(guid).forEach(result -> freestyleResultRepository.delete(result.getId()));
        competitionFinder.retrieveByGuid(guid).map(entity -> competitionRepository.delete(entity.getId())).orElseThrow(DataAccessException::entityNotFound);
    }

    public void startCompetition(Guid guid, TournamentEdition edition) {
        if(!edition.isTournamentStarted()) {
            throw CompetitionException.tournamentNotOpened();
        }

        LOG.info("Starting {} [{}]", LineFollowerCompetition.class.getCanonicalName(), guid);
        FreestyleCompetition freestyleCompetition = competitionFinder.retrieveByGuid(guid, true)
                .orElseThrow(DataAccessException::entityNotFound);

        freestyleCompetition.start();
        competitionRepository.update(freestyleCompetition);
    }

    public void stopCompetition(Guid guid) {
        LOG.info("Stopping {} [{}]", LineFollowerCompetition.class.getCanonicalName(), guid);
        FreestyleCompetition freestyleCompetition = competitionFinder.retrieveByGuid(guid, true)
                .orElseThrow(DataAccessException::entityNotFound);

        freestyleCompetition.finish();
        competitionRepository.update(freestyleCompetition);
    }
}
