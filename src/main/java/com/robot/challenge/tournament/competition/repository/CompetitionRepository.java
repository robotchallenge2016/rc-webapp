package com.robot.challenge.tournament.competition.repository;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competition.domain.competition.Competition;

import javax.ejb.Stateless;

@Stateless
public class CompetitionRepository extends JpaRepository<Competition> {

    public CompetitionRepository() {
        super(Competition.class);
    }
}
