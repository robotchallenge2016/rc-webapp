package com.robot.challenge.tournament.competition.domain.competition;

public enum CompetitionStatus {
    CREATED,
    STARTED,
    FINISHED
}
