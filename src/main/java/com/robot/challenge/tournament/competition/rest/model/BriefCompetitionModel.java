package com.robot.challenge.tournament.competition.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.dictionary.rest.model.CompetitionSubtypeModel;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BriefCompetitionModel {
    private String guid;
    private String name;
    private boolean started;
    private CompetitionSubtypeModel competitionSubtype;

    private BriefCompetitionModel() {

    }

    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public boolean isStarted() {
        return started;
    }

    public CompetitionSubtypeModel getCompetitionSubtype() {
        return competitionSubtype;
    }

    public static BriefCompetitionModel from(Competition competition) {
        BriefCompetitionModel model = new BriefCompetitionModel();
        model.guid = competition.getGuid().getValue();
        model.name = competition.getName();
        model.competitionSubtype = CompetitionSubtypeModel.from(competition.getCompetitionSubtype());
        model.started = competition.isStarted();

        return model;
    }
}
