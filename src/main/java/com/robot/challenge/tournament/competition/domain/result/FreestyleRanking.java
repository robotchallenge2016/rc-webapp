package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePlace;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePoints;
import com.robot.challenge.tournament.competitor.domain.Robot;

public class FreestyleRanking implements Comparable<FreestyleRanking> {

    private final FreestylePoints pointRules;
    private final Robot robot;
    private int points;
    private int firstPlaceCount;
    private int secondPlaceCount;
    private int thirdPlaceCount;

    public FreestyleRanking(FreestylePoints pointRules, Robot robot) {
        this.robot = robot;
        this.pointRules = pointRules;
    }

    public void addResult(FreestylePlace place, int count) {
        switch(place) {
            case FIRST:
                firstPlaceCount += count;
                points += count * pointRules.getFirstPlace();
                break;
            case SECOND:
                secondPlaceCount += count;
                points += count * pointRules.getSecondPlace();
                break;
            case THIRD:
                thirdPlaceCount += count;
                points += count * pointRules.getThirdPlace();
                break;
        }
    }

    public Robot getRobot() {
        return robot;
    }

    public int getPoints() {
        return points;
    }

    public int getFirstPlaceCount() {
        return firstPlaceCount;
    }

    public int getSecondPlaceCount() {
        return secondPlaceCount;
    }

    public int getThirdPlaceCount() {
        return thirdPlaceCount;
    }

    @Override
    public int compareTo(FreestyleRanking o) {
        if(points > o.points) {
            return 1;
        } else if(points < o.points) {
            return -1;
        } else {
            if(firstPlaceCount > o.firstPlaceCount) {
                return 1;
            } else if (firstPlaceCount < o.firstPlaceCount) {
                return -1;
            } else {
                if(secondPlaceCount > o.secondPlaceCount) {
                    return 1;
                } else if (secondPlaceCount < o.secondPlaceCount) {
                    return -1;
                } else {
                    if(thirdPlaceCount > o.thirdPlaceCount) {
                        return 1;
                    } else if (thirdPlaceCount < o.thirdPlaceCount) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        }
    }
}
