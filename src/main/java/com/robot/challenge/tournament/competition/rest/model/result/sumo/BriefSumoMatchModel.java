package com.robot.challenge.tournament.competition.rest.model.result.sumo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competition.domain.competition.SumoRules;
import com.robot.challenge.tournament.competition.domain.result.SumoMatch;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;

import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BriefSumoMatchModel {

    private String guid;
    private Integer matchDayNumber;
    private BriefRobotModel home;
    private BriefRobotModel away;
    private BriefRobotModel winner;
    private BriefRobotModel looser;
    private List<BriefSumoResultModel> results;

    public String getGuid() {
        return guid;
    }

    public Integer getMatchDayNumber() {
        return matchDayNumber;
    }

    public BriefRobotModel getHome() {
        return home;
    }

    public BriefRobotModel getAway() {
        return away;
    }

    public BriefRobotModel getWinner() {
        return winner;
    }

    public BriefRobotModel getLooser() {
        return looser;
    }

    public List<BriefSumoResultModel> getResults() {
        return results;
    }

    public static BriefSumoMatchModel from(SumoMatch sumoMatch) {
        SumoCompetition competition = sumoMatch.getMatchDay().getCompetition();
        SumoRules sumoRules = competition.getRules();

        BriefSumoMatchModel model = new BriefSumoMatchModel();
        model.guid = sumoMatch.getGuid().getValue();
        model.matchDayNumber = sumoMatch.getMatchDay().getNumber();
        model.home = BriefRobotModel.from(sumoMatch.getHomeRobot());
        model.away = BriefRobotModel.from(sumoMatch.getAwayRobot());
        sumoMatch.getWinner(sumoRules).ifPresent(winner -> model.winner = BriefRobotModel.from(winner));
        sumoMatch.getLooser(sumoRules).ifPresent(looser -> model.looser = BriefRobotModel.from(looser));
        model.results = sumoMatch.getResults().stream().map(BriefSumoResultModel::from).collect(Collectors.toList());

        return model;
    }
}
