package com.robot.challenge.tournament.competition.rest.resource;

import com.robot.challenge.infrastructure.rest.resource.CrudResource;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.competition.rest.model.sumo.ExistingSumoCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.sumo.NewSumoCompetitionModel;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface SumoCompetitionResource extends CrudResource<NewSumoCompetitionModel, ExistingSumoCompetitionModel> {

    String DEFAULT_SUMO_COMPETITION_PREFIX = "/sumo";
    String CREATE = "/";
    String UPDATE = "/guid/{guid}";
    String DELETE = "/guid/{guid}";
    String RETRIEVE_SINGLE = "/guid/{guid}";
    String RETRIEVE_ALL = "/all";
    String RETRIEVE_BY_COMPETITOR = "/competitor/guid/{guid}";
    String COMPETITION_NAME_EXISTS = "/name/{name}/existss";
    String COMPETITION_NAME_IS_FREE = "/name/{name}/free";
    String COMPETITION_START = "/start/guid/{guid}";
    String COMPETITION_STOP = "/stop/guid/{guid}";
    String SCHEDULE = "/schedule/guid/{guid}";

    @POST
    @Path(CREATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid NewSumoCompetitionModel competition);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingSumoCompetitionModel competition);

    @javax.ws.rs.DELETE
    @Path(DELETE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response retrieveAll(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);

    @GET
    @Path(RETRIEVE_BY_COMPETITOR)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByCompetitor(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                  @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(COMPETITION_NAME_EXISTS)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response nameExists(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                        @PathParam("name") @NotNull @Size(min = 1) String name);

    @GET
    @Path(COMPETITION_NAME_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response nameIsFree(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                        @PathParam("name") @NotNull @Size(min = 1) String name);

    @POST
    @Path(COMPETITION_START)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response startCompetition(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                              @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @POST
    @Path(COMPETITION_STOP)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response stopCompetition(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                             @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @POST
    @Path(SCHEDULE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response generateOrReplaceSchedule(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                       @PathParam("guid") @NotNull @Size(min = 1) String guid);
}
