package com.robot.challenge.tournament.competition.service;

import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competition.domain.result.SumoMatchDay;
import com.robot.challenge.tournament.competition.domain.result.SumoMatchDayFactory;
import com.robot.challenge.tournament.competition.repository.result.SumoMatchDayFinder;
import com.robot.challenge.tournament.competition.repository.result.SumoMatchDayRepository;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.domain.RobotFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SumoScheduleService {

    private final SumoMatchDayFactory matchDayFactory;
    private final SumoMatchDayRepository matchDayRepository;
    private final SumoMatchDayFinder matchDayFinder;
    private final RobotFactory robotFactory;

    @Inject
    public SumoScheduleService(SumoMatchDayFactory matchDayFactory, SumoMatchDayRepository matchDayRepository,
                               SumoMatchDayFinder matchDayFinder, RobotFactory robotFactory) {
        this.matchDayFactory = matchDayFactory;
        this.matchDayRepository = matchDayRepository;
        this.matchDayFinder = matchDayFinder;
        this.robotFactory = robotFactory;
    }

    @Transactional
    public List<SumoMatchDay> generateAndStoreSchedule(SumoCompetition sumoCompetition) {
        if(sumoCompetition.isStarted()) {
            return matchDayFinder.retrieveByCompetiton(sumoCompetition.getGuid());
        }

        List<SumoMatchDay> matchDays = generateSchedule(sumoCompetition);
        matchDays.forEach(matchDayRepository::create);

        return matchDays;
    }

    @Transactional
    public void deleteSchedule(SumoCompetition sumoCompetition) {
        List<SumoMatchDay> matchDays = matchDayFinder.retrieveByCompetiton(sumoCompetition.getGuid());
        matchDays.forEach(matchDay -> matchDayRepository.delete(matchDay.getId()));
    }

    private List<SumoMatchDay> generateSchedule(SumoCompetition competition) {
        List<Robot> robots = new ArrayList<>(competition.getRobots());

        Optional<Robot> maybeFakeRobot = Optional.empty();
        if( (robots.size() % 2) != 0) {
            Robot fakeRobot = robotFactory.create(robots.get(0).getType(), competition.getEdition());
            maybeFakeRobot = Optional.of(fakeRobot);
            robots.add(fakeRobot);
        }

        int matchDaysCount = robots.size() - 1;
        int matchesInMatchDay = robots.size() / 2;

        List<SumoMatchDay> firstLegMatchDays = new ArrayList<>(matchDaysCount);
        List<SumoMatchDay> secondLegMatchDays = new ArrayList<>(matchDaysCount);

        SumoMatchDay firstRound = matchDayFactory.create(0, competition);
        SumoMatchDay secondRound = matchDayFactory.create(matchDaysCount, competition);
        firstLegMatchDays.add(firstRound);
        secondLegMatchDays.add(secondRound);
        for(int matchCounter = 0; matchCounter < matchesInMatchDay; ++matchCounter) {
            Robot home = robots.get(matchCounter);
            Robot away = robots.get(matchDaysCount - matchCounter);
            firstRound.addMatch(home, away);
            secondRound.addMatch(away, home);
        }

        SumoMatchDay firstLegBaseMatchDay = firstRound;
        for(int matchDay = 0; matchDay < (matchDaysCount - 1) / 2; ++matchDay) {
            SumoMatchDay firstLegEvenRound = matchDayFactory.create(2 * matchDay + 1, competition);
            SumoMatchDay firstLegOddRound = matchDayFactory.create(2 * (matchDay + 1), competition);

            firstLegs(firstLegBaseMatchDay, firstLegEvenRound, firstLegOddRound, matchesInMatchDay);
            firstLegMatchDays.add(firstLegEvenRound);
            firstLegMatchDays.add(firstLegOddRound);

            SumoMatchDay secondLegEvenRound = matchDayFactory.create(matchDaysCount + 2 * matchDay + 1, competition);
            firstLegEvenRound.createSecondLeg(secondLegEvenRound);
            SumoMatchDay secondLegOddRound = matchDayFactory.create(matchDaysCount + 2 * (matchDay + 1), competition);
            firstLegOddRound.createSecondLeg(secondLegOddRound);

            secondLegMatchDays.add(secondLegEvenRound);
            secondLegMatchDays.add(secondLegOddRound);
            firstLegBaseMatchDay = firstLegOddRound;
        }
        List<SumoMatchDay> finalMatchDays = new ArrayList<SumoMatchDay>() {
            {
                addAll(firstLegMatchDays);
                if(competition.isSecondLeg()) {
                    addAll(secondLegMatchDays);
                }
            }
        };
        maybeFakeRobot.ifPresent(robot -> removeFakeMatches(robot, finalMatchDays));

        return finalMatchDays;
    }

    private void firstLegs(SumoMatchDay baseMatchDay, SumoMatchDay evenRound, SumoMatchDay oddRound, int matchesInMatchDay) {
        evenRound.addMatch(baseMatchDay.awayOfMatchDay(0), baseMatchDay.awayOfMatchDay(matchesInMatchDay - 1));
        for(int matchCounter = 1; matchCounter < matchesInMatchDay - 1; ++matchCounter) {
            Robot home = baseMatchDay.awayOfMatchDay(matchesInMatchDay - matchCounter - 1);
            Robot away = baseMatchDay.homeOfMatchDay(matchesInMatchDay - matchCounter);
            evenRound.addMatch(home, away);
        }
        evenRound.addMatch(baseMatchDay.homeOfMatchDay(0), baseMatchDay.homeOfMatchDay(1));

        oddRound.addMatch(evenRound.awayOfMatchDay(matchesInMatchDay - 1), evenRound.homeOfMatchDay(0));
        for(int matchCounter = 1; matchCounter < matchesInMatchDay - 1; ++matchCounter) {
            Robot home = evenRound.awayOfMatchDay(matchesInMatchDay - matchCounter - 1);
            Robot away = evenRound.homeOfMatchDay(matchesInMatchDay - matchCounter);
            oddRound.addMatch(home, away);
        }
        oddRound.addMatch(evenRound.awayOfMatchDay(0), evenRound.homeOfMatchDay(1));
    }

    private void removeFakeMatches(Robot fakeRobot, List<SumoMatchDay> matchDays) {
        matchDays.forEach(matchDay -> matchDay.removeFakeMatch(fakeRobot));
    }
}
