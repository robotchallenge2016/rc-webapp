package com.robot.challenge.tournament.competition.rest.model.sumo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competition.domain.competition.CompetitionStatus;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingSumoCompetitionModel implements GloballyUniqueModel, VersionedModel {
    @NotNull
    @Size(min = 1)
    private String name;
    private String guid;
    @NotNull
    @Size(min = 1)
    private String competitionSubtypeGuid;
    private List<String> robotGuids = new ArrayList<>();
    @NotNull
    @Min(value = 0)
    private Integer requiredNumberOfWins;
    @NotNull
    @Min(value = 0)
    private Integer bigWinThreshold;
    @NotNull
    private Integer bigWinPoints;
    @NotNull
    private Integer bigLostPoints;
    @NotNull
    private Integer smallWinPoints;
    @NotNull
    private Integer smallLostPoints;
    private CompetitionStatus status;
    private boolean secondLeg;
    @NotNull
    @Min(value = 0L)
    private Long version;

    ExistingSumoCompetitionModel() {

    }

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public String getCompetitionSubtypeGuid() {
        return competitionSubtypeGuid;
    }

    public List<String> getRobotGuids() {
        return robotGuids;
    }

    public Integer getRequiredNumberOfWins() {
        return requiredNumberOfWins;
    }

    public Integer getBigWinThreshold() {
        return bigWinThreshold;
    }

    public Integer getBigWinPoints() {
        return bigWinPoints;
    }

    public Integer getBigLostPoints() {
        return bigLostPoints;
    }

    public Integer getSmallWinPoints() {
        return smallWinPoints;
    }

    public Integer getSmallLostPoints() {
        return smallLostPoints;
    }

    public CompetitionStatus getStatus() {
        return status;
    }

    public boolean isSecondLeg() {
        return secondLeg;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingSumoCompetitionModel from(SumoCompetition competition) {
        ExistingSumoCompetitionModel model = new ExistingSumoCompetitionModel();
        model.name = competition.getName();
        model.guid = competition.getGuid().getValue();
        model.competitionSubtypeGuid = competition.getCompetitionSubtype().getGuid().getValue();
        model.robotGuids.addAll(competition.getRobots().stream().map(Robot::getGuid).map(Guid::getValue).collect(Collectors.toList()));
        model.requiredNumberOfWins = competition.getRules().getRequiredNumberOfWins();
        model.bigWinThreshold = competition.getRules().getBigWinThreshold();
        model.bigWinPoints = competition.getRules().getBigWin();
        model.bigLostPoints = competition.getRules().getBigLost();
        model.smallWinPoints = competition.getRules().getSmallWin();
        model.smallLostPoints = competition.getRules().getSmallLost();
        model.status = competition.getStatus();
        model.secondLeg = competition.isSecondLeg();
        model.version = competition.getVersion();

        return model;
    }

    public static ExistingCompetitionModelBuilder builder() {
        return new ExistingCompetitionModelBuilder();
    }

    public static class ExistingCompetitionModelBuilder {

        private final ExistingSumoCompetitionModel model = new ExistingSumoCompetitionModel();

        public ExistingCompetitionModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public ExistingCompetitionModelBuilder setType(String competitionSubtypeGuid) {
            model.competitionSubtypeGuid = competitionSubtypeGuid;
            return this;
        }

        public ExistingCompetitionModelBuilder addRobot(String guid) {
            model.robotGuids.add(guid);
            return this;
        }

        public ExistingCompetitionModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingCompetitionModelBuilder requiredNumberOfWins(Integer requiredNumberOfWins) {
            model.requiredNumberOfWins = requiredNumberOfWins;
            return this;
        }

        public ExistingCompetitionModelBuilder bigWinThreshold(Integer threshold) {
            model.bigWinThreshold = threshold;
            return this;
        }

        public ExistingCompetitionModelBuilder bigWinPoints(Integer points) {
            model.bigWinPoints = points;
            return this;
        }

        public ExistingCompetitionModelBuilder bigLosePoints(Integer points) {
            model.bigLostPoints = points;
            return this;
        }

        public ExistingCompetitionModelBuilder smallWinPoints(Integer points) {
            model.smallWinPoints = points;
            return this;
        }

        public ExistingCompetitionModelBuilder smallLosePoints(Integer points) {
            model.smallLostPoints = points;
            return this;
        }

        public ExistingCompetitionModelBuilder rematch(boolean rematch) {
            model.secondLeg = rematch;
            return this;
        }

        public ExistingSumoCompetitionModel build() {
            return model;
        }
    }    
}
