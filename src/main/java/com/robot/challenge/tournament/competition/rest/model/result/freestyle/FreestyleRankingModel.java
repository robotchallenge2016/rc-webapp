package com.robot.challenge.tournament.competition.rest.model.result.freestyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competition.domain.result.FreestyleRanking;
import com.robot.challenge.tournament.competitor.rest.model.competitor.SimpleCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FreestyleRankingModel {

    private BriefRobotModel robot;
    private SimpleCompetitorModel owner;
    private Integer points;
    private Integer firstPlaceCount;
    private Integer secondPlaceCount;
    private Integer thirdPlaceCount;

    public BriefRobotModel getRobot() {
        return robot;
    }

    public SimpleCompetitorModel getOwner() {
        return owner;
    }

    public Integer getPoints() {
        return points;
    }

    public Integer getFirstPlaceCount() {
        return firstPlaceCount;
    }

    public Integer getSecondPlaceCount() {
        return secondPlaceCount;
    }

    public Integer getThirdPlaceCount() {
        return thirdPlaceCount;
    }

    public static FreestyleRankingModel from(FreestyleRanking ranking) {
        FreestyleRankingModel model = new FreestyleRankingModel();
        model.robot = BriefRobotModel.from(ranking.getRobot());
        model.owner = SimpleCompetitorModel.from(ranking.getRobot().getOwner());
        model.points = ranking.getPoints();
        model.firstPlaceCount = ranking.getFirstPlaceCount();
        model.secondPlaceCount = ranking.getSecondPlaceCount();
        model.thirdPlaceCount = ranking.getThirdPlaceCount();

        return model;
    }
}
