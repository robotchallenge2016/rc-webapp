package com.robot.challenge.tournament.competition.rest.resource;

import com.robot.challenge.infrastructure.rest.resource.CrudResource;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.competition.rest.model.freestyle.ExistingFreestyleCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.freestyle.NewFreestyleCompetitionModel;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface FreestyleCompetitionResource extends CrudResource<NewFreestyleCompetitionModel, ExistingFreestyleCompetitionModel> {

    String DEFAULT_FREESTYLE_COMPETITION_PREFIX = "/freestyle";
    String CREATE = "/";
    String UPDATE = "/guid/{guid}";
    String DELETE = "/guid/{guid}";
    String RETRIEVE_SINGLE = "/guid/{guid}";
    String RETRIEVE_ALL = "/all";
    String RETRIEVE_BY_COMPETITOR = "/competitor/guid/{guid}";
    String COMPETITION_NAME_EXISTS = "/name/{name}/existss";
    String COMPETITION_NAME_IS_FREE = "/name/{name}/free";
    String SEARCH_ROBOT_IN_COMPETITION_MATCHING_ANY_PARAM = "/guid/{guid}/robot/match/any";
    String COMPETITION_START = "/start/guid/{guid}";
    String COMPETITION_STOP = "/stop/guid/{guid}";

    @POST
    @Path(CREATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid NewFreestyleCompetitionModel competition);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingFreestyleCompetitionModel competition);

    @javax.ws.rs.DELETE
    @Path(DELETE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response retrieveAll(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);

    @GET
    @Path(RETRIEVE_BY_COMPETITOR)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByCompetitor(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                  @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(SEARCH_ROBOT_IN_COMPETITION_MATCHING_ANY_PARAM)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response searchMatchingAnyParam(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                    @PathParam("guid") @NotNull @Size(min = 1) String competitionGuid,
                                    @QueryParam(value = "search") @NotNull @Size(min = 1) String search);

    @GET
    @Path(COMPETITION_NAME_EXISTS)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response nameExists(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                        @PathParam("name") @NotNull @Size(min = 1) String name);

    @GET
    @Path(COMPETITION_NAME_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response nameIsFree(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                        @PathParam("name") @NotNull @Size(min = 1) String name);

    @POST
    @Path(COMPETITION_START)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response startCompetition(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                              @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @POST
    @Path(COMPETITION_STOP)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response stopCompetition(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                             @PathParam("guid") @NotNull @Size(min = 1) String guid);
}
