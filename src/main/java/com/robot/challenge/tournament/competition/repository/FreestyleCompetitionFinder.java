package com.robot.challenge.tournament.competition.repository;

import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetition;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetitionFactory;

import javax.inject.Inject;

public class FreestyleCompetitionFinder extends CompetitionFinder<FreestyleCompetition> {

    @Inject
    public FreestyleCompetitionFinder(FreestyleCompetitionFactory factory) {
        super(factory);
    }
}
