package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competitor.domain.Robot;

public class SumoResultFactory extends Factory<SumoResult> {

    protected SumoResultFactory() {
        super(SumoResult.class);
    }

    public SumoResult create(SumoMatch match, Robot winner, Robot looser, SystemUser author) {
        return new SumoResult(Guid.generate(), author, match, winner, looser);
    }
}
