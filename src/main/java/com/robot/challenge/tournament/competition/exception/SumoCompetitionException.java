package com.robot.challenge.tournament.competition.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class SumoCompetitionException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        UNABLE_TO_GENERATE_SCHEDULE("Schedule cannot be generated when competition is started or finished");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public SumoCompetitionException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static SumoCompetitionException unableToGenerateSchedule() {
        return new SumoCompetitionException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.UNABLE_TO_GENERATE_SCHEDULE,
                Cause.UNABLE_TO_GENERATE_SCHEDULE.getErrorDescription());
    }
}
