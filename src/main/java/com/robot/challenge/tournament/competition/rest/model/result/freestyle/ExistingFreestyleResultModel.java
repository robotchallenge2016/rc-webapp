package com.robot.challenge.tournament.competition.rest.model.result.freestyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competition.domain.result.FreestyleResult;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingFreestyleResultModel implements GloballyUniqueModel, VersionedModel {

    private String guid;
    private String competitionGuid;
    private String robotGuid;
    @NotNull
    private FreestylePlaceModel place;
    @NotNull
    @Min(value = 0L)
    private Long version;

    ExistingFreestyleResultModel() {

    }

    @Override
    public String getGuid() {
        return guid;
    }

    public String getCompetitionGuid() {
        return competitionGuid;
    }

    public String getRobotGuid() {
        return robotGuid;
    }

    public FreestylePlaceModel getPlace() {
        return place;
    }

    @Override
    public Long getVersion() {
        return version;
    }

    public static ExistingFreestyleResultModel from(FreestyleResult result) {
        ExistingFreestyleResultModel model = new ExistingFreestyleResultModel();
        model.place = FreestylePlaceModel.from(result.getPlace());
        model.guid = result.getGuid().getValue();
        model.competitionGuid = result.getCompetition().getGuid().getValue();
        model.robotGuid = result.getRobot().getGuid().getValue();
        model.version = result.getVersion();

        return model;
    }

    public static ExistingFreestyleResultModelBuilder builder() {
        return new ExistingFreestyleResultModelBuilder();
    }

    public static class ExistingFreestyleResultModelBuilder {

        private final ExistingFreestyleResultModel model = new ExistingFreestyleResultModel();

        public ExistingFreestyleResultModelBuilder setResult(FreestylePlaceModel place) {
            model.place = place;
            return this;
        }

        public ExistingFreestyleResultModelBuilder setCompetitionGuid(String competitionGuid) {
            model.competitionGuid = competitionGuid;
            return this;
        }

        public ExistingFreestyleResultModelBuilder setRobotGuid(String robotGuid) {
            model.robotGuid = robotGuid;
            return this;
        }

        public ExistingFreestyleResultModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingFreestyleResultModelBuilder setPlace(FreestylePlaceModel place) {
            model.place = place;
            return this;
        }

        public ExistingFreestyleResultModel build() {
            return model;
        }
    }
}
