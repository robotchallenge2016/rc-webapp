package com.robot.challenge.tournament.competition.domain.competition;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Competition implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "competition_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "NAME", unique = true, nullable = false)
    private String name;
    @Enumerated(EnumType.STRING)
    @Column(name = "COMPETITION_TYPE", nullable = false)
    private CompetitionType competitionType;
    @ManyToOne
    @JoinColumn(name = "COMPETITION_SUBTYPE_ID", nullable = false)
    private CompetitionSubtype competitionSubtype;
    @ManyToMany
    @JoinTable(name = "COMPETITION_ROBOTS",
            joinColumns = @JoinColumn(name="COMPETITION_ID", referencedColumnName="ID"),
            inverseJoinColumns = @JoinColumn(name="ROBOT_ID", referencedColumnName="ID"))
    private List<Robot> robots = new ArrayList<>();
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false)
    private CompetitionStatus status;
    @ManyToOne(optional = false)
    @JoinColumn(name = "TOURNAMENT_EDITION_ID", nullable = false)
    private TournamentEdition edition;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    protected Competition() {

    }

    protected Competition(Guid guid,
                       CompetitionType competitionType,
                       CompetitionSubtype competitionSubtype,
                       TournamentEdition edition) {
        this.guid = guid;
        this.competitionType = competitionType;
        this.competitionSubtype = competitionSubtype;
        this.status = CompetitionStatus.CREATED;
        this.edition = edition;
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public Competition setName(String name) {
        this.name = name;
        return this;
    }

    public CompetitionType getCompetitionType() {
        return competitionType;
    }

    public CompetitionSubtype getCompetitionSubtype() {
        return competitionSubtype;
    }

    public Competition setCompetitionSubtype(CompetitionSubtype competitionSubtype) {
        this.competitionSubtype = competitionSubtype;
        return this;
    }

    public Competition replaceListOfRobots(List<Robot> robots) {
        this.robots.clear();
        this.robots.addAll(robots);
        return this;
    }

    public Competition addRobot(Robot robot) {
        robots.add(robot);
        return this;
    }

    public Competition removeRobot(Robot robot) {
        robots.remove(robot);
        return this;
    }

    public List<Robot> getRobots() {
        return Collections.unmodifiableList(robots);
    }

    public CompetitionStatus getStatus() {
        return status;
    }

    public boolean isStarted() {
        return this.status.equals(CompetitionStatus.STARTED);
    }

    public boolean isFinished() {
        return this.status.equals(CompetitionStatus.FINISHED);
    }

    public void start() {
        this.status = CompetitionStatus.STARTED;
    }

    public void finish() {
        this.status = CompetitionStatus.FINISHED;
    }

    public TournamentEdition getEdition() {
        return edition;
    }

    public Long getVersion() {
        return version;
    }

    public Competition setVersion(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Competition that = (Competition) o;

        if (status != that.status) return false;
        if (!id.equals(that.id)) return false;
        if (!guid.equals(that.guid)) return false;
        if (!name.equals(that.name)) return false;
        if (competitionType != that.competitionType) return false;
        if (!competitionSubtype.equals(that.competitionSubtype)) return false;
        return !(version != null ? !version.equals(that.version) : that.version != null);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + guid.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + competitionType.hashCode();
        result = 31 * result + competitionSubtype.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
