package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competition.domain.result.SumoResult;

import javax.ejb.Stateless;

@Stateless
public class SumoResultRepository extends JpaRepository<SumoResult> {

    public SumoResultRepository() {
        super(SumoResult.class);
    }
}
