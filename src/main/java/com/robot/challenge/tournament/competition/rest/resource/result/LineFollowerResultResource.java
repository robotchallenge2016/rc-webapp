package com.robot.challenge.tournament.competition.rest.resource.result;

import com.robot.challenge.infrastructure.rest.resource.CrudResource;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.ExistingLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.NewLineFollowerResultModel;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface LineFollowerResultResource extends CrudResource<NewLineFollowerResultModel, ExistingLineFollowerResultModel> {

    String DEFAULT_LINEFOLLOWER_RESULT_PREFIX = "/linefollower";
    String CREATE = "/";
    String UPDATE = "/guid/{guid}";
    String DELETE = "/guid/{guid}";
    String RETRIEVE_SINGLE = "/guid/{guid}";
    String RETRIEVE_ALL = "/all";
    String RETRIEVE_BY_COMPETITION = "/competition/guid/{guid}";
    String RETRIEVE_BY_ROBOT = "/robot/guid/{guid}";

    @POST
    @Path(CREATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid NewLineFollowerResultModel resultModel);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingLineFollowerResultModel resultModel);

    @DELETE
    @Path(DELETE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response retrieveAll(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);

    @GET
    @Path(RETRIEVE_BY_COMPETITION)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByCompetition(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                   @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_BY_ROBOT)
    @Consumes("application/json")
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByRobot(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                             @PathParam("guid") @NotNull @Size(min = 1) String robotGuid);
}
