package com.robot.challenge.tournament.competition.domain.competition.freestyle;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "FREESTYLE_COMPETITION")
public class FreestyleCompetition extends Competition {

    @Embedded
    private FreestylePoints points;

    protected FreestyleCompetition() {

    }

    protected FreestyleCompetition(Guid guid,
                                   CompetitionSubtype competitionSubtype,
                                   FreestylePoints points,
                                   TournamentEdition edition) {
        super(guid, CompetitionType.FREESTYLE, competitionSubtype, edition);
        this.points = points;
    }

    public FreestylePoints getPoints() {
        return points;
    }

    public FreestyleCompetition setPoints(FreestylePoints points) {
        this.points = points;
        return this;
    }
}
