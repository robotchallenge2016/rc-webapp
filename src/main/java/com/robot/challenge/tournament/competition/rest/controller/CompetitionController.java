package com.robot.challenge.tournament.competition.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetition;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetition;
import com.robot.challenge.tournament.competition.repository.FreestyleCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.LineFollowerCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.SumoCompetitionFinder;
import com.robot.challenge.tournament.competition.rest.model.BriefCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.ExistingCompetitionModel;
import com.robot.challenge.tournament.competition.rest.resource.CompetitionResource;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + CompetitionResource.DEFAULT_COMPETITION_PREFIX)
public class CompetitionController implements CompetitionResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private LineFollowerCompetitionFinder lfCompetitionFinder;
    @Inject
    private SumoCompetitionFinder sumoCompetitionFinder;
    @Inject
    private FreestyleCompetitionFinder fsCompetitionFinder;

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        Guid unique = Guid.from(guid);
        Optional<LineFollowerCompetition> maybeLfCompetition = lfCompetitionFinder.retrieveByGuid(unique, true);
        if(maybeLfCompetition.isPresent()) {
            return Response.status(Response.Status.OK).entity(ExistingCompetitionModel.from(maybeLfCompetition.get())).build();
        }
        Optional<SumoCompetition> maybeSumoCompetition = sumoCompetitionFinder.retrieveByGuid(unique, true);
        if(maybeSumoCompetition.isPresent()) {
            return Response.status(Response.Status.OK).entity(ExistingCompetitionModel.from(maybeSumoCompetition.get())).build();
        }
        Optional<FreestyleCompetition> maybeFsCompetition = fsCompetitionFinder.retrieveByGuid(unique, true);
        if(maybeFsCompetition.isPresent()) {
            return Response.status(Response.Status.OK).entity(ExistingCompetitionModel.from(maybeFsCompetition.get())).build();
        }
        throw DataAccessException.entityNotFound();
    }

    @Override
    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);

        List<BriefCompetitionModel> lfCompetitions = lfCompetitionFinder.retrieveAll(edition)
                .stream()
                .map(BriefCompetitionModel::from)
                .collect(Collectors.toList());

        List<BriefCompetitionModel> sumoCompetitions = sumoCompetitionFinder.retrieveAll(edition)
                .stream()
                .map(BriefCompetitionModel::from)
                .collect(Collectors.toList());

        List<BriefCompetitionModel> fsCompetitions = fsCompetitionFinder.retrieveAll(edition)
                .stream()
                .map(BriefCompetitionModel::from)
                .collect(Collectors.toList());

        lfCompetitions.addAll(sumoCompetitions);
        lfCompetitions.addAll(fsCompetitions);
        return Response.status(Response.Status.OK).entity(lfCompetitions).build();
    }

    @Override
    public Response retrieveByCompetitor(@NotNull @Size(min = 1) String editionGuid,
                                         @NotNull @Size(min = 1) String guid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);

        List<BriefCompetitionModel> lfCompetitions = lfCompetitionFinder
                .retrieveByCompetitor(Guid.from(guid), true, edition)
                .stream()
                .map(BriefCompetitionModel::from)
                .collect(Collectors.toList());

        List<BriefCompetitionModel> sumoCompetitions = sumoCompetitionFinder
                .retrieveByCompetitor(Guid.from(guid), true, edition)
                .stream()
                .map(BriefCompetitionModel::from)
                .collect(Collectors.toList());

        List<BriefCompetitionModel> fsCompetitions = fsCompetitionFinder
                .retrieveByCompetitor(Guid.from(guid), true, edition)
                .stream()
                .map(BriefCompetitionModel::from)
                .collect(Collectors.toList());

        lfCompetitions.addAll(sumoCompetitions);
        lfCompetitions.addAll(fsCompetitions);
        return Response.status(Response.Status.OK).entity(lfCompetitions).build();
    }
}
