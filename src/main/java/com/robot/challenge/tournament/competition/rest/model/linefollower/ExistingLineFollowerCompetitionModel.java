package com.robot.challenge.tournament.competition.rest.model.linefollower;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competition.domain.competition.CompetitionStatus;
import com.robot.challenge.tournament.competition.domain.competition.LineFollowerCompetition;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingLineFollowerCompetitionModel implements GloballyUniqueModel, VersionedModel {
    @NotNull
    @Size(min = 1)
    private String name;
    private String guid;
    @NotNull
    @Size(min = 1)
    private String competitionSubtypeGuid;
    @Min(value = 1)
    @NotNull
    private Integer maxNumberOfRuns;
    private List<String> robotGuids = new ArrayList<>();
    private CompetitionStatus status;
    @NotNull
    @Min(value = 0L)
    private Long version;

    ExistingLineFollowerCompetitionModel() {

    }

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public String getCompetitionSubtypeGuid() {
        return competitionSubtypeGuid;
    }

    public Integer getMaxNumberOfRuns() {
        return maxNumberOfRuns;
    }

    public List<String> getRobotGuids() {
        return robotGuids;
    }

    public CompetitionStatus getStatus() {
        return status;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingLineFollowerCompetitionModel from(LineFollowerCompetition competition) {
        ExistingLineFollowerCompetitionModel model = new ExistingLineFollowerCompetitionModel();
        model.name = competition.getName();
        model.guid = competition.getGuid().getValue();
        model.competitionSubtypeGuid = competition.getCompetitionSubtype().getGuid().getValue();
        model.robotGuids.addAll(competition.getRobots().stream().map(Robot::getGuid).map(Guid::getValue).collect(Collectors.toList()));
        model.maxNumberOfRuns = competition.getMaxNumberOfRuns();
        model.status = competition.getStatus();
        model.version = competition.getVersion();

        return model;
    }

    public static ExistingCompetitionModelBuilder builder() {
        return new ExistingCompetitionModelBuilder();
    }

    public static class ExistingCompetitionModelBuilder {

        private final ExistingLineFollowerCompetitionModel model = new ExistingLineFollowerCompetitionModel();

        public ExistingCompetitionModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public ExistingCompetitionModelBuilder setType(String competitionSubtypeGuid) {
            model.competitionSubtypeGuid = competitionSubtypeGuid;
            return this;
        }

        public ExistingCompetitionModelBuilder setMaxNumberOfRuns(Integer maxNumberOfRuns) {
            model.maxNumberOfRuns = maxNumberOfRuns;
            return this;
        }

        public ExistingCompetitionModelBuilder addRobot(String guid) {
            model.robotGuids.add(guid);
            return this;
        }

        public ExistingCompetitionModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingLineFollowerCompetitionModel build() {
            return model;
        }
    }    
}
