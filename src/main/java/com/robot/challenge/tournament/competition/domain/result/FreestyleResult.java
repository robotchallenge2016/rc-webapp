package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestyleCompetition;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePlace;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "FREESTYLE_RESULT", indexes = { @Index(name = "freestyle_result_idx", columnList = "guid") })
public class FreestyleResult implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "freestyle_result_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @ManyToOne(optional = false)
    @JoinColumn(name = "COMPETITION_ID", nullable = false, updatable = false)
    private FreestyleCompetition competition;
    @ManyToOne(optional = false)
    @JoinColumn(name = "ROBOT_ID", nullable = false, updatable = false)
    private Robot robot;
    @ManyToOne(optional = false)
    @JoinColumn(name = "AUTHOR_ID", nullable = false, updatable = false)
    private SystemUser author;
    @Enumerated(EnumType.STRING)
    @Column(name = "PLACE", nullable = false)
    private FreestylePlace place;
    @ManyToOne
    @JoinColumn(name = "EDITOR_ID")
    private SystemUser editor;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE_TIME", nullable = false, updatable = false)
    private Date createDateTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE_TIME")
    private Date updateDateTime;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;


    private FreestyleResult() {

    }

    FreestyleResult(Guid guid, SystemUser author, FreestyleCompetition competition, Robot robot, FreestylePlace place) {
        this.guid = guid;
        this.competition = competition;
        this.robot = robot;
        this.author = author;
        this.place = place;
        this.createDateTime = Date.from(ZonedDateTime.now(ZoneId.of("UTC")).toInstant());
    }

    public Long getId() {
        return id;
    }

    @Override
    public Guid getGuid() {
        return guid;
    }

    public Robot getRobot() {
        return robot;
    }

    public SystemUser getAuthor() {
        return author;
    }

    public FreestyleCompetition getCompetition() {
        return competition;
    }

    public Optional<SystemUser> getEditor() {
        return Optional.ofNullable(editor);
    }

    public FreestyleResult setEditor(SystemUser editor) {
        this.editor = editor;
        return this;
    }

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    public FreestyleResult setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = Date.from(updateDateTime.toInstant());
        return this;
    }

    public FreestylePlace getPlace() {
        return place;
    }

    public FreestyleResult setPlace(FreestylePlace place) {
        this.place = place;
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public FreestyleResult setVersion(Long version) {
        this.version = version;
        return this;
    }
}
