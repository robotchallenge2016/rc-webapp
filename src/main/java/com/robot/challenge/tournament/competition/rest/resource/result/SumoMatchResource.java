package com.robot.challenge.tournament.competition.rest.resource.result;

import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.competition.rest.model.result.sumo.ExistingSumoMatchModel;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

public interface SumoMatchResource {

    String DEFAULT_SUMO_RESULT_PREFIX = "/sumo";
    String UPDATE = "/guid/{guid}";
    String RETRIEVE_BY_GUID = "/guid/{guid}";
    String RETRIEVE_BY_COMPETITION = "/competition/guid/{guid}";
    String RETRIEVE_BY_ROBOT = "/robot/guid/{guid}";

    @POST
    @Path(UPDATE)
    @Consumes("application/json")
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingSumoMatchModel model);

    @GET
    @Path(RETRIEVE_BY_GUID)
    @Consumes("application/json")
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_BY_COMPETITION)
    @Consumes("application/json")
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response retrieveAllByCompetition(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                      @PathParam("guid") @NotNull @Size(min = 1) String competitionGuid);

    @GET
    @Path(RETRIEVE_BY_ROBOT)
    @Consumes("application/json")
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByRobot(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                             @PathParam("guid") @NotNull @Size(min = 1) String robotGuid);
}
