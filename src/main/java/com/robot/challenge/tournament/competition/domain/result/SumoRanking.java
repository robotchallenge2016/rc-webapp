package com.robot.challenge.tournament.competition.domain.result;

import com.robot.challenge.tournament.competition.domain.competition.SumoRules;
import com.robot.challenge.tournament.competitor.domain.Robot;

public class SumoRanking implements Comparable<SumoRanking> {

    private Robot robot;
    private int fightsWon;
    private int played;
    private int win;
    private int lost;
    private int points;
    private SumoRules sumoRules;

    public SumoRanking(SumoRules sumoRules, Robot robot) {
        this.robot = robot;
        this.sumoRules = sumoRules;
    }

    public Robot getRobot() {
        return robot;
    }

    public int getFightsWon() {
        return fightsWon;
    }

    public int getPlayed() {
        return played;
    }

    public Integer getWin() {
        return win;
    }

    public Integer getLost() {
        return lost;
    }

    public Integer getPoints() {
        return points;
    }

    public void addMatchResult(SumoMatch match) {
        ++played;
        if(match.getWinner(sumoRules).get().equals(robot)) {
            addWin(match);
        } else {
            addLose(match);
        }
    }

    private void addWin(SumoMatch result) {
        ++win;
        fightsWon += result.numberOfWinnerWonFights(sumoRules);
        if(isBigWin(result.numberOfLoserWonFights(sumoRules))) {
            points += sumoRules.getBigWin();
        } else {
            points += sumoRules.getSmallWin();
        }
    }

    private void addLose(SumoMatch result) {
        ++lost;
        fightsWon += result.numberOfLoserWonFights(sumoRules);
        if(isBigWin(result.numberOfLoserWonFights(sumoRules))) {
            points += sumoRules.getBigLost();
        }
        if(isSmallWin(result.numberOfLoserWonFights(sumoRules))) {
            points += sumoRules.getSmallLost();
        }
    }

    private boolean isBigWin(int looserWins) {
        if(looserWins < sumoRules.getBigWinThreshold()) {
            return true;
        }
        return false;
    }

    private boolean isSmallWin(int looserWins) {
        if(looserWins >= sumoRules.getBigWinThreshold()) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(SumoRanking o) {
        if(win > o.win) {
            return 1;
        } else if(win < o.win) {
            return -1;
        } else {
            if(points > o.points) {
                return 1;
            } else if(points < o.points) {
                return -1;
            } else {
                if(fightsWon > o.fightsWon) {
                    return 1;
                } else if(fightsWon < o.fightsWon) {
                    return -1;
                } else {
                    if(lost < o.lost) {
                        return 1;
                    } else if(lost > o.lost) {
                        return -1;
                    }
                    return 0;
                }
            }
        }
    }
}
