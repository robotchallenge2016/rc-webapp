package com.robot.challenge.tournament.competition.repository.result;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competition.domain.result.LineFollowerResult;

import javax.ejb.Stateless;

@Stateless
public class LineFollowerResultRepository extends JpaRepository<LineFollowerResult> {

    public LineFollowerResultRepository() {
        super(LineFollowerResult.class);
    }
}
