package com.robot.challenge.tournament.competition.domain.competition;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SUMO_COMPETITION")
public class SumoCompetition extends Competition {

    @Embedded
    private SumoRules points;
    @Column(name = "SECOND_LEG", nullable = false)
    private boolean secondLeg;

    protected SumoCompetition() {

    }

    protected SumoCompetition(Guid guid,
                              CompetitionSubtype competitionSubtype,
                              SumoRules points,
                              TournamentEdition edition) {
        super(guid, CompetitionType.SUMO, competitionSubtype, edition);
        this.points = points;
    }

    public SumoRules getRules() {
        return points;
    }

    public SumoCompetition setPoints(SumoRules sumoRules) {
        this.points = sumoRules;
        return this;
    }

    public boolean isSecondLeg() {
        return secondLeg;
    }

    public SumoCompetition setSecondLeg(boolean rematch) {
        this.secondLeg = rematch;
        return this;
    }
}
