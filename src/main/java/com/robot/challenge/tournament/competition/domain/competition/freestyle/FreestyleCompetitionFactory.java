package com.robot.challenge.tournament.competition.domain.competition.freestyle;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.repository.TournamentDictionary;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.inject.Inject;

public class FreestyleCompetitionFactory extends Factory<FreestyleCompetition> {

    private final TournamentDictionary tournamentDictionary;

    @Inject
    public FreestyleCompetitionFactory(TournamentDictionary tournamentDictionary) {
        super(FreestyleCompetition.class);
        this.tournamentDictionary = tournamentDictionary;
    }

    public FreestyleCompetition create(Guid competitionSubtypeGuid,
                                       FreestylePoints points,
                                       TournamentEdition edition) {
        CompetitionSubtype competitionSubtype = tournamentDictionary
                .retrieveCompetitionSubtypeByGuidAndCompetitionType(competitionSubtypeGuid, CompetitionType.FREESTYLE)
                .orElseThrow(DataAccessException::entityNotFound);
        return new FreestyleCompetition(Guid.generate(), competitionSubtype, points, edition);
    }
}
