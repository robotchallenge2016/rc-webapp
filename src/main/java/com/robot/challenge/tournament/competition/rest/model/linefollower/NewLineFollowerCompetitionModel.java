package com.robot.challenge.tournament.competition.rest.model.linefollower;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewLineFollowerCompetitionModel {

    @NotNull
    @Size(min = 1)
    private String name;
    @NotNull
    @Size(min = 1)
    private String competitionSubtypeGuid;
    @Min(value = 1)
    @NotNull
    private Integer maxNumberOfRuns;
    private List<String> robotGuids = new ArrayList<>();

    public String getName() {
        return name;
    }

    public String getCompetitionSubtypeGuid() {
        return competitionSubtypeGuid;
    }

    public Integer getMaxNumberOfRuns() {
        return maxNumberOfRuns;
    }

    public List<String> getRobotGuids() {
        return robotGuids;
    }

    public static NewCompetitionModelBuilder builder() {
        return new NewCompetitionModelBuilder();
    }

    public static class NewCompetitionModelBuilder {

        private final NewLineFollowerCompetitionModel model = new NewLineFollowerCompetitionModel();

        public NewCompetitionModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public NewCompetitionModelBuilder setCompetitionTypeGuid(String competitionTypeGuid) {
            model.competitionSubtypeGuid = competitionTypeGuid;
            return this;
        }

        public NewCompetitionModelBuilder setMaxNumberOfRuns(Integer maxNumberOfRuns) {
            model.maxNumberOfRuns = maxNumberOfRuns;
            return this;
        }

        public NewCompetitionModelBuilder addRobot(String guid) {
            model.robotGuids.add(guid);
            return this;
        }

        public NewLineFollowerCompetitionModel build() {
            return model;
        }
    }
}
