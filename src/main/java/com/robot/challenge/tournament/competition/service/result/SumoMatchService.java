package com.robot.challenge.tournament.competition.service.result;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.system.authn.service.IntrospectionService;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competition.domain.competition.SumoRules;
import com.robot.challenge.tournament.competition.domain.result.SumoMatch;
import com.robot.challenge.tournament.competition.exception.CompetitionException;
import com.robot.challenge.tournament.competition.repository.result.SumoMatchFinder;
import com.robot.challenge.tournament.competition.repository.result.SumoMatchRepository;
import com.robot.challenge.tournament.competition.rest.model.result.sumo.ExistingSumoMatchModel;
import com.robot.challenge.tournament.competition.rest.model.result.sumo.SumoResultModel;
import com.robot.challenge.tournament.competition.service.result.exception.SumoMatchException;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

public class SumoMatchService {

    private final SumoMatchRepository matchRepository;
    private final SumoMatchFinder matchFinder;
    private final RobotFinder robotFinder;
    private final IntrospectionService introspectionService;;

    @Inject
    public SumoMatchService(SumoMatchRepository matchRepository, SumoMatchFinder matchFinder,
                            RobotFinder robotFinder, IntrospectionService introspectionService) {
        this.matchRepository = matchRepository;
        this.matchFinder = matchFinder;
        this.robotFinder = robotFinder;
        this.introspectionService = introspectionService;
    }

    private void checkIfCompetitionIsStarted(Competition competition) {
        if(competition.isFinished()) {
            throw CompetitionException.competitionFinished();
        }
        if(!competition.isStarted()) {
            throw CompetitionException.competitionNotStartedYet();
        }
    }

    private void validateMatchResults(SumoMatch match) {
        SumoCompetition competition = match.getMatchDay().getCompetition();
        SumoRules rules = competition.getRules();

        if(!match.hasRequiredNumberOfWins(rules)) {
            throw SumoMatchException.notEnoughResults();
        }
        if(match.numberOfWinnerWonFights(rules).equals(match.numberOfLoserWonFights(rules))) {
            throw SumoMatchException.theSameNumberOfMatches();
        }
    }

    @Transactional
    public void updateMatch(Guid guid, ExistingSumoMatchModel model, TournamentEdition edition) {
        SystemUser author = introspectionService.introspection(edition.getTournament())
                .orElseThrow(DataAccessException::entityNotFound);

        SumoMatch match = matchFinder.retrieveByGuid(guid).orElseThrow(DataAccessException::entityNotFound);
        checkIfCompetitionIsStarted(match.getMatchDay().getCompetition());

        match.removeAllFightResults();

        for(SumoResultModel resultModel : model.getResults()) {
            Robot winner = robotFinder.retrieveByGuid(Guid.from(resultModel.getWinnerGuid())).orElseThrow(DataAccessException::entityNotFound);
            Robot looser = robotFinder.retrieveByGuid(Guid.from(resultModel.getLooserGuid())).orElseThrow(DataAccessException::entityNotFound);

            match.addFightResult(winner, looser, author);
        }

        validateMatchResults(match);
        matchRepository.update(match);
    }

    @Transactional
    public void delete(List<Guid> guids) {
        guids.forEach(guid -> matchFinder.retrieveByGuid(guid)
                .map(entity -> matchRepository.delete(entity.getId()))
                .orElseThrow(DataAccessException::entityNotFound));
    }
}
