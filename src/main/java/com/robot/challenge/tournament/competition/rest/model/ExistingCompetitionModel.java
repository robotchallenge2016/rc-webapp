package com.robot.challenge.tournament.competition.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingCompetitionModel implements GloballyUniqueModel, VersionedModel {
    @NotNull
    @Size(min = 1)
    private String name;
    private String guid;
    @NotNull
    @Size(min = 1)
    private String competitionSubtypeGuid;
    private List<String> robotGuids = new ArrayList<>();
    private boolean started;
    @NotNull
    @Min(value = 0L)
    private Long version;

    ExistingCompetitionModel() {

    }

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public String getCompetitionSubtypeGuid() {
        return competitionSubtypeGuid;
    }

    public List<String> getRobotGuids() {
        return robotGuids;
    }

    public boolean isStarted() {
        return started;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingCompetitionModel from(Competition competition) {
        ExistingCompetitionModel model = new ExistingCompetitionModel();
        model.name = competition.getName();
        model.guid = competition.getGuid().getValue();
        model.competitionSubtypeGuid = competition.getCompetitionSubtype().getGuid().getValue();
        model.robotGuids.addAll(competition.getRobots().stream().map(Robot::getGuid).map(Guid::getValue).collect(Collectors.toList()));
        model.started = competition.isStarted();
        model.version = competition.getVersion();

        return model;
    }

    public static ExistingCompetitionModelBuilder builder() {
        return new ExistingCompetitionModelBuilder();
    }

    public static class ExistingCompetitionModelBuilder {

        private final ExistingCompetitionModel model = new ExistingCompetitionModel();

        public ExistingCompetitionModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public ExistingCompetitionModelBuilder setType(String competitionSubtypeGuid) {
            model.competitionSubtypeGuid = competitionSubtypeGuid;
            return this;
        }

        public ExistingCompetitionModelBuilder addRobot(String guid) {
            model.robotGuids.add(guid);
            return this;
        }

        public ExistingCompetitionModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingCompetitionModel build() {
            return model;
        }
    }    
}
