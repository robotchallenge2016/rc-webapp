package com.robot.challenge.tournament.competition.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.domain.competition.SumoCompetition;
import com.robot.challenge.tournament.competition.exception.SumoCompetitionException;
import com.robot.challenge.tournament.competition.repository.SumoCompetitionFinder;
import com.robot.challenge.tournament.competition.rest.model.sumo.BriefSumoCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.sumo.ExistingSumoCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.sumo.NewSumoCompetitionModel;
import com.robot.challenge.tournament.competition.rest.resource.CompetitionResource;
import com.robot.challenge.tournament.competition.rest.resource.SumoCompetitionResource;
import com.robot.challenge.tournament.competition.service.SumoCompetitionService;
import com.robot.challenge.tournament.competition.service.SumoScheduleService;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + CompetitionResource.DEFAULT_COMPETITION_PREFIX + SumoCompetitionResource.DEFAULT_SUMO_COMPETITION_PREFIX)
public class SumoCompetitionController implements SumoCompetitionResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private SumoCompetitionService competitionService;
    @Inject
    private SumoCompetitionFinder competitionFinder;
    @Inject
    private SumoScheduleService scheduleGenerator;

    @Override
    public Response create(@NotNull @Size(min = 1) String editionGuid,
                           @Valid NewSumoCompetitionModel competition) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        SumoCompetition createdCompetition = competitionService.create(competition, edition);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdCompetition)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid,
                           @Valid ExistingSumoCompetitionModel competition) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        competitionService.update(Guid.from(guid), competition, edition);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid) {
        competitionService.delete(Guid.from(guid));
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        ExistingSumoCompetitionModel result = ExistingSumoCompetitionModel
                .from(competitionFinder.retrieveByGuid(Guid.from(guid), true)
                        .orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefSumoCompetitionModel> result = competitionFinder.retrieveAll(edition)
                .stream()
                .map(BriefSumoCompetitionModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByCompetitor(@NotNull @Size(min = 1) String editionGuid,
                                         @NotNull @Size(min = 1) String guid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefSumoCompetitionModel> result = competitionFinder
                .retrieveByCompetitor(Guid.from(guid), true, edition)
                .stream()
                .map(BriefSumoCompetitionModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response nameExists(@NotNull @Size(min = 1) String editionGuid,
                               @NotNull @Size(min = 1) String name) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        return competitionFinder.retrieveByName(name, edition)
                .map(r -> Response.status(Response.Status.OK))
                .orElse(Response.status(Response.Status.BAD_REQUEST))
                .build();
    }

    @Override
    public Response nameIsFree(@NotNull @Size(min = 1) String editionGuid,
                               @NotNull @Size(min = 1) String name) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        return competitionFinder.retrieveByName(name, edition)
                .map(r -> Response.status(Response.Status.BAD_REQUEST))
                .orElse(Response.status(Response.Status.OK))
                .build();
    }

    @Override
    public Response startCompetition(@NotNull @Size(min = 1) String editionGuid,
                                     @NotNull @Size(min = 1) String guid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        competitionService.startCompetition(Guid.from(guid), edition);
        return Response.ok().build();
    }

    @Override
    public Response stopCompetition(@NotNull @Size(min = 1) String editionGuid,
                                    @NotNull @Size(min = 1) String guid) {
        competitionService.stopCompetition(Guid.from(guid));
        return Response.ok().build();
    }

    @Override
    @Transactional
    public Response generateOrReplaceSchedule(@NotNull @Size(min = 1) String editionGuid,
                                              @NotNull @Size(min = 1) String guid) {
        SumoCompetition competition = competitionFinder.retrieveByGuid(Guid.from(guid)).orElseThrow(DataAccessException::entityNotFound);
        if(competition.isStarted() || competition.isFinished()) {
            throw SumoCompetitionException.unableToGenerateSchedule();
        }
        scheduleGenerator.deleteSchedule(competition);
        scheduleGenerator.generateAndStoreSchedule(competition);
        return Response.ok().build();
    }
}
