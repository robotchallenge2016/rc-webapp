package com.robot.challenge.tournament.competition.rest.model.result.sumo;


import com.robot.challenge.tournament.competition.domain.result.SumoMatch;
import com.robot.challenge.tournament.competition.rest.model.sumo.BriefSumoCompetitionModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;

import java.util.List;
import java.util.stream.Collectors;

public class ExistingSumoMatchModel {

    private String guid;
    private BriefSumoCompetitionModel competition;
    private BriefRobotModel home;
    private BriefRobotModel away;
    private List<SumoResultModel> results;

    public String getGuid() {
        return guid;
    }

    public BriefSumoCompetitionModel getCompetition() {
        return competition;
    }

    public BriefRobotModel getHome() {
        return home;
    }

    public BriefRobotModel getAway() {
        return away;
    }

    public void setResults(List<SumoResultModel> results) {
        this.results = results;
    }

    public List<SumoResultModel> getResults() {
        return results;
    }

    public static ExistingSumoMatchModel from(SumoMatch match) {
        ExistingSumoMatchModel model = new ExistingSumoMatchModel();
        model.guid = match.getGuid().getValue();
        model.competition = BriefSumoCompetitionModel.from(match.getMatchDay().getCompetition());
        model.home = BriefRobotModel.from(match.getHomeRobot());
        model.away = BriefRobotModel.from(match.getAwayRobot());
        model.results = match.getResults().stream().map(SumoResultModel::from).collect(Collectors.toList());

        return model;
    }
}
