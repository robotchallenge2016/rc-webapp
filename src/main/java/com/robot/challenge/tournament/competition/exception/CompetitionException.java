package com.robot.challenge.tournament.competition.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class CompetitionException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        CANT_START_TOURNAMENT_NOT_OPENED("Cant start competition when tournament is closed"),
        NAME_ALREADY_EXISTS("Competition name already exists"),
        COMPETITION_NOT_STARTED("Competition has not been started yet"),
        COMPETITION_FINISHED("Competition has been finished");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public CompetitionException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static CompetitionException tournamentNotOpened() {
        return new CompetitionException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.CANT_START_TOURNAMENT_NOT_OPENED,
                Cause.CANT_START_TOURNAMENT_NOT_OPENED.getErrorDescription());
    }

    public static CompetitionException nameAlreadyExists() {
        return new CompetitionException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.NAME_ALREADY_EXISTS,
                Cause.NAME_ALREADY_EXISTS.getErrorDescription());
    }

    public static CompetitionException competitionNotStartedYet() {
        return new CompetitionException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.COMPETITION_NOT_STARTED,
                Cause.COMPETITION_NOT_STARTED.getErrorDescription());
    }

    public static CompetitionException competitionFinished() {
        return new CompetitionException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.COMPETITION_FINISHED,
                Cause.COMPETITION_FINISHED.getErrorDescription());
    }
}
