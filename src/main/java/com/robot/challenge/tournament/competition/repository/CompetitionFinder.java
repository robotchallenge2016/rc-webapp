package com.robot.challenge.tournament.competition.repository;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.competition.domain.competition.Competition;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.dictionary.domain.RobotSubType;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.domain.TournamentEdition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

public class CompetitionFinder<T extends Competition> extends JpaFinder<T> {
    private static final Logger LOG = LoggerFactory.getLogger(CompetitionFinder.class);

    protected CompetitionFinder(Factory<T> clazz) {
        super(clazz);
    }

    @Override
    public Optional<T> retrieveByGuid(@NotNull Guid guid) {
        return this.retrieveByGuid(guid, false);
    }

    public Optional<T> retrieveByGuid(@NotNull Guid guid, boolean fetchRobots) {
        LOG.info("Retrieving {} by guid", target());
        CriteriaQuery<T> q = criteriaBuilder().createQuery(target());
        Root<T> competition = q.from(target());

        competition.fetch("competitionSubtype", JoinType.LEFT);

        if(fetchRobots) {
            competition.fetch("robots", JoinType.LEFT);
        }

        q.where(criteriaBuilder().equal(competition.get("guid"), guid));

        q.select(competition);
        try {
            return Optional.of(entityManager().createQuery(q).getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

    public Optional<T> retrieveByName(String name, TournamentEdition edition) {
        LOG.info("Retrieving {} by name", target().getName());
        return single(entityManager().createQuery(
                "SELECT u FROM " +
                        target().getName() +
                        " u WHERE u.name = :name AND u.edition = :edition", target())
                .setParameter("name", name)
                .setParameter("edition", edition));
    }

    public List<T> retrieveByCompetitor(@NotNull Guid guid, boolean fetchRobots, TournamentEdition edition) {
        LOG.info("Retrieving {} by guid", target().getName());
        CriteriaQuery<T> q = criteriaBuilder().createQuery(target());
        Root<T> competition = q.from(target());

        competition.join("competitionSubtype", JoinType.LEFT);
        Join<Competition, Robot> join = competition.join("robots", JoinType.LEFT);

        if(fetchRobots) {
            competition.fetch("robots", JoinType.LEFT);
        }

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(join.get("owner").get("systemUser").get("guid"), guid),
                criteriaBuilder().equal(competition.get("edition"), edition)
        ));

        q.distinct(true);
        q.select(competition);

        return list(entityManager().createQuery(q));
    }

    public Optional<T> retrieveByRobot(@NotNull Guid guid) {
        return retrieveByRobot(guid, false);
    }

    public Optional<T> retrieveByRobot(@NotNull Guid guid, boolean fetchRobots) {
        LOG.info("Retrieving {} by guid", target().getName());
        CriteriaQuery<T> q = criteriaBuilder().createQuery(target());
        Root<T> competition = q.from(target());

        competition.join("competitionSubtype", JoinType.LEFT);
        Join<Competition, Robot> join = competition.join("robots", JoinType.LEFT);

        if(fetchRobots) {
            competition.fetch("robots", JoinType.LEFT);
        }

        q.where(criteriaBuilder().equal(join.get("guid"), guid));

        q.distinct(true);
        q.select(competition);

        try {
            return Optional.of(entityManager().createQuery(q).getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

    public Optional<T> retrieveByRobotSubType(@NotNull RobotSubType robotSubType,
                                              TournamentEdition edition) {
        return retrieveByRobotSubType(robotSubType, false, edition);
    }

    public Optional<T> retrieveByRobotSubType(@NotNull RobotSubType robotSubType,
                                              boolean fetchRobots,
                                              TournamentEdition edition) {
        LOG.info("Retrieving {} by RobotType", target().getName());
        CriteriaQuery<T> q = criteriaBuilder().createQuery(target());
        Root<T> competition = q.from(target());

        competition.fetch("competitionSubtype", JoinType.LEFT).fetch("robotSubType");
        if(fetchRobots) {
            competition.fetch("robots", JoinType.LEFT);
        }

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competition.get("competitionSubtype").get("robotSubType"), robotSubType),
                criteriaBuilder().equal(competition.get("edition"), edition)
        ));

        q.distinct(true);
        q.select(competition);

        return single(entityManager().createQuery(q));
    }

    public List<T> retrieveAll(TournamentEdition edition) {
        LOG.info("Retrieving all {}", target().getName());
        return list(entityManager().createQuery(
                "SELECT u FROM " +
                        target().getName() +
                        " u WHERE u.edition = :edition" +
                        " ORDER BY u.name", target())
                .setParameter("edition", edition));
    }

    public Optional<T> retrieveRobotsInCompetitionMatchingAnyParam(@NotNull Guid guid,
                                                                   @NotNull @Size(min = 1) String search,
                                                                   TournamentEdition edition) {
        LOG.info("Retrieving Robots in Competition matching any param", target().getName());
        CriteriaQuery<T> q = criteriaBuilder().createQuery(target());
        Root<T> competition = q.from(target());

        competition.join("competitionSubtype", JoinType.LEFT);
        Join<Competition, Robot> join = competition.join("robots", JoinType.LEFT);

        competition.fetch("robots", JoinType.LEFT);

        String param = ("%" + search + "%").toLowerCase();
        q.where(criteriaBuilder().and(
                criteriaBuilder().like(criteriaBuilder().lower(join.get("name")), param),
                criteriaBuilder().equal(competition.get("guid"), guid),
                criteriaBuilder().equal(competition.get("edition"), edition)
        ));

        q.distinct(true);
        q.select(competition);

        return single(entityManager().createQuery(q));
    }
}
