package com.robot.challenge.tournament.competition.domain.competition;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LINEFOLLOWER_COMPETITION")
public class LineFollowerCompetition extends Competition {

    @Column(name = "MAX_NUMBER_OF_RUNS", nullable = false)
    private Integer maxNumberOfRuns;

    protected LineFollowerCompetition() {

    }

    protected LineFollowerCompetition(Guid guid,
                                      CompetitionSubtype competitionSubtype,
                                      TournamentEdition edition) {
        super(guid, CompetitionType.LINEFOLLOWER, competitionSubtype, edition);
    }

    public Integer getMaxNumberOfRuns() {
        return maxNumberOfRuns;
    }

    public LineFollowerCompetition setMaxNumberOfRuns(Integer maxNumberOfRuns) {
        this.maxNumberOfRuns = maxNumberOfRuns;
        return this;
    }
}
