package com.robot.challenge.tournament.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class TournamentException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        TOURNAMENT_OWNERS_LIST_IS_EMPTY("List of tournament owners is empty");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    private TournamentException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static TournamentException tournamentOwnersListIsEmpty() {
        return new TournamentException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.TOURNAMENT_OWNERS_LIST_IS_EMPTY,
                Cause.TOURNAMENT_OWNERS_LIST_IS_EMPTY.getErrorDescription());
    }
}
