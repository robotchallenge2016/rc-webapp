package com.robot.challenge.tournament.registration.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competition.repository.FreestyleCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.LineFollowerCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.SumoCompetitionFinder;
import com.robot.challenge.tournament.competition.service.FreestyleCompetitionService;
import com.robot.challenge.tournament.competition.service.LineFollowerCompetitionService;
import com.robot.challenge.tournament.competition.service.SumoCompetitionService;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.repository.CompetitorRepository;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.competitor.repository.TeamFinder;
import com.robot.challenge.tournament.competitor.service.RobotService;
import com.robot.challenge.tournament.dictionary.domain.RobotSubType;
import com.robot.challenge.tournament.dictionary.domain.RobotType;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.registration.exception.ReceptionException;
import com.robot.challenge.tournament.registration.rest.model.CompetitorReceptionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

public class ReceptionService {

    private static final Logger LOG = LoggerFactory.getLogger(ReceptionService.class);

    private final CompetitorRepository competitorRepository;
    private final CompetitorFinder competitorFinder;
    private final TeamFinder teamFinder;
    private final RobotService robotService;
    private final RobotFinder robotFinder;

    private final LineFollowerCompetitionFinder lineFollowerCompetitionFinder;
    private final SumoCompetitionFinder sumoCompetitionFinder;
    private final FreestyleCompetitionFinder freestyleCompetitionFinder;

    private final LineFollowerCompetitionService lineFollowerCompetition;
    private final SumoCompetitionService sumoCompetition;
    private final FreestyleCompetitionService freestyleCompetition;

    @Inject
    public ReceptionService(CompetitorRepository competitorRepository, CompetitorFinder competitorFinder, TeamFinder teamFinder,
                            RobotService robotService, RobotFinder robotFinder,
                            LineFollowerCompetitionFinder lineFollowerCompetitionFinder,
                            SumoCompetitionFinder sumoCompetitionFinder,
                            FreestyleCompetitionFinder freestyleCompetitionFinder,
                            LineFollowerCompetitionService lineFollowerCompetition,
                            SumoCompetitionService sumoCompetition,
                            FreestyleCompetitionService freestyleCompetition) {

        this.competitorRepository = competitorRepository;
        this.competitorFinder = competitorFinder;
        this.teamFinder = teamFinder;
        this.robotService = robotService;
        this.robotFinder = robotFinder;
        this.lineFollowerCompetitionFinder = lineFollowerCompetitionFinder;
        this.sumoCompetitionFinder = sumoCompetitionFinder;
        this.freestyleCompetitionFinder = freestyleCompetitionFinder;
        this.lineFollowerCompetition = lineFollowerCompetition;
        this.sumoCompetition = sumoCompetition;
        this.freestyleCompetition = freestyleCompetition;
    }

    @Transactional
    public void reception(CompetitorReceptionModel model, TournamentEdition edition) {
        if(edition.isTournamentStarted()) {
            throw ReceptionException.tournamentAlreadyStarted();
        }

        List<Robot> robots = model.getTeamGuid()
                .map(teamGuid -> receptionOfTeam(model, edition))
                .orElseGet(() -> receptionOfCompetitor(model, edition));
        assignRobotsToCompetitions(model, robots, edition);
    }

    private List<Robot> receptionOfTeam(CompetitorReceptionModel model, TournamentEdition edition) {
        LOG.info("Confirming {} [guid={}]", Team.class.getCanonicalName(), model.getTeamGuid());
        Guid teamGuid = Guid.from(model.getTeamGuid().get());
        Team team = teamFinder.retrieveByGuid(teamGuid, true)
                .orElseThrow(() -> DataAccessException.entityNotFound(Competitor.class, teamGuid));

        team.getMembers().forEach(member -> {
            member.confirm();
            competitorRepository.update(member);
        });

        return robotFinder.retrieveByTeam(teamGuid, edition);
    }

    private List<Robot> receptionOfCompetitor(CompetitorReceptionModel model, TournamentEdition edition) {
        LOG.info("Confirming {} [guid={}]", Competitor.class.getCanonicalName(), model.getCompetitorGuid());
        Guid competitorGuid = Guid.from(model.getCompetitorGuid());
        Competitor competitor = competitorFinder.retrieveByGuid(competitorGuid)
                .orElseThrow(() -> DataAccessException.entityNotFound(Competitor.class, competitorGuid));

        if(competitor.getTeam().isPresent()) {
            throw new RuntimeException("Cannot confirm Competitor which is team member");
        }

        competitor.confirm();
        competitorRepository.update(competitor);

        return robotFinder.retrieveByCompetitor(competitorGuid, edition);
    }

    private void assignRobotsToCompetitions(CompetitorReceptionModel model, List<Robot> robots, TournamentEdition edition) {
        for(Robot robot: robots) {
            boolean addRobotToCompetition = model.getRobotGuids().contains(robot.getGuid().getValue());
            if(addRobotToCompetition) {
                RobotSubType subType = robot.getType();
                RobotType type = robot.getType().getRobotType();
                switch(type) {
                    case LINEFOLLOWER: {
                        lineFollowerCompetitionFinder.retrieveByRobotSubType(subType, true, edition)
                                .ifPresent(competition -> lineFollowerCompetition.addRobotToCompetition(competition, robot));
                        break;
                    }
                    case SUMO: {
                        sumoCompetitionFinder.retrieveByRobotSubType(subType, true, edition)
                                .ifPresent(competition -> sumoCompetition.addRobotToCompetition(competition, robot));
                        break;
                    }
                    case FREESTYLE: {
                        freestyleCompetitionFinder.retrieveByRobotSubType(subType, true, edition)
                                .ifPresent(competition -> freestyleCompetition.addRobotToCompetition(competition, robot));
                        break;
                    }
                }
            } else {
                robotService.delete(robot.getGuid());
            }
        }
    }
}
