package com.robot.challenge.tournament.registration.domain;

import com.robot.challenge.infrastructure.service.MailPlaceholderProvider;
import com.robot.challenge.infrastructure.service.MailTemplateResolver;
import com.robot.challenge.infrastructure.service.RcMail;
import com.robot.challenge.tournament.competitor.domain.Competitor;

public class VerificationMail extends RcMail {

    private final String content;
    private final String template =
            "<h4>Witaj $firstName $lastName ($login),</h4>\n" +
            "<p>Twoje konto na stronie <a href=\"http://rc.tl.krakow.pl\">rc.tl.krakow.pl</a> zostało właśnie utworzone. W celu potwierdzenia rejestracji kliknij w poniższy link:</p>\n" +
            "<p><a href=\"$scheme://$host$verificationPath\">$scheme://$host$verificationPath</a></p>\n" +
            "<p>Do zobaczenia na zawodach !!!</p>\n" +
            "<p>----------</p>\n" +
            "<p>ZSŁ Robot Challenge 2016</p>";

    private VerificationMail(MailTemplateResolver mailTemplateResolver, MailPlaceholderProvider placeholderProvider, Competitor competitor) {
        super(competitor);
        this.content = mailTemplateResolver.resolve(template, placeholderProvider.toVelocityContext(competitor));
    }

    @Override
    protected String getSubject() {
        return "ZSŁ Robot Challenge 2016 - potwierdzenie rejestracji.";
    }

    @Override
    protected String getContent() {
        return content;
    }

    public static VerificationMail of(MailTemplateResolver mailTemplateResolver, MailPlaceholderProvider placeholderProvider, Competitor competitor) {
        return new VerificationMail(mailTemplateResolver, placeholderProvider, competitor);
    }
}
