package com.robot.challenge.tournament.registration.rest.resource;

import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.registration.rest.model.CompetitorReceptionModel;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface ReceptionResource {

    String DEFAULT_RECEPTION_PREFIX = "/reception";
    String COMPETITOR_RECEPTION = "/competitor";

    @POST
    @Path(COMPETITOR_RECEPTION)
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = {SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response competitorReception(@PathParam("tournamentGuid") @NotNull @Size(min = 1) String tournamentGuid,
                                 @PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                 @Valid CompetitorReceptionModel receptionModel);
}
