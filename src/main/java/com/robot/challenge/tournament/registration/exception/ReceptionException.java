package com.robot.challenge.tournament.registration.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

/**
 * Created by mklys on 13/09/16.
 */
public class ReceptionException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        CANT_ACCEPT_TOURNAMENT_ALREADY_STARTED("Cant accept competitors as tournament is already started");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public ReceptionException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static ReceptionException tournamentAlreadyStarted() {
        return new ReceptionException(Response.Status.BAD_REQUEST.getStatusCode(),
                ReceptionException.Cause.CANT_ACCEPT_TOURNAMENT_ALREADY_STARTED,
                ReceptionException.Cause.CANT_ACCEPT_TOURNAMENT_ALREADY_STARTED.getErrorDescription());
    }
}
