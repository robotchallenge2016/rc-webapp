package com.robot.challenge.tournament.registration.rest.controller;

import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.registration.rest.model.CompetitorReceptionModel;
import com.robot.challenge.tournament.registration.rest.resource.ReceptionResource;
import com.robot.challenge.tournament.registration.service.ReceptionService;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + ReceptionResource.DEFAULT_RECEPTION_PREFIX)
public class ReceptionController implements ReceptionResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private ReceptionService receptionService;

    @Override
    public Response competitorReception(@NotNull @Size(min = 1) String tournamentGuid,
                                        @NotNull @Size(min = 1) String editionGuid,
                                        @Valid CompetitorReceptionModel receptionModel) {
        receptionService.reception(receptionModel, contextFactory.getEditionContext(editionGuid));
        return Response.ok().build();
    }
}
