package com.robot.challenge.tournament.registration.domain;

/**
 * Created by mklys on 21/02/16.
 */
public enum RegistrationStatus {
    INACTIVE,
    ACTIVE
}
