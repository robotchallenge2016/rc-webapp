package com.robot.challenge.tournament.registration.rest.controller;

import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.repository.CompetitorRepository;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.registration.rest.resource.VerificationResource;
import com.robot.challenge.tournament.repository.TournamentFinder;
import com.robot.challenge.tournament.rest.model.TournamentDomain;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path(TournamentResource.TOURNAMENT_PREFIX + VerificationResource.DEFAULT_VERIFICATION_PREFIX)
public class VerificationController implements VerificationResource {

    @Inject
    private CompetitorFinder competitorFinder;
    @Inject
    private CompetitorRepository competitorRepository;
    @Inject
    private TournamentFinder tournamentFinder;

    @Override
    public Response verify(@NotNull @Size(min = 1) String hash, @Context HttpServletRequest request) {
        Tournament tournament = tournamentFinder.retrieveByDomain(TournamentDomain.of(request))
                .orElseThrow(DataAccessException::entityNotFound);
        Competitor competitorToVerify = competitorFinder.searchInactiveCompetitorByHash(hash, tournament)
                .orElseThrow(DataAccessException::entityNotFound);
        competitorToVerify.getRegistrationInfo().markAsVerified();
        competitorRepository.update(competitorToVerify);
        return Response.ok().build();
    }
}
