package com.robot.challenge.tournament.registration.rest.resource;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface VerificationResource {

    String DEFAULT_VERIFICATION_PREFIX = "/verification";
    String VERIFICATION = "/hash/{hash}";

    @POST
    @Path(VERIFICATION)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response verify(@PathParam("hash") @NotNull @Size(min = 1) String hash,
                    @Context HttpServletRequest request);
}
