package com.robot.challenge.tournament.registration.rest.controller;

import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.infrastructure.service.MailPlaceholderProvider;
import com.robot.challenge.infrastructure.service.MailService;
import com.robot.challenge.infrastructure.service.MailTemplateResolver;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.rest.model.competitor.NewCompetitorModel;
import com.robot.challenge.tournament.competitor.service.CompetitorService;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.registration.domain.VerificationMail;
import com.robot.challenge.tournament.registration.exception.RegistrationException;
import com.robot.challenge.tournament.registration.rest.resource.RegistrationResource;
import com.robot.challenge.tournament.repository.TournamentEditionFinder;
import com.robot.challenge.tournament.repository.TournamentFinder;
import com.robot.challenge.tournament.rest.model.TournamentDomain;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path(TournamentResource.TOURNAMENT_PREFIX + RegistrationResource.DEFAULT_REGISTRATION_PREFIX)
public class RegistrationController implements RegistrationResource {
    @Inject
    private CompetitorService competitorService;
    @Inject
    private CompetitorFinder competitorFinder;
    @Inject
    private MailTemplateResolver mailTemplateResolver;
    @Inject
    private MailService mailService;
    @Inject
    private TournamentFinder finder;
    @Inject
    private TournamentEditionFinder editionFinder;

    @Override
    public Response register(@Valid NewCompetitorModel competitor, @Context HttpServletRequest request) {
        TournamentEdition edition = finder.retrieveByDomain(TournamentDomain.of(request))
                .flatMap(Tournament::getLatestEdition)
                .orElseThrow(DataAccessException::entityNotFound);
        if(!edition.isRegistrationOpened()) {
            throw RegistrationException.registrationClosed();
        }

        Competitor createdCompetitor = competitorService.create(competitor, edition.getTournament());
        sendVerificationEmail(createdCompetitor, request);

        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdCompetitor)).build();
    }

    private void sendVerificationEmail(Competitor competitor, HttpServletRequest request) {
        MailPlaceholderProvider provider = new MailPlaceholderProvider(request);
        VerificationMail mail = VerificationMail.of(mailTemplateResolver, provider, competitor);
        mailService.sendMail(mail);
    }

    @Override
    public Response loginIsFree(@NotNull @Size(min = 1) String login, @Context HttpServletRequest request) {
        Tournament tournament = finder.retrieveByDomain(TournamentDomain.of(request))
                .orElseThrow(DataAccessException::entityNotFound);
        return competitorFinder.retrieveByLogin(login, tournament)
                .map(r -> Response.status(Response.Status.BAD_REQUEST))
                .orElse(Response.status(Response.Status.OK)).build();
    }
}
