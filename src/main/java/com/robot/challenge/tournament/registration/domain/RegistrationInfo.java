package com.robot.challenge.tournament.registration.domain;

import com.robot.challenge.infrastructure.component.SecretDigester;
import com.robot.challenge.infrastructure.component.SecretGenerator;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class RegistrationInfo {

    @Column(name = "REGISTRATION_HASH", unique = true, nullable = false, updatable = false)
    private String hash;
    @Enumerated(EnumType.STRING)
    @Column(name = "REGISTRATION_STATUS", nullable = false)
    private RegistrationStatus status;

    public RegistrationInfo() {
        String plainTextSecret = SecretGenerator.INSTANCE.generate();
        this.hash = SecretDigester.INSTANCE.hashSecret(plainTextSecret);
        this.status = RegistrationStatus.INACTIVE;
    }

    public String getHash() {
        return hash;
    }

    public RegistrationStatus getStatus() {
        return status;
    }

    public RegistrationInfo markAsVerified() {
        this.status = RegistrationStatus.ACTIVE;
        return this;
    }

    public RegistrationInfo markAsUnverified() {
        this.status = RegistrationStatus.INACTIVE;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationInfo that = (RegistrationInfo) o;

        if (hash != null ? !hash.equals(that.hash) : that.hash != null) return false;
        return status == that.status;

    }

    @Override
    public int hashCode() {
        int result = hash != null ? hash.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}
