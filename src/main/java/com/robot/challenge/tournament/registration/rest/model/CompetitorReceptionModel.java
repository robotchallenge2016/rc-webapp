package com.robot.challenge.tournament.registration.rest.model;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public class CompetitorReceptionModel {

    @NotNull
    private String competitorGuid;
    private String teamGuid;
    private List<String> robotGuids;

    public String getCompetitorGuid() {
        return competitorGuid;
    }

    public Optional<String> getTeamGuid() {
        return Optional.ofNullable(teamGuid);
    }

    public List<String> getRobotGuids() {
        return robotGuids;
    }
}
