package com.robot.challenge.tournament.registration.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

/**
 * Created by mklys on 13/09/16.
 */
public class RegistrationException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        REGISTRATION_CLOSED("Registration is  closed");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public RegistrationException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static RegistrationException registrationClosed() {
        return new RegistrationException(Response.Status.BAD_REQUEST.getStatusCode(),
                RegistrationException.Cause.REGISTRATION_CLOSED,
                RegistrationException.Cause.REGISTRATION_CLOSED.getErrorDescription());
    }
}
