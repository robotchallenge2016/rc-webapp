package com.robot.challenge.tournament.registration.rest.resource;

import com.robot.challenge.tournament.competitor.rest.model.competitor.NewCompetitorModel;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface RegistrationResource {

    String DEFAULT_REGISTRATION_PREFIX = "/registration";
    String REGISTRATION = "/";
    String COMPETITOR_LOGIN_IS_FREE = "/login/{login}/free";

    @POST
    @Path(REGISTRATION)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response register(@Valid NewCompetitorModel competitor, @Context HttpServletRequest request);

    @GET
    @Path(COMPETITOR_LOGIN_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response loginIsFree(@PathParam("login") @NotNull @Size(min = 1) String login,
                         @Context HttpServletRequest request);
}
