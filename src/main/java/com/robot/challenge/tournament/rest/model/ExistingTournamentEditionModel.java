package com.robot.challenge.tournament.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.domain.TournamentEdition;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ExistingTournamentEditionModel implements GloballyUniqueModel, VersionedModel {

    private String guid;
    @NotNull
    @Size(min = 1)
    private String nameSuffix;
    @NotNull
    private Boolean registrationOpened;
    @NotNull
    private Boolean tournamentStarted;
    @NotNull
    @Min(value = 0)
    private Integer maxLineFollowerPerCompetitor;
    @NotNull
    @Min(value = 0)
    private Integer maxSumoPerCompetitor;
    @NotNull
    @Min(value = 0)
    private Integer maxFreestylePerCompetitor;
    @NotNull
    @Min(value = 0L)
    private Long version;

    private ExistingTournamentEditionModel() {

    }

    @Override
    public String getGuid() {
        return guid;
    }

    public String getNameSuffix() {
        return nameSuffix;
    }

    public boolean isRegistrationOpened() {
        return registrationOpened;
    }

    public boolean isTournamentStarted() {
        return tournamentStarted;
    }

    public Integer getMaxLineFollowerPerCompetitor() {
        return maxLineFollowerPerCompetitor;
    }

    public Integer getMaxSumoPerCompetitor() {
        return maxSumoPerCompetitor;
    }

    public Integer getMaxFreestylePerCompetitor() {
        return maxFreestylePerCompetitor;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingTournamentEditionModel of(TournamentEdition edition) {
        ExistingTournamentEditionModel model = new ExistingTournamentEditionModel();
        model.guid = edition.getGuid().getValue();
        model.nameSuffix = edition.getSuffix();
        model.registrationOpened = edition.isRegistrationOpened();
        model.tournamentStarted = edition.isTournamentStarted();
        model.maxLineFollowerPerCompetitor = edition.getMaxLineFollowerPerCompetitor();
        model.maxSumoPerCompetitor = edition.getMaxSumoPerCompetitor();
        model.maxFreestylePerCompetitor = edition.getMaxFreestylePerCompetitor();
        model.version = edition.getVersion();

        return model;
    }

    public static ExistingTournamentModelBuilder builder() {
        return new ExistingTournamentModelBuilder();
    }

    public static class ExistingTournamentModelBuilder {
        private final ExistingTournamentEditionModel tournamentSettings = new ExistingTournamentEditionModel();

        ExistingTournamentModelBuilder() {

        }

        public ExistingTournamentModelBuilder setNameSuffix(String nameSuffix) {
            this.tournamentSettings.nameSuffix = nameSuffix;
            return this;
        }

        public ExistingTournamentModelBuilder setRegistrationOpened(Boolean registrationOpened) {
            this.tournamentSettings.registrationOpened = registrationOpened;
            return this;
        }

        public ExistingTournamentModelBuilder setTournamentStarted(Boolean tournamentStarted) {
            this.tournamentSettings.tournamentStarted = tournamentStarted;
            return this;
        }

        public ExistingTournamentModelBuilder setMaxLineFollowerPerCompetitor(Integer maxLineFollowerPerCompetitor) {
            this.tournamentSettings.maxLineFollowerPerCompetitor = maxLineFollowerPerCompetitor;
            return this;
        }

        public ExistingTournamentModelBuilder setMaxSumoPerCompetitor(Integer maxSumoPerCompetitor) {
            this.tournamentSettings.maxSumoPerCompetitor = maxSumoPerCompetitor;
            return this;
        }

        public ExistingTournamentModelBuilder setMaxFreestylePerCompetitor(Integer maxFreestylePerCompetitor) {
            this.tournamentSettings.maxFreestylePerCompetitor = maxFreestylePerCompetitor;
            return this;
        }

        public ExistingTournamentModelBuilder setVersion(Long version) {
            this.tournamentSettings.version = version;
            return this;
        }

        public ExistingTournamentEditionModel build() {
            return tournamentSettings;
        }
    }
}
