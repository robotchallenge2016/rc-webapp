package com.robot.challenge.tournament.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.system.user.repository.SystemUserFinder;
import com.robot.challenge.system.user.service.SystemUserService;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.domain.TournamentFactory;
import com.robot.challenge.tournament.repository.TournamentFinder;
import com.robot.challenge.tournament.repository.TournamentRepository;
import com.robot.challenge.tournament.rest.model.ExistingTournamentModel;
import com.robot.challenge.tournament.rest.model.NewTournamentModel;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.security.Principal;

@Path(TournamentResource.TOURNAMENT_PREFIX)
public class TournamentController implements TournamentResource {

    @Inject
    private SystemUserService systemUserService;
    @Inject
    private Principal principal;
    @Inject
    private SystemUserFinder systemUserFinder;
    @Inject
    private TournamentFactory tournamentFactory;
    @Inject
    private TournamentFinder tournamentFinder;
    @Inject
    private TournamentRepository tournamentRepository;

    @Override
    @Transactional
    public Response create(@Valid NewTournamentModel model) {
        Tournament tournament = tournamentFactory.create(
                model.getName(),
                model.getShortName(),
                model.getDomain());
        tournamentRepository.create(tournament);
        systemUserService.create(model.getOwner(), tournament);

        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(tournament)).build();
    }

    @Override
    public Response update(String tournamentGuid, @Valid ExistingTournamentModel model) {
        Tournament tournament = tournamentFinder.retrieveByGuid(Guid.from(tournamentGuid))
                .orElseThrow(() -> DataAccessException.entityNotFound(Tournament.class, Guid.from(tournamentGuid)))
                .setName(model.getName())
                .setShortName(model.getShortName())
                .setDomain(model.getDomain())
                .setVersion(model.getVersion());

        tournamentRepository.update(tournament);
        return Response.ok().build();
    }

    @Override
    public Response retrieveSingle(String tournamentGuid) {
        return Response.ok(tournamentFinder.retrieveByGuid(Guid.from(tournamentGuid))
                .map(ExistingTournamentModel::of)
                .orElseThrow(() -> DataAccessException.entityNotFound(Tournament.class, Guid.from(tournamentGuid))))
                .build();
    }

    @Override
    public Response nameIsFree(@NotNull @Size(min = 1) String name) {
        return tournamentFinder.retrieveByName(name)
                .map((edition) -> Response.status(Response.Status.BAD_REQUEST).build())
                .orElseGet(() -> Response.ok().build());
    }

    @Override
    public Response domainIsFree(@NotNull @Size(min = 1) String domain) {
        return tournamentFinder.retrieveByDomain(domain)
                .map((edition) -> Response.status(Response.Status.BAD_REQUEST).build())
                .orElseGet(() -> Response.ok().build());
    }
}
