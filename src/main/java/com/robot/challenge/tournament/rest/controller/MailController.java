package com.robot.challenge.tournament.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.service.MailPlaceholderProvider;
import com.robot.challenge.infrastructure.service.MailService;
import com.robot.challenge.infrastructure.service.MailTemplateResolver;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.domain.CompetitorMail;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.repository.TournamentFinder;
import com.robot.challenge.tournament.rest.model.CompetitorMailModel;
import com.robot.challenge.tournament.rest.model.TournamentDomain;
import com.robot.challenge.tournament.rest.resource.MailResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + MailResource.MAIL_CONTEXT)
public class MailController implements MailResource {

    @Inject
    private CompetitorFinder competitorFinder;
    @Inject
    private MailTemplateResolver mailTemplateResolver;
    @Inject
    private MailService mailService;
    @Inject
    private TournamentFinder tournamentFinder;

    @Override
    public Response notification(@Valid CompetitorMailModel model, @Context HttpServletRequest request) {
        Tournament tournament = tournamentFinder.retrieveByDomain(TournamentDomain.of(request))
                .orElseThrow(DataAccessException::entityNotFound);
        List<Competitor> competitors;
        if(model.getCompetitorGuids().isEmpty()) {
            competitors = competitorFinder.retrieveAll(tournament);
        } else {
            List<Guid> guids = model.getCompetitorGuids().stream().map(Guid::from).collect(Collectors.toList());
            competitors = competitorFinder.retrieveByGuids(guids);
        }
        MailPlaceholderProvider provider = new MailPlaceholderProvider(request);
        List<CompetitorMail> mails = competitors.stream()
                .map(competitor -> CompetitorMail.of(competitor,
                        model.getTitle(),
                        mailTemplateResolver.resolve(model.getContent(), provider.toVelocityContext(competitor))))
                .collect(Collectors.toList());
        mailService.sendMail(mails);
        return Response.status(Response.Status.OK).build();
    }
}
