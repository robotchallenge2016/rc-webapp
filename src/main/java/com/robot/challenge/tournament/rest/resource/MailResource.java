package com.robot.challenge.tournament.rest.resource;

import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.rest.model.CompetitorMailModel;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface MailResource {
    String MAIL_CONTEXT = "/mail";

    String NOTIFICATION = "/notification";

    @POST
    @Path(NOTIFICATION)
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response notification(@Valid CompetitorMailModel model, @Context HttpServletRequest request);
}
