package com.robot.challenge.tournament.rest.resource;

import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.rest.model.ExistingTournamentEditionModel;
import com.robot.challenge.tournament.rest.model.NewTournamentEditionModel;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface TournamentEditionResource {

    String TOURNAMENT_EDITION_PREFIX = "/edition/guid/{editionGuid}";
    String TOURNAMENT_EDITION_CONTEXT = "/edition";

    String CREATE = "/";
    String UPDATE = "/guid/{editionGuid}";
    String DELETE = "/guid/{editionGuid}";
    String RETRIEVE_BY_SINGLE = "/guid/{editionGuid}";
    String SUFFIX_IS_FREE = "/suffix/{suffix}/free";

    @POST
    @Path(CREATE)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response create(@Valid NewTournamentEditionModel model,
                    @Context HttpServletRequest request);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid ExistingTournamentEditionModel model);

    @DELETE
    @Path(DELETE)
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);

    @GET
    @Path(RETRIEVE_BY_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response retrieveSingle(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);

    @GET
    @Path(SUFFIX_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response suffixIsFree(@PathParam("suffix") @NotNull @Size(min = 1) String suffix,
                          @Context HttpServletRequest request);
}
