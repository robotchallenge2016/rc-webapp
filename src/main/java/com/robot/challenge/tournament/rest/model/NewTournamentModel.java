package com.robot.challenge.tournament.rest.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewTournamentModel {

    @NotNull
    @Size(min = 1)
    private String name;
    @NotNull
    @Size(min = 1)
    private String shortName;
    @NotNull
    @Size(min = 1)
    private String domain;
    @Valid
    private NewTournamentOwnerModel owner;

    private NewTournamentModel() {

    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public String getDomain() {
        return domain;
    }

    public NewTournamentOwnerModel getOwner() {
        return owner;
    }

    public static ExistingTournamentModelBuilder builder() {
        return new ExistingTournamentModelBuilder();
    }

    public static class ExistingTournamentModelBuilder {
        private final NewTournamentModel tournamentSettings = new NewTournamentModel();

        ExistingTournamentModelBuilder() {

        }

        public ExistingTournamentModelBuilder setName(String name) {
            this.tournamentSettings.name = name;
            return this;
        }

        public ExistingTournamentModelBuilder setShortName(String shortName) {
            this.tournamentSettings.shortName = shortName;
            return this;
        }

        public ExistingTournamentModelBuilder setDomain(String domain) {
            this.tournamentSettings.domain = domain;
            return this;
        }

        public ExistingTournamentModelBuilder setOwner(NewTournamentOwnerModel owner) {
            this.tournamentSettings.owner = owner;
            return this;
        }

        public NewTournamentModel build() {
            return tournamentSettings;
        }
    }
}
