package com.robot.challenge.tournament.rest.resource;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface TournamentContextResource {
    String DEFAULT_CONTEXT_PREFIX = "/context";

    String RETRIEVE_TOURNAMENT_CONTEXT_BY_DOMAIN = "/tournament";
    String RETRIEVE_TOURNAMENT_EDITION_CONTEXT_BY_DOMAIN = "/tournament/edition";

    @GET
    @Path(RETRIEVE_TOURNAMENT_CONTEXT_BY_DOMAIN)
    @Produces(MediaType.APPLICATION_JSON)
    Response retrieveByDomain(@Context HttpServletRequest request);

    @GET
    @Path(RETRIEVE_TOURNAMENT_EDITION_CONTEXT_BY_DOMAIN)
    @Produces(MediaType.APPLICATION_JSON)
    Response retrieveEditionsByDomain(@Context HttpServletRequest request);
}
