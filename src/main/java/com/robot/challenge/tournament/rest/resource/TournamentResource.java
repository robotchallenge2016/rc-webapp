package com.robot.challenge.tournament.rest.resource;

import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.rest.model.ExistingTournamentModel;
import com.robot.challenge.tournament.rest.model.NewTournamentModel;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface TournamentResource {
    String TOURNAMENT_PREFIX = "/tournament";
    String FULL_TOURNAMENT_EDITION_PREFIX = TOURNAMENT_PREFIX + TournamentEditionResource.TOURNAMENT_EDITION_PREFIX;

    String CREATE = "/";
    String UPDATE = "/guid/{tournamentGuid}";
    String RETRIEVE_BY_SINGLE = "/guid/{tournamentGuid}";
    String NAME_IS_FREE = "/name/{name}/free";
    String DOMAIN_IS_FREE = "/domain/{domain}/free";

    @POST
    @Path(CREATE)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response create(@Valid NewTournamentModel model);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response update(@PathParam("tournamentGuid") String tournamentGuid,
                    @Valid ExistingTournamentModel model);

    @GET
    @Path(RETRIEVE_BY_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response retrieveSingle(@PathParam("tournamentGuid") String tournamentGuid);

    @GET
    @Path(NAME_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response nameIsFree(@PathParam("name") @NotNull @Size(min = 1) String name);

    @GET
    @Path(DOMAIN_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response domainIsFree(@PathParam("domain") @NotNull @Size(min = 1) String domain);
}
