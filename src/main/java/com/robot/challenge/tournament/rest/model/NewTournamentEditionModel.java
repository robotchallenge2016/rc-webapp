package com.robot.challenge.tournament.rest.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewTournamentEditionModel {

    @NotNull
    @Size(min = 1)
    private String nameSuffix;
    @NotNull
    private Boolean registrationOpened;
    @NotNull
    private Boolean tournamentStarted;
    @NotNull
    @Min(value = 0)
    private Integer maxLineFollowerPerCompetitor;
    @NotNull
    @Min(value = 0)
    private Integer maxSumoPerCompetitor;
    @NotNull
    @Min(value = 0)
    private Integer maxFreestylePerCompetitor;

    private NewTournamentEditionModel() {

    }

    public String getNameSuffix() {
        return nameSuffix;
    }

    public Boolean getRegistrationOpened() {
        return registrationOpened;
    }

    public Boolean getTournamentStarted() {
        return tournamentStarted;
    }

    public Integer getMaxLineFollowerPerCompetitor() {
        return maxLineFollowerPerCompetitor;
    }

    public Integer getMaxSumoPerCompetitor() {
        return maxSumoPerCompetitor;
    }

    public Integer getMaxFreestylePerCompetitor() {
        return maxFreestylePerCompetitor;
    }

    public static NewTournamentBuilder builder() {
        return new NewTournamentBuilder();
    }

    public static class NewTournamentBuilder {
        private final NewTournamentEditionModel tournamentSettings = new NewTournamentEditionModel();

        NewTournamentBuilder() {

        }

        public NewTournamentBuilder setNameSuffix(String nameSuffix) {
            this.tournamentSettings.nameSuffix = nameSuffix;
            return this;
        }

        public NewTournamentBuilder setRegistrationOpened(Boolean registrationOpened) {
            this.tournamentSettings.registrationOpened = registrationOpened;
            return this;
        }

        public NewTournamentBuilder setTournamentStarted(Boolean tournamentStarted) {
            this.tournamentSettings.tournamentStarted = tournamentStarted;
            return this;
        }

        public NewTournamentBuilder setMaxLineFollowerPerCompetitor(Integer maxLineFollowerPerCompetitor) {
            this.tournamentSettings.maxLineFollowerPerCompetitor = maxLineFollowerPerCompetitor;
            return this;
        }

        public NewTournamentBuilder setMaxSumoPerCompetitor(Integer maxSumoPerCompetitor) {
            this.tournamentSettings.maxSumoPerCompetitor = maxSumoPerCompetitor;
            return this;
        }

        public NewTournamentBuilder setMaxFreestylePerCompetitor(Integer maxFreestylePerCompetitor) {
            this.tournamentSettings.maxFreestylePerCompetitor = maxFreestylePerCompetitor;
            return this;
        }
    }
}
