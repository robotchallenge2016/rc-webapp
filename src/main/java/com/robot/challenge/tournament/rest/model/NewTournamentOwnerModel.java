
package com.robot.challenge.tournament.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewTournamentOwnerModel {

    @NotNull
    @Size(min = 1)
    private String login;
    @NotNull
    @Size(min = 1)
    private String firstName;
    @NotNull
    @Size(min = 1)
    private String lastName;
    @NotNull
    @Size(min = 1)
    private String password;

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public static NewSystemUserModelBuilder builder() {
        return new NewSystemUserModelBuilder();
    }

    public static class NewSystemUserModelBuilder {

        private final NewTournamentOwnerModel model = new NewTournamentOwnerModel();

        private NewSystemUserModelBuilder() {

        }

        public NewSystemUserModelBuilder setLogin(String login) {
            model.login = login;
            return this;
        }

        public NewSystemUserModelBuilder setFirstName(String firstName) {
            model.firstName = firstName;
            return this;
        }

        public NewSystemUserModelBuilder setLastName(String lastName) {
            model.lastName = lastName;
            return this;
        }

        public NewSystemUserModelBuilder setPassword(String password) {
            model.password = password;
            return this;
        }

        public NewTournamentOwnerModel build() {
            return model;
        }
    }
}
