package com.robot.challenge.tournament.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.domain.Tournament;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ExistingTournamentModel implements GloballyUniqueModel, VersionedModel {

    private String guid;
    @NotNull
    @Size(min = 1)
    private String name;
    @NotNull
    @Size(min = 1)
    private String shortName;
    @NotNull
    @Size(min = 1)
    private String domain;
    @NotNull
    @Min(value = 0L)
    private Long version;

    private ExistingTournamentModel() {

    }

    @Override
    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public String getDomain() {
        return domain;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingTournamentModel of(Tournament tournament) {
        ExistingTournamentModel model = new ExistingTournamentModel();
        model.guid = tournament.getGuid().getValue();
        model.name = tournament.getName();
        model.shortName = tournament.getShortName();
        model.domain = tournament.getDomain();
        model.version = tournament.getVersion();

        return model;
    }

    public static ExistingTournamentModelBuilder builder() {
        return new ExistingTournamentModelBuilder();
    }

    public static class ExistingTournamentModelBuilder {
        private final ExistingTournamentModel tournamentSettings = new ExistingTournamentModel();

        ExistingTournamentModelBuilder() {

        }

        public ExistingTournamentModelBuilder setNameSuffix(String nameSuffix) {
            this.tournamentSettings.name = nameSuffix;
            return this;
        }

        public ExistingTournamentModelBuilder setShortName(String shortName) {
            this.tournamentSettings.shortName = shortName;
            return this;
        }

        public ExistingTournamentModelBuilder setDomain(String domain) {
            this.tournamentSettings.domain = domain;
            return this;
        }

        public ExistingTournamentModelBuilder setVersion(Long version) {
            this.tournamentSettings.version = version;
            return this;
        }

        public ExistingTournamentModel build() {
            return tournamentSettings;
        }
    }
}
