package com.robot.challenge.tournament.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.system.user.repository.SystemUserFinder;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.domain.TournamentEditionFactory;
import com.robot.challenge.tournament.domain.TournamentFactory;
import com.robot.challenge.tournament.repository.TournamentEditionFinder;
import com.robot.challenge.tournament.repository.TournamentEditionRepository;
import com.robot.challenge.tournament.repository.TournamentFinder;
import com.robot.challenge.tournament.repository.TournamentRepository;
import com.robot.challenge.tournament.rest.model.ExistingTournamentEditionModel;
import com.robot.challenge.tournament.rest.model.NewTournamentEditionModel;
import com.robot.challenge.tournament.rest.model.TournamentDomain;
import com.robot.challenge.tournament.rest.resource.TournamentEditionResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.security.Principal;

@Path(TournamentResource.TOURNAMENT_PREFIX + TournamentEditionResource.TOURNAMENT_EDITION_CONTEXT)
public class TournamentEditionController implements TournamentEditionResource {

    @Inject
    private TournamentFactory tournamentFactory;
    @Inject
    private TournamentEditionFactory editionFactory;
    @Inject
    private TournamentFinder tournamentFinder;
    @Inject
    private TournamentEditionFinder editionFinder;
    @Inject
    private TournamentRepository tournamentRepository;
    @Inject
    private TournamentEditionRepository editionRepository;
    @Inject
    private SystemUserFinder systemUserFinder;
    @Inject
    private Principal principal;

    @Override
    public Response create(@Valid NewTournamentEditionModel model, @Context HttpServletRequest request) {
        Tournament tournament = tournamentFinder.retrieveByDomain(TournamentDomain.of(request))
                .orElseThrow(DataAccessException::entityNotFound);

        TournamentEdition edition = editionFactory.create(model.getNameSuffix(), tournament)
                .setMaxLineFollowerPerCompetitor(model.getMaxLineFollowerPerCompetitor())
                .setMaxSumoPerCompetitor(model.getMaxSumoPerCompetitor())
                .setMaxFreestylePerCompetitor(model.getMaxFreestylePerCompetitor());
        editionRepository.create(edition);

        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(edition)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @Valid ExistingTournamentEditionModel model) {
        TournamentEdition edition = editionFinder.retrieveByGuid(Guid.from(editionGuid))
                .orElseThrow(() -> DataAccessException.entityNotFound(Tournament.class, Guid.from(editionGuid)))
                .setSuffix(model.getNameSuffix())
                .setRegistrationOpened(model.isRegistrationOpened())
                .setTournamentStarted(model.isTournamentStarted())
                .setMaxLineFollowerPerCompetitor(model.getMaxLineFollowerPerCompetitor())
                .setMaxSumoPerCompetitor(model.getMaxSumoPerCompetitor())
                .setMaxFreestylePerCompetitor(model.getMaxFreestylePerCompetitor())
                .setVersion(model.getVersion());

        editionRepository.update(edition);
        return Response.ok().build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid) {
        editionFinder.retrieveByGuid(Guid.from(editionGuid))
                .map(edition -> editionRepository.delete(edition.getId()))
                .orElseThrow(() -> DataAccessException.entityNotFound(Tournament.class, Guid.from(editionGuid)));
        return Response.ok().build();
    }

    @Override
    public Response retrieveSingle(@NotNull @Size(min = 1) String editionGuid) {
        return Response.ok(editionFinder.retrieveByGuid(Guid.from(editionGuid))
                .map(ExistingTournamentEditionModel::of)
                .orElseThrow(() -> DataAccessException.entityNotFound(Tournament.class, Guid.from(editionGuid))))
                .build();
    }

    @Override
    public Response suffixIsFree(@NotNull @Size(min = 1) String suffix, @Context HttpServletRequest request) {
        Tournament tournament = tournamentFinder.retrieveByDomain(TournamentDomain.of(request))
                .orElseThrow(DataAccessException::entityNotFound);
        return editionFinder.retrieveBySuffix(tournament, suffix)
                .map((edition) -> Response.status(Response.Status.BAD_REQUEST).build())
                .orElseGet(() -> Response.ok().build());
    }
}
