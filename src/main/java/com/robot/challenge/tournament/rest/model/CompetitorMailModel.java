package com.robot.challenge.tournament.rest.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mklys on 30/09/16.
 */
public class CompetitorMailModel {

    private List<String> competitorGuids = new ArrayList<>();
    @NotNull
    @Size(min = 1)
    private String title;
    @NotNull
    @Size(min = 1)
    private String content;

    public List<String> getCompetitorGuids() {
        return competitorGuids;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
