package com.robot.challenge.tournament.rest.controller;

import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.repository.TournamentEditionFinder;
import com.robot.challenge.tournament.repository.TournamentFinder;
import com.robot.challenge.tournament.rest.model.ExistingTournamentEditionModel;
import com.robot.challenge.tournament.rest.model.ExistingTournamentModel;
import com.robot.challenge.tournament.rest.model.TournamentDomain;
import com.robot.challenge.tournament.rest.resource.TournamentContextResource;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@Path(TournamentContextResource.DEFAULT_CONTEXT_PREFIX)
public class TournamentContextController implements TournamentContextResource {

    @Inject
    private TournamentFinder tournamentFinder;
    @Inject
    private TournamentEditionFinder editionFinder;

    @Override
    public Response retrieveByDomain(@Context HttpServletRequest request) {
        return Response.ok(tournamentFinder.retrieveByDomain(TournamentDomain.of(request))
                .map(ExistingTournamentModel::of)
                .orElseThrow(DataAccessException::entityNotFound))
                .build();
    }

    @Override
    public Response retrieveEditionsByDomain(@Context HttpServletRequest request) {
        return Response.ok(editionFinder.retrieveByDomain(TournamentDomain.of(request))
                .stream()
                .map(ExistingTournamentEditionModel::of)
                .collect(Collectors.toList()))
                .build();
    }
}
