package com.robot.challenge.tournament.rest.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

public class TournamentDomain {

    private static final Logger LOG = LoggerFactory.getLogger(TournamentDomain.class);
    private final String domain;

    private TournamentDomain(String domain) {
        this.domain = domain;
    }

    public String getDomain() {
        return domain;
    }

    public static TournamentDomain of(HttpServletRequest request) {
        String domain = request.getHeader("X-Remote-Host");
        LOG.info("Request domain: {}", domain);
        return new TournamentDomain(domain);
    }

    @Override
    public String toString() {
        return "TournamentDomain{" +
                "domain='" + domain + '\'' +
                '}';
    }
}
