package com.robot.challenge.tournament.dictionary.domain;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "ROBOT_SUBTYPE", indexes = { @Index(name = "robot_subtype_idx", columnList = "guid") })
public class RobotSubType implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "robot_subtype_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "NAME", updatable = true, nullable = false, unique = false)
    private String name;
    @Enumerated(EnumType.STRING)
    @Column(name = "ROBOT_TYPE", nullable = false)
    private RobotType robotType;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    protected RobotSubType() {

    }

    public Long getId() {
        return id;
    }

    @Override
    public Guid getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public RobotType getRobotType() {
        return robotType;
    }

    public Long getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RobotSubType robotSubType = (RobotSubType) o;

        if (!id.equals(robotSubType.id)) return false;
        if (!guid.equals(robotSubType.guid)) return false;
        if (!name.equals(robotSubType.name)) return false;
        return version.equals(robotSubType.version);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + guid.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "RobotType{" +
                "id=" + id +
                ", guid=" + guid +
                ", name='" + name + '\'' +
                ", version=" + version +
                '}';
    }
}
