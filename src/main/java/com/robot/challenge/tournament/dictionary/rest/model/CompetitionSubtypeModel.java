package com.robot.challenge.tournament.dictionary.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompetitionSubtypeModel {

    @JsonProperty
    private String guid;
    @JsonProperty
    private String name;
    @JsonProperty
    private CompetitionTypeModel competitionType;
    @JsonProperty
    private RobotTypeModel robotType;
    @JsonProperty
    private Long version;

    private CompetitionSubtypeModel() {

    }

    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public CompetitionTypeModel getCompetitionType() {
        return competitionType;
    }

    public RobotTypeModel getRobotType() {
        return robotType;
    }

    public Long getVersion() {
        return version;
    }

    public static CompetitionSubtypeModel from(CompetitionSubtype competitionSubtype) {
        CompetitionSubtypeModel model = new CompetitionSubtypeModel();
        model.guid = competitionSubtype.getGuid().getValue();
        model.name = competitionSubtype.getName();
        model.competitionType = CompetitionTypeModel.from(competitionSubtype.getCompetitionType());
        model.robotType = RobotTypeModel.from(competitionSubtype.getRobotSubType());
        model.version = competitionSubtype.getVersion();

        return model;
    }
}
