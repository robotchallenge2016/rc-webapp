package com.robot.challenge.tournament.dictionary.domain;

public enum CompetitionType {
    SUMO,
    LINEFOLLOWER,
    FREESTYLE
}
