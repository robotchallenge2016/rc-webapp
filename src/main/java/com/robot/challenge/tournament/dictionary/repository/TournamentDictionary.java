package com.robot.challenge.tournament.dictionary.repository;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.dictionary.domain.CompetitionSubtype;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.domain.RobotSubType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class TournamentDictionary {
    private static final Logger LOG = LoggerFactory.getLogger(TournamentDictionary.class);

    @PersistenceContext(name = "PostgresqlPersistenceUnit")
    private EntityManager em;

    public List<CompetitionSubtype> retrieveCompetitionSubtypesByCompetitionType(CompetitionType competitionType) {
        LOG.info("Retrieving {} by {}", CompetitionSubtype.class, CompetitionType.class);
        TypedQuery<CompetitionSubtype> q = em.createQuery("SELECT c FROM CompetitionSubtype c " +
                "LEFT JOIN c.robotSubType crt " +
                "WHERE c.competitionType = :competitionType order by c.name", CompetitionSubtype.class);
        q.setParameter("competitionType", competitionType);

        return q.getResultList();
    }

    public Optional<CompetitionSubtype> retrieveCompetitionSubtypeByGuidAndCompetitionType(Guid guid, CompetitionType competitionType) {
        LOG.info("Retrieving {} by GUID and {}", CompetitionSubtype.class, CompetitionType.class);
        TypedQuery<CompetitionSubtype> q = em.createQuery("SELECT c FROM CompetitionSubtype c " +
                "LEFT JOIN c.robotSubType crt " +
                "WHERE c.guid = :guid AND c.competitionType = :competitionType order by c.name", CompetitionSubtype.class);
        q.setParameter("guid", guid);
        q.setParameter("competitionType", competitionType);

        try {
            return Optional.of(q.getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

    public List<CompetitionSubtype> retrieveCompetitionSubtypes() {
        LOG.info("Retrieving all {}", CompetitionSubtype.class);
        TypedQuery<CompetitionSubtype> q = em.createQuery("SELECT c FROM CompetitionSubtype c " +
                "LEFT JOIN FETCH c.robotSubType crt " +
                "ORDER BY c.name", CompetitionSubtype.class);
        return q.getResultList();
    }

    public List<RobotSubType> retrieveRobotTypes() {
        LOG.info("Retrieving all {}", RobotSubType.class);
        TypedQuery<RobotSubType> q = em.createQuery("SELECT r FROM RobotSubType r ORDER BY r.name", RobotSubType.class);
        return q.getResultList();
    }

    public Optional<RobotSubType> retrieveRobotTypeByGuid(Guid guid) {
        TypedQuery<RobotSubType> q = em.createQuery("SELECT r FROM RobotSubType r WHERE r.guid = :guid", RobotSubType.class);
        q.setParameter("guid", guid);

        try {
            return Optional.of(q.getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

    public Optional<RobotSubType> retrieveRobotTypeByName(String name) {
        TypedQuery<RobotSubType> q = em.createQuery("SELECT r FROM RobotSubType r WHERE r.name = :name", RobotSubType.class);
        q.setParameter("name", name);

        try {
            return Optional.of(q.getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }
}
