package com.robot.challenge.tournament.dictionary.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompetitionTypeModel {

    @JsonProperty
    private String name;

    private CompetitionTypeModel() {

    }

    public String getName() {
        return name;
    }

    public static CompetitionTypeModel from(CompetitionType competitionType) {
        CompetitionTypeModel model = new CompetitionTypeModel();
        model.name = competitionType.name();

        return model;
    }
}
