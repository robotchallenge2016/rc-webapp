package com.robot.challenge.tournament.dictionary.rest.resource;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

public interface TournamentDictionaryResource {
    String DICTIONARY_PREFIX = "/dictionary";
    String COMPETITION_TYPE = "/competition/type/all";
    String COMPETITION_SUBTYPE = "/competition/subtype/all";
    String ROBOT_TYPE = "/robot/type/all";

    @GET
    @Path(COMPETITION_TYPE)
    @Produces("application/json")
    @PermitAll
    Response competitionTypeDictionary();

    @GET
    @Path(COMPETITION_SUBTYPE)
    @Produces("application/json")
    @PermitAll
    Response competitionSubtypeDictionary();

    @GET
    @Path(ROBOT_TYPE)
    @Produces("application/json")
    @PermitAll
    Response robotTypeDictionary();
}
