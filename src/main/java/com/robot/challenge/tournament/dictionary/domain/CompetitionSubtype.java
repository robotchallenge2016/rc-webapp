package com.robot.challenge.tournament.dictionary.domain;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "COMPETITION_SUBTYPE", indexes = { @Index(name = "competition_subtype_idx", columnList = "guid") })
public class CompetitionSubtype implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "competition_subtype_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "NAME", nullable = false, unique = true)
    private String name;
    @Enumerated(EnumType.STRING)
    @Column(name = "COMPETITION_TYPE", nullable = false)
    private CompetitionType competitionType;
    @OneToOne(optional = false)
    @JoinColumn(name = "ROBOT_TYPE_ID", nullable = false, unique = true)
    private RobotSubType robotSubType;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    public Long getId() {
        return id;
    }

    @Override
    public Guid getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public CompetitionType getCompetitionType() {
        return competitionType;
    }

    public RobotSubType getRobotSubType() {
        return robotSubType;
    }

    public Long getVersion() {
        return version;
    }
}
