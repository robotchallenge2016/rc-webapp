package com.robot.challenge.tournament.dictionary.domain;

/**
 * Created by mklys on 13/09/16.
 */
public enum RobotType {
    LINEFOLLOWER,
    SUMO,
    FREESTYLE
}
