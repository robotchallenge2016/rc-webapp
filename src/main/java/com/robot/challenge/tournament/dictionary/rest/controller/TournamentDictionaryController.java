package com.robot.challenge.tournament.dictionary.rest.controller;

import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.repository.TournamentDictionary;
import com.robot.challenge.tournament.dictionary.rest.model.CompetitionSubtypeModel;
import com.robot.challenge.tournament.dictionary.rest.model.CompetitionTypeModel;
import com.robot.challenge.tournament.dictionary.rest.model.RobotTypeModel;
import com.robot.challenge.tournament.dictionary.rest.resource.TournamentDictionaryResource;
import com.robot.challenge.tournament.rest.resource.TournamentEditionResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + TournamentDictionaryResource.DICTIONARY_PREFIX)
public class TournamentDictionaryController implements TournamentDictionaryResource {

    @Inject
    private TournamentDictionary tournamentDictionary;

    @Override
    public Response competitionTypeDictionary() {
        return Response.status(Response.Status.OK)
                .entity(Arrays.stream(CompetitionType.values())
                        .map(CompetitionTypeModel::from)
                        .collect(Collectors.toList()))
                .build();
    }

    @Override
    public Response competitionSubtypeDictionary() {
        return Response.status(Response.Status.OK)
                .entity(tournamentDictionary.retrieveCompetitionSubtypes().stream()
                        .map(CompetitionSubtypeModel::from)
                        .collect(Collectors.toList()))
                .build();
    }

    @Override
    public Response robotTypeDictionary() {
        return Response.status(Response.Status.OK)
                .entity(tournamentDictionary.retrieveRobotTypes().stream()
                        .map(RobotTypeModel::from)
                        .collect(Collectors.toList()))
                .build();
    }
}
