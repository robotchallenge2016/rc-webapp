package com.robot.challenge.tournament.dictionary.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.tournament.dictionary.domain.RobotSubType;

/**
 * Created by mklys on 05/02/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RobotTypeModel {

    @JsonProperty
    private String guid;
    @JsonProperty
    private String name;
    @JsonProperty
    private Long version;

    RobotTypeModel() {

    }

    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public Long getVersion() {
        return version;
    }

    public static RobotTypeModel from(RobotSubType robotSubType) {
        RobotTypeModel model = new RobotTypeModel();
        model.guid = robotSubType.getGuid().getValue();
        model.name = robotSubType.getName();
        model.version = robotSubType.getVersion();

        return model;
    }
}
