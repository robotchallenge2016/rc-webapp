package com.robot.challenge.tournament.competitor.rest.model.team;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewTeamModel {
    @NotNull
    @Size(min = 1)
    private String name;
    @NotNull
    @Size(min = 1)
    private String captainGuid;
    @NotNull
    private List<String> competitorGuids;

    public String getName() {
        return name;
    }

    public String getCaptainGuid() {
        return captainGuid;
    }

    public List<String> getCompetitorGuids() {
        return competitorGuids;
    }

    public static NewTeamModelBuilder builder() {
        return new NewTeamModelBuilder();
    }

    public static class NewTeamModelBuilder {
        private final NewTeamModel model = new NewTeamModel();

        public NewTeamModelBuilder() {
            model.competitorGuids = new ArrayList<>();
        }

        public NewTeamModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public NewTeamModelBuilder setCaptainGuid(String captainGuid) {
            model.captainGuid = captainGuid;
            return this;
        }

        public NewTeamModelBuilder addCompetitorGuid(String competitorGuid) {
            model.competitorGuids.add(competitorGuid);
            return this;
        }

        public NewTeamModel build() {
            return model;
        }
    }    
}
