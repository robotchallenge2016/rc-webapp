package com.robot.challenge.tournament.competitor.domain;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.registration.domain.RegistrationInfo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "COMPETITOR")
public class Competitor implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "competitor_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "SYSTEM_USER_ID", nullable = false)
    private SystemUser systemUser;
    @ManyToOne
    @JoinColumn(name = "TEAM_ID")
    private Team team;
    @Column(name = "SCHOOL_NAME")
    private String schoolName;
    @Column(name = "CAPTAIN_OF_TEAM")
    private boolean captainOfTeam;
    @OneToMany(mappedBy = "owner")
    private List<Robot> robots;
    @Column(name = "CONFIRMED", nullable = false)
    private boolean confirmed;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    private Competitor() {

    }

    public Competitor(SystemUser systemUser) {
        this.systemUser = systemUser;
        this.captainOfTeam = false;
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return systemUser.getGuid();
    }

    public String getLogin() {
        return systemUser.getLogin();
    }

    public Competitor setLogin(String login) {
        systemUser.setLogin(login);
        return this;
    }

    public String getPassword() {
        return systemUser.getPassword();
    }

    public Competitor setPassword(String password) {
        systemUser.setPassword(password);
        return this;
    }

    public String getFirstName() {
        return systemUser.getFirstName();
    }

    public Competitor setFirstName(String firstName) {
        systemUser.setFirstName(firstName);
        return this;
    }

    public String getLastName() {
        return systemUser.getLastName();
    }

    public Competitor setLastName(String lastName) {
        systemUser.setLastName(lastName);
        return this;
    }

    public SystemRole getSystemRole() {
        return systemUser.getSystemRole();
    }

    public Competitor setSystemRole(SystemRole systemRole) {
        systemUser.setSystemRole(systemRole);
        return this;
    }

    public Optional<Team> getTeam() {
        return Optional.ofNullable(team);
    }

    public Competitor setTeam(Team team) {
        this.team = team;
        return this;
    }

    public Competitor setCaptainOfTeam(boolean captainOfTeam) {
        this.captainOfTeam = captainOfTeam;
        return this;
    }

    public boolean isCaptainOfTeam() {
        return captainOfTeam;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public Competitor setSchoolName(String schoolName) {
        this.schoolName = schoolName;
        return this;
    }

    public List<Robot> getRobots() {
        return Collections.unmodifiableList(robots);
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void confirm() {
        this.confirmed = true;
    }

    public RegistrationInfo getRegistrationInfo() {
        return systemUser.getRegistrationInfo();
    }

    public Competitor setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.systemUser.setUpdateDateTime(updateDateTime);
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public Competitor setVersion(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Competitor that = (Competitor) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (systemUser != null ? !systemUser.equals(that.systemUser) : that.systemUser != null) return false;
        if (team != null ? !team.equals(that.team) : that.team != null) return false;
        return !(version != null ? !version.equals(that.version) : that.version != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (systemUser != null ? systemUser.hashCode() : 0);
        result = 31 * result + (team != null ? team.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Competitor{" +
                "team=" + team +
                ", systemUser=" + systemUser +
                ", id=" + id +
                '}';
    }
}
