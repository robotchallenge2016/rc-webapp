package com.robot.challenge.tournament.competitor.domain;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;

public class TeamFactory extends Factory<Team> {

    public TeamFactory() {
        super(Team.class);
    }

    public Team create(Competitor captain) {
        return new Team(Guid.generate(), captain);
    }
}
