package com.robot.challenge.tournament.competitor.repository;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competitor.domain.Team;

import javax.ejb.Stateless;

@Stateless
public class TeamRepository extends JpaRepository<Team> {

    public TeamRepository() {
        super(Team.class);
    }
}
