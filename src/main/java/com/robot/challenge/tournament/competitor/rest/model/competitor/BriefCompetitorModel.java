package com.robot.challenge.tournament.competitor.rest.model.competitor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.registration.domain.RegistrationStatus;

import java.util.List;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BriefCompetitorModel extends SimpleCompetitorModel {

    private Integer robotsCount;

    private BriefCompetitorModel() {

    }

    private BriefCompetitorModel(String guid,
                                 String login,
                                 Optional<Team> teamName,
                                 String schoolName,
                                 boolean active,
                                 boolean confirmed) {
        super(guid, login, teamName, schoolName, active, confirmed);
    }

    public Integer getRobotsCount() {
        return robotsCount;
    }

    public static BriefCompetitorModel from(Competitor competitor, List<Robot> robots) {
        BriefCompetitorModel model = new BriefCompetitorModel(
                competitor.getGuid().getValue(),
                competitor.getLogin(),
                competitor.getTeam(),
                competitor.getSchoolName(),
                competitor.getRegistrationInfo().getStatus() == RegistrationStatus.ACTIVE,
                competitor.isConfirmed());
        model.robotsCount = robots.size();

        return model;
    }
}
