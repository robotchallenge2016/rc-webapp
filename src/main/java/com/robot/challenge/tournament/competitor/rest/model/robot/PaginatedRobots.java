package com.robot.challenge.tournament.competitor.rest.model.robot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by mklys on 01/10/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaginatedRobots {

    private Long total;
    private List<PaginatedRobotModel> items;

    public Long getTotal() {
        return total;
    }

    public List<PaginatedRobotModel> getItems() {
        return items;
    }

    private PaginatedRobots(Long total, List<PaginatedRobotModel> items) {
        this.total = total;
        this.items = items;
    }

    public static PaginatedRobots of(Long total, List<PaginatedRobotModel> items) {
        return new PaginatedRobots(total, items);
    }
}
