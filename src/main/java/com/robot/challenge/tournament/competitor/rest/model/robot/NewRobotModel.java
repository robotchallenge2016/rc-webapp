package com.robot.challenge.tournament.competitor.rest.model.robot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by mklys on 08/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewRobotModel {
    @NotNull
    @Size(min = 1)
    private String name;
    @NotNull
    @Size(min = 1)
    private String ownerGuid;
    @NotNull
    @Size(min = 1)
    private String robotTypeGuid;
    private String description;

    public String getRobotTypeGuid() {
        return robotTypeGuid;
    }

    public String getName() {
        return name;
    }

    public String getOwnerGuid() {
        return ownerGuid;
    }

    public String getDescription() {
        return description;
    }

    public static NewRobotModelBuilder builder() {
        return new NewRobotModelBuilder();
    }

    public static class NewRobotModelBuilder {
        private final NewRobotModel model = new NewRobotModel();

        public NewRobotModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public NewRobotModelBuilder setOwnerGuid(String ownerGuid) {
            model.ownerGuid = ownerGuid;
            return this;
        }

        public NewRobotModelBuilder setType(String competitionTypeGuid){
            model.robotTypeGuid = competitionTypeGuid;
            return this;
        }

        public NewRobotModelBuilder setDescription(String description){
            model.description = description;
            return this;
        }

        public NewRobotModel build() {
            return model;
        }
    }
}
