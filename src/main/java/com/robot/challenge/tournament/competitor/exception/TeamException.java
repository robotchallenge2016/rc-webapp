package com.robot.challenge.tournament.competitor.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class TeamException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {
        CAPTAIN_ALREADY_OCCUPIED("Specified Competitor is already a captain of some Team"),
        NAME_ALREADY_EXISTS("Team name already exists");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public TeamException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static TeamException nameAlreadyExists() {
        return new TeamException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.NAME_ALREADY_EXISTS,
                Cause.NAME_ALREADY_EXISTS.getErrorDescription());
    }

    public static TeamException captainAlreadyOccupied() {
        return new TeamException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.CAPTAIN_ALREADY_OCCUPIED,
                Cause.CAPTAIN_ALREADY_OCCUPIED.getErrorDescription());
    }
}
