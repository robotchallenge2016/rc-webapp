package com.robot.challenge.tournament.competitor.rest.model.competitor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.registration.domain.RegistrationStatus;

import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SimpleCompetitorModel {

    private String guid;
    private String login;
    private String teamName;
    private String schoolName;
    private boolean active;
    private boolean confirmed;

    protected SimpleCompetitorModel() {

    }

    protected SimpleCompetitorModel(String guid,
                                    String login,
                                    Optional<Team> team,
                                    String schoolName,
                                    boolean active,
                                    boolean confirmed) {
        this.guid = guid;
        this.login = login;
        team.ifPresent(v -> this.teamName = v.getName());
        this.schoolName = schoolName;
        this.active = active;
        this.confirmed = confirmed;
    }

    public String getGuid() {
        return guid;
    }

    public String getLogin() {
        return login;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public static SimpleCompetitorModel from(Competitor competitor) {
        SimpleCompetitorModel model = new SimpleCompetitorModel();
        model.guid = competitor.getGuid().getValue();
        model.login = competitor.getLogin();
        competitor.getTeam().ifPresent(v -> model.teamName = v.getName());
        model.schoolName = competitor.getSchoolName();
        model.active = competitor.getRegistrationInfo().getStatus() == RegistrationStatus.ACTIVE;
        model.confirmed = competitor.isConfirmed();

        return model;
    }
}
