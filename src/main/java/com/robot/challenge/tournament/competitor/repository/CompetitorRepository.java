package com.robot.challenge.tournament.competitor.repository;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competitor.domain.Competitor;

import javax.ejb.Stateless;

/**
 * Created by mklys on 05/01/16.
 */
@Stateless
public class CompetitorRepository extends JpaRepository<Competitor> {

    public CompetitorRepository() {
        super(Competitor.class);
    }
}
