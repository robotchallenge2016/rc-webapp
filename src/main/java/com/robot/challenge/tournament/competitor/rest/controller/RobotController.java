package com.robot.challenge.tournament.competitor.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.GuidsModel;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.system.file.rest.model.FileUpload;
import com.robot.challenge.system.file.service.SystemFileService;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.competitor.repository.RobotRepository;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.ExistingRobotModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.NewRobotModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.PaginatedRobotModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.PaginatedRobots;
import com.robot.challenge.tournament.competitor.rest.model.robot.SearchParams;
import com.robot.challenge.tournament.competitor.rest.resource.RobotResource;
import com.robot.challenge.tournament.competitor.service.RobotService;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX)
public class RobotController implements RobotResource {

    @Inject
    private CompetitorFinder competitorFinder;
    @Inject
    private ContextFactory contextFactory;
    @Inject
    private Principal principal;
    @Inject
    private RobotFinder robotFinder;
    @Inject
    private RobotRepository robotRepository;
    @Inject
    private RobotService robotService;
    @Inject
    private SystemFileService systemFileService;


    @Override
    public Response create(@NotNull @Size(min = 1) String editionGuid,
                           @Valid NewRobotModel robot) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        Robot createdRobot = robotService.create(robot, edition);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdRobot)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid,
                           @Valid ExistingRobotModel robot) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        robotService.update(Guid.from(guid), robot, edition);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid) {
        robotService.delete(Guid.from(guid));
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response uploadImage(@NotNull @Size(min = 1) String editionGuid,
                                @NotNull @Size(min = 1) String guid, MultipartFormDataInput input) {
        systemFileService.addSystemFile(Guid.from(guid), FileUpload.from(input), robotRepository, robotFinder);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response nameExists(@NotNull @Size(min = 1) String editionGuid,
                               @NotNull @Size(min = 1) String name) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        return robotFinder.retrieveByName(name, edition)
                .map(r -> Response.status(Response.Status.OK))
                .orElse(Response.status(Response.Status.BAD_REQUEST)).build();
    }

    @Override
    public Response nameIsFree(@NotNull @Size(min = 1) String editionGuid,
                               @NotNull @Size(min = 1) String name) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        return robotFinder.retrieveByName(name, edition)
                .map(r -> Response.status(Response.Status.BAD_REQUEST))
                .orElse(Response.status(Response.Status.OK)).build();
    }

    @Override
    public Response retrieveByCompetitor(@NotNull @Size(min = 1) String editionGuid,
                                         @NotNull @Size(min = 1) String competitorGuid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefRobotModel> result = robotFinder.retrieveByCompetitor(Guid.from(competitorGuid), edition)
                .stream()
                .map(BriefRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByTeam(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String teamGuid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefRobotModel> result = robotFinder.retrieveByTeam(Guid.from(teamGuid), edition).stream()
                .map(BriefRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByType(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull String robotTypeGuid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefRobotModel> result = robotFinder.retrieveByType(Guid.from(robotTypeGuid), edition)
                .stream()
                .map(BriefRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        ExistingRobotModel result = ExistingRobotModel.from(robotFinder.retrieveByGuid(Guid.from(guid))
                .orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveByGuids(@NotNull @Size(min = 1) String editionGuid,
                                    @Valid GuidsModel guidsModel) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<Guid> guids = guidsModel.getGuids().stream().map(Guid::from).collect(Collectors.toList());
        List<BriefRobotModel> result = robotFinder.retrieveByGuids(guids, edition)
                .stream()
                .map(BriefRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAllBriefed(@NotNull @Size(min = 1) String editionGuid) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefRobotModel> result = robotFinder.retrieveAll(edition)
                .stream()
                .map(BriefRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAllFull(@NotNull @Size(min = 1) String editionGuid,
                                    Integer limit, Integer offset) {
        SearchParams params = new SearchParams(limit, offset);
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        Long total = robotFinder.count(edition);
        List<PaginatedRobotModel> result = robotFinder.retrieveBySearchParams(params, edition)
                .stream()
                .map(PaginatedRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(PaginatedRobots.of(total, result)).build();
    }

    @Override
    public Response searchMatchingAnyParam(@NotNull @Size(min = 1) String editionGuid,
                                           @NotNull @Size(min = 1) String search) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefRobotModel> result = robotFinder.searchRobotWithAnyMatchingParam(search, edition)
                .stream()
                .map(BriefRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response searchByTypeMatchingAnyParam(@NotNull @Size(min = 1) String editionGuid,
                                                 @NotNull @Size(min = 1) String guid,
                                                 @NotNull @Size(min = 1) String search) {
        TournamentEdition edition = contextFactory.getEditionContext(editionGuid);
        List<BriefRobotModel> result = robotFinder.searchRobotByTypeWithAnyMatchingParam(Guid.from(guid), search, edition)
                .stream()
                .map(BriefRobotModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }
}
