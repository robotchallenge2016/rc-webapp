package com.robot.challenge.tournament.competitor.rest.resource;

import com.robot.challenge.infrastructure.rest.resource.CrudResource;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.competitor.rest.model.competitor.ExistingCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.model.competitor.NewCompetitorModel;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface CompetitorResource extends CrudResource<NewCompetitorModel, ExistingCompetitorModel> {

    String COMPETITOR_PREFIX = "/competitor";
    String CREATE = "/";
    String UPDATE = "/guid/{guid}";
    String DELETE = "/guid/{guid}";
    String PROMOTE = "/promote/guid/{guid}";
    String RETRIEVE_SINGLE = "/guid/{guid}";
    String RETRIEVE_ALL = "/all";
    String COMPETITOR_LOGIN_EXISTS = "/login/{login}/exists";
    String COMPETITOR_LOGIN_IS_FREE = "/login/{login}/free";
    String SEARCH_MATCHING_ANY_PARAM = "/match/any";
    String SEARCH_UNCONFIRMED_MATCHING_ANY_PARAM = "/unconfirmed";
    String SEARCH_COMPETITOR_WITHOUT_TEAM_MATCHING_ANY_PARAM = "/team/empty/any";
    String SEARCH_TEAM_MEMBERS = "/team/{team}/all";
    String SEARCH_ROBOT_OWNER = "/robot/{robot}";

    @POST
    @Path(CREATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid NewCompetitorModel competitor);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingCompetitorModel competitor);

    @DELETE
    @Path(DELETE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @PUT
    @Path(PROMOTE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE })
    Response promote(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                     @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response retrieveAll(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);

    @GET
    @Path(COMPETITOR_LOGIN_EXISTS)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response loginExists(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                         @PathParam("login") @NotNull @Size(min = 1) String login);

    @GET
    @Path(COMPETITOR_LOGIN_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response loginIsFree(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                         @PathParam("login") @NotNull @Size(min = 1) String login);

    @GET
    @Path(SEARCH_MATCHING_ANY_PARAM)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response searchMatchingAnyParam(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                    @QueryParam(value = "search") @NotNull @Size(min = 1) String search);

    @GET
    @Path(SEARCH_UNCONFIRMED_MATCHING_ANY_PARAM)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response searchUnconfirmedCompetitorMatchingAnyParam(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                                         @QueryParam(value = "search") @NotNull @Size(min = 1) String search);

    @GET
    @Path(SEARCH_COMPETITOR_WITHOUT_TEAM_MATCHING_ANY_PARAM)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response searchCompetitorWithoutTeamMatchingAnyParam(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                                         @QueryParam(value = "search") @NotNull @Size(min = 1) String search);

    @GET
    @Path(SEARCH_TEAM_MEMBERS)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response searchTeamMembers(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                               @PathParam("team") @NotNull @Size(min = 1) String teamGuid);

    @GET
    @Path(SEARCH_ROBOT_OWNER)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response searchRobotOwner(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                              @PathParam("robot") @NotNull @Size(min = 1) String robotGuid);
}
