package com.robot.challenge.tournament.competitor.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.repository.CompetitorRepository;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.competitor.rest.model.competitor.BriefCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.model.competitor.ExistingCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.model.competitor.NewCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.resource.CompetitorResource;
import com.robot.challenge.tournament.competitor.service.CompetitorService;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + CompetitorResource.COMPETITOR_PREFIX)
public class CompetitorController implements CompetitorResource {

    @Inject
    private CompetitorService service;
    @Inject
    private CompetitorRepository repository;
    @Inject
    private CompetitorFinder competitorFinder;
    @Inject
    private ContextFactory contextFactory;
    @Inject
    private RobotFinder robotFinder;


    @Override
    public Response create(@NotNull @Size(min = 1) String editionGuid,
                           @Valid NewCompetitorModel competitor) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        Competitor createdCompetitor = service.create(competitor, tournament);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdCompetitor)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid, 
                           @NotNull @Size(min = 1) String guid, 
                           @Valid ExistingCompetitorModel competitor) {
        service.update(Guid.from(guid), competitor);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid, 
                           @NotNull @Size(min = 1) String guid) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        service.delete(Guid.from(guid), tournament);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response promote(@NotNull @Size(min = 1) String editionGuid, 
                            @NotNull @Size(min = 1) String guid) {
        service.promote(Guid.from(guid));
        return Response.status(Response.Status.ACCEPTED).build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid, 
                                   @NotNull @Size(min = 1) String guid) {
        ExistingCompetitorModel result = ExistingCompetitorModel.from(competitorFinder.retrieveByGuid(Guid.from(guid)).orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        List<BriefCompetitorModel> result = competitorFinder.retrieveAll(Boolean.TRUE, tournament).stream()
                .map(competitor -> BriefCompetitorModel.from(competitor, competitor.getRobots()) )
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response loginExists(@NotNull @Size(min = 1) String editionGuid, 
                                @NotNull @Size(min = 1) String login) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        return competitorFinder.retrieveByLogin(login, tournament)
                .map(r -> Response.status(Response.Status.OK))
                .orElse(Response.status(Response.Status.BAD_REQUEST))
                .build();
    }

    @Override
    public Response loginIsFree(@NotNull @Size(min = 1) String editionGuid, @NotNull @Size(min = 1) String login) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        return competitorFinder.retrieveByLogin(login, tournament)
                .map(r -> Response.status(Response.Status.BAD_REQUEST))
                .orElse(Response.status(Response.Status.OK))
                .build();
    }

    @Override
    public Response searchMatchingAnyParam(@NotNull @Size(min = 1) String editionGuid,
                                           @NotNull @Size(min = 1) String search) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        List<ExistingCompetitorModel> result = competitorFinder.searchCompetitorWithAnyMatchingParam(search, tournament)
                .stream()
                .map(ExistingCompetitorModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response searchUnconfirmedCompetitorMatchingAnyParam(@NotNull @Size(min = 1) String editionGuid, 
                                                                @NotNull @Size(min = 1) String search) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        List<ExistingCompetitorModel> result = competitorFinder
                .searchUnconfirmedCompetitorWithAnyMatchingParam(search, tournament)
                .stream()
                .map(ExistingCompetitorModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response searchCompetitorWithoutTeamMatchingAnyParam(@NotNull @Size(min = 1) String editionGuid, 
                                                                @NotNull @Size(min = 1) String search) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        List<ExistingCompetitorModel> result = competitorFinder
                .searchCompetitorWithEmptyTeamAndMatchingAnyParam(search, tournament)
                .stream()
                .map(ExistingCompetitorModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response searchTeamMembers(@NotNull @Size(min = 1) String editionGuid, 
                                      @NotNull @Size(min = 1) String teamGuid) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        List<ExistingCompetitorModel> result = competitorFinder.searchCompetitorByTeam(Guid.from(teamGuid), tournament)
                .stream()
                .map(ExistingCompetitorModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response searchRobotOwner(@NotNull @Size(min = 1) String editionGuid, 
                                     @NotNull @Size(min = 1) String robotGuid) {
        Robot robot = robotFinder.retrieveByGuid(Guid.from(robotGuid)).orElseThrow(DataAccessException::entityNotFound);
        ExistingCompetitorModel result = ExistingCompetitorModel.from(competitorFinder.retrieveByGuid(robot.getOwner().getGuid()).orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }
}
