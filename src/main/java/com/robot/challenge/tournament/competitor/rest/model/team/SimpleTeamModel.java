package com.robot.challenge.tournament.competitor.rest.model.team;

import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.competitor.rest.model.competitor.SimpleCompetitorModel;

public class SimpleTeamModel {

    private String guid;
    private String name;
    private SimpleCompetitorModel captain;

    protected SimpleTeamModel() {

    }

    protected SimpleTeamModel(String guid, String name, SimpleCompetitorModel captain) {
        this.guid = guid;
        this.name = name;
        this.captain = captain;
    }

    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public SimpleCompetitorModel getCaptain() {
        return captain;
    }

    public static SimpleTeamModel from(Team team) {
        return new SimpleTeamModel(team.getGuid().getValue(), team.getName(), SimpleCompetitorModel.from(team.getCaptain()));
    }
}
