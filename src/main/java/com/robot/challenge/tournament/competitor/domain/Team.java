package com.robot.challenge.tournament.competitor.domain;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.domain.Tournament;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "TEAM", indexes = { @Index(name = "team_idx", columnList = "guid") })
public class Team implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "team_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "NAME", unique = true, nullable = false)
    private String name;
    @Transient
    private Competitor captain;
    @OneToMany(mappedBy = "team")
    private List<Competitor> members = new ArrayList<>();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE_TIME", nullable = false, updatable = false)
    private Date createDateTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE_TIME")
    private Date updateDateTime;
    @ManyToOne(optional = false)
    @JoinColumn(name = "TOURNAMENT_ID", nullable = false, updatable = false)
    private Tournament tournament;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    protected Team() {

    }

    public Team(Guid guid, Competitor captain) {
        this.guid = guid;
        this.captain = captain;
        this.members.add(captain);
        this.createDateTime = Date.from(ZonedDateTime.now(ZoneId.of("UTC")).toInstant());
        captain.setTeam(this);
        captain.setCaptainOfTeam(true);
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public Team setName(String name) {
        this.name = name;
        return this;
    }

    public Competitor getCaptain() {
        if(captain == null) {
            captain = members.stream().filter(Competitor::isCaptainOfTeam).findFirst().get();
        }
        return captain;
    }

    public List<Competitor> getMembers() {
        return Collections.unmodifiableList(members);
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime.toInstant().atZone(ZoneId.of("UTC"));
    }

    public Optional<ZonedDateTime> getUpdateDateTime() {
        if(updateDateTime != null) {
            return Optional.of(updateDateTime.toInstant().atZone(ZoneId.of("UTC")));
        }
        return Optional.empty();
    }

    public Team setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = Date.from(updateDateTime.toInstant());
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public Team setVersion(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        if (id != null ? !id.equals(team.id) : team.id != null) return false;
        if (guid != null ? !guid.equals(team.guid) : team.guid != null) return false;
        if (name != null ? !name.equals(team.name) : team.name != null) return false;
        if (captain != null ? !captain.equals(team.captain) : team.captain != null) return false;
        if (members != null ? !members.equals(team.members) : team.members != null) return false;
        if (createDateTime != null ? !createDateTime.equals(team.createDateTime) : team.createDateTime != null)
            return false;
        if (updateDateTime != null ? !updateDateTime.equals(team.updateDateTime) : team.updateDateTime != null)
            return false;
        return version != null ? version.equals(team.version) : team.version == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (guid != null ? guid.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (captain != null ? captain.hashCode() : 0);
        result = 31 * result + (members != null ? members.hashCode() : 0);
        result = 31 * result + (createDateTime != null ? createDateTime.hashCode() : 0);
        result = 31 * result + (updateDateTime != null ? updateDateTime.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
