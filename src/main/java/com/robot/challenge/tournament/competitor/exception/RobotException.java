package com.robot.challenge.tournament.competitor.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class RobotException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        ROBOT_NAME_ALREADY_EXISTS("Robot name already exists"),
        ROBOT_CONNECTED_TO_RESULT_OR_MATCH_SCHEDULE("Robot is connected to result or match schedule. Delete it before deleting robot"),
        ROBOT_ASSIGNED_TO_COMPETITION("Robot is assigned to competition. Disassociate it before deleting robot");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public RobotException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static RobotException robotNameAlreadyExists() {
        return new RobotException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.ROBOT_NAME_ALREADY_EXISTS,
                Cause.ROBOT_NAME_ALREADY_EXISTS.getErrorDescription());
    }

    public static RobotException robotConnectedToResultOrMatchSchedule() {
        return new RobotException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.ROBOT_CONNECTED_TO_RESULT_OR_MATCH_SCHEDULE,
                Cause.ROBOT_CONNECTED_TO_RESULT_OR_MATCH_SCHEDULE.getErrorDescription());
    }

    public static RobotException robotAssignedToCompetition() {
        return new RobotException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.ROBOT_ASSIGNED_TO_COMPETITION,
                Cause.ROBOT_ASSIGNED_TO_COMPETITION.getErrorDescription());
    }
}
