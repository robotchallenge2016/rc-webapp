package com.robot.challenge.tournament.competitor.rest.model.robot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.rest.model.competitor.ExistingCompetitorModel;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaginatedRobotModel implements GloballyUniqueModel, VersionedModel {
    @NotNull
    @Size(min = 1)
    private String name;
    private String guid;
    @NotNull
    @Size(min = 1)
    private String robotTypeGuid;
    @NotNull
    @Size(min = 1)
    private ExistingCompetitorModel owner;
    private String description;
    private String robotImagePath;
    @NotNull
    @Min(value = 0L)
    private Long version;

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public ExistingCompetitorModel getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public String getRobotImagePath() {
        return robotImagePath;
    }

    public Long getVersion() {
        return version;
    }

    public String getRobotTypeGuid() {
        return robotTypeGuid;
    }

    public static PaginatedRobotModel from(Robot robot) {
        PaginatedRobotModel model = new PaginatedRobotModel();
        model.name = robot.getName();
        model.guid = robot.getGuid().getValue();
        model.owner = ExistingCompetitorModel.from(robot.getOwner());
        model.robotTypeGuid = robot.getType().getGuid().getValue();
        model.description = robot.getDescription();
        model.robotImagePath = robot.getRobotImagePath();
        model.version = robot.getVersion();

        return model;
    }
}
