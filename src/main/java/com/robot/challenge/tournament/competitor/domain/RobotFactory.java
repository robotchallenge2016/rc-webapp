package com.robot.challenge.tournament.competitor.domain;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.tournament.dictionary.domain.RobotSubType;
import com.robot.challenge.tournament.domain.TournamentEdition;

public class RobotFactory extends Factory<Robot> {

    public RobotFactory() {
        super(Robot.class);
    }

    public Robot create(RobotSubType type, TournamentEdition edition) {
        return new Robot(Guid.generate(), type, edition);
    }
}
