package com.robot.challenge.tournament.competitor.rest.model.competitor;

import java.util.Optional;

/**
 * Created by mklys on 10/01/16.
 */
public class SearchCompetitor {
    private String firstName;
    private String lastName;
    private String email;

    private SearchCompetitor(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public Optional<String> getFirstName() {
        return Optional.ofNullable(firstName);
    }

    public Optional<String> getLastName() {
        return Optional.ofNullable(lastName);
    }

    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }

    public static SearchCompetitor from(String firstName, String lastName, String email) {
        return new SearchCompetitor(firstName, lastName, email);
    }
}
