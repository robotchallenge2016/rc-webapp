package com.robot.challenge.tournament.competitor.repository;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.competitor.domain.TeamFactory;
import com.robot.challenge.tournament.domain.Tournament;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

public class TeamFinder extends JpaFinder<Team> {
    private static final Logger LOG = LoggerFactory.getLogger(TeamFinder.class);

    @Inject
    public TeamFinder(TeamFactory factory) {
        super(factory);
    }

    @Override
    public Optional<Team> retrieveByGuid(@NotNull Guid guid) {
        return this.retrieveByGuid(guid, false);
    }

    public Optional<Team> retrieveByGuid(@NotNull Guid guid, boolean fetchMembers) {
        LOG.info("Retrieving {} by guid", Team.class);
        CriteriaQuery<Team> q = criteriaBuilder().createQuery(Team.class);
        Root<Team> team = q.from(Team.class);

        if(fetchMembers) {
            team.fetch("members", JoinType.LEFT);
        }

        q.where(criteriaBuilder().equal(team.get("guid"), guid));
        return single(entityManager().createQuery(q));
    }

    public Optional<Team> retrieveByName(@NotNull @Size(min = 1) String name, Tournament tournament) {
        return retrieveByName(name, Boolean.FALSE, tournament);
    }

    public Optional<Team> retrieveByName(@NotNull @Size(min = 1) String name, boolean fetchMembers, Tournament tournament) {
        LOG.info("Retrieving {} by name", Team.class);
        CriteriaQuery<Team> q = criteriaBuilder().createQuery(Team.class);
        Root<Team> team = q.from(Team.class);

        if(fetchMembers) {
            team.fetch("members", JoinType.LEFT);
        }

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(team.get("name"), name),
                criteriaBuilder().equal(team.get("tournament"), tournament)
        ));
        q.select(team);
        return single(entityManager().createQuery(q));
    }

    public List<Team> retrieveAll(Tournament tournament) {
        return retrieveAll(Boolean.FALSE, tournament);
    }

    public List<Team> retrieveAll(boolean fetchMembers, Tournament tournament) {
        LOG.info("Retrieving all {}", Team.class);
        CriteriaQuery<Team> q = criteriaBuilder().createQuery(Team.class);
        Root<Team> team = q.from(Team.class);

        if(fetchMembers) {
            team.fetch("members", JoinType.LEFT);
            q.distinct(Boolean.TRUE);
        }
        q.where(criteriaBuilder().equal(team.get("tournament"), tournament));
        q.orderBy(criteriaBuilder().asc(team.get("name")));
        q.select(team);

        return list(entityManager().createQuery(q));
    }

    public List<Team> searchTeamWithAnyMatchingParam(@NotNull @Size(min = 1) String search, Tournament tournament) {
        return searchTeamWithAnyMatchingParam(search, Boolean.FALSE, tournament);
    }

    public List<Team> searchTeamWithAnyMatchingParam(@NotNull @Size(min = 1) String search,
                                                     boolean fetchTeamMembers,
                                                     Tournament tournament) {
        LOG.info("Searching {} by any matching param", Team.class);
        CriteriaQuery<Team> q = criteriaBuilder().createQuery(Team.class);
        Root<Team> team = q.from(Team.class);

        if(fetchTeamMembers) {
            team.fetch("members", JoinType.LEFT);
            q.distinct(Boolean.TRUE);
        }

        String param = ("%" + search + "%").toLowerCase();
        q.where(criteriaBuilder().and(
                criteriaBuilder().like(criteriaBuilder().lower(team.get("name")), param),
                criteriaBuilder().equal(team.get("tournament"), tournament))
        );

        q.select(team);

        return list(entityManager().createQuery(q));
    }

    public Optional<Team> retrieveByTeamMember(@NotNull Competitor competitor, Tournament tournament) {
        return retrieveByTeamMember(competitor, Boolean.FALSE, tournament);
    }

    public Optional<Team> retrieveByTeamMember(@NotNull Competitor competitor, boolean fetchTeamMembers, Tournament tournament) {
        LOG.info("Retrieving {} by team member", Team.class);
        CriteriaQuery<Team> q = criteriaBuilder().createQuery(Team.class);
        Root<Team> team = q.from(Team.class);

        if(fetchTeamMembers) {
            team.fetch("members", JoinType.LEFT);
            q.distinct(Boolean.TRUE);
        }

        q.where(criteriaBuilder().and(
                criteriaBuilder().isMember(competitor, team.get("members")),
                criteriaBuilder().equal(team.get("tournament"), tournament)
        ));
        q.select(team);

        return single(entityManager().createQuery(q));
    }
}
