package com.robot.challenge.tournament.competitor.repository;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.CompetitorFactory;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.competitor.rest.model.competitor.SearchCompetitor;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.registration.domain.RegistrationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

public class CompetitorFinder extends JpaFinder<Competitor> {

    private static final Logger LOG = LoggerFactory.getLogger(CompetitorRepository.class);

    @Inject
    public CompetitorFinder(CompetitorFactory factory) {
        super(factory);
    }

    public Optional<Competitor> retrieveByLogin(@NotNull String login, Tournament tournament) {
        return retrieveByLogin(login, Boolean.FALSE, tournament);
    }

    public Optional<Competitor> retrieveByLogin(@NotNull String login, boolean fetchRobots, Tournament tournament) {
        LOG.info("Retrieving {} by login", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.fetch("systemUser", JoinType.LEFT);
        competitor.fetch("team", JoinType.LEFT);

        if(fetchRobots) {
            competitor.fetch("robots", JoinType.LEFT);
        }

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competitor.get("systemUser").get("login"), login)),
                criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament)
        );

        q.select(competitor);
        return single(entityManager().createQuery(q));
    }

    @Override
    public Optional<Competitor> retrieveByGuid(@NotNull Guid guid) {
        return retrieveByGuid(guid, Boolean.FALSE);
    }

    public Optional<Competitor> retrieveByGuid(@NotNull Guid guid, boolean fetchRobots) {
        LOG.info("Retrieving {} by guid", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.fetch("systemUser", JoinType.LEFT);
        competitor.fetch("team", JoinType.LEFT);

        if(fetchRobots) {
            competitor.fetch("robots", JoinType.LEFT);
        }

        q.where(criteriaBuilder().equal(competitor.get("systemUser").get("guid"), guid));
        q.select(competitor);

        return single(entityManager().createQuery(q));
    }

    public List<Competitor> retrieveByGuids(@NotNull @Size(min = 1) List<Guid> guids) {
        return retrieveByGuids(guids, Boolean.FALSE);
    }

    public List<Competitor> retrieveByGuids(@NotNull @Size(min = 1) List<Guid> guids, boolean fetchRobots) {
        LOG.info("Retrieving {} by guids", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        if(fetchRobots) {
            competitor.fetch("robots", JoinType.LEFT);
            q.distinct(true);
        }

        q.where(competitor.get("systemUser").get("guid").in(guids));

        q.select(competitor);

        return list(entityManager().createQuery(q));
    }

    public List<Competitor> retrieveByTeam(@NotNull Team team, Tournament tournament) {
        LOG.info("Retrieving {} by Team", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competitor.get("team"), team),
                criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament)
        ));

        q.select(competitor);

        return list(entityManager().createQuery(q));
    }

    public Optional<Competitor> retrieveByTeamCaptain(@NotNull Team team, Tournament tournament) {
        LOG.info("Retrieving {} by Team", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competitor.get("team"), team),
                criteriaBuilder().equal(competitor.get("captainOfTeam"), true)),
                criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament)
        );

        q.select(competitor);

        return single(entityManager().createQuery(q));
    }

    public boolean isTeamCaptain(@NotNull Guid competitorGuid, Tournament tournament) {
        LOG.info("Retrieving {} by Team", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competitor.get("systemUser").get("guid"), competitorGuid),
                criteriaBuilder().isNotNull(competitor.get("team")),
                criteriaBuilder().equal(competitor.get("captainOfTeam"), true)),
                criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament)
        );

        q.select(competitor);

        return single(entityManager().createQuery(q)).map(result -> true).orElse(false);
    }

    public List<Competitor> retrieveAll(Tournament tournament) {
        return retrieveAll(Boolean.FALSE, tournament);
    }

    public List<Competitor> retrieveAll(boolean fetchRobots, Tournament tournament) {
        LOG.info("Retrieving all {}", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.fetch("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        if(fetchRobots) {
            competitor.fetch("robots", JoinType.LEFT);
            q.distinct(Boolean.TRUE);
        }

        q.where(criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament));
        q.orderBy(criteriaBuilder().asc(competitor.get("systemUser").get("login")));

        return list(entityManager().createQuery(q));
    }

    public List<Competitor> searchCompetitorWithAllMatchingParams(@NotNull SearchCompetitor search, Tournament tournament) {
        LOG.info("Searching {} by all matching params", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament),
                search.getFirstName().map(v -> criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("firstName")), ("%" + v + "%").toLowerCase())).orElse(alwaysTrue()),
                search.getLastName().map(v -> criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("lastName")), ("%" + v + "%").toLowerCase())).orElse(alwaysTrue()),
                search.getEmail().map(v -> criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("login")), ("%" + v + "%").toLowerCase())).orElse(alwaysTrue())
        ));

        q.select(competitor);

        return list(entityManager().createQuery(q));
    }

    public List<Competitor> searchCompetitorWithAnyMatchingParam(@NotNull @Size(min = 1) String search, Tournament tournament) {
        LOG.info("Searching {} by any matching param", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        String param = ("%" + search + "%").toLowerCase();
        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament),
                criteriaBuilder().or(
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("firstName")), param),
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("lastName")), param),
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("login")), param)
        )));

        q.select(competitor);

        return list(entityManager().createQuery(q));
    }

    public List<Competitor> searchUnconfirmedCompetitorWithAnyMatchingParam(@NotNull @Size(min = 1) String search, Tournament tournament) {
        LOG.info("Searching {} by any matching param", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        String param = ("%" + search + "%").toLowerCase();
        q.where(criteriaBuilder().and(
                        criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament),
                        criteriaBuilder().equal(competitor.get("confirmed"), false),
                        criteriaBuilder().or(
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("firstName")), param),
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("lastName")), param),
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("login")), param)
        )));

        q.select(competitor);

        return list(entityManager().createQuery(q));
    }

    public List<Competitor> searchCompetitorByTeam(@NotNull Guid teamGuid, Tournament tournament) {
        LOG.info("Searching {} by Team", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competitor.get("team").get("guid"), teamGuid),
                criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament)
        ));
        q.select(competitor);

        return list(entityManager().createQuery(q));
    }

    public List<Competitor> searchCompetitorWithEmptyTeamAndMatchingAnyParam(@NotNull @Size(min = 1) String search, Tournament tournament) {
        LOG.info("Searching {} by any matching param with entityManager()pty Team", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        String param = ("%" + search + "%").toLowerCase();
        q.where(criteriaBuilder().and(
                criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament),
                criteriaBuilder().isNull(competitor.get("team")),
                criteriaBuilder().or(
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("firstName")), param),
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("lastName")), param),
                        criteriaBuilder().like(criteriaBuilder().lower(competitor.get("systemUser").get("login")), param)
                )
        ));

        q.select(competitor);

        return list(entityManager().createQuery(q));
    }

    public Optional<Competitor> searchInactiveCompetitorByHash(@NotNull @Size(min = 1) String hash, Tournament tournament) {
        LOG.info("Retrieving {} by guid", Competitor.class);
        CriteriaQuery<Competitor> q = criteriaBuilder().createQuery(Competitor.class);
        Root<Competitor> competitor = q.from(Competitor.class);

        competitor.join("systemUser", JoinType.LEFT);
        competitor.join("team", JoinType.LEFT);

        q.where(
            criteriaBuilder().and(
                    criteriaBuilder().equal(competitor.get("systemUser").get("tournament"), tournament),
                    criteriaBuilder().equal(competitor.get("systemUser").get("registrationInfo").get("hash"), hash),
                    criteriaBuilder().equal(competitor.get("systemUser").get("registrationInfo").get("status"), RegistrationStatus.INACTIVE)
            )
        );

        q.select(competitor);
        return single(entityManager().createQuery(q));
    }
}
