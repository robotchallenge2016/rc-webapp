package com.robot.challenge.tournament.competitor.domain;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.system.user.domain.SystemUserFactory;
import com.robot.challenge.tournament.domain.Tournament;

import javax.inject.Inject;

public class CompetitorFactory extends Factory<Competitor> {

    private final SystemUserFactory systemUserFactory;

    @Inject
    public CompetitorFactory(SystemUserFactory systemUserFactory) {
        super(Competitor.class);
        this.systemUserFactory = systemUserFactory;
    }

    public Competitor create(Tournament tournament, SystemRole systemRole) {
        return new Competitor(systemUserFactory.create(tournament, systemRole));
    }

    public Competitor create(SystemUser systemUser) {
        return new Competitor(systemUser);
    }
}
