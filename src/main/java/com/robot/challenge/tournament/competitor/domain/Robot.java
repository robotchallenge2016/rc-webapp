package com.robot.challenge.tournament.competitor.domain;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.file.domain.SystemFileAware;
import com.robot.challenge.tournament.dictionary.domain.RobotSubType;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.website.domain.Notice;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "ROBOT", indexes = { @Index(name = "robot_idx", columnList = "guid") })
public class Robot implements GloballyUnique, SystemFileAware {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "robot_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "NAME", nullable = false)
    private String name;
    @ManyToOne(optional = false)
    @JoinColumn(name = "OWNER_ID", nullable = false)
    private Competitor owner;
    @OneToOne
    @JoinColumn(name = "ROBOT_TYPE_ID", nullable = false)
    private RobotSubType type;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ROBOT_IMAGE_PATH")
    private String robotImagePath;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE_TIME", nullable = false, updatable = false)
    private Date createDateTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE_TIME")
    private Date updateDateTime;
    @ManyToOne(optional = false)
    @JoinColumn(name = "TOURNAMENT_EDITION_ID", nullable = false, updatable = false)
    private TournamentEdition edition;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    private Robot() {

    }

    Robot(Guid guid, RobotSubType type, TournamentEdition edition) {
        this.guid = guid;
        this.type = type;
        this.edition = edition;
        this.createDateTime = Date.from(ZonedDateTime.now(ZoneId.of("UTC")).toInstant());
    }

    public Long getId() {
        return id;
    }

    @Override
    public Guid getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public Robot setName(String name) {
        this.name = name;
        return this;
    }

    public RobotSubType getType() {return type;}

    public Robot setType(RobotSubType type) {
        this.type = type;
        return this;
    }

    public Competitor getOwner() {
        return owner;
    }

    public Robot setOwner(Competitor owner) {
        this.owner = owner;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Robot setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getRobotImagePath() {
        return robotImagePath;
    }

    @Override
    public Optional<Path> getFullFilename() {
        return Optional.ofNullable(this.robotImagePath).map(Paths::get);
    }

    @Override
    public void setFullFilename(Path fullFileName) {
        this.robotImagePath = fullFileName.toString();
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime.toInstant().atZone(ZoneId.of("UTC"));
    }

    public Optional<ZonedDateTime> getUpdateDateTime() {
        if(updateDateTime != null) {
            return Optional.of(updateDateTime.toInstant().atZone(ZoneId.of("UTC")));
        }
        return Optional.empty();
    }

    public Robot setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = Date.from(updateDateTime.toInstant());
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public Robot setVersion(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Robot robot = (Robot) o;

        if (id != null ? !id.equals(robot.id) : robot.id != null) return false;
        if (!guid.equals(robot.guid)) return false;
        if (name != null ? !name.equals(robot.name) : robot.name != null) return false;
        if (owner != null ? !owner.equals(robot.owner) : robot.owner != null) return false;
        if (!type.equals(robot.type)) return false;
        if (description != null ? !description.equals(robot.description) : robot.description != null) return false;
        if (robotImagePath != null ? !robotImagePath.equals(robot.robotImagePath) : robot.robotImagePath != null)
            return false;
        if (!createDateTime.equals(robot.createDateTime)) return false;
        if (updateDateTime != null ? !updateDateTime.equals(robot.updateDateTime) : robot.updateDateTime != null)
            return false;
        if (!edition.equals(robot.edition)) return false;
        return version != null ? version.equals(robot.version) : robot.version == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + guid.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + type.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (robotImagePath != null ? robotImagePath.hashCode() : 0);
        result = 31 * result + createDateTime.hashCode();
        result = 31 * result + (updateDateTime != null ? updateDateTime.hashCode() : 0);
        result = 31 * result + edition.hashCode();
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
