package com.robot.challenge.tournament.competitor.rest.model.team;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Team;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingTeamModel implements GloballyUniqueModel, VersionedModel {
    @NotNull
    @Size(min = 1)
    private String name;
    private String guid;
    private String captainGuid;
    @NotNull
    private List<String> competitorGuids;
    @NotNull
    @Min(value = 0L)
    private Long version;

    private ExistingTeamModel() {
        competitorGuids = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public String getCaptainGuid() {
        return captainGuid;
    }

    public List<String> getCompetitorGuids() {
        return competitorGuids;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingTeamModel from(Team team) {
        ExistingTeamModel model = new ExistingTeamModel();
        model.name = team.getName();
        model.guid = team.getGuid().getValue();
        model.captainGuid = team.getCaptain().getGuid().getValue();
        model.competitorGuids = team.getMembers().stream().map(Competitor::getGuid).map(Guid::getValue).collect(Collectors.toList());
        model.version = team.getVersion();

        return model;
    }

    public static ExistingTeamModelBuilder builder() {
        return new ExistingTeamModelBuilder();
    }

    public static class ExistingTeamModelBuilder {
        private final ExistingTeamModel model = new ExistingTeamModel();

        public ExistingTeamModelBuilder() {
            model.competitorGuids = new ArrayList<>();
        }

        public ExistingTeamModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public ExistingTeamModelBuilder setCaptainGuid(String captainGuid) {
            model.captainGuid = captainGuid;
            return this;
        }

        public ExistingTeamModelBuilder addCompetitorGuid(String competitorGuid) {
            model.competitorGuids.add(competitorGuid);
            return this;
        }

        public ExistingTeamModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingTeamModel build() {
            return model;
        }
    }    
}
