package com.robot.challenge.tournament.competitor.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.competitor.repository.TeamFinder;
import com.robot.challenge.tournament.competitor.rest.model.team.BriefTeamModel;
import com.robot.challenge.tournament.competitor.rest.model.team.ExistingTeamModel;
import com.robot.challenge.tournament.competitor.rest.model.team.NewTeamModel;
import com.robot.challenge.tournament.competitor.rest.resource.TeamResource;
import com.robot.challenge.tournament.competitor.service.TeamService;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.rest.resource.TournamentResource;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Path(TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX + TeamResource.TEAM_PREFIX)
public class TeamController implements TeamResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private TeamService teamService;
    @Inject
    private TeamFinder teamFinder;
    @Inject
    private CompetitorFinder competitorFinder;
    @Inject
    private RobotFinder robotFinder;

    @Override
    public Response create(@NotNull @Size(min = 1) String editionGuid,
                           @Valid NewTeamModel team) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        Team createdTeam = teamService.create(team, tournament);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(createdTeam)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid,
                           @Valid ExistingTeamModel team) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        teamService.update(Guid.from(guid), team, tournament);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid) {
        teamService.delete(Guid.from(guid));
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response searchMatchingAnyParam(@NotNull @Size(min = 1) String editionGuid,
                                           @NotNull @Size(min = 1) String search) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        List<ExistingTeamModel> result = teamFinder.searchTeamWithAnyMatchingParam(search, Boolean.TRUE, tournament)
                .stream()
                .map(ExistingTeamModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response teamNameExists(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String name) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        return teamFinder.retrieveByName(name, tournament).map(r -> Response.status(Response.Status.OK))
                .orElse(Response.status(Response.Status.BAD_REQUEST))
                .build();
    }

    @Override
    public Response teamNameIsFree(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String name) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        return teamFinder.retrieveByName(name, tournament).map(r -> Response.status(Response.Status.BAD_REQUEST))
                .orElse(Response.status(Response.Status.OK))
                .build();
    }

    @Override
    public Response retrieveByCompetitor(@NotNull @Size(min = 1) String editionGuid,
                                         @NotNull @Size(min = 1) String competitorGuid) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        return competitorFinder.retrieveByGuid(Guid.from(competitorGuid))
                .flatMap(competitor -> teamFinder.retrieveByTeamMember(competitor, Boolean.TRUE, tournament))
                .map(team -> Response.status(Response.Status.OK).entity(ExistingTeamModel.from(team)))
                .orElseGet(() -> Response.status(Response.Status.NOT_FOUND)).build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        ExistingTeamModel result = ExistingTeamModel.from(
                teamFinder.retrieveByGuid(Guid.from(guid), Boolean.TRUE)
                .orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        List<BriefTeamModel> result = teamFinder.retrieveAll(Boolean.TRUE, tournament).stream().map(team -> {
//            List<Guid> owners = team.getMembers().stream().map(Competitor::getGuid).collect(Collectors.toList());
//            List<Robot> robots = Collections.emptyList();
//            if(!owners.isEmpty()) {
//                robots = robotFinder.retrieveByCompetitors(owners);
//            }
            return BriefTeamModel.from(team, team.getMembers(), Collections.emptyList());
        }).collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }
}
