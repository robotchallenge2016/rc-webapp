package com.robot.challenge.tournament.competitor.rest.model.competitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * Created by mklys on 08/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewCompetitorModel {
    @NotNull
    @Size(min = 1)
    private String login;
    @NotNull
    @Size(min = 1)
    private String firstName;
    @NotNull
    @Size(min = 1)
    private String lastName;
    @JsonProperty
    private String teamGuid;
    @NotNull
    @Size(min = 1)
    private String schoolName;
    @NotNull
    @Size(min = 1)
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    @JsonIgnore
    public Optional<String> getTeamGuid() {
        return Optional.ofNullable(teamGuid);
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getPassword() {
        return password;
    }

    public static NewCompetitorModelBuilder builder() {
        return new NewCompetitorModelBuilder();
    }

    public static class NewCompetitorModelBuilder {

        private final NewCompetitorModel model = new NewCompetitorModel();

        private NewCompetitorModelBuilder() {

        }

        public NewCompetitorModelBuilder setLogin(String login) {
            model.login = login;
            return this;
        }

        public NewCompetitorModelBuilder setFirstName(String firstName) {
            model.firstName = firstName;
            return this;
        }

        public NewCompetitorModelBuilder setLastName(String lastName) {
            model.lastName = lastName;
            return this;
        }

        public NewCompetitorModelBuilder setPassword(String password) {
            model.password = password;
            return this;
        }

        public NewCompetitorModelBuilder setTeamGuid(String teamGuid) {
            model.teamGuid = teamGuid;
            return this;
        }

        public NewCompetitorModelBuilder setSchoolName(String schoolName) {
            model.schoolName = schoolName;
            return this;
        }

        public NewCompetitorModel build() {
            return model;
        }
    }
}
