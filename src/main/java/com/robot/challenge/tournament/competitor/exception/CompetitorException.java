package com.robot.challenge.tournament.competitor.exception;

import com.robot.challenge.infrastructure.exception.RcException;

import javax.ws.rs.core.Response;

public class CompetitorException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {

        TOO_MANY_ROBOTS_OF_TYPE_PER_COMPETITOR("Too many robots of particular type per competitor"),
        ALREADY_PROMOTED("Competitor has already been promoted");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public CompetitorException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static CompetitorException tooManyRobotOfTypePerCompetitor() {
        return new CompetitorException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.TOO_MANY_ROBOTS_OF_TYPE_PER_COMPETITOR,
                Cause.TOO_MANY_ROBOTS_OF_TYPE_PER_COMPETITOR.getErrorDescription());
    }

    public static CompetitorException alreadyPromoted() {
        return new CompetitorException(Response.Status.BAD_REQUEST.getStatusCode(),
                Cause.ALREADY_PROMOTED,
                Cause.ALREADY_PROMOTED.getErrorDescription());
    }
}
