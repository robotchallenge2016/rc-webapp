package com.robot.challenge.tournament.competitor.repository;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.domain.RobotFactory;
import com.robot.challenge.tournament.competitor.rest.model.robot.SearchParams;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.domain.TournamentEdition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

public class RobotFinder extends JpaFinder<Robot>  {
    private static final Logger LOG = LoggerFactory.getLogger(RobotFinder.class);

    @Inject
    public RobotFinder(RobotFactory factory) {
        super(factory);
    }

    public List<Robot> retrieveByGuids(@NotNull @Size(min = 1) List<Guid> guids,
                                       TournamentEdition edition) {
        LOG.info("Retrieving {} by guids", Robot.class);
        CriteriaQuery<Robot> q = criteriaBuilder().createQuery(Robot.class);
        Root<Robot> robot = q.from(Robot.class);

        robot.fetch("owner", JoinType.LEFT);

        q.where(criteriaBuilder().and(
                robot.get("guid").in(guids)),
                criteriaBuilder().equal(robot.get("edition"), edition)
        );
        q.select(robot);

        return list(entityManager().createQuery(q));
    }

    public Optional<Robot> retrieveByName(@NotNull @Size(min = 1) String name,
                                          TournamentEdition edition) {
        LOG.info("Retrieving {} by name", Robot.class);
        return single(entityManager().createQuery("SELECT u FROM Robot u " +
                "LEFT JOIN FETCH u.owner uo " +
                "LEFT JOIN FETCH uo.systemUser uos " +
                "LEFT JOIN FETCH uos.systemRole uosr " +
                "LEFT JOIN FETCH u.type ut " +
                "WHERE LOWER(u.name) = :name AND " +
                "u.edition = :edition", Robot.class)
                .setParameter("name", name.toLowerCase())
                .setParameter("edition", edition));
    }

    public List<Robot> retrieveByCompetitor(@NotNull Guid ownerGuid, TournamentEdition edition) {
        LOG.info("Retrieving robots by Competitor");
        return list(entityManager().createQuery("SELECT u FROM Robot u " +
                "LEFT JOIN FETCH u.owner uo " +
                "LEFT JOIN FETCH uo.systemUser uos " +
                "LEFT JOIN FETCH uos.systemRole uosr " +
                "LEFT JOIN FETCH u.type ut " +
                "WHERE u.owner.systemUser.guid = :guid AND " +
                "u.edition = :edition", Robot.class)
                .setParameter("edition", edition)
                .setParameter("guid", ownerGuid));
    }

    public List<Robot> retrieveByCompetitor(@NotNull Guid ownerGuid, Tournament tournament) {
        LOG.info("Retrieving robots by Competitor");
        return list(entityManager().createQuery("SELECT u FROM Robot u " +
                "LEFT JOIN FETCH u.owner uo " +
                "LEFT JOIN FETCH uo.systemUser uos " +
                "LEFT JOIN FETCH uos.systemRole uosr " +
                "LEFT JOIN FETCH u.type ut " +
                "WHERE u.owner.systemUser.guid = :guid AND " +
                "uos.tournament = :tournament", Robot.class)
                .setParameter("tournament", tournament)
                .setParameter("guid", ownerGuid));
    }

    public List<Robot> retrieveByTeam(@NotNull Guid teamGuid, TournamentEdition edition) {
        LOG.info("Retrieving robots by Team");
        return list(entityManager().createQuery("SELECT u FROM Robot u " +
                "LEFT JOIN FETCH u.owner uo " +
                "LEFT JOIN FETCH uo.systemUser uos " +
                "LEFT JOIN FETCH uos.systemRole uosr " +
                "LEFT JOIN FETCH u.type ut " +
                "WHERE u.owner.team.guid = :guid AND " +
                "u.edition = :edition", Robot.class)
                .setParameter("guid", teamGuid)
                .setParameter("edition", edition));
    }

    public List<Robot> retrieveByType(@NotNull Guid robotTypeGuid, TournamentEdition edition) {
        LOG.info("Retrieving robots by Type");
        return list(entityManager().createQuery("SELECT u FROM Robot u " +
                "LEFT JOIN FETCH u.owner uo " +
                "LEFT JOIN FETCH uo.systemUser uos " +
                "LEFT JOIN FETCH uos.systemRole uosr " +
                "LEFT JOIN FETCH u.type ut " +
                "WHERE ut.guid = :guid AND " +
                "u.edition = :edition", Robot.class)
                .setParameter("guid", robotTypeGuid)
                .setParameter("edition", edition));
    }

    public List<Robot> retrieveByCompetitors(@NotNull @Size(min = 1) List<Guid> ownerGuids,
                                             TournamentEdition edition) {
        LOG.info("Retrieving robots by Competitors");
        return list(entityManager().createQuery("SELECT u FROM Robot u " +
                "LEFT JOIN FETCH u.owner uo " +
                "LEFT JOIN FETCH uo.systemUser uos " +
                "LEFT JOIN FETCH uos.systemRole uosr " +
                "LEFT JOIN FETCH u.type ut " +
                "WHERE uo.systemUser.guid IN :ownerGuids AND " +
                "u.edition = :edition", Robot.class)
                .setParameter("ownerGuids", ownerGuids)
                .setParameter("edition", edition));
    }

    public List<Robot> retrieveAll(TournamentEdition edition) {
        LOG.info("Retrieving all robots");
        return list(entityManager().createQuery("SELECT u FROM Robot u " +
                "LEFT JOIN FETCH u.owner uo " +
                "LEFT JOIN FETCH uo.systemUser uos " +
                "LEFT JOIN FETCH uos.systemRole uosr " +
                "LEFT JOIN FETCH u.type ut " +
                "WHERE u.edition = :edition " +
                "ORDER BY u.name", Robot.class)
                .setParameter("edition", edition));
    }

    public List<Robot> retrieveBySearchParams(SearchParams params, TournamentEdition edition) {
        LOG.info("Retrieving all robots by search params");
        TypedQuery<Robot> q = entityManager().createQuery("SELECT u FROM Robot u " +
                "LEFT JOIN FETCH u.owner uo " +
                "LEFT JOIN FETCH uo.systemUser uos " +
                "LEFT JOIN FETCH uos.systemRole uosr " +
                "LEFT JOIN FETCH u.type ut " +
                "WHERE u.edition = :edition " +
                "ORDER BY u.name", Robot.class)
                .setParameter("edition", edition);

        params.getLimit().ifPresent(limit -> {
            q.setMaxResults(limit);
            params.getOffset().ifPresent(offset -> q.setFirstResult(limit * offset));
        });

        return list(q);
    }

    public Long count(TournamentEdition edition) {
        LOG.info("Counting {}", Robot.class.getName());
        TypedQuery<Long> q = entityManager().createQuery("SELECT COUNT(r) FROM Robot r " +
                "WHERE r.edition = :edition", Long.class)
                .setParameter("edition", edition);
        return q.getSingleResult();
    }

    public List<Robot> searchRobotWithAnyMatchingParam(@NotNull @Size(min = 1) String search, TournamentEdition edition) {
        LOG.info("Searching {} BY any matching param", Robot.class);
        CriteriaQuery<Robot> q = criteriaBuilder().createQuery(Robot.class);
        Root<Robot> robot = q.from(Robot.class);

        robot.join("owner", JoinType.LEFT);
        robot.join("type", JoinType.LEFT);

        String param = ("%" + search + "%").toLowerCase();
        q.where(criteriaBuilder().and(
                criteriaBuilder().like(criteriaBuilder().lower(robot.get("name")), param),
                criteriaBuilder().equal(robot.get("edition"), edition))
        );

        q.select(robot);

        return list(entityManager().createQuery(q));
    }

    public List<Robot> searchRobotByTypeWithAnyMatchingParam(@NotNull Guid robotTypeGuid,
                                                             @NotNull @Size(min = 1) String search,
                                                             TournamentEdition edition) {
        LOG.info("Searching {} BY any matching param", Robot.class);
        CriteriaQuery<Robot> q = criteriaBuilder().createQuery(Robot.class);
        Root<Robot> robot = q.from(Robot.class);

        robot.join("owner", JoinType.LEFT);
        robot.join("type", JoinType.LEFT);

        String param = ("%" + search + "%").toLowerCase();
        q.where(criteriaBuilder().and(
                criteriaBuilder().like(criteriaBuilder().lower(robot.get("name")), param),
                criteriaBuilder().equal(robot.get("type").get("guid"), robotTypeGuid),
                criteriaBuilder().equal(robot.get("edition"), edition))
        );

        q.select(robot);

        return list(entityManager().createQuery(q));
    }
}
