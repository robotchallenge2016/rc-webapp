package com.robot.challenge.tournament.competitor.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.system.dictionary.repository.SystemDictionary;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.system.user.exception.SystemUserException;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.CompetitorFactory;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.competitor.exception.CompetitorException;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.repository.CompetitorRepository;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.competitor.repository.TeamFinder;
import com.robot.challenge.tournament.competitor.rest.model.competitor.ExistingCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.model.competitor.NewCompetitorModel;
import com.robot.challenge.tournament.domain.Tournament;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

public class CompetitorService {

    private static final Logger LOG = LoggerFactory.getLogger(CompetitorService.class);

    private final CompetitorFactory competitorFactory;
    private final CompetitorRepository competitorRepository;
    private final CompetitorFinder competitorFinder;
    private final TeamFinder teamFinder;
    private final SystemDictionary systemDictionary;
    private final RobotFinder robotFinder;

    @Inject
    public CompetitorService(CompetitorFactory competitorFactory, CompetitorRepository competitorRepository,
                             CompetitorFinder competitorFinder, TeamFinder teamFinder, SystemDictionary systemDictionary,
                             RobotFinder robotFinder) {
        this.competitorFactory = competitorFactory;
        this.competitorRepository = competitorRepository;
        this.competitorFinder = competitorFinder;
        this.teamFinder = teamFinder;
        this.systemDictionary = systemDictionary;
        this.robotFinder = robotFinder;
    }

    public Competitor create(SystemUser systemUser) {
        Competitor competitor = competitorFactory.create(systemUser);
        competitorRepository.create(competitor);

        return competitor;
    }

    private void validateLogin(String login, Tournament tournament) {
        competitorFinder.retrieveByLogin(login, tournament).ifPresent(systemUser -> {
            throw SystemUserException.loginAlreadyExists();
        });
    }

    public Competitor create(NewCompetitorModel model, Tournament tournament) {
        LOG.info("Creating {} [login={}]", Competitor.class.getCanonicalName(), model.getLogin());
        validateLogin(model.getLogin(), tournament);

        SystemRole systemRole = systemDictionary.retrieveRoleByName(SystemRole.COMPETITOR_VALUE)
                .orElseThrow(DataAccessException::entityNotFound);
        Competitor competitor = competitorFactory.create(tournament, systemRole);

        competitor.setLogin(model.getLogin())
                .setFirstName(model.getFirstName())
                .setLastName(model.getLastName())
                .setSchoolName(model.getSchoolName())
                .setPassword(model.getPassword());

        if(model.getTeamGuid().isPresent()) {
            teamFinder.retrieveByGuid(Guid.from(model.getTeamGuid().get()))
                    .map(competitor::setTeam).orElseThrow(DataAccessException::entityNotFound);
        }
        competitorRepository.create(competitor);

        return competitor;
    }

    public void update(Guid guid, ExistingCompetitorModel model) {
        LOG.info("Updating {} [login={}, {}]", SystemUser.class.getCanonicalName(), model);

        Competitor competitor = competitorFinder.retrieveByGuid(guid)
                .orElseThrow(() -> DataAccessException.entityNotFound(Competitor.class, guid));

        competitor.setLogin(model.getLogin())
                .setFirstName(model.getFirstName())
                .setLastName(model.getLastName())
                .setSchoolName(model.getSchoolName())
                .setUpdateDateTime(ZonedDateTime.now(ZoneId.of("UTC")))
                .setVersion(model.getVersion());
        model.getPassword().ifPresent(competitor::setPassword);

        Team team = model.getTeamGuid()
                .map(Guid::from)
                .map(teamFinder::retrieveByGuid)
                .orElse(Optional.empty())
                .orElse(null);
        competitor.setTeam(team);

        competitorRepository.update(competitor);
    }

    private void checkIfHasRobots(Guid guid, Tournament tournament) {
        List<Robot> robots = robotFinder.retrieveByCompetitor(guid, tournament);
        if(!robots.isEmpty()) {
            throw SystemUserException.stillConntectedToRobots();
        }
    }

    public void delete(Guid guid, Tournament tournament) {
        LOG.info("Deleting {} [{}]", Competitor.class.getCanonicalName(), guid);
        checkIfHasRobots(guid, tournament);
        competitorFinder.retrieveByGuid(guid).map(entity -> competitorRepository.delete(entity.getId()))
                .orElseThrow(DataAccessException::entityNotFound);
    }

    @Transactional
    public void promote(Guid guid) {
        LOG.info("Promoting competitor {} [{}]", Competitor.class.getCanonicalName(), guid);
        Competitor competitor = competitorFinder.retrieveByGuid(guid)
                .orElseThrow(DataAccessException::entityNotFound);
        if(competitor.getSystemRole().getName().equals(SystemRole.COMPETITOR_VALUE)) {
            SystemRole userRole = systemDictionary.retrieveRoleByName(SystemRole.USER_VALUE)
                    .orElseThrow(DataAccessException::entityNotFound);
            competitor.setSystemRole(userRole);
            competitorRepository.update(competitor);
        } else {
            throw CompetitorException.alreadyPromoted();
        }
    }
}
