package com.robot.challenge.tournament.competitor.rest.model.competitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competitor.domain.Competitor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Optional;

/**
 * Created by mklys on 08/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingCompetitorModel implements GloballyUniqueModel, VersionedModel {

    private String guid;
    @NotNull
    @Size(min = 1)
    private String login;
    @NotNull
    @Size(min = 1)
    private String firstName;
    @NotNull
    @Size(min = 1)
    private String lastName;
    @JsonProperty
    private String password;
    @JsonProperty
    private String teamGuid;
    @NotNull
    @Size(min = 1)
    private String schoolName;
    @JsonProperty
    private boolean captainOfTeam;
    @NotNull
    @Min(value = 0L)
    private Long version;

    public String getGuid() {
        return guid;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @JsonIgnore
    public Optional<String> getPassword() {
        return Optional.ofNullable(password);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public Optional<String> getTeamGuid() {
        return Optional.ofNullable(teamGuid);
    }

    public String getSchoolName() {
        return schoolName;
    }

    public boolean isCaptainOfTeam() {
        return captainOfTeam;
    }

    public void setTeamGuid(String teamGuid) {
        this.teamGuid = teamGuid;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingCompetitorModel from(Competitor competitor) {
        ExistingCompetitorModel model = new ExistingCompetitorModel();
        model.guid = competitor.getGuid().getValue();
        model.login = competitor.getLogin();
        model.firstName = competitor.getFirstName();
        model.lastName = competitor.getLastName();
        competitor.getTeam().ifPresent(team -> model.teamGuid = team.getGuid().getValue());
        model.schoolName = competitor.getSchoolName();
        model.captainOfTeam = competitor.isCaptainOfTeam();
        model.version = competitor.getVersion();

        return model;
    }

    public static ExistingCompetitorModelBuilder builder() {
        return new ExistingCompetitorModelBuilder();
    }

    public static class ExistingCompetitorModelBuilder {

        private final ExistingCompetitorModel model = new ExistingCompetitorModel();

        private ExistingCompetitorModelBuilder() {

        }

        public ExistingCompetitorModelBuilder setLogin(String login) {
            model.login = login;
            return this;
        }

        public ExistingCompetitorModelBuilder setFirstName(String firstName) {
            model.firstName = firstName;
            return this;
        }

        public ExistingCompetitorModelBuilder setLastName(String lastName) {
            model.lastName = lastName;
            return this;
        }

        public ExistingCompetitorModelBuilder setPassword(String password) {
            model.password = password;
            return this;
        }

        public ExistingCompetitorModelBuilder setTeamGuid(String teamGuid) {
            model.teamGuid = teamGuid;
            return this;
        }

        public ExistingCompetitorModelBuilder setSchoolName(String schoolName) {
            model.schoolName = schoolName;
            return this;
        }

        public ExistingCompetitorModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingCompetitorModel build() {
            return model;
        }
    }    
}
