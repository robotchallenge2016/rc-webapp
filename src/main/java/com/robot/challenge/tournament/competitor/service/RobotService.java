package com.robot.challenge.tournament.competitor.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competition.repository.FreestyleCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.LineFollowerCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.SumoCompetitionFinder;
import com.robot.challenge.tournament.competition.repository.result.FreestyleResultFinder;
import com.robot.challenge.tournament.competition.repository.result.LineFollowerResultFinder;
import com.robot.challenge.tournament.competition.repository.result.SumoMatchFinder;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.domain.RobotFactory;
import com.robot.challenge.tournament.competitor.exception.CompetitorException;
import com.robot.challenge.tournament.competitor.exception.RobotException;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.repository.RobotFinder;
import com.robot.challenge.tournament.competitor.repository.RobotRepository;
import com.robot.challenge.tournament.competitor.rest.model.robot.ExistingRobotModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.NewRobotModel;
import com.robot.challenge.tournament.dictionary.domain.RobotSubType;
import com.robot.challenge.tournament.dictionary.domain.RobotType;
import com.robot.challenge.tournament.dictionary.repository.TournamentDictionary;
import com.robot.challenge.tournament.domain.TournamentEdition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class RobotService {
    private static final Logger LOG = LoggerFactory.getLogger(RobotService.class);

    private final RobotFactory robotFactory;
    private final RobotRepository robotRepository;
    private final RobotFinder robotFinder;
    private final CompetitorFinder competitorFinder;
    private final TournamentDictionary tournamentDictionary;

    private final LineFollowerCompetitionFinder lineFollowerCompetitionFinder;
    private final SumoCompetitionFinder sumoCompetitionFinder;
    private final FreestyleCompetitionFinder freestyleCompetitionFinder;

    private final LineFollowerResultFinder lineFollowerResultFinder;
    private final SumoMatchFinder sumoMatchFinder;
    private final FreestyleResultFinder freestyleResultFinder;

    @Inject
    public RobotService(RobotFactory robotFactory, RobotRepository robotRepository, RobotFinder robotFinder,
                        CompetitorFinder competitorFinder, TournamentDictionary tournamentDictionary,
                        LineFollowerCompetitionFinder lineFollowerCompetitionFinder, SumoCompetitionFinder sumoCompetitionFinder,
                        FreestyleCompetitionFinder freestyleCompetitionFinder, LineFollowerResultFinder lineFollowerResultFinder,
                        SumoMatchFinder sumoMatchFinder, FreestyleResultFinder freestyleResultFinder) {
        this.robotFactory = robotFactory;
        this.robotRepository = robotRepository;
        this.robotFinder = robotFinder;
        this.competitorFinder = competitorFinder;
        this.tournamentDictionary = tournamentDictionary;
        this.lineFollowerCompetitionFinder = lineFollowerCompetitionFinder;
        this.sumoCompetitionFinder = sumoCompetitionFinder;
        this.freestyleCompetitionFinder = freestyleCompetitionFinder;
        this.lineFollowerResultFinder = lineFollowerResultFinder;
        this.sumoMatchFinder = sumoMatchFinder;
        this.freestyleResultFinder = freestyleResultFinder;
    }

    private void validateNumberOfRobotPerOwner(TournamentEdition edition, RobotSubType robotSubType, Competitor owner) {
        long numberOfRobotOfType = owner.getRobots().stream().filter(robot -> robot.getType().equals(robotSubType)).count();

        RobotType type = robotSubType.getRobotType();
        boolean overflow = false;
        switch(type) {
            case LINEFOLLOWER: {
                overflow = edition.getMaxLineFollowerPerCompetitor() <= numberOfRobotOfType;
                break;
            }
            case SUMO: {
                overflow = edition.getMaxSumoPerCompetitor() <= numberOfRobotOfType;
                break;
            }
            case FREESTYLE: {
                overflow = edition.getMaxFreestylePerCompetitor() <= numberOfRobotOfType;
                break;
            }
        }
        LOG.warn("Competitor [login={}, {}] has {} robots of type {}", owner.getLogin(), owner.getGuid(), numberOfRobotOfType, robotSubType.getName());
        if (overflow) {
            LOG.warn("Competitor [login={}, {}] has maximum number of robots of type {}", owner.getLogin(), owner.getGuid(), robotSubType.getName());
            throw CompetitorException.tooManyRobotOfTypePerCompetitor();
        }
    }

    private void validateRobotName(String name, TournamentEdition edition) {
        robotFinder.retrieveByName(name, edition).ifPresent(robot -> {
            throw RobotException.robotNameAlreadyExists();
        });
    }

    public Robot create(NewRobotModel model, TournamentEdition edition) {
        LOG.info("Creating {} [name={}]", Robot.class.getCanonicalName(), model.getName());

        Guid robotTypeGuid = Guid.from(model.getRobotTypeGuid());
        RobotSubType robotSubType = tournamentDictionary.retrieveRobotTypeByGuid(robotTypeGuid)
                .orElseThrow(() -> DataAccessException.entityNotFound(RobotSubType.class, robotTypeGuid));

        Guid competitorGuid = Guid.from(model.getOwnerGuid());
        Competitor owner = competitorFinder.retrieveByGuid(competitorGuid, Boolean.TRUE)
                .orElseThrow(() -> DataAccessException.entityNotFound(Competitor.class, competitorGuid));

        validateNumberOfRobotPerOwner(edition, robotSubType, owner);
        validateRobotName(model.getName(), edition);

        Robot robot = robotFactory.create(robotSubType, edition)
                .setName(model.getName())
                .setOwner(owner)
                .setDescription(model.getDescription());

        robotRepository.create(robot);

        return robot;
    }

    public void update(Guid guid, ExistingRobotModel model, TournamentEdition edition) {
        LOG.info("Updating {} [name={}, {}]", Robot.class.getCanonicalName(), model.getName(), guid);

        Guid robotTypeGuid = Guid.from(model.getRobotTypeGuid());
        RobotSubType robotSubType = tournamentDictionary.retrieveRobotTypeByGuid(Guid.from(model.getRobotTypeGuid()))
                .orElseThrow(() -> DataAccessException.entityNotFound(RobotSubType.class, robotTypeGuid));

        Guid competitorGuid = Guid.from(model.getOwnerGuid());
        Competitor owner = competitorFinder.retrieveByGuid(Guid.from(model.getOwnerGuid()), Boolean.TRUE)
                .orElseThrow(() -> DataAccessException.entityNotFound(Competitor.class, competitorGuid));

        validateNumberOfRobotPerOwner(edition, robotSubType, owner);

        Robot robot = robotFinder.retrieveByGuid(guid)
                .orElseThrow(() -> DataAccessException.entityNotFound(Robot.class, guid))
                .setName(model.getName())
                .setOwner(owner)
                .setType(robotSubType)
                .setDescription(model.getDescription())
                .setUpdateDateTime(ZonedDateTime.now(ZoneId.of("UTC")))
                .setVersion(model.getVersion());

        if(!robot.getName().equals(model.getName())) {
            validateRobotName(model.getName(), edition);
        }

        robotRepository.update(robot);
    }

    public void delete(Guid guid) {
        LOG.info("Deleting {} [{}]", Robot.class.getCanonicalName(), guid);
        Robot robotToDelete = robotFinder.retrieveByGuid(guid).orElseThrow(DataAccessException::entityNotFound);

        checkExistingResults(robotToDelete);
        checkExistingAssignmentToCompetition(robotToDelete);
        robotRepository.delete(robotToDelete.getId());
    }

    private void checkExistingResults(Robot robot) {
        RobotType type = robot.getType().getRobotType();
        boolean isAssigned = false;
        switch(type) {
            case LINEFOLLOWER: {
                isAssigned = !lineFollowerResultFinder.retrieveAllByRobot(robot.getGuid()).isEmpty();
                break;
            }
            case SUMO: {
                isAssigned = !sumoMatchFinder.retrieveAllByRobot(robot.getGuid()).isEmpty();
                break;
            }
            case FREESTYLE: {
                isAssigned = !freestyleResultFinder.retrieveAllByRobot(robot.getGuid()).isEmpty();
                break;
            }
        }
        if(isAssigned) {
            throw RobotException.robotConnectedToResultOrMatchSchedule();
        }
    }

    private void checkExistingAssignmentToCompetition(Robot robot) {
        RobotType type = robot.getType().getRobotType();
        boolean isAssigned = false;
        switch(type) {
            case LINEFOLLOWER: {
                isAssigned = lineFollowerCompetitionFinder.retrieveByRobot(robot.getGuid()).isPresent();
                break;
            }
            case SUMO: {
                isAssigned = sumoCompetitionFinder.retrieveByRobot(robot.getGuid()).isPresent();
                break;
            }
            case FREESTYLE: {
                isAssigned = freestyleCompetitionFinder.retrieveByRobot(robot.getGuid()).isPresent();
                break;
            }
        }
        if(isAssigned) {
            throw RobotException.robotAssignedToCompetition();
        }
    }
}
