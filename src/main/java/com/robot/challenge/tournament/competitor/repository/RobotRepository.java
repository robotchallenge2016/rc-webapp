package com.robot.challenge.tournament.competitor.repository;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.ejb.Stateless;

/**
 * Created by mklys on 08/01/16.
 */
@Stateless
public class RobotRepository extends JpaRepository<Robot> {

    protected RobotRepository() {
        super(Robot.class);
    }
}
