package com.robot.challenge.tournament.competitor.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.competitor.domain.TeamFactory;
import com.robot.challenge.tournament.competitor.exception.TeamException;
import com.robot.challenge.tournament.competitor.repository.CompetitorFinder;
import com.robot.challenge.tournament.competitor.repository.CompetitorRepository;
import com.robot.challenge.tournament.competitor.repository.TeamFinder;
import com.robot.challenge.tournament.competitor.repository.TeamRepository;
import com.robot.challenge.tournament.competitor.rest.model.team.ExistingTeamModel;
import com.robot.challenge.tournament.competitor.rest.model.team.NewTeamModel;
import com.robot.challenge.tournament.domain.Tournament;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TeamService {

    private static final Logger LOG = LoggerFactory.getLogger(TeamService.class);

    private final TeamFactory teamFactory;
    private final TeamRepository teamRepository;
    private final TeamFinder teamFinder;
    private final CompetitorRepository competitorRepository;
    private final CompetitorFinder competitorFinder;

    @Inject
    public TeamService(TeamFactory teamFactory, TeamRepository teamRepository, TeamFinder teamFinder,
                       CompetitorRepository competitorRepository, CompetitorFinder competitorFinder) {
        this.teamFactory = teamFactory;
        this.teamRepository = teamRepository;
        this.teamFinder = teamFinder;
        this.competitorRepository = competitorRepository;
        this.competitorFinder = competitorFinder;
    }

    private void validateName(String name, Tournament tournament) {
        teamFinder.retrieveByName(name, tournament).ifPresent(systemUser -> {
            throw TeamException.nameAlreadyExists();
        });
    }

    private void validateCaptain(Guid captainGuid, Tournament tournament) {
        if(competitorFinder.isTeamCaptain(captainGuid, tournament)) {
            throw TeamException.captainAlreadyOccupied();
        }
    }

    @Transactional
    public Team create(NewTeamModel teamModel, Tournament tournament) {
        LOG.info("Creating {} [name={}]", Team.class.getCanonicalName(), teamModel.getName());
        Guid captainGuid = Guid.from(teamModel.getCaptainGuid());
        validateCaptain(captainGuid, tournament);
        validateName(teamModel.getName(), tournament);

        Competitor captain = competitorFinder.retrieveByGuid(captainGuid)
                .orElseThrow(() -> DataAccessException.entityNotFound(Competitor.class, captainGuid));

        Team team = teamFactory.create(captain)
                .setName(teamModel.getName());

        teamRepository.create(team);

        if(!teamModel.getCompetitorGuids().isEmpty()) {
            List<Guid> competitorGuids = teamModel.getCompetitorGuids().stream().map(Guid::from).collect(Collectors.toList());
            List<Competitor> competitors = competitorFinder.retrieveByGuids(competitorGuids);
            updateTeamInCompetitors(team, competitors);
        }

        return team;
    }

    private void updateTeamInCompetitors(Team team, List<Competitor> competitors) {
        competitors.stream().forEach(competitor -> {
            competitor.setTeam(team);
            competitorRepository.update(competitor);
        });
    }

    @Transactional
    public void update(Guid guid, ExistingTeamModel teamModel, Tournament tournament) {
        LOG.info("Updating {} [name={}, {}]", Team.class.getCanonicalName(), teamModel.getName(), guid);

        Team team = teamFinder.retrieveByGuid(guid).orElseThrow(() -> DataAccessException.entityNotFound(Team.class, guid))
                .setName(teamModel.getName())
                .setUpdateDateTime(ZonedDateTime.now(ZoneId.of("UTC")))
                .setVersion(teamModel.getVersion());

        if(!team.getName().equals(teamModel.getName())) {
            validateName(teamModel.getName(), tournament);
        }

        List<Competitor> oldCompetitors = competitorFinder.retrieveByTeam(team, tournament);

        List<Competitor> newCompetitors = new ArrayList<>();
        if(!teamModel.getCompetitorGuids().isEmpty()) {
            List<Guid> guids = teamModel.getCompetitorGuids().stream().map(Guid::from).collect(Collectors.toList());
            newCompetitors.addAll(competitorFinder.retrieveByGuids(guids));
        }

        performUpdate(team, oldCompetitors, newCompetitors);
    }

    private void performUpdate(Team team, List<Competitor> oldCompetitors, List<Competitor> newCompetitors) {
        teamRepository.update(team);
        oldCompetitors.stream().forEach(competitor -> {
            if(!competitor.isCaptainOfTeam()) {
                competitor.setTeam(null);
                competitorRepository.update(competitor);
            }
        });
        newCompetitors.stream().forEach(competitor -> {
            competitor.setTeam(team);
            competitorRepository.update(competitor);
        });
    }

    @Transactional
    public void delete(Guid guid) {
        LOG.info("Deleting {} [{}]", Team.class.getCanonicalName(), guid);
        teamFinder.retrieveByGuid(guid).map(entity -> {
            entity.getMembers().forEach(competitor -> {
                competitor.setTeam(null);
                competitor.setCaptainOfTeam(false);
                competitorRepository.update(competitor);
            });
            teamRepository.delete(entity.getId());
            return entity;
        }).orElseThrow(DataAccessException::entityNotFound);
    }
}
