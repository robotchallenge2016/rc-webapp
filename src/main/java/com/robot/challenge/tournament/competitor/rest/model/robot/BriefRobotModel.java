package com.robot.challenge.tournament.competitor.rest.model.robot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.rest.model.competitor.SimpleCompetitorModel;
import com.robot.challenge.tournament.dictionary.rest.model.RobotTypeModel;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BriefRobotModel {

    private String guid;
    private String name;
    private RobotTypeModel robotType;
    private SimpleCompetitorModel owner;

    private BriefRobotModel() {

    }

    public String getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public RobotTypeModel getRobotType() {
        return robotType;
    }

    public SimpleCompetitorModel getOwner() {
        return owner;
    }

    public static BriefRobotModel from(Robot robot) {
        BriefRobotModel model = new BriefRobotModel();
        model.guid = robot.getGuid().getValue();
        model.name = robot.getName();
        model.robotType = RobotTypeModel.from(robot.getType());
        model.owner = SimpleCompetitorModel.from(robot.getOwner());

        return model;
    }
}
