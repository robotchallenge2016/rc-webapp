package com.robot.challenge.tournament.competitor.rest.resource;

import com.robot.challenge.infrastructure.rest.model.GuidsModel;
import com.robot.challenge.infrastructure.rest.resource.CrudResource;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.system.file.rest.resource.SystemFileResource;
import com.robot.challenge.tournament.competitor.rest.model.robot.ExistingRobotModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.NewRobotModel;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface RobotResource extends CrudResource<NewRobotModel, ExistingRobotModel>, SystemFileResource {

    String DEFAULT_ROBOT_PREFIX = "/robot";
    String CREATE = "/";
    String UPDATE = "/guid/{guid}";
    String DELETE = "/guid/{guid}";
    String ROBOT_NAME_EXISTS = "/name/{name}/exists";
    String ROBOT_NAME_IS_FREE = "/name/{name}/free";
    String RETRIEVE_ROBOTS_BY_TYPE = "/type/guid/{guid}";
    String RETRIEVE_ROBOTS_BY_COMPETITOR = "/competitor/guid/{guid}";
    String RETRIEVE_ROBOTS_BY_TEAM = "/team/guid/{guid}";
    String RETRIEVE_SINGLE = "/guid/{guid}";
    String RETRIEVE_BY_GUIDS = "/guids";
    String RETRIEVE_ALL_BRIEFED = "/all/brief";
    String RETRIEVE_ALL_FULL = "/all/full";
    String SEARCH_MATCHING_ANY_PARAM = "/match/any";
    String SEARCH_BY_TYPE_AND_MATCHING_ANY_PARAM = "/type/guid/{guid}/match/any";

    @POST
    @Path(CREATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid NewRobotModel robot);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingRobotModel robot);

    @DELETE
    @Path(DELETE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(ROBOT_NAME_EXISTS)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response nameExists(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                        @PathParam("name") @NotNull @Size(min = 1) String name);

    @GET
    @Path(ROBOT_NAME_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response nameIsFree(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                        @PathParam("name") @NotNull @Size(min = 1) String name);

    @GET
    @Path(RETRIEVE_ROBOTS_BY_COMPETITOR)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByCompetitor(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                  @PathParam("guid") @NotNull @Size(min = 1) String competitorGuid);

    @GET
    @Path(RETRIEVE_ROBOTS_BY_TEAM)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByTeam(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String teamGuid);

    @GET
    @Path(RETRIEVE_ROBOTS_BY_TYPE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response retrieveByType(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull String robotTypeGuid);

    @GET
    @Path(RETRIEVE_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @POST
    @Path(RETRIEVE_BY_GUIDS)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByGuids(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                             @Valid GuidsModel guidsModel);

    @GET
    @Path(RETRIEVE_ALL_BRIEFED)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response retrieveAllBriefed(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);

    @GET
    @Path(RETRIEVE_ALL_FULL)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response retrieveAllFull(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                             @QueryParam("limit") Integer limit, @QueryParam("offset") Integer offset);

    @GET
    @Path(SEARCH_MATCHING_ANY_PARAM)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE })
    Response searchMatchingAnyParam(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                    @QueryParam(value = "search") @NotNull @Size(min = 1) String search);

    @GET
    @Path(SEARCH_BY_TYPE_AND_MATCHING_ANY_PARAM)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response searchByTypeMatchingAnyParam(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                          @PathParam("guid") @NotNull @Size(min = 1) String guid,
                                          @QueryParam(value = "search") @NotNull @Size(min = 1) String search);
}
