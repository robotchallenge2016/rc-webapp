package com.robot.challenge.tournament.competitor.rest.resource;

import com.robot.challenge.infrastructure.rest.resource.CrudResource;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.tournament.competitor.rest.model.team.ExistingTeamModel;
import com.robot.challenge.tournament.competitor.rest.model.team.NewTeamModel;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface TeamResource extends CrudResource<NewTeamModel, ExistingTeamModel> {
    String TEAM_PREFIX = "/team";
    String CREATE = "/";
    String UPDATE = "/guid/{guid}";
    String DELETE = "/guid/{guid}";
    String SEARCH_MATCHING_ANY_PARAM = "/match/any";
    String TEAM_NAME_EXISTS = "/name/{name}/exists";
    String TEAM_NAME_IS_FREE = "/name/{name}/free";
    String RETRIEVE_BY_COMPETITOR = "/competitor/guid/{guid}";
    String RETRIEVE_SINGLE = "/guid/{guid}";
    String RETRIEVE_ALL = "/all";

    @POST
    @Path(CREATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid NewTeamModel team);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingTeamModel team);

    @DELETE
    @Path(DELETE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(SEARCH_MATCHING_ANY_PARAM)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response searchMatchingAnyParam(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                    @QueryParam(value = "search") @NotNull @Size(min = 1) String search);

    @GET
    @Path(TEAM_NAME_EXISTS)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response teamNameExists(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("name") @NotNull @Size(min = 1) String name);

    @GET
    @Path(TEAM_NAME_IS_FREE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response teamNameIsFree(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("name") @NotNull @Size(min = 1) String name);

    @GET
    @Path(RETRIEVE_BY_COMPETITOR)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(value = { SystemRole.ADMIN_VALUE, SystemRole.USER_VALUE, SystemRole.COMPETITOR_VALUE })
    Response retrieveByCompetitor(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                                  @PathParam("guid") @NotNull @Size(min = 1) String competitorGuid);

    @GET
    @Path(RETRIEVE_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response retrieveAll(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);
}
