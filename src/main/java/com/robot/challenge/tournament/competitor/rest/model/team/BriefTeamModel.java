package com.robot.challenge.tournament.competitor.rest.model.team;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.tournament.competitor.domain.Competitor;
import com.robot.challenge.tournament.competitor.domain.Robot;
import com.robot.challenge.tournament.competitor.domain.Team;
import com.robot.challenge.tournament.competitor.rest.model.competitor.SimpleCompetitorModel;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BriefTeamModel extends SimpleTeamModel {

    private Integer teamMembersCount;
    private Integer robotsCount;

    private BriefTeamModel() {

    }

    private BriefTeamModel(String guid, String name, SimpleCompetitorModel captain) {
        super(guid, name, captain);
    }

    public Integer getTeamMembersCount() {
        return teamMembersCount;
    }

    public Integer getRobotsCount() {
        return robotsCount;
    }

    public static BriefTeamModel from(Team team, List<Competitor> members, List<Robot> robots) {
        BriefTeamModel model = new BriefTeamModel(team.getGuid().getValue(), team.getName(), SimpleCompetitorModel.from(team.getCaptain()));
        model.teamMembersCount = members.size();
        model.robotsCount = robots.size();

        return model;
    }
}
