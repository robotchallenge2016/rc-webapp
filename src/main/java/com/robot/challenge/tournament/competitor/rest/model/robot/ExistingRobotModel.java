package com.robot.challenge.tournament.competitor.rest.model.robot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.tournament.competitor.domain.Robot;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by mklys on 08/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingRobotModel implements GloballyUniqueModel, VersionedModel {
    @NotNull
    @Size(min = 1)
    private String name;
    private String guid;
    @NotNull
    @Size(min = 1)
    private String robotTypeGuid;
    @NotNull
    @Size(min = 1)
    private String ownerGuid;
    private String description;
    private String robotImagePath;
    @NotNull
    @Min(value = 0L)
    private Long version;

    public String getName() {
        return name;
    }

    public String getGuid() {
        return guid;
    }

    public String getOwnerGuid() {
        return ownerGuid;
    }

    public String getDescription() {
        return description;
    }

    public String getRobotImagePath() {
        return robotImagePath;
    }

    public Long getVersion() {
        return version;
    }

    public String getRobotTypeGuid() {
        return robotTypeGuid;
    }

    public static ExistingRobotModel from(Robot robot) {
        ExistingRobotModel model = new ExistingRobotModel();
        model.name = robot.getName();
        model.guid = robot.getGuid().getValue();
        model.ownerGuid = robot.getOwner().getGuid().getValue();
        model.robotTypeGuid = robot.getType().getGuid().getValue();
        model.description = robot.getDescription();
        model.robotImagePath = robot.getRobotImagePath();
        model.version = robot.getVersion();

        return model;
    }

    public static ExistingRobotModelBuilder builder() {
        return new ExistingRobotModelBuilder();
    }

    public static class ExistingRobotModelBuilder {
        private final ExistingRobotModel model = new ExistingRobotModel();

        public ExistingRobotModelBuilder setName(String name) {
            model.name = name;
            return this;
        }

        public ExistingRobotModelBuilder setOwnerGuid(String ownerGuid) {
            model.ownerGuid = ownerGuid;
            return this;
        }

        public ExistingRobotModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingRobotModelBuilder setType(String competitionTypeGuid){
            model.robotTypeGuid = competitionTypeGuid;
            return this;
        }

        public ExistingRobotModelBuilder setDescription(String description){
            model.description = description;
            return this;
        }

        public ExistingRobotModel build() {
            return model;
        }
    }
}
