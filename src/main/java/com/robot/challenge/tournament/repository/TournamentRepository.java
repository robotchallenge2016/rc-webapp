package com.robot.challenge.tournament.repository;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.domain.Tournament;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;

@Stateless
public class TournamentRepository extends JpaRepository<Tournament> {

    private static final Logger LOG = LoggerFactory.getLogger(TournamentRepository.class);

    public TournamentRepository() {
        super(Tournament.class);
    }
}
