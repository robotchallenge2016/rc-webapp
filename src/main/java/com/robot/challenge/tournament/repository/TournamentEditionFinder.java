package com.robot.challenge.tournament.repository;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.domain.TournamentEdition;
import com.robot.challenge.tournament.domain.TournamentEditionFactory;
import com.robot.challenge.tournament.rest.model.TournamentDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class TournamentEditionFinder extends JpaFinder<TournamentEdition> {

    private static final Logger LOG = LoggerFactory.getLogger(TournamentEditionFinder.class);

    @Inject
    public TournamentEditionFinder(TournamentEditionFactory factory) {
        super(factory);
    }

    public List<TournamentEdition> retrieveByCompetitor(@NotNull Guid competitorGuid) {
        LOG.info("Retrieving {} by login", TournamentEdition.class);
        TypedQuery<TournamentEdition> q = entityManager().createQuery(
        "SELECT t FROM TournamentEdition t " +
                "LEFT JOIN FETCH t.details td " +
                "LEFT JOIN FETCH td.robots tdc " +
                "WHERE tdc.owner.systemUser.guid = :ownerGuid " +
                "ORDER BY t.createDate DESC", TournamentEdition.class);
        q.setParameter("ownerGuid", competitorGuid);
        return q.getResultList();
    }

    public List<TournamentEdition> retrieveByDomain(TournamentDomain domain) {
        LOG.info("Retrieving {} by domain", TournamentEdition.class);
        TypedQuery<Tournament> q = entityManager().createQuery(
        "SELECT t FROM Tournament t " +
                "LEFT JOIN FETCH t.editions ted " +
                "WHERE t.domain = :domain " +
                "ORDER BY ted.createDate DESC", Tournament.class);
        q.setParameter("domain", domain.getDomain());
        try {
            return q.getSingleResult().getEditions();
        } catch (NoResultException ex) {
            return Collections.emptyList();
        }
    }

    public Optional<TournamentEdition> retrieveBySuffix(@NotNull Tournament tournament, @NotNull @Size(min = 1) String suffix) {
        LOG.info("Retrieving {} by domain", TournamentEdition.class);
        TypedQuery<TournamentEdition> q = entityManager().createQuery(
                "SELECT t FROM TournamentEdition t " +
                        "WHERE t.suffix = :suffix AND t.tournament = :tournament", TournamentEdition.class);
        q.setParameter("suffix", suffix);
        q.setParameter("tournament", tournament);
        try {
            return Optional.of(q.getSingleResult());
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }
}
