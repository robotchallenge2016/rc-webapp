package com.robot.challenge.tournament.repository;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.domain.TournamentFactory;
import com.robot.challenge.tournament.rest.model.TournamentDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;

public class TournamentFinder extends JpaFinder<Tournament> {

    private static final Logger LOG = LoggerFactory.getLogger(TournamentFinder.class);

    @Inject
    public TournamentFinder(TournamentFactory factory) {
        super(factory);
    }

    public List<Tournament> retrieveByOwner(@NotNull Guid ownerGuid) {
        LOG.info("Retrieving {} by login", Tournament.class);
        TypedQuery<Tournament> q = entityManager().createQuery("SELECT t FROM Tournament t " +
                "LEFT JOIN FETCH t.owner to " +
                "WHERE to.guid = :ownerGuid", Tournament.class);
        q.setParameter("ownerGuid", ownerGuid);
        return q.getResultList();
    }

    public Optional<Tournament> retrieveByName(@NotNull @Size(min = 1) String name) {
        LOG.info("Retrieving {} by name", Tournament.class);
        TypedQuery<Tournament> q = entityManager().createQuery(
                "SELECT t FROM Tournament t " +
                        "WHERE t.name = :name", Tournament.class);
        q.setParameter("name", name);
        try {
            return Optional.of(q.getSingleResult());
        } catch(NoResultException ex) {
            return Optional.empty();
        }
    }


    public Optional<Tournament> retrieveByDomain(@NotNull @Size(min = 1) String domain) {
        LOG.info("Retrieving {} by domain", Tournament.class);
        TypedQuery<Tournament> q = entityManager().createQuery(
                "SELECT t FROM Tournament t " +
                        "WHERE t.domain = :domain", Tournament.class);
        q.setParameter("domain", domain);
        try {
            return Optional.of(q.getSingleResult());
        } catch(NoResultException ex) {
            return Optional.empty();
        }
    }

    public Optional<Tournament> retrieveByDomain(TournamentDomain domain) {
        return retrieveByDomain(domain.getDomain());
    }
}
