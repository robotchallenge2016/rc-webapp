package com.robot.challenge.tournament.repository;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.domain.TournamentEdition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;

@Stateless
public class TournamentEditionRepository extends JpaRepository<TournamentEdition> {

    private static final Logger LOG = LoggerFactory.getLogger(TournamentEditionRepository.class);

    public TournamentEditionRepository() {
        super(TournamentEdition.class);
    }
}
