package com.robot.challenge.infrastructure.component;

import java.util.UUID;

/**
 * Created by mklys on 21/02/16.
 */
public enum SecretGenerator {
    INSTANCE;

    public String generate() {
        return UUID.randomUUID().toString();
    }
}
