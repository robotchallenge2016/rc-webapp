package com.robot.challenge.infrastructure.service;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

/**
 * Created by mklys on 30/09/16.
 */
public class MailTemplateResolver {

    private final VelocityEngine engine;

    public MailTemplateResolver() {
        initVelocity();
        this.engine = initVelocityEngine();
    }

    private void initVelocity() {
        Velocity.setProperty(Velocity.RESOURCE_LOADER, "string");
        Velocity.addProperty("string.resource.loader.class", StringResourceLoader.class.getName());
        Velocity.addProperty("string.resource.loader.modificationCheckInterval", "1");
        Velocity.init();
    }

    private VelocityEngine initVelocityEngine() {
        VelocityEngine engine = new VelocityEngine();
        engine.setProperty(Velocity.RESOURCE_LOADER, "string");
        engine.addProperty("string.resource.loader.class", StringResourceLoader.class.getName());
        engine.addProperty("string.resource.loader.repository.static", "false");
        engine.addProperty("string.resource.loader.modificationCheckInterval", "1");
        engine.init();

        return engine;
    }

    private StringResourceRepository provideRepository() {
        return (StringResourceRepository)engine.getApplicationAttribute(StringResourceLoader.REPOSITORY_NAME_DEFAULT);
    }

    protected String render(VelocityContext context, Template template)
    {
        StringWriter out = new StringWriter();
        template.merge(context, out);
        return out.toString();
    }

    public String resolve(String template, VelocityContext context) {
        StringResourceRepository repo = provideRepository();
        repo.putStringResource("mail", template);

        return render(context, engine.getTemplate("mail", StandardCharsets.UTF_8.toString()));
    }
}
