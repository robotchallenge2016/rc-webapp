package com.robot.challenge.infrastructure.service;

import com.robot.challenge.tournament.competitor.domain.Competitor;
import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by mklys on 30/09/16.
 */
public abstract class RcMail implements Mail {

    private static final Logger LOG = LoggerFactory.getLogger(RcMail.class);

    private final String sender;
    private final String contentType = "text/html; charset=utf-8";

    private final Competitor competitor;

    protected RcMail(Competitor competitor) {
        MailAuthenticationProperties mailAuthenticationProperties = ConfigFactory.create(MailAuthenticationProperties.class);
        this.sender = mailAuthenticationProperties.login();
        this.competitor = competitor;
    }

    protected abstract String getSubject();
    protected abstract String getContent();

    @Override
    public Message toMessage(Session session) {
        LOG.info("Preparing mail message");
        Message message = new MimeMessage(session);
        try {
            message.setContent(getContent(), contentType);
            message.setFrom(new InternetAddress(sender));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(competitor.getLogin()));
            message.setSubject(getSubject());
        } catch (MessagingException ex) {
            LOG.warn(ex.getMessage());
        }
        return message;
    }
}
