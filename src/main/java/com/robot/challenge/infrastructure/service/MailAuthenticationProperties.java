package com.robot.challenge.infrastructure.service;

import org.aeonbits.owner.Accessible;
import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

@Config.Sources(value = {"classpath:mail-authentication.properties"})
public interface MailAuthenticationProperties extends Reloadable, Accessible {
    @Key("mail.login")
    String login();
    @Key("mail.password")
    String password();
}
