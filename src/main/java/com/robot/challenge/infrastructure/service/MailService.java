package com.robot.challenge.infrastructure.service;

import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MailService {

    private static final Logger LOG = LoggerFactory.getLogger(MailService.class);
    private final Session session;

    @Inject
    public MailService() {
        LOG.info("Preparing mail settings");
        Map<String, String> bridge = new HashMap<>();

        MailProperties mailProperties = ConfigFactory.create(MailProperties.class);
        mailProperties.fill(bridge);

        Properties properties = new Properties();
        properties.putAll(bridge);

        LOG.info("Preparing mail authentication");
        MailAuthenticationProperties authenticationProperties = ConfigFactory.create(MailAuthenticationProperties.class);
        session = Session.getInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(authenticationProperties.login(),authenticationProperties.password());
            }
        });
    }

    public void sendMail(List<? extends Mail> mails) {
        mails.forEach(this::sendMail);
    }

    public void sendMail(Mail mail) {
        LOG.info("Sending...");
        try {
            Transport.send(mail.toMessage(session));
            LOG.info("Done");
        } catch (MessagingException ex) {
            LOG.warn(ex.getMessage(), ex);
        }
    }
}
