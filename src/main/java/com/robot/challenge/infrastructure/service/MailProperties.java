package com.robot.challenge.infrastructure.service;

import org.aeonbits.owner.Accessible;
import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

@Config.Sources(value = {"classpath:mail.properties"})
public interface MailProperties extends Reloadable, Accessible {
    @Key("mail.smtp.user")
    String user();
    @Key("mail.smtp.host")
    String host();
    @Key("mail.smtp.port")
    Integer port();
    @Key("mail.smtp.socketFactory.port")
    Integer sockerFactoryPort();
    @Key("mail.smtp.socketFactory.class")
    String socketClass();
    @Key("mail.smtp.ssl.enable")
    boolean sslEnabled();
    @Key("mail.smtp.auth")
    boolean authEnabled();
    @Key("mail.debug")
    boolean debugEnabled();
}
