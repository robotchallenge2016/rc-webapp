package com.robot.challenge.infrastructure.service;

import com.robot.challenge.tournament.competitor.domain.Competitor;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created by mklys on 30/09/16.
 */
public class MailPlaceholderProvider {

    private static final Logger LOG = LoggerFactory.getLogger(MailPlaceholderProvider.class);

    public static final String HOST = "host";
    public static final String SCHEME = "scheme";
    public static final String LOGIN = "login";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String VERIFICATION_PATH = "verificationPath";

    private final HttpServletRequest request;

    public MailPlaceholderProvider(HttpServletRequest request) {
        this.request = request;
    }

    public VelocityContext toVelocityContext(Competitor competitor) {
        String host = request.getHeader("X-Remote-Host");
        String scheme = request.getHeader("X-Remote-Scheme");

        VelocityContext context = new VelocityContext();
        context.put(HOST, host);
        context.put(SCHEME, scheme);
        context.put(LOGIN, competitor.getLogin());
        context.put(FIRST_NAME, competitor.getFirstName());
        context.put(LAST_NAME, competitor.getLastName());
        try {
            String verificationPath = String.format("/panel/login/#verification?hash=%s",
                    URLEncoder.encode(competitor.getRegistrationInfo().getHash(), StandardCharsets.UTF_8.toString()));
            context.put(VERIFICATION_PATH, verificationPath);
        } catch (UnsupportedEncodingException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return context;
    }
}
