package com.robot.challenge.infrastructure.service;

import javax.mail.Message;
import javax.mail.Session;

/**
 * Created by mklys on 24/02/16.
 */
public interface Mail {
    Message toMessage(Session session);
}
