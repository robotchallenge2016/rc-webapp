package com.robot.challenge.infrastructure.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mklys on 13/02/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GuidsModel {

    @NotNull
    @Size(min = 1)
    private List<String> guids = new ArrayList<>();

    public List<String> getGuids() {
        return guids;
    }

    public static GuidsModelBuilder builder() {
        return new GuidsModelBuilder();
    }

    public static class GuidsModelBuilder {
        private final GuidsModel guidsModel = new GuidsModel();

        public GuidsModelBuilder addGuid(String guid) {
            guidsModel.guids.add(guid);
            return this;
        }

        public GuidsModel build() {
            return guidsModel;
        }
    }
}
