package com.robot.challenge.infrastructure.rest.model;

/**
 * Created by mklys on 31/01/16.
 */
public interface GloballyUniqueModel {
    String getGuid();
}
