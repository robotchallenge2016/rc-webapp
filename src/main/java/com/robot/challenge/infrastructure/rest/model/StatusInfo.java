package com.robot.challenge.infrastructure.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * Created by mklys on 30/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatusInfo {
    private static final String DOCKER_TAG = "Docker-tag";
    private static final String BUILD_JDK = "Build-Jdk";
    private static final String ARTIFACT_ID = "Implementation-Title";
    private static final String VERSION = "Implementation-Version";
    private static final String BUILD_TIME = "Build-time";
    private static final String SCM_BRANCH = "SCM-branch";
    private static final String SCM_DETAILS = "SCM-Details";

    @JsonProperty
    private String dockerTag;
    @JsonProperty
    private String buildJdk;
    @JsonProperty
    private String artifactId;
    @JsonProperty
    private String version;
    @JsonProperty
    private String buildTime;
    @JsonProperty
    private String scmBranch;
    @JsonProperty
    private String scmDetails;

    private StatusInfo() {

    }

    public static StatusInfo of(Manifest manifest) {
        Attributes attributes = manifest.getMainAttributes();

        StatusInfo info = new StatusInfo();
        info.dockerTag = attributes.getValue(DOCKER_TAG);
        info.buildJdk = attributes.getValue(BUILD_JDK);
        info.artifactId = attributes.getValue(ARTIFACT_ID);
        info.version = attributes.getValue(VERSION);
        info.buildTime = attributes.getValue(BUILD_TIME);
        info.scmBranch = attributes.getValue(SCM_BRANCH);
        info.scmDetails = attributes.getValue(SCM_DETAILS);

        return info;
    }
}
