package com.robot.challenge.infrastructure.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.domain.GloballyUnique;

/**
 * Created by mklys on 31/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelCreated {

    private String guid;

    private ModelCreated() {

    }

    public String getGuid() {
        return guid;
    }

    public static ModelCreated from(GloballyUnique unique) {
        ModelCreated model =  new ModelCreated();
        model.guid = unique.getGuid().getValue();

        return model;
    }
}
