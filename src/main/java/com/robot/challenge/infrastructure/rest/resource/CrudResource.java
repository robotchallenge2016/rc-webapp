package com.robot.challenge.infrastructure.rest.resource;

import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

public interface CrudResource<N, E extends GloballyUniqueModel & VersionedModel> {

    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid N user);
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid E user);
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);
}
