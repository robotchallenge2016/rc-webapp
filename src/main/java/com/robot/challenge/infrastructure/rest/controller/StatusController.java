package com.robot.challenge.infrastructure.rest.controller;

import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.StatusInfo;
import com.robot.challenge.infrastructure.rest.resource.StatusResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.jar.Manifest;

/**
 * Created by mklys on 30/01/16.
 */
@Path("/")
public class StatusController implements StatusResource {
    private static final Logger LOG = LoggerFactory.getLogger(StatusController.class);
    private static final String MANIFEST_FILE = "/META-INF/MANIFEST.MF";

    @Inject
    private ServletContext servletContext;
    private Optional<StatusInfo> statusInfo;

    @PostConstruct
    public void setup() {
        this.statusInfo = generateStatusInfo(servletContext);
    }

    @Override
    public Response status() {
        LOG.debug("StatusInfo request received");
        return Response.status(Response.Status.OK).entity(statusInfo.orElseThrow(DataAccessException::invalidManifest)).build();
    }

    private Optional<StatusInfo> generateStatusInfo(ServletContext servletContext) {
        try {
            Manifest manifest = new Manifest(getManifestStream(servletContext));
            LOG.info("MANIFEST.MF read correctly");
            StatusInfo statusInfo = StatusInfo.of(manifest);
            return Optional.of(statusInfo);
        } catch (IOException | NullPointerException ex) {
            LOG.warn("Could not read MANIFEST.MF: {}", ex.getMessage());
            return Optional.empty();
        }
    }

    private InputStream getManifestStream(ServletContext servletContext) {
        return servletContext.getResourceAsStream(MANIFEST_FILE);
    }
}
