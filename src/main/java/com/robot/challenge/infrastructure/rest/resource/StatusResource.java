package com.robot.challenge.infrastructure.rest.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mklys on 30/01/16.
 */
public interface StatusResource {

    String STATUS_ENDPOINT = "/status";

    @GET
    @Path(value = STATUS_ENDPOINT)
    @Produces(MediaType.APPLICATION_JSON)
    Response status();
}
