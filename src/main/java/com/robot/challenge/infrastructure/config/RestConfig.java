package com.robot.challenge.infrastructure.config;

import com.robot.challenge.infrastructure.exception.mappers.ExceptionMapper;
import com.robot.challenge.infrastructure.exception.mappers.LockingExceptionMapper;
import com.robot.challenge.infrastructure.exception.mappers.RcExceptionMapper;
import com.robot.challenge.infrastructure.exception.mappers.ValidationExceptionMapper;
import com.robot.challenge.infrastructure.exception.mappers.WebApplicationExceptionMapper;
import com.robot.challenge.infrastructure.filter.CORSFilter;
import com.robot.challenge.infrastructure.rest.controller.StatusController;
import com.robot.challenge.system.authn.rest.controller.AuthenticationController;
import com.robot.challenge.system.dictionary.rest.controller.SystemDictionaryController;
import com.robot.challenge.system.user.rest.controller.SystemUserController;
import com.robot.challenge.tournament.competition.rest.controller.CompetitionController;
import com.robot.challenge.tournament.competition.rest.controller.FreestyleCompetitionController;
import com.robot.challenge.tournament.competition.rest.controller.LineFollowerCompetitionController;
import com.robot.challenge.tournament.competition.rest.controller.SumoCompetitionController;
import com.robot.challenge.tournament.competition.rest.controller.result.FreestyleResultController;
import com.robot.challenge.tournament.competition.rest.controller.result.LineFollowerResultController;
import com.robot.challenge.tournament.competition.rest.controller.result.RankingController;
import com.robot.challenge.tournament.competition.rest.controller.result.SumoMatchController;
import com.robot.challenge.tournament.competitor.rest.controller.CompetitorController;
import com.robot.challenge.tournament.competitor.rest.controller.RobotController;
import com.robot.challenge.tournament.competitor.rest.controller.TeamController;
import com.robot.challenge.tournament.dictionary.rest.controller.TournamentDictionaryController;
import com.robot.challenge.tournament.registration.rest.controller.ReceptionController;
import com.robot.challenge.tournament.registration.rest.controller.RegistrationController;
import com.robot.challenge.tournament.registration.rest.controller.VerificationController;
import com.robot.challenge.tournament.rest.controller.MailController;
import com.robot.challenge.tournament.rest.controller.TournamentContextController;
import com.robot.challenge.tournament.rest.controller.TournamentController;
import com.robot.challenge.tournament.rest.controller.TournamentEditionController;
import com.robot.challenge.website.rest.controller.NoticeController;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/" + RestConfig.ROOT_CONTEXT)
public class RestConfig extends Application {

    public static final String ROOT_CONTEXT = "v1";

    private Set<Class<?>> classes = new HashSet<>();

    public RestConfig() {
        classes.add(AuthenticationController.class);
        classes.add(CompetitionController.class);
        classes.add(CompetitorController.class);
        classes.add(FreestyleCompetitionController.class);
        classes.add(FreestyleResultController.class);
        classes.add(LineFollowerCompetitionController.class);
        classes.add(LineFollowerResultController.class);
        classes.add(MailController.class);
        classes.add(NoticeController.class);
        classes.add(RankingController.class);
        classes.add(ReceptionController.class);
        classes.add(RegistrationController.class);
        classes.add(RobotController.class);
        classes.add(StatusController.class);
        classes.add(SumoCompetitionController.class);
        classes.add(SumoMatchController.class);
        classes.add(SystemDictionaryController.class);
        classes.add(SystemUserController.class);
        classes.add(TeamController.class);
        classes.add(TournamentController.class);
        classes.add(TournamentContextController.class);
        classes.add(TournamentEditionController.class);
        classes.add(TournamentDictionaryController.class);
        classes.add(VerificationController.class);

        classes.add(CORSFilter.class);

        classes.add(ExceptionMapper.class);
        classes.add(LockingExceptionMapper.class);
        classes.add(RcExceptionMapper.class);
        classes.add(WebApplicationExceptionMapper.class);
        classes.add(ValidationExceptionMapper.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }
}
