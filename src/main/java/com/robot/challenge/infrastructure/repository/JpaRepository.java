package com.robot.challenge.infrastructure.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

public class JpaRepository<T> {
    private static final Logger LOG = LoggerFactory.getLogger(JpaRepository.class);

    private final Class<T> clazz;

    @PersistenceContext(name = "PostgresqlPersistenceUnit")
    private EntityManager em;
    private CriteriaBuilder cb;

    protected JpaRepository(Class<T> clazz) {
        this.clazz = clazz;
    }

    @PostConstruct
    public void setup() {
        cb = em.getCriteriaBuilder();
    }

    protected EntityManager entityManager() {
        return em;
    }

    protected CriteriaBuilder criteriaBuilder() {
        return cb;
    }

    public void create(T entity) {
        LOG.debug("Creating {}", clazz.getName());
        em.persist(entity);
    }

    public T update(T entity) {
        LOG.debug("Updating {}", clazz.getName());
        return em.merge(entity);
    }

    public boolean delete(Long id) {
        LOG.debug("Removing {}", clazz.getName());
        return retrieveById(id).map(entity ->
                {
                    em.remove(entity);
                    return true;
                }
        ).orElse(false);
    }

    public Optional<T> retrieveById(Long id) {
        LOG.debug("Retrieving {} by id", clazz.getName());
        T competitor = em.find(clazz, id);
        return Optional.ofNullable(competitor);
    }
}
