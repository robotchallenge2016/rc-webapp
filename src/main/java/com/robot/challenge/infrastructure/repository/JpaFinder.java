package com.robot.challenge.infrastructure.repository;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class JpaFinder<T> {
    private static final Logger LOG = LoggerFactory.getLogger(JpaRepository.class);

    @PersistenceContext(name = "PostgresqlPersistenceUnit")
    private EntityManager em;
    private CriteriaBuilder cb;
    private final Factory<T> factory;
    private final Class<T> clazz;

    @PostConstruct
    public void setup() {
        cb = em.getCriteriaBuilder();
    }

    public JpaFinder(Factory<T> factory) {
        this.factory = factory;
        this.clazz = factory.getClazz();
    }

    protected EntityManager entityManager() {
        return em;
    }

    protected CriteriaBuilder criteriaBuilder() {
        return cb;
    }

    protected Class<T> target() {
        return clazz;
    }

    protected Optional<T> single(TypedQuery<T> query) {
        try {
            return Optional.of(query.getSingleResult()).map(factory.decorator());
        } catch (NoResultException ex) {
            LOG.warn("Query for {} returned no result", clazz);
            return Optional.empty();
        }
    }

    protected List<T> list(TypedQuery<T> query) {
        return query.getResultList().stream().map(factory.decorator()).collect(Collectors.toList());
    }

    public Optional<T> retrieveByGuid(@NotNull Guid guid) {
        LOG.debug("Retrieving {} by guid", clazz.getName());
        String query = "SELECT e FROM " + clazz.getName() + " e WHERE e.guid = :guid";
        return single(em.createQuery(query, clazz).setParameter("guid", guid));
    }

    protected Predicate alwaysTrue() {
        return criteriaBuilder().and();
    }

    protected Predicate alwaysFalse() {
        return criteriaBuilder().or();
    }
}
