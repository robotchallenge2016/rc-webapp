package com.robot.challenge.infrastructure.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class JsonDeserializer {

    private static final Logger LOG = LoggerFactory.getLogger(JsonDeserializer.class);
    private final ObjectMapper mapper = new ObjectMapper();

    public JsonDeserializer failOnUnknownProperties(boolean value) {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, value);
        return this;
    }

    public <X> X deserialize(String json, Class<X> cls) throws IOException {
        Object obj = mapper.readValue(json, cls);
        return cls.cast(obj);
    }

    public <X> Optional<X> deserializeToOptional(String json, Class<X> cls) {
        Object obj = null;
        try {
            obj = mapper.readValue(json, cls);
            return Optional.of(cls.cast(obj));
        } catch (IOException ex) {
            LOG.warn(ex.getMessage());
            return Optional.empty();
        }
    }

    public Map<String, Object> deserializeToMap(String json) throws IOException {
        return mapper.readValue(json, new TypeReference<HashMap<String,Object>>(){});
    }
}