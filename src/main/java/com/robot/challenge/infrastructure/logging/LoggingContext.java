package com.robot.challenge.infrastructure.logging;

import com.google.common.collect.Sets;
import org.slf4j.MDC;

import java.util.Set;

public class LoggingContext implements AutoCloseable {
    private Set<String> contextParams = Sets.newHashSet();

    public void put(String key, String value) {
        contextParams.add(key);
        MDC.put(key, value);
    }

    public static LoggingContext getContext() {
        return new LoggingContext();
    }

    @Override
    public void close() {
        contextParams.forEach(MDC::remove);
    }
}