package com.robot.challenge.infrastructure.exception;

import com.robot.challenge.infrastructure.domain.Guid;

import javax.ws.rs.core.Response;

public class DataAccessException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {
        INVALID_MANIFEST("MANIFEST.MF is corrupted or does not exist"),
        ENTITY_NOT_FOUND("Requested entity not found");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public DataAccessException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(status, code, description);
    }

    public static DataAccessException entityNotFound() {
        return new DataAccessException(Response.Status.NOT_FOUND.getStatusCode(), Cause.ENTITY_NOT_FOUND, Cause.ENTITY_NOT_FOUND.getErrorDescription());
    }

    public static DataAccessException entityNotFound(String description) {
        return new DataAccessException(Response.Status.NOT_FOUND.getStatusCode(), Cause.ENTITY_NOT_FOUND, description);
    }

    public static DataAccessException entityNotFound(Class<?> clazz, Guid guid) {
        return new DataAccessException(Response.Status.NOT_FOUND.getStatusCode(), Cause.ENTITY_NOT_FOUND, String.format("%s [%s]", clazz.getCanonicalName(), guid));
    }

    public static DataAccessException invalidManifest() {
        return new DataAccessException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), Cause.INVALID_MANIFEST, Cause.INVALID_MANIFEST.getErrorDescription());
    }
}
