package com.robot.challenge.infrastructure.exception.mappers;

import com.robot.challenge.infrastructure.exception.RcException;
import com.robot.challenge.infrastructure.exception.model.Problem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RcExceptionMapper implements ExceptionMapper<RcException> {

    private static final Logger LOG = LoggerFactory.getLogger(RcException.class);

    @Override
    public Response toResponse(RcException ex) {
        LOG.warn("{} [incidentId={}] : {}", ex.getCode().toString().toLowerCase(), ex.getIncidentId(), ex.getMessage());
        return Response.status(ex.getStatus())
                .type(MediaType.APPLICATION_JSON)
                .entity(Problem.build(ex.getCode().toString().toLowerCase(), ex.getMessage(), ex.getIncidentId()))
                .build();
    }
}
