package com.robot.challenge.infrastructure.exception;

import javax.ws.rs.core.Response;
import java.util.UUID;

public class RcException extends RuntimeException {

    private final int status;
    private final UUID incidentId;

    private final com.robot.challenge.infrastructure.exception.Cause code;

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {
        SERVER_ERROR("Unknown server error occurred");

        private String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public RcException(int status, com.robot.challenge.infrastructure.exception.Cause code, String description) {
        super(description);
        this.status = status;
        this.code = code;
        this.incidentId = UUID.randomUUID();
    }

    public int getStatus() {
        return status;
    }

    public UUID getIncidentId() {
        return incidentId;
    }

    public com.robot.challenge.infrastructure.exception.Cause getCode() {
        return code;
    }

    public static RcException serverError(String exceptionMessage) {
        return new RcException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), Cause.SERVER_ERROR, exceptionMessage);
    }
}
