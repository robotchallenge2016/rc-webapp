package com.robot.challenge.infrastructure.exception;

public interface Cause {
    String getErrorDescription();
}