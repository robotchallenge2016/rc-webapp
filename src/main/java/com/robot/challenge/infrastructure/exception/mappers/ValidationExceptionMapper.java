package com.robot.challenge.infrastructure.exception.mappers;

import com.robot.challenge.infrastructure.exception.RcException;
import com.robot.challenge.infrastructure.exception.RequestException;
import com.robot.challenge.infrastructure.exception.model.Problem;
import org.jboss.resteasy.api.validation.ResteasyViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.UUID;
import java.util.stream.Collectors;

@Provider
public class ValidationExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<ValidationException> {

    private static final Logger LOG = LoggerFactory.getLogger(RcException.class);

    @Override
    public Response toResponse(ValidationException ex) {
        UUID incidentId = UUID.randomUUID();

        Response.ResponseBuilder response = Response.status(HttpServletResponse.SC_BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON);

        if(ex instanceof ResteasyViolationException) {
            ResteasyViolationException originalEx = (ResteasyViolationException) ex;

            String details = originalEx.getParameterViolations().stream().map(cv -> "[" + cv.getPath() + "] : " + cv.getMessage()).collect(Collectors.joining(", "));
            LOG.warn("{} [incidentId={}] : {}", RequestException.Cause.VALIDATION_FAILED.toString().toLowerCase(), incidentId, details);

            Problem problem = Problem.build(RequestException.Cause.VALIDATION_FAILED.toString().toLowerCase(),
                    RequestException.Cause.VALIDATION_FAILED.getErrorDescription(),
                    incidentId);
            return response.entity(problem).build();
        }

        LOG.warn("{} [incidentId={}] : {}", RequestException.Cause.VALIDATION_FAILED.toString().toLowerCase(), incidentId, ex.toString());
        Problem problem = Problem.build(RequestException.Cause.VALIDATION_FAILED.toString().toLowerCase(), ex.toString(), incidentId);
        return response.entity(problem).build();
    }
}