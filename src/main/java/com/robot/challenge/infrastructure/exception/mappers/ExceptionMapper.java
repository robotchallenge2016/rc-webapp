package com.robot.challenge.infrastructure.exception.mappers;

import com.robot.challenge.infrastructure.exception.RcException;
import com.robot.challenge.infrastructure.exception.model.Problem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.UUID;

@Provider
public class ExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Exception> {

	private static final Logger LOG = LoggerFactory.getLogger(ExceptionMapper.class);

	@Override
	public Response toResponse(final Exception ex) {
		UUID incidentId = UUID.randomUUID();
		LOG.error("{} [incidentId={}] : {}", RcException.Cause.SERVER_ERROR.toString().toLowerCase(), incidentId, ex.getMessage(), ex);

		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.type(MediaType.APPLICATION_JSON)
				.entity(Problem.build(RcException.Cause.SERVER_ERROR.toString().toLowerCase(),
						RcException.Cause.SERVER_ERROR.getErrorDescription(),
						incidentId))
				.build();
	}	
}