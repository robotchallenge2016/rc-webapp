package com.robot.challenge.infrastructure.exception.mappers;

import com.robot.challenge.infrastructure.exception.RequestException;
import com.robot.challenge.infrastructure.exception.model.Problem;

import javax.ejb.EJBException;
import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.UUID;

/**
 * Created by mklys on 30/09/16.
 */
@Provider
public class LockingExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<EJBException> {

    @Override
    public Response toResponse(EJBException exception) {
        UUID incidentId = UUID.randomUUID();

        Response.ResponseBuilder response = Response.status(HttpServletResponse.SC_CONFLICT)
                .type(MediaType.APPLICATION_JSON);
        Problem problem;
        if(exception.getCausedByException() instanceof OptimisticLockException) {
            problem = Problem.build(RequestException.Cause.STALE_OBJECT.toString().toLowerCase(),
                    RequestException.Cause.STALE_OBJECT.getErrorDescription(),
                    incidentId);
        } else {
            problem = Problem.build(RequestException.Cause.STALE_OBJECT.toString().toLowerCase(),
                    exception.getMessage(),
                    incidentId);
        }

        return response.entity(problem).build();
    }
}
