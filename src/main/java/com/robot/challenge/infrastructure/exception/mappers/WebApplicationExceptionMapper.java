package com.robot.challenge.infrastructure.exception.mappers;

import com.robot.challenge.infrastructure.exception.RequestException;
import com.robot.challenge.infrastructure.exception.model.Problem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.UUID;

@Provider
public class WebApplicationExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<WebApplicationException> {

	private static final Logger LOG = LoggerFactory.getLogger(WebApplicationExceptionMapper.class);

	@Override
	public Response toResponse(final WebApplicationException ex) {
		UUID incidentId = UUID.randomUUID();

		LOG.warn("{} [incidentId={}] : {}", RequestException.Cause.REQUEST_FAILED.toString(), incidentId, ex.getMessage());
		Problem problem = Problem.build(RequestException.Cause.REQUEST_FAILED.toString().toLowerCase(), ex.getMessage(), incidentId);
		return Response.status(ex.getResponse().getStatus())
				.type(MediaType.APPLICATION_JSON)
				.entity(problem)
				.build();
	}	
}