package com.robot.challenge.infrastructure.exception.model;

import java.util.UUID;

public class Problem {
    private String code;
    private String title;
    private String incidentId;

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public String getIncidentId() {
        return incidentId;
    }

    public static Problem build(String code, String title, UUID incidentId) {
        Problem problem = new Problem();
        problem.code = code;
        problem.title = title;
        problem.incidentId = incidentId.toString();

        return problem;
    }
}
