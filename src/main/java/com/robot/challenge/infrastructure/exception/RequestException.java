package com.robot.challenge.infrastructure.exception;

import javax.ws.rs.core.Response;

public class RequestException extends RcException {

    public enum Cause implements com.robot.challenge.infrastructure.exception.Cause {
        REQUEST_FAILED("Request failed"),
        STALE_OBJECT("Stale object update attempt"),
        INVALID_PARAMETER("Request contains invalid parameter"),
        VALIDATION_FAILED("One or more fields are invalid");

        private final String description;

        Cause(String description) {
            this.description = description;
        }

        @Override
        public String getErrorDescription() {
            return description;
        }
    }

    public RequestException(int status, Cause code, String description) {
        super(status, code, description);
    }

    public static RequestException validationFailed() {
        return new RequestException(Response.Status.BAD_REQUEST.getStatusCode(), Cause.VALIDATION_FAILED, Cause.VALIDATION_FAILED.getErrorDescription());
    }

    public static RequestException invalidParameter(String description) {
        return new RequestException(Response.Status.BAD_REQUEST.getStatusCode(), Cause.INVALID_PARAMETER, description);
    }

    public static RequestException validationFailed(String description) {
        return new RequestException(Response.Status.BAD_REQUEST.getStatusCode(), Cause.VALIDATION_FAILED, description);
    }
}
