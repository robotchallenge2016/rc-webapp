package com.robot.challenge.infrastructure.domain;

public interface GloballyUnique {
    Guid getGuid();
}
