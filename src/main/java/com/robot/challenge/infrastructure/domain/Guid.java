package com.robot.challenge.infrastructure.domain;

import com.robot.challenge.infrastructure.exception.RequestException;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class Guid {

    @Column(name = "GUID", unique = true, nullable = false, updatable = false)
    private String value;

    private Guid() {

    }

    private Guid(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Guid generate() {
        return new Guid(UUID.randomUUID().toString());
    }

    public static Guid from(String value) {
        try {
            return new Guid(UUID.fromString(value).toString());
        } catch(IllegalArgumentException ex) {
            throw RequestException.invalidParameter(ex.getMessage());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Guid guid = (Guid) o;

        return Objects.equals(value, guid.value);

    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        return "Guid{" +
                "value='" + value + '\'' +
                '}';
    }
}
