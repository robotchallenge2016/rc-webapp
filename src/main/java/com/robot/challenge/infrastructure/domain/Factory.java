package com.robot.challenge.infrastructure.domain;

import java.util.function.Function;

public class Factory<T> {

    private final Class<T> clazz;

    protected Factory(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Function<T, T> decorator() {
        return Function.identity();
    }

    public Class<T> getClazz() {
        return clazz;
    }
}
