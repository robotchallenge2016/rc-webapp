package com.robot.challenge.website.domain;

import com.robot.challenge.infrastructure.domain.Factory;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;

public class NoticeFactory extends Factory<Notice> {

    public NoticeFactory() {
        super(Notice.class);
    }

    public Notice create(SystemUser author) {
        return new Notice(Guid.generate(), author);
    }
}
