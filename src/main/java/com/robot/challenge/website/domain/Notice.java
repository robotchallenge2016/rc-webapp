package com.robot.challenge.website.domain;

import com.robot.challenge.infrastructure.domain.GloballyUnique;
import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.system.user.domain.SystemUser;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

/**
 * Created by mklys on 20/01/16.
 */
@Entity
@Table(name = "NOTICE", indexes = { @Index(name = "notice_idx", columnList = "guid") })
public class Notice implements GloballyUnique {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "notice_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Long id;
    @Embedded
    private Guid guid;
    @Column(name = "TITLE", nullable = false, length = 1024)
    private String title;
    @Column(name = "TEXT", nullable = false, columnDefinition = "text")
    private String text;
    @ManyToOne(optional = false)
    @JoinColumn(name = "AUTHOR_ID", nullable = false, updatable = false)
    private SystemUser author;
    @ManyToOne
    @JoinColumn(name = "EDITOR_ID")
    private SystemUser editor;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE_TIME", nullable = false, updatable = false)
    private Date createDateTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE_TIME")
    private Date updateDateTime;
    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    private Notice() {

    }

    public Notice(Guid guid, SystemUser author) {
        this.guid = guid;
        this.author = author;
        this.createDateTime = Date.from(Instant.now());
    }

    public Long getId() {
        return id;
    }

    public Guid getGuid() {
        return guid;
    }

    public String getTitle() {
        return title;
    }

    public Notice setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getText() {
        return text;
    }

    public Notice setText(String text) {
        this.text = text;
        return this;
    }

    public SystemUser getAuthor() {
        return author;
    }

    public SystemUser getEditor() {
        return editor;
    }

    public Notice setEditor(SystemUser editor) {
        this.editor = editor;
        return this;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime.toInstant().atZone(ZoneId.of("UTC"));
    }

    public Optional<ZonedDateTime> getUpdateDateTime() {
        if(updateDateTime != null) {
            return Optional.of(updateDateTime.toInstant().atZone(ZoneId.of("UTC")));
        }
        return Optional.empty();
    }

    public Notice setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = Date.from(updateDateTime.toInstant());
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public Notice setVersion(Long version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notice notice = (Notice) o;

        if (id != null ? !id.equals(notice.id) : notice.id != null) return false;
        if (guid != null ? !guid.equals(notice.guid) : notice.guid != null) return false;
        if (title != null ? !title.equals(notice.title) : notice.title != null) return false;
        if (text != null ? !text.equals(notice.text) : notice.text != null) return false;
        if (author != null ? !author.equals(notice.author) : notice.author != null) return false;
        if (editor != null ? !editor.equals(notice.editor) : notice.editor != null) return false;
        if (createDateTime != null ? !createDateTime.equals(notice.createDateTime) : notice.createDateTime != null)
            return false;
        if (updateDateTime != null ? !updateDateTime.equals(notice.updateDateTime) : notice.updateDateTime != null)
            return false;
        return version != null ? version.equals(notice.version) : notice.version == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (guid != null ? guid.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (editor != null ? editor.hashCode() : 0);
        result = 31 * result + (createDateTime != null ? createDateTime.hashCode() : 0);
        result = 31 * result + (updateDateTime != null ? updateDateTime.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
