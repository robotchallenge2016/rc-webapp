package com.robot.challenge.website.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by mklys on 01/10/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaginatedNotices {

    private Long total;
    private List<PaginatedNoticeModel> items;

    public Long getTotal() {
        return total;
    }

    public List<PaginatedNoticeModel> getItems() {
        return items;
    }

    private PaginatedNotices(Long total, List<PaginatedNoticeModel> items) {
        this.total = total;
        this.items = items;
    }

    public static PaginatedNotices of(Long total, List<PaginatedNoticeModel> items) {
        return new PaginatedNotices(total, items);
    }
}
