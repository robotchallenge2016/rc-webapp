package com.robot.challenge.website.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by mklys on 08/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewNoticeModel {

    @NotNull
    @Size(min = 1)
    private String title;
    @NotNull
    @Size(min = 1)
    private String text;

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public static NewNoticeModelBuilder builder() {
        return new NewNoticeModelBuilder();
    }

    public static class NewNoticeModelBuilder {

        private final NewNoticeModel model = new NewNoticeModel();

        public NewNoticeModelBuilder setTitle(String title) {
            model.title = title;
            return this;
        }

        public NewNoticeModelBuilder setText(String text) {
            model.text = text;
            return this;
        }

        public NewNoticeModel build() {
            return model;
        }
    }    
}
