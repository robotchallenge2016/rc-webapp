package com.robot.challenge.website.rest.resource;

import com.robot.challenge.infrastructure.rest.resource.CrudResource;
import com.robot.challenge.system.dictionary.domain.SystemRole;
import com.robot.challenge.website.rest.model.ExistingNoticeModel;
import com.robot.challenge.website.rest.model.NewNoticeModel;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface NoticeResource extends CrudResource<NewNoticeModel, ExistingNoticeModel> {

    String NOTICE_PREFIX = "/notice";
    String CREATE = NOTICE_PREFIX;
    String UPDATE = NOTICE_PREFIX + "/guid/{guid}";
    String DELETE = NOTICE_PREFIX + "/guid/{guid}";
    String RETRIEVE_SINGLE = NOTICE_PREFIX + "/guid/{guid}";
    String RETRIEVE_ALL = NOTICE_PREFIX + "/all";
    String RETRIEVE_ALL_BRIEF = NOTICE_PREFIX + "/all/brief";

    @POST
    @Path(CREATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response create(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @Valid NewNoticeModel notice);

    @PUT
    @Path(UPDATE)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response update(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid,
                    @Valid ExistingNoticeModel notice);

    @DELETE
    @Path(DELETE)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(SystemRole.ADMIN_VALUE)
    Response delete(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                    @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_SINGLE)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response retrieveByGuid(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                            @PathParam("guid") @NotNull @Size(min = 1) String guid);

    @GET
    @Path(RETRIEVE_ALL)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response retrieveAll(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid,
                         @QueryParam("limit") Integer limit,
                         @QueryParam("offset") Integer offset);

    @GET
    @Path(RETRIEVE_ALL_BRIEF)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    Response retrieveAllBriefed(@PathParam("editionGuid") @NotNull @Size(min = 1) String editionGuid);
}
