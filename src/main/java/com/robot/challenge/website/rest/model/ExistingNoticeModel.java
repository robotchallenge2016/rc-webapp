package com.robot.challenge.website.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.website.domain.Notice;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by mklys on 08/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExistingNoticeModel implements GloballyUniqueModel, VersionedModel {

    private String guid;
    @NotNull
    @Size(min = 1)
    private String title;
    @NotNull
    @Size(min = 1)
    private String text;
    @NotNull
    @Min(value = 0L)
    private Long version;

    public String getGuid() {
        return guid;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Long getVersion() {
        return version;
    }

    public static ExistingNoticeModel from(Notice notice) {
        ExistingNoticeModel model = new ExistingNoticeModel();
        model.guid = notice.getGuid().getValue();
        model.title = notice.getTitle();
        model.text = notice.getText();
        model.version = notice.getVersion();

        return model;
    }

    public static ExistingNoticeModelBuilder builder() {
        return new ExistingNoticeModelBuilder();
    }

    public static class ExistingNoticeModelBuilder {

        private final ExistingNoticeModel model = new ExistingNoticeModel();

        public ExistingNoticeModelBuilder setTitle(String title) {
            model.title = title;
            return this;
        }

        public ExistingNoticeModelBuilder setText(String text) {
            model.text = text;
            return this;
        }

        public ExistingNoticeModelBuilder setVersion(Long version) {
            model.version = version;
            return this;
        }

        public ExistingNoticeModel build() {
            return model;
        }
    }    
}
