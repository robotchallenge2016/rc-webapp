package com.robot.challenge.website.rest.model;

import java.util.Optional;

/**
 * Created by mklys on 01/10/16.
 */
public class SearchParams {

    private Integer limit;
    private Integer offset;

    public SearchParams(Integer limit, Integer offset) {
        this.limit = limit;
        this.offset = offset;
    }

    public Optional<Integer> getLimit() {
        return Optional.ofNullable(limit);
    }

    public Optional<Integer> getOffset() {
        return Optional.ofNullable(offset);
    }
}
