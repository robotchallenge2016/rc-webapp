package com.robot.challenge.website.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.website.domain.Notice;

import java.time.format.DateTimeFormatter;

/**
 * Created by mklys on 08/01/16.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaginatedNoticeModel implements GloballyUniqueModel {

    private String guid;
    private String title;
    private String text;
    private String authorGuid;
    private String authorLogin;
    private String createDateTime;
    private String updateDateTime;

    public String getGuid() {
        return guid;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getAuthorGuid() {
        return authorGuid;
    }

    public String getAuthorLogin() {
        return authorLogin;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public static PaginatedNoticeModel from(Notice notice) {
        PaginatedNoticeModel model = new PaginatedNoticeModel();
        model.guid = notice.getGuid().getValue();
        model.title = notice.getTitle();
        model.text = notice.getText();
        model.authorGuid = notice.getAuthor().getGuid().getValue();
        model.authorLogin = notice.getAuthor().getLogin();

        DateTimeFormatter dtf = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        model.createDateTime = dtf.format(notice.getCreateDateTime());
        notice.getUpdateDateTime().ifPresent(updateDateTime -> model.updateDateTime = dtf.format(updateDateTime.toLocalDateTime()));

        return model;
    }
}
