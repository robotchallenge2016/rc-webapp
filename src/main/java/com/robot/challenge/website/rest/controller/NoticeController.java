package com.robot.challenge.website.rest.controller;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.domain.ContextFactory;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import com.robot.challenge.website.domain.Notice;
import com.robot.challenge.website.repository.NoticeFinder;
import com.robot.challenge.website.repository.NoticeRepository;
import com.robot.challenge.website.rest.model.BriefNoticeModel;
import com.robot.challenge.website.rest.model.ExistingNoticeModel;
import com.robot.challenge.website.rest.model.NewNoticeModel;
import com.robot.challenge.website.rest.model.PaginatedNoticeModel;
import com.robot.challenge.website.rest.model.PaginatedNotices;
import com.robot.challenge.website.rest.model.SearchParams;
import com.robot.challenge.website.rest.resource.NoticeResource;
import com.robot.challenge.website.rest.resource.WebsiteResource;
import com.robot.challenge.website.service.NoticeService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path(WebsiteResource.WEBSITE_CONTEXT + TournamentResource.FULL_TOURNAMENT_EDITION_PREFIX)
public class NoticeController implements NoticeResource {

    @Inject
    private ContextFactory contextFactory;
    @Inject
    private NoticeService noticeService;
    @Inject
    private NoticeRepository noticeRepository;
    @Inject
    private NoticeFinder noticeFinder;

    @Override
    public Response create(@NotNull @Size(min = 1) String editionGuid,
                           @Valid NewNoticeModel notice) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        Notice noticeCreated = noticeService.create(notice, tournament);
        return Response.status(Response.Status.CREATED).entity(ModelCreated.from(noticeCreated)).build();
    }

    @Override
    public Response update(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid, @Valid ExistingNoticeModel notice) {
        Tournament tournament = contextFactory.getEditionContext(editionGuid).getTournament();
        noticeService.update(Guid.from(guid), notice, tournament);
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response delete(@NotNull @Size(min = 1) String editionGuid,
                           @NotNull @Size(min = 1) String guid) {
        noticeService.delete(Guid.from(guid));
        return Response.status(Response.Status.OK).build();
    }

    @Override
    public Response retrieveByGuid(@NotNull @Size(min = 1) String editionGuid,
                                   @NotNull @Size(min = 1) String guid) {
        ExistingNoticeModel result = ExistingNoticeModel.from(noticeFinder.retrieveByGuid(Guid.from(guid)).orElseThrow(DataAccessException::entityNotFound));
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @Override
    public Response retrieveAll(@NotNull @Size(min = 1) String editionGuid,
                                Integer limit, Integer offset) {
        SearchParams params = new SearchParams(limit, offset);
        Long total = noticeFinder.count();
        List<PaginatedNoticeModel> notices = noticeFinder.retrieveBySearchParams(params)
                .stream()
                .map(PaginatedNoticeModel::from)
                .collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(PaginatedNotices.of(total, notices)).build();
    }

    @Override
    public Response retrieveAllBriefed(@NotNull @Size(min = 1) String editionGuid) {
        List<BriefNoticeModel> result = noticeFinder.retrieveAll().stream().map(BriefNoticeModel::from).collect(Collectors.toList());
        return Response.status(Response.Status.OK).entity(result).build();
    }
}
