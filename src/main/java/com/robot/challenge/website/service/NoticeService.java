package com.robot.challenge.website.service;

import com.robot.challenge.infrastructure.domain.Guid;
import com.robot.challenge.infrastructure.exception.DataAccessException;
import com.robot.challenge.system.authn.service.IntrospectionService;
import com.robot.challenge.system.user.domain.SystemUser;
import com.robot.challenge.tournament.domain.Tournament;
import com.robot.challenge.website.domain.Notice;
import com.robot.challenge.website.domain.NoticeFactory;
import com.robot.challenge.website.repository.NoticeFinder;
import com.robot.challenge.website.repository.NoticeRepository;
import com.robot.challenge.website.rest.model.ExistingNoticeModel;
import com.robot.challenge.website.rest.model.NewNoticeModel;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class NoticeService {

    private final NoticeFactory noticeFactory;
    private final NoticeRepository noticeRepository;
    private final NoticeFinder noticeFinder;
    private final IntrospectionService introspectionService;

    @Inject
    public NoticeService(NoticeFactory noticeFactory, NoticeRepository noticeRepository, NoticeFinder noticeFinder, IntrospectionService introspectionService) {
        this.noticeFactory = noticeFactory;
        this.noticeRepository = noticeRepository;
        this.noticeFinder = noticeFinder;
        this.introspectionService = introspectionService;
    }

    public Notice create(NewNoticeModel noticeModel, Tournament context) {
        SystemUser author = introspectionService.introspection(context)
                .orElseThrow(DataAccessException::entityNotFound);
        Notice notice = noticeFactory.create(author)
                .setTitle(noticeModel.getTitle())
                .setText(noticeModel.getText());

        noticeRepository.create(notice);

        return notice;
    }

    public void update(Guid guid, ExistingNoticeModel noticeModel, Tournament context) {
        SystemUser editor = introspectionService.introspection(context)
                .orElseThrow(DataAccessException::entityNotFound);
        Notice notice = noticeFinder.retrieveByGuid(guid).orElseThrow(DataAccessException::entityNotFound)
                .setEditor(editor)
                .setTitle(noticeModel.getTitle())
                .setText(noticeModel.getText())
                .setUpdateDateTime(ZonedDateTime.now(ZoneId.of("UTC")))
                .setVersion(noticeModel.getVersion());

        noticeRepository.update(notice);
    }

    public void delete(Guid guid) {
        noticeFinder.retrieveByGuid(guid)
                .map(entity -> noticeRepository.delete(entity.getId()))
                .orElseThrow(DataAccessException::entityNotFound);
    }
}
