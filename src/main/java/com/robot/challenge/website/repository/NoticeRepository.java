package com.robot.challenge.website.repository;

import com.robot.challenge.infrastructure.repository.JpaRepository;
import com.robot.challenge.website.domain.Notice;

import javax.ejb.Stateless;

/**
 * Created by mklys on 20/01/16.
 */
@Stateless
public class NoticeRepository extends JpaRepository<Notice> {

    public NoticeRepository() {
        super(Notice.class);
    }
}
