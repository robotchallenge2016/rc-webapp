package com.robot.challenge.website.repository;

import com.robot.challenge.infrastructure.repository.JpaFinder;
import com.robot.challenge.website.domain.Notice;
import com.robot.challenge.website.domain.NoticeFactory;
import com.robot.challenge.website.rest.model.SearchParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import java.util.List;

public class NoticeFinder extends JpaFinder<Notice> {
    private static final Logger LOG = LoggerFactory.getLogger(NoticeFinder.class);

    @Inject
    public NoticeFinder(NoticeFactory factory) {
        super(factory);
    }

    public List<Notice> retrieveAll() {
        LOG.info("Retrieving all {}", Notice.class.getName());
        TypedQuery<Notice> q = entityManager().createQuery("SELECT n FROM Notice n LEFT JOIN FETCH n.author LEFT JOIN FETCH n.editor ORDER BY n.createDateTime DESC", Notice.class);
        return q.getResultList();
    }

    public List<Notice> retrieveBySearchParams(SearchParams params) {
        LOG.info("Retrieving all {} by search params", Notice.class.getName());
        TypedQuery<Notice> q = entityManager().createQuery("SELECT n FROM Notice n LEFT JOIN FETCH n.author LEFT JOIN FETCH n.editor ORDER BY n.createDateTime DESC", Notice.class);
        params.getLimit().ifPresent(limit -> {
            q.setMaxResults(limit);
            params.getOffset().ifPresent(offset -> q.setFirstResult(limit * offset));
        });
        return q.getResultList();
    }

    public Long count() {
        LOG.info("Counting {}", Notice.class.getName());
        TypedQuery<Long> q = entityManager().createQuery("SELECT COUNT(n) FROM Notice n", Long.class);
        return q.getSingleResult();
    }
}
