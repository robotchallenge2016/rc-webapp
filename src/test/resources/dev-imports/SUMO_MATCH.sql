--SUMO MATCH 1
INSERT INTO SUMO_MATCH_DAY (id, guid, number, sumo_competition_id, version) VALUES (nextval('sumo_match_day_id_seq'), 'bd166b17-679b-41b0-b623-260f8f238934', 0, 4, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), '2f2ef829-fad1-4910-80c8-21c397ac841f', 39, 4, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'a0181ed0-e39a-41e5-9983-67ae19a91c91', 39, 4, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '93a331c0-9b50-40ae-b2dd-1549cf2ba978', 39, 4, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '94dbafa2-5813-4d80-ac03-4a32744a4bcb', 39, 4, currval('sumo_match_id_seq'), 1, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), '5b5c0289-8446-418e-9bce-6d5ea782a432', 32, 11, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '25fdf4bd-3550-4530-9b6b-b9131d18d265', 32, 11, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '212c099c-f9e9-48af-8b75-5bf4ca946ffb', 32, 11, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'd6a93f0d-51df-43df-8fe9-10804b336fc1', 32, 11, currval('sumo_match_id_seq'), 1, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), 'c4715e64-6f84-48d4-b8e9-9980bd6d7f47', 25, 18, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '7697eab9-ac51-424f-9dcb-4e7dc007f59a', 25, 18, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'cc799600-baf7-4181-8362-0f8744347e60', 25, 18, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'ed1719be-eb6e-4649-b076-6897a847bcfe', 25, 18, currval('sumo_match_id_seq'), 1, 0);

--SUMO MATCH 2
INSERT INTO SUMO_MATCH_DAY (id, guid, number, sumo_competition_id, version) VALUES (nextval('sumo_match_day_id_seq'), 'b18b44f5-99a1-479a-80fc-c5b6b14762b9', 1, 4, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), 'f65036b2-b0a0-4726-9675-e0b0f950606d', 25, 39, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '5ab40c19-7674-4b70-b7da-05f264313d8c', 39, 25, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'd8dc61ac-01c9-4ff5-9c5f-6ff1ab3e8636', 39, 25, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'f373d79f-c32c-4c5b-8f17-685453d41de2', 25, 39, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'd5a28351-c34a-466a-9dbf-59a58590c1fc', 25, 39, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '43c07f74-464e-4102-bdf6-fff235a5472e', 39, 25, currval('sumo_match_id_seq'), 1, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), 'c91c8a40-226a-428c-9765-cac11883c532', 18, 32, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '68653e41-e6fa-4204-8d90-f3a9db8f8ef6', 18, 32, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '552adcff-4a77-4b0d-a593-599582b1af56', 32, 18, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '088cc31e-573c-4e30-b31d-abf375194191', 32, 18, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '815153f9-ff39-4b3e-b2e5-925245531b61', 32, 18, currval('sumo_match_id_seq'), 1, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), 'af233015-db39-4032-b708-ff304e2aa245', 11, 4, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), '6f7eba2b-3e4a-4fc7-bca2-b777381b97d1', 11, 4, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'c0b13a6f-3431-46ab-bbc2-96b92c3e0311', 11, 4, currval('sumo_match_id_seq'), 1, 0);
INSERT INTO SUMO_RESULT(id, guid, winner_id, looser_id, match_id, author_id, version) VALUES (nextval('sumo_result_id_seq'), 'b50278e2-5ba0-432a-87f1-3b4411745cb6', 11, 4, currval('sumo_match_id_seq'), 1, 0);

--SUMO MATCH 3
INSERT INTO SUMO_MATCH_DAY (id, guid, number, sumo_competition_id, version) VALUES (nextval('sumo_match_day_id_seq'), '038af980-747b-48ee-b22d-7bf1acc3b152', 2, 4, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), 'e6cb2b00-4fec-4330-850a-0d8e0a99e367', 39, 11, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), 'b68558bf-0517-4e1b-aea9-8a60c02dce36', 4, 18, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), '4c98ee2f-7d17-4ff4-80ec-24f1133dbdc6', 32, 25, currval('sumo_match_day_id_seq'), 0);

--SUMO MATCH 4
INSERT INTO SUMO_MATCH_DAY (id, guid, number, sumo_competition_id, version) VALUES (nextval('sumo_match_day_id_seq'), '8a1bded8-b6aa-4221-8215-c1c81b7fb8fe', 3, 4, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), '06d274e5-4ba0-4da4-9bdc-e687950f703f', 32, 39, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), 'e9fead81-421f-4074-9ef8-72d725dc4665', 25, 4, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), '12c05864-7856-4aa3-8bdc-e8ed528218e4', 18, 11, currval('sumo_match_day_id_seq'), 0);

--SUMO MATCH 5
INSERT INTO SUMO_MATCH_DAY (id, guid, number, sumo_competition_id, version) VALUES (nextval('sumo_match_day_id_seq'), '4d4bf33a-11a4-4944-a154-40cf35ecf863', 4, 4, 0);

INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), '4358337f-35a4-4463-81e0-8fec638b96ed', 39, 18, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), 'bee5e094-4ce4-403a-8bbf-cdaa40de923b', 11, 25, currval('sumo_match_day_id_seq'), 0);
INSERT INTO SUMO_MATCH (id, guid, away_robot_id, home_robot_id, match_day_id, version) VALUES (nextval('sumo_match_id_seq'), '36977da5-d84d-48a0-803d-544b58a9de68', 4, 32, currval('sumo_match_day_id_seq'), 0);