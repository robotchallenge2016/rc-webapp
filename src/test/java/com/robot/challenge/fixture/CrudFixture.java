package com.robot.challenge.fixture;

import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;

public interface CrudFixture<N, E extends GloballyUniqueModel & VersionedModel> {
    N newPreloadedModel();
    N newModel();
    E updateModel(E existing);
}
