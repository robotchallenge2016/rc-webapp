package com.robot.challenge.fixture.system;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.system.user.rest.model.user.ExistingSystemUserModel;
import com.robot.challenge.system.user.rest.model.user.NewSystemUserModel;

public class SystemUserFixture implements CrudFixture<NewSystemUserModel, ExistingSystemUserModel> {

    @Override
    public NewSystemUserModel newPreloadedModel() {
        return NewSystemUserModel.builder()
                .setLogin("PreLoadedSystemUserLogin")
                .setFirstName("TestFirstName")
                .setLastName("TestLastName")
                .setPassword("TestPassword")
                .setSystemRoleGuid(PreLoadedSystemRole.ADMIN.guid())
                .build();
    }

    @Override
    public NewSystemUserModel newModel() {
        return NewSystemUserModel.builder()
                .setLogin("SystemUserLogin")
                .setFirstName("TestFirstName")
                .setLastName("TestLastName")
                .setPassword("TestPassword")
                .setSystemRoleGuid(PreLoadedSystemRole.ADMIN.guid())
                .build();
    }

    @Override
    public ExistingSystemUserModel updateModel(ExistingSystemUserModel existing) {
        return ExistingSystemUserModel.builder()
                .setLogin("UpdatedSystemUserLogin")
                .setFirstName("UpdatedFirstName")
                .setLastName("UpdatedLastName")
                .setPassword("UpdatedPassword")
                .setSystemRoleGuid(PreLoadedSystemRole.USER.guid())
                .setVersion(existing.getVersion())
                .build();
    }
}
