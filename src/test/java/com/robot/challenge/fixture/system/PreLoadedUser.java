package com.robot.challenge.fixture.system;

import com.robot.challenge.fixture.tournament.competitor.PreLoadedTeam;

import java.util.Optional;

/**
 * Created by mklys on 14/02/16.
 */
public interface PreLoadedUser {
    String guid();
    String login();
    String firstName();
    String lastName();
    Optional<PreLoadedTeam> team();
}
