package com.robot.challenge.fixture.system;

import com.google.common.base.MoreObjects;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedTeam;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by mklys on 04/02/16.
 */
public enum PreLoadedSystemUser implements PreLoadedUser {
    MACIEJ_KLYS("2671023a-1a19-41d2-b1a3-c61f70bf20b3", "kmaci3k@gmail.com", "Maciej", "Kłyś"),
    SLAWOMIR_BANDO("61a3a3e1-9a9f-4f0a-a4b2-a68e720da254", "sbando@gmail.com", "Sławomir", "Bańdo"),
    JAKUB_PIKON("a5552d82-4c3f-438c-b1ca-5770f896c761", "jpikon@gmail.com", "Jakub", "Pikoń"),
    TOMASZ_KOS("1faae82c-b359-4194-8f6e-8b3a51e2506d", "tkos@gmail.com", "Tomasz", "Kos");

    private final String guid;
    private final String login;
    private final String firstName;
    private final String lastName;

    PreLoadedSystemUser(String guid, String login, String firstName, String lastName) {
        this.guid = guid;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String guid() {
        return guid;
    }

    public String login() {
        return login;
    }

    public String firstName() {
        return firstName;
    }

    public String lastName() {
        return lastName;
    }

    public Optional<PreLoadedTeam> team() {
        return Optional.empty();
    }

    public static List<PreLoadedUser> teamLessSystemUser() {
        return Arrays.stream(PreLoadedSystemUser.values()).filter(preLoadedCompetitor -> !preLoadedCompetitor.team().isPresent()).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("login", login)
                .add("firstName", firstName)
                .add("lastName", lastName).toString();
    }
}
