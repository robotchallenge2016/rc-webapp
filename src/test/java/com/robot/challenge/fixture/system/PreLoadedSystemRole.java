package com.robot.challenge.fixture.system;

import com.google.common.base.MoreObjects;

/**
 * Created by mklys on 30/01/16.
 */
public enum PreLoadedSystemRole {
    ADMIN("5be95d8d-ead7-46ac-a4eb-8b5959906b74", "ADMIN"),
    USER("5cabaa8c-1aea-4af1-a618-0854d87ab8fd", "USER"),
    COMPETITOR("a3382de7-71f5-4451-834a-1065ce48543d", "COMPETITOR");

    private final String guid;
    private final String name;

    PreLoadedSystemRole(String guid, String name) {
        this.guid = guid;
        this.name = name;
    }

    public String guid() {
        return guid;
    }

    public String systemRoleName() {
        return name;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("name", name).toString();
    }
}
