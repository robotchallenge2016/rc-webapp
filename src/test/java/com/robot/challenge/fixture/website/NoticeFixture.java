package com.robot.challenge.fixture.website;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.website.rest.model.ExistingNoticeModel;
import com.robot.challenge.website.rest.model.NewNoticeModel;

public class NoticeFixture implements CrudFixture<NewNoticeModel, ExistingNoticeModel> {

    @Override
    public NewNoticeModel newPreloadedModel() {
        return NewNoticeModel.builder()
                .setTitle("PreLoadedTitle")
                .setText("Text")
                .build();
    }

    @Override
    public NewNoticeModel newModel() {
        return NewNoticeModel.builder()
                .setTitle("Title")
                .setText("Text")
                .build();
    }

    @Override
    public ExistingNoticeModel updateModel(ExistingNoticeModel existing) {
        return ExistingNoticeModel.builder()
                .setTitle("UpdatedTitle")
                .setText("UpdatedText")
                .setVersion(existing.getVersion())
                .build();
    }
}
