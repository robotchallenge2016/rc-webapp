package com.robot.challenge.fixture.tournament.competition.result;

import com.google.common.base.MoreObjects;
import com.robot.challenge.fixture.system.PreLoadedSystemUser;
import com.robot.challenge.fixture.tournament.competition.PreLoadedCompetition;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;

public enum PreLoadedLineFollowerResult {
    LINEFOLLOWER_RESULT_LINEFOLLOWER_ROBOT_1("1fe7c713-22c4-49da-a889-a76935da23fb", 23874L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.LINEFOLLOWER_ROBOT, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_LINEFOLLOWER_ROBOT_2_BEST("d458a436-37ef-4f1d-894e-00780314e04d", 17231L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.LINEFOLLOWER_ROBOT, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_LINEFOLLOWER_ROBOT_3("2b17c5ea-ee0f-4f88-997b-97fd80e88bfa", 27981L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.LINEFOLLOWER_ROBOT, PreLoadedSystemUser.MACIEJ_KLYS),

    LINEFOLLOWER_RESULT_ENERGIZER_BUNNY_1("abb36ce7-f434-430b-9c4e-741e264e11a2", 2981L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.ENERGIZER_BUNNY, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_ENERGIZER_BUNNY_2("f7edd280-6464-4d76-8cf6-49d91b9d1775", 7231L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.ENERGIZER_BUNNY, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_ENERGIZER_BUNNY_3_BEST("81f7d723-2434-40c9-bd6e-44db7b6b6ce4", 874L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.ENERGIZER_BUNNY, PreLoadedSystemUser.MACIEJ_KLYS),

    LINEFOLLOWER_RESULT_WALL_E_1_BEST("0041049b-4197-4b56-b248-bd2691962887", 234L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.WALL_E, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_WALL_E_2("f708a0b0-9824-4d33-b99b-d657b771e0bb", 3455L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.WALL_E, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_WALL_E_3("9cf03647-7c2d-4094-9424-008d05b94ffc", 6756L, PreLoadedCompetition.LINEFOLLOWER, PreLoadedRobot.WALL_E, PreLoadedSystemUser.MACIEJ_KLYS);

    private final String guid;
    private final Long result;
    private final PreLoadedSystemUser author;
    private final PreLoadedCompetition competition;
    private final PreLoadedRobot robot;

    PreLoadedLineFollowerResult(String guid, Long result, PreLoadedCompetition competition, PreLoadedRobot robot, PreLoadedSystemUser author) {
        this.guid = guid;
        this.result = result;
        this.author = author;
        this.competition = competition;
        this.robot = robot;
    }

    public String guid() {
        return guid;
    }

    public Long result() {
        return result;
    }

    public PreLoadedSystemUser author() {
        return author;
    }

    public PreLoadedCompetition competition() {
        return competition;
    }

    public PreLoadedRobot robot() {
        return robot;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("result", result)
                .add("author", author)
                .add("competition", competition)
                .add("robot", robot).toString();
    }
}
