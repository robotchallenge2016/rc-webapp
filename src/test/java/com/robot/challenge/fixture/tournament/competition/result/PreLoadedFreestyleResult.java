package com.robot.challenge.fixture.tournament.competition.result;

import com.google.common.base.MoreObjects;
import com.robot.challenge.fixture.system.PreLoadedSystemUser;
import com.robot.challenge.fixture.tournament.competition.PreLoadedCompetition;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePlace;

public enum PreLoadedFreestyleResult {
    LINEFOLLOWER_RESULT_ROBOCOP_1("86231437-a20d-4a94-989a-39f499160f9c", FreestylePlace.FIRST, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.ROBOCOP, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_ROBOCOP_2("a809a511-2bca-4a73-8aa5-e31415ada15d", FreestylePlace.FIRST, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.ROBOCOP, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_ROBOCOP_3("ce57ad3c-979a-42fe-aaf6-189bb1d22ac5", FreestylePlace.FIRST, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.ROBOCOP, PreLoadedSystemUser.MACIEJ_KLYS),

    LINEFOLLOWER_RESULT_ASIMO_1("85f693c4-ae4f-4630-ab14-b953af438490", FreestylePlace.SECOND, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.ASIMO, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_ASIMO_2("115583c1-d979-4e6f-8ba4-0abe9eee2c48", FreestylePlace.SECOND, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.ASIMO, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_ASIMO_3("6b57a6d6-8a60-4619-ba23-6bd538c11deb", FreestylePlace.THIRD, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.ASIMO, PreLoadedSystemUser.MACIEJ_KLYS),

    LINEFOLLOWER_RESULT_T_800_1("d8532ae2-c139-41e5-9729-4c7b9ff6c5ce", FreestylePlace.THIRD, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.T_800, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_T_800_2("912b9e3e-badd-48dc-802a-aea927c55a2d", FreestylePlace.THIRD, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.T_800, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_RESULT_T_800_3("b63f34cb-a000-40fe-8cc4-34c8ab02786d", FreestylePlace.SECOND, PreLoadedCompetition.FREESTYLE, PreLoadedRobot.T_1000, PreLoadedSystemUser.MACIEJ_KLYS);

    private final String guid;
    private final FreestylePlace place;
    private final PreLoadedSystemUser author;
    private final PreLoadedCompetition competition;
    private final PreLoadedRobot robot;

    PreLoadedFreestyleResult(String guid, FreestylePlace place, PreLoadedCompetition competition, PreLoadedRobot robot, PreLoadedSystemUser author) {
        this.guid = guid;
        this.place = place;
        this.author = author;
        this.competition = competition;
        this.robot = robot;
    }

    public String guid() {
        return guid;
    }

    public FreestylePlace result() {
        return place;
    }

    public PreLoadedSystemUser author() {
        return author;
    }

    public PreLoadedCompetition competition() {
        return competition;
    }

    public PreLoadedRobot robot() {
        return robot;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("place", place)
                .add("author", author)
                .add("competition", competition)
                .add("robot", robot).toString();
    }
}
