package com.robot.challenge.fixture.tournament.competition;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.fixture.tournament.dictionary.PreLoadedCompetitionSubtype;
import com.robot.challenge.tournament.competition.rest.model.linefollower.ExistingLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.linefollower.NewLineFollowerCompetitionModel;

public class LineFollowerCompetitionFixture implements CrudFixture<NewLineFollowerCompetitionModel, ExistingLineFollowerCompetitionModel> {

    @Override
    public NewLineFollowerCompetitionModel newPreloadedModel() {
        return NewLineFollowerCompetitionModel.builder()
                .setName("PreLoadedLineFollowerCompetitionName")
                .setCompetitionTypeGuid(PreLoadedCompetitionSubtype.LINEFOLLOWER.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ENHANCED_ROBOT.guid())
                .setMaxNumberOfRuns(5)
                .build();
    }

    @Override
    public NewLineFollowerCompetitionModel newModel() {
        return NewLineFollowerCompetitionModel.builder()
                .setName("LineFollowerCompetitionName")
                .setCompetitionTypeGuid(PreLoadedCompetitionSubtype.LINEFOLLOWER.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ENHANCED_ROBOT.guid())
                .setMaxNumberOfRuns(10)
                .build();
    }

    @Override
    public ExistingLineFollowerCompetitionModel updateModel(ExistingLineFollowerCompetitionModel existing) {
        return ExistingLineFollowerCompetitionModel.builder()
                .setName("UpdatedLineFollowerCompetitionName")
                .setType(PreLoadedCompetitionSubtype.LINEFOLLOWER_ENHANCED.guid())
                .addRobot(PreLoadedRobot.MINI_SUMO_ROBOT.guid())
                .addRobot(PreLoadedRobot.STANDARD_SUMO_ROBOT.guid())
                .setMaxNumberOfRuns(15)
                .setVersion(existing.getVersion())
                .build();
    }
}
