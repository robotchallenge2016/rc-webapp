package com.robot.challenge.fixture.tournament.competition.result;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.fixture.tournament.competition.PreLoadedCompetition;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.tournament.competition.domain.competition.freestyle.FreestylePlace;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.ExistingFreestyleResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.FreestylePlaceModel;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.NewFreestyleResultModel;

public class FreestyleResultFixture implements CrudFixture<NewFreestyleResultModel, ExistingFreestyleResultModel> {

    @Override
    public NewFreestyleResultModel newPreloadedModel() {
        return NewFreestyleResultModel.builder()
                .setResult(FreestylePlaceModel.from(FreestylePlace.FIRST))
                .setCompetitionGuid(PreLoadedCompetition.FREESTYLE.guid())
                .setRobotGuid(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .build();
    }

    @Override
    public NewFreestyleResultModel newModel() {
        return NewFreestyleResultModel.builder()
                .setResult(FreestylePlaceModel.from(FreestylePlace.FIRST))
                .setCompetitionGuid(PreLoadedCompetition.FREESTYLE.guid())
                .setRobotGuid(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .build();
    }

    @Override
    public ExistingFreestyleResultModel updateModel(ExistingFreestyleResultModel existing) {
        return ExistingFreestyleResultModel.builder()
                .setResult(FreestylePlaceModel.from(FreestylePlace.FIRST))
                .setVersion(existing.getVersion())
                .build();
    }
}
