package com.robot.challenge.fixture.tournament.competition.result;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.fixture.tournament.competition.PreLoadedCompetition;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.ExistingLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.NewLineFollowerResultModel;

public class LineFollowerResultFixture implements CrudFixture<NewLineFollowerResultModel, ExistingLineFollowerResultModel> {

    @Override
    public NewLineFollowerResultModel newPreloadedModel() {
        return NewLineFollowerResultModel.builder()
                .setResult(1000L)
                .setCompetitionGuid(PreLoadedCompetition.LINEFOLLOWER.guid())
                .setRobotGuid(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .build();
    }

    @Override
    public NewLineFollowerResultModel newModel() {
        return NewLineFollowerResultModel.builder()
                .setResult(1000L)
                .setCompetitionGuid(PreLoadedCompetition.LINEFOLLOWER.guid())
                .setRobotGuid(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .build();
    }

    @Override
    public ExistingLineFollowerResultModel updateModel(ExistingLineFollowerResultModel existing) {
        return ExistingLineFollowerResultModel.builder()
                .setResult(2000L)
                .setVersion(existing.getVersion())
                .build();
    }
}
