package com.robot.challenge.fixture.tournament.dictionary;

import com.google.common.base.MoreObjects;

/**
 * Created by mklys on 16/02/16.
 */
public enum PreLoadedRobotType {
    LINEFOLLOWER("8a6d1c7a-6fbc-42ec-a3ac-1469ebc70fc3", "LINEFOLLOWER"),
    STANDARD_LINEFOLLOWER("ff4c7c40-a4c8-4a4e-b218-06ab79df2fb8", "STANDARD LINEFOLLOWER"),
    LINEFOLLOWER_ENHANCED("855f4809-f865-4623-b3a3-3daad0ead444", "LINEFOLLOWER ENHANCED"),
    STANDARD_SUMO("a2c92a36-d1c4-44ce-bcfe-fdb25e0c1dbb", "STANDARD SUMO"),
    MINI_SUMO("af74b14c-185f-4f6e-8082-c7c2ccb0af06", "MINI SUMO"),
    MICRO_SUMO("c53ca140-030a-4055-9d17-3b9c10a58c21", "MICRO SUMO"),
    FREESTYLE("0c955a94-12cd-4e5b-b36d-0d8b5e3c5096", "FREESTYLE");

    private final String guid;
    private final String name;

    PreLoadedRobotType(String guid, String name) {
        this.guid = guid;
        this.name = name;
    }

    public String guid() {
        return guid;
    }

    public String typeName() {
        return name;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("name", name).toString();
    }
}
