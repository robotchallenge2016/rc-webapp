package com.robot.challenge.fixture.tournament.competitor;

import com.google.common.base.MoreObjects;
import com.robot.challenge.fixture.system.PreLoadedUser;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public enum PreLoadedCompetitor implements PreLoadedUser {
    TOMASZ_FRANKOWSKI("b1179170-93c8-4ad2-8403-563f9a9aa36c", "tfrankowski@gmail.com", "Tomasz", "Frankowski", PreLoadedTeam.LINEFOLLOWER_TEAM),
    KAMIL_KOSOWSKI("42c255ca-5ecc-4b64-b9b9-1fae6a6e978c", "kkosowski@gmail.com", "Kamil", "Kosowski", PreLoadedTeam.LINEFOLLOWER_ENHANCED_TEAM),
    DAMIAN_GORAWSKI("7b77ea1d-8ed8-4c0e-ba87-4bd4e64341d5", "dgorawski@gmail.com", "Damian", "Gorawski", PreLoadedTeam.STANDARD_SUMO_TEAM),
    MACIEJ_ZURAWSKI("9eb794be-a955-4967-9483-e4d0760b8133", "mzurawski@gmail.com", "Maciej", "Zurawski", PreLoadedTeam.MINI_SUMO_TEAM),
    MACIEJ_STOLARCZYK("b7112a50-a7e7-41cc-8623-d5751d49093e", "mstolarczyk@gmail.com", "Maciej", "Stolarczyk", PreLoadedTeam.FREESTYLE_TEAM),
    MARCIN_KUZBA("c83acee1-3b0d-4ee6-a195-98fe166f0a16", "mkuzba@gmail.com", "Marcin", "Kuzba", PreLoadedTeam.LINEFOLLOWER_TEAM),
    MIROSLAW_SZYMKOWIAK("a38e611a-e8f0-4135-966c-96377907b52c", "mszymkowiak@gmail.com", "Miroslaw", "Szymkowiak", PreLoadedTeam.LINEFOLLOWER_TEAM),
    MARCIN_BASZCZYNSKI("694a6b64-1528-41f1-af7c-a9d64e850188", "mbaszczynski@gmail.com", "Marcin", "Baszczynski", PreLoadedTeam.LINEFOLLOWER_ENHANCED_TEAM),
    ARKADIUSZ_GLOWACKI("86901dad-d41d-478a-9131-d09bd93f5dea", "aglowacki@gmail.com", "Arkadiusz", "Glowacki", PreLoadedTeam.STANDARD_SUMO_TEAM),
    TOMASZ_KLOS("d533cf92-da19-4ce0-8af0-9b40e4577d02", "tklos@gmail.com", "Tomasz", "Klos", PreLoadedTeam.STANDARD_SUMO_TEAM),
    RADOSLAW_MAJDAN("12a10ad5-6607-4633-99dd-e58085896f21", "rmajdan@gmail.com", "Radoslaw", "Majdan"),
    GRZEGORZ_PATER("90977c17-4790-42b6-ac05-fb3f5b78a746", "gpater@gmail.com", "Grzegorz", "Pater"),
    KAZIMIERZ_MOSKAL("60d500ac-4617-4e51-83d1-c04b8531ed52", "kmoskal@gmail.com", "Kazimierz", "Moskal"),
    DANIEL_DUBICKI("971f9dd5-c2b9-4893-b989-8de9216a3099", "ddubicki@gmail.com", "Daniel", "Dubicki");

    private final String guid;
    private final String login;
    private final String firstName;
    private final String lastName;
    private final PreLoadedTeam team;

    PreLoadedCompetitor(String guid, String login, String firstName, String lastName) {
        this(guid, login, firstName, lastName, null);
    }

    PreLoadedCompetitor(String guid, String login, String firstName, String lastName, PreLoadedTeam team) {
        this.guid = guid;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
    }

    public String guid() {
        return guid;
    }

    public String login() {
        return login;
    }

    public String firstName() {
        return firstName;
    }

    public String lastName() {
        return lastName;
    }

    public Optional<PreLoadedTeam> team() {
        return Optional.ofNullable(team);
    }

    public static List<PreLoadedUser> teamLessCompetitors() {
        return Arrays.stream(PreLoadedCompetitor.values()).filter(preLoadedCompetitor -> !preLoadedCompetitor.team().isPresent()).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("login", login)
                .add("firstName", firstName)
                .add("lastName", lastName).toString();
    }
}
