package com.robot.challenge.fixture.tournament.competitor;

import com.google.common.base.MoreObjects;

public enum PreLoadedTeam {
    LINEFOLLOWER_TEAM("f6a70af1-1b08-470a-9baf-897759a825ef", "Linefollower Team", PreLoadedCompetitor.TOMASZ_FRANKOWSKI),
    LINEFOLLOWER_ENHANCED_TEAM("a01fa0bc-a8a0-4947-8c69-ef275df21406", "Linefollower Enhanced Team", PreLoadedCompetitor.KAMIL_KOSOWSKI),
    STANDARD_SUMO_TEAM("82e5405d-a78d-4457-b351-cb8c679ebb07", "Standard Sumo Team", PreLoadedCompetitor.DAMIAN_GORAWSKI),
    MINI_SUMO_TEAM("148e6145-3fe9-4f9a-ab64-f7f6369ea0b9", "Mini Sumo Team", PreLoadedCompetitor.MACIEJ_ZURAWSKI),
    FREESTYLE_TEAM("a192d3cf-11aa-4073-b1a0-1c288a59fe45", "Freestyle Team", PreLoadedCompetitor.MACIEJ_STOLARCZYK);

    private final String guid;
    private final String name;
    private final PreLoadedCompetitor captain;

    PreLoadedTeam(String guid, String name, PreLoadedCompetitor captain) {
        this.guid = guid;
        this.name = name;
        this.captain = captain;
    }

    public String guid() {
        return guid;
    }

    public String teamName() {
        return name;
    }

    public PreLoadedCompetitor captain() {
        return captain;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("name", name).toString();
    }
}
