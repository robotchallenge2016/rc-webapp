package com.robot.challenge.fixture.tournament.competition;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.fixture.tournament.dictionary.PreLoadedCompetitionSubtype;
import com.robot.challenge.tournament.competition.rest.model.freestyle.ExistingFreestyleCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.freestyle.NewFreestyleCompetitionModel;

public class FreestyleCompetitionFixture implements CrudFixture<NewFreestyleCompetitionModel, ExistingFreestyleCompetitionModel> {

    @Override
    public NewFreestyleCompetitionModel newPreloadedModel() {
        return NewFreestyleCompetitionModel.builder()
                .setName("PreLoadedFreestyleCompetitionName")
                .setCompetitionSubtypeGuid(PreLoadedCompetitionSubtype.FREESTYLE.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ENHANCED_ROBOT.guid())
                .setFirstPlacePoints(3)
                .setSecondPlacePoints(2)
                .setThirdPlacePoints(1)
                .build();
    }

    @Override
    public NewFreestyleCompetitionModel newModel() {
        return NewFreestyleCompetitionModel.builder()
                .setName("FreestyleCompetitionName")
                .setCompetitionSubtypeGuid(PreLoadedCompetitionSubtype.FREESTYLE.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ENHANCED_ROBOT.guid())
                .setFirstPlacePoints(3)
                .setSecondPlacePoints(2)
                .setThirdPlacePoints(1)
                .build();
    }

    @Override
    public ExistingFreestyleCompetitionModel updateModel(ExistingFreestyleCompetitionModel existing) {
        return ExistingFreestyleCompetitionModel.builder()
                .setName("UpdatedFreestyleCompetitionName")
                .setType(PreLoadedCompetitionSubtype.FREESTYLE.guid())
                .addRobot(PreLoadedRobot.MINI_SUMO_ROBOT.guid())
                .addRobot(PreLoadedRobot.STANDARD_SUMO_ROBOT.guid())
                .setFirstPlacePoints(6)
                .setSecondPlacePoints(4)
                .setThirdPlacePoints(2)
                .setVersion(existing.getVersion())
                .build();
    }
}
