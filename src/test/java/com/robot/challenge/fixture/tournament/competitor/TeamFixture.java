package com.robot.challenge.fixture.tournament.competitor;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.tournament.competitor.rest.model.team.ExistingTeamModel;
import com.robot.challenge.tournament.competitor.rest.model.team.NewTeamModel;

public class TeamFixture implements CrudFixture<NewTeamModel, ExistingTeamModel> {

    @Override
    public NewTeamModel newPreloadedModel() {
        return NewTeamModel.builder()
                .setName("PreLoadedTeamName")
                .setCaptainGuid(PreLoadedCompetitor.RADOSLAW_MAJDAN.guid())
                .addCompetitorGuid(PreLoadedCompetitor.GRZEGORZ_PATER.guid())
                .build();
    }

    @Override
    public NewTeamModel newModel() {
        return NewTeamModel.builder()
                .setName("NewTeamName")
                .setCaptainGuid(PreLoadedCompetitor.KAZIMIERZ_MOSKAL.guid())
                .addCompetitorGuid(PreLoadedCompetitor.DANIEL_DUBICKI.guid())
                .build();
    }

    public NewTeamModel newTeamWithoutName() {
        return NewTeamModel.builder()
                .setName(null)
                .setCaptainGuid(PreLoadedCompetitor.KAZIMIERZ_MOSKAL.guid())
                .build();
    }

    public NewTeamModel newTeamWithoutCaptain() {
        return NewTeamModel.builder()
                .setName("TeamName")
                .build();
    }

    @Override
    public ExistingTeamModel updateModel(ExistingTeamModel existing) {
        return ExistingTeamModel.builder()
                .setName("UpdatedTeamName")
                .addCompetitorGuid(PreLoadedCompetitor.DANIEL_DUBICKI.guid())
                .setVersion(existing.getVersion())
                .build();
    }

    public ExistingTeamModel existingTeamWithoutName(ExistingTeamModel existing) {
        return ExistingTeamModel.builder()
                .setName(null)
                .addCompetitorGuid(PreLoadedCompetitor.DANIEL_DUBICKI.guid())
                .setVersion(existing.getVersion())
                .build();
    }

    public ExistingTeamModel existingTeamWithoutVersion(ExistingTeamModel existing) {
        return ExistingTeamModel.builder()
                .setName("UpdatedTeamName")
                .addCompetitorGuid(PreLoadedCompetitor.DANIEL_DUBICKI.guid())
                .setVersion(null)
                .build();
    }
}
