package com.robot.challenge.fixture.tournament.competitor;

import com.google.common.base.MoreObjects;
import com.robot.challenge.fixture.system.PreLoadedSystemUser;
import com.robot.challenge.fixture.system.PreLoadedUser;
import com.robot.challenge.fixture.tournament.dictionary.PreLoadedRobotType;

public enum PreLoadedRobot {
    LINEFOLLOWER_ROBOT("d48c2505-a036-4748-b0b4-5aa0468e3152", "Linefollower Robot", PreLoadedRobotType.LINEFOLLOWER, PreLoadedSystemUser.MACIEJ_KLYS),
    LINEFOLLOWER_ENHANCED_ROBOT("853e253b-9c5a-4aa6-bd3f-7c50c8a52837", "Linefollower Enhanced Robot", PreLoadedRobotType.STANDARD_LINEFOLLOWER, PreLoadedSystemUser.SLAWOMIR_BANDO),
    STANDARD_SUMO_ROBOT("1161ae3b-110e-4443-ab2b-9855147efde3", "Standard Sumo Robot", PreLoadedRobotType.LINEFOLLOWER_ENHANCED, PreLoadedSystemUser.JAKUB_PIKON),
    MINI_SUMO_ROBOT("d7490ceb-44f0-4880-87dd-e2f4ab7d1657", "Mini Sumo Robot", PreLoadedRobotType.STANDARD_SUMO, PreLoadedSystemUser.TOMASZ_KOS),
    R2_D2("50804035-71d7-4aa3-b40a-a6ebda0cc746", "R2-D2", PreLoadedRobotType.MINI_SUMO, PreLoadedCompetitor.TOMASZ_FRANKOWSKI),
    C_3PO("5e6adc85-9a1e-4c91-beb0-b1be4423e718", "C-3PO", PreLoadedRobotType.MICRO_SUMO, PreLoadedCompetitor.KAMIL_KOSOWSKI),
    ROBOCOP("46a4bb05-9fff-4100-a14a-4898689453c9", "RoboCop", PreLoadedRobotType.FREESTYLE, PreLoadedCompetitor.DAMIAN_GORAWSKI),
    ENERGIZER_BUNNY("49b59e87-f394-4fda-83db-553fe37ca71c", "Energizer Bunny", PreLoadedRobotType.LINEFOLLOWER, PreLoadedCompetitor.MACIEJ_ZURAWSKI),
    LEGO_MINDSTORM("24141314-c6d3-46f1-96c9-aa3abe656e60", "LEGO MINDSTORM", PreLoadedRobotType.STANDARD_LINEFOLLOWER, PreLoadedCompetitor.MACIEJ_STOLARCZYK),
    OPTIMUS_PRIME("96f6e022-047a-4f03-824a-1846f997f6aa", "Optimus Prime", PreLoadedRobotType.LINEFOLLOWER_ENHANCED, PreLoadedCompetitor.MARCIN_KUZBA),
    ROOMBA("8e4f591e-ad48-4aeb-bcfc-789b0dde3b35", "Roomba", PreLoadedRobotType.STANDARD_SUMO, PreLoadedCompetitor.MIROSLAW_SZYMKOWIAK),
    ROSIE("3bbb4457-0fa7-4494-9cee-09f6862e7969", "Rosie", PreLoadedRobotType.MINI_SUMO, PreLoadedCompetitor.MARCIN_BASZCZYNSKI),
    TERMINATOR("96038625-2252-4a4d-9325-e5977f51067b", "Terminator", PreLoadedRobotType.MICRO_SUMO, PreLoadedCompetitor.ARKADIUSZ_GLOWACKI),
    ASIMO("1454a307-23d7-4726-9bf0-9526af70f588", "ASIMO", PreLoadedRobotType.FREESTYLE, PreLoadedCompetitor.TOMASZ_KLOS),

    WALL_E("7735b4a1-b8c2-4326-9c29-de36b4419d8f", "Wall-E", PreLoadedRobotType.LINEFOLLOWER, PreLoadedSystemUser.MACIEJ_KLYS),
    COLOBOT("443c49c1-b23f-4ba0-a8e0-ba44e112f2ae", "COLOBOT", PreLoadedRobotType.STANDARD_LINEFOLLOWER, PreLoadedSystemUser.SLAWOMIR_BANDO),
    KITT("00781216-fafc-4eca-b042-97e62b11d59e", "KITT", PreLoadedRobotType.LINEFOLLOWER_ENHANCED, PreLoadedSystemUser.JAKUB_PIKON),
    T_1000("6d2ed92a-4ad5-4dbc-a921-846f77192197", "T-1000", PreLoadedRobotType.STANDARD_SUMO, PreLoadedSystemUser.TOMASZ_KOS),
    THE_IRON_GIANT("f52ebd79-1824-4fbd-bfe5-e0d7107d7d36", "The Iron Giant", PreLoadedRobotType.MINI_SUMO, PreLoadedCompetitor.TOMASZ_FRANKOWSKI),
    MEGATRON("4b922e85-32a2-4791-81e9-8363116a8c42", "Megatron", PreLoadedRobotType.MICRO_SUMO, PreLoadedCompetitor.KAMIL_KOSOWSKI),
    T_800("3569baf3-a021-4fe3-b283-e10f1c9210d2", "T-800", PreLoadedRobotType.FREESTYLE, PreLoadedCompetitor.DAMIAN_GORAWSKI),
    SKYNET("122f0027-8921-4ac6-9f09-e07010ba309c", "Skynet", PreLoadedRobotType.LINEFOLLOWER, PreLoadedCompetitor.MACIEJ_ZURAWSKI),
    BB_8("21e2a577-19d4-47af-bf27-d4bc2b86cd05", "BB-8", PreLoadedRobotType.STANDARD_LINEFOLLOWER, PreLoadedCompetitor.MACIEJ_STOLARCZYK),
    BUMBLEBEE("c7dad333-423b-4baf-9fd0-eadd1db1ff92", "Bumblebee", PreLoadedRobotType.LINEFOLLOWER_ENHANCED, PreLoadedCompetitor.MARCIN_KUZBA),
    CLIFFJUMPER("a9f6b284-cdf9-4951-a760-58ce4e3dd8a9", "Cliffjumper", PreLoadedRobotType.STANDARD_SUMO, PreLoadedCompetitor.MIROSLAW_SZYMKOWIAK),
    PROWL("6b6b4817-a400-4e27-9279-08964db7cc1b", "Prowl", PreLoadedRobotType.MINI_SUMO, PreLoadedCompetitor.MARCIN_BASZCZYNSKI),
    SIDESWIPE("12ce6623-382d-45a1-97b1-12d247a7cbde", "Sideswipe", PreLoadedRobotType.MICRO_SUMO, PreLoadedCompetitor.ARKADIUSZ_GLOWACKI),
    RATCHET("0c9aa4e8-814d-4f5a-ac39-ce891bedb224", "Ratchet", PreLoadedRobotType.FREESTYLE, PreLoadedCompetitor.TOMASZ_KLOS),

    IRONHIDE("7b3fae6b-6692-46ee-8803-7a803ba56ee5", "Ironhide", PreLoadedRobotType.LINEFOLLOWER, PreLoadedSystemUser.MACIEJ_KLYS),
    SPRINGER("443d9d8d-97f2-4d05-8583-d28e529831aa", "Springer", PreLoadedRobotType.STANDARD_LINEFOLLOWER, PreLoadedSystemUser.SLAWOMIR_BANDO),
    HOUND("031989d8-9461-4bc6-967a-6a0725ad9b54", "Hound", PreLoadedRobotType.LINEFOLLOWER_ENHANCED, PreLoadedSystemUser.JAKUB_PIKON),
    MIRAGE("9f83de6a-ed8b-4729-8962-88fe670b78c8", "Mirage", PreLoadedRobotType.STANDARD_SUMO, PreLoadedSystemUser.TOMASZ_KOS),
    TRAILBREAKER("d46d2b21-7872-44c7-8bf0-3b5e7c6a6dd0", "Trailbreaker", PreLoadedRobotType.MINI_SUMO, PreLoadedSystemUser.MACIEJ_KLYS),
    SUNSTREAKER("1f248c44-14d6-4850-8d6c-5ad364f58e24", "Sunstreaker", PreLoadedRobotType.MICRO_SUMO, PreLoadedSystemUser.SLAWOMIR_BANDO),
    BLUESTREAK("5e604f2e-7ecb-4388-bd80-6a03b31aea0e", "Bluestreak", PreLoadedRobotType.FREESTYLE, PreLoadedSystemUser.JAKUB_PIKON),
    TRACKS("c7617717-b97f-4828-9263-0cfbbab33adb", "Tracks", PreLoadedRobotType.LINEFOLLOWER, PreLoadedSystemUser.JAKUB_PIKON),
    GRAPPLE("50906a7a-d2ee-4261-b519-8abbdbac295a", "Grapple", PreLoadedRobotType.STANDARD_LINEFOLLOWER, PreLoadedSystemUser.JAKUB_PIKON),
    RED_ALERT("a6e90f2c-c73b-47fc-9b28-1e92cd806fd4", "Red Alert", PreLoadedRobotType.LINEFOLLOWER_ENHANCED, PreLoadedSystemUser.TOMASZ_KOS),
    SMOKESCREEN("17b5ed44-b5f3-4497-9a52-61f35625b1d2", "Smokescreen", PreLoadedRobotType.STANDARD_SUMO, PreLoadedSystemUser.TOMASZ_KOS),
    HOIST("cded40fd-4f43-4995-853d-462c1d73c708", "Hoist", PreLoadedRobotType.MINI_SUMO, PreLoadedSystemUser.TOMASZ_KOS);

    private final String guid;
    private final String name;
    private final PreLoadedUser owner;
    private final PreLoadedRobotType type;

    PreLoadedRobot(String guid, String name, PreLoadedRobotType type, PreLoadedUser owner) {
        this.guid = guid;
        this.name = name;
        this.owner = owner;
        this.type = type;
    }

    public String guid() {
        return guid;
    }

    public String robotName() {
        return name;
    }

    public PreLoadedUser owner() {
        return owner;
    }

    public PreLoadedRobotType robotType(){return type;}

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("name", name).toString();
    }
}
