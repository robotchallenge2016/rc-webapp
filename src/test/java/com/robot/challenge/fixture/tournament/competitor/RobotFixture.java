package com.robot.challenge.fixture.tournament.competitor;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.fixture.tournament.dictionary.PreLoadedRobotType;
import com.robot.challenge.tournament.competitor.rest.model.robot.ExistingRobotModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.NewRobotModel;

public class RobotFixture implements CrudFixture<NewRobotModel, ExistingRobotModel> {

    @Override
    public NewRobotModel newPreloadedModel() {
        return NewRobotModel.builder()
                .setName("PreloadedRobotName")
                .setType(PreLoadedRobotType.LINEFOLLOWER.guid())
                .setOwnerGuid(PreLoadedCompetitor.KAMIL_KOSOWSKI.guid())
                .build();
    }

    @Override
    public NewRobotModel newModel() {
        return NewRobotModel.builder()
                .setName("RobotName")
                .setType(PreLoadedRobotType.LINEFOLLOWER.guid())
                .setOwnerGuid(PreLoadedCompetitor.KAMIL_KOSOWSKI.guid())
                .build();
    }

    @Override
    public ExistingRobotModel updateModel(ExistingRobotModel existing) {
        return ExistingRobotModel.builder()
                .setName("UpdatedRobotName")
                .setType(PreLoadedRobotType.STANDARD_SUMO.guid())
                .setOwnerGuid(PreLoadedCompetitor.TOMASZ_FRANKOWSKI.guid())
                .setVersion(existing.getVersion())
                .build();
    }

    public NewRobotModel newRobotWithoutName() {
        return NewRobotModel.builder()
                .setName(null)
                .setType(PreLoadedRobotType.LINEFOLLOWER.guid())
                .setOwnerGuid(PreLoadedCompetitor.MACIEJ_ZURAWSKI.guid())
                .build();
    }

    public NewRobotModel newRobotWithoutType() {
        return NewRobotModel.builder()
                .setName("RobotName")
                .setType(null)
                .setOwnerGuid(PreLoadedCompetitor.MACIEJ_ZURAWSKI.guid())
                .build();
    }

    public NewRobotModel newRobotWithoutOwner() {
        return NewRobotModel.builder()
                .setName("RobotName")
                .setType(PreLoadedRobotType.LINEFOLLOWER.guid())
                .setOwnerGuid(null)
                .build();
    }

    public ExistingRobotModel existingRobotWithoutName(ExistingRobotModel existing) {
        return ExistingRobotModel.builder()
                .setName(null)
                .setType(PreLoadedRobotType.STANDARD_SUMO.guid())
                .setOwnerGuid(PreLoadedCompetitor.TOMASZ_FRANKOWSKI.guid())
                .setVersion(existing.getVersion())
                .build();
    }

    public ExistingRobotModel existingRobotWithoutType(ExistingRobotModel existing) {
        return ExistingRobotModel.builder()
                .setName("UpdatedRobotName")
                .setType(null)
                .setOwnerGuid(PreLoadedCompetitor.TOMASZ_FRANKOWSKI.guid())
                .setVersion(existing.getVersion())
                .build();
    }

    public ExistingRobotModel existingRobotWithoutOwner(ExistingRobotModel existing) {
        return ExistingRobotModel.builder()
                .setName("UpdatedRobotName")
                .setType(PreLoadedRobotType.STANDARD_SUMO.guid())
                .setOwnerGuid(null)
                .setVersion(existing.getVersion())
                .build();
    }

    public ExistingRobotModel existingRobotWithoutVersion(ExistingRobotModel existing) {
        return ExistingRobotModel.builder()
                .setName("UpdatedRobotName")
                .setType(PreLoadedRobotType.STANDARD_SUMO.guid())
                .setOwnerGuid(PreLoadedCompetitor.TOMASZ_FRANKOWSKI.guid())
                .setVersion(null)
                .build();
    }
}
