package com.robot.challenge.fixture.tournament.competitor;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.tournament.competitor.rest.model.competitor.ExistingCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.model.competitor.NewCompetitorModel;

public class CompetitorFixture implements CrudFixture<NewCompetitorModel, ExistingCompetitorModel> {

    @Override
    public NewCompetitorModel newPreloadedModel() {
        return NewCompetitorModel.builder()
                .setLogin("PreloadedTestLogin")
                .setFirstName("TestFirstName")
                .setLastName("TestLastName")
                .setPassword("TestPassword")
                .setTeamGuid(PreLoadedTeam.LINEFOLLOWER_TEAM.guid())
                .setSchoolName("Preloaded School Name")
                .build();
    }

    @Override
    public NewCompetitorModel newModel() {
        return NewCompetitorModel.builder()
                .setLogin("TestLogin")
                .setFirstName("TestFirstName")
                .setLastName("TestLastName")
                .setPassword("TestPassword")
                .setSchoolName("Test School Name")
                .setTeamGuid(PreLoadedTeam.LINEFOLLOWER_TEAM.guid())
                .build();
    }

    @Override
    public ExistingCompetitorModel updateModel(ExistingCompetitorModel existing) {
        return ExistingCompetitorModel.builder()
                .setLogin("UpdatedLogin")
                .setFirstName("UpdatedFirstName")
                .setLastName("UpdatedLastName")
                .setPassword("UpdatedPassword")
                .setSchoolName("Updated School Name")
                .setTeamGuid(PreLoadedTeam.LINEFOLLOWER_ENHANCED_TEAM.guid())
                .setVersion(existing.getVersion())
                .build();
    }
}
