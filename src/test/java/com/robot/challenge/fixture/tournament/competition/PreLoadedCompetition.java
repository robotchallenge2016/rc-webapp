package com.robot.challenge.fixture.tournament.competition;

import com.google.common.base.MoreObjects;
import com.robot.challenge.fixture.tournament.dictionary.PreLoadedCompetitionSubtype;

public enum PreLoadedCompetition {
    LINEFOLLOWER("65c18086-9af6-4938-9f88-bb7d42aeb343", PreLoadedCompetitionSubtype.LINEFOLLOWER),
    STANDARD_LINEFOLLOWER("ea089f9e-01ab-46bf-bd58-d82af025bce1", PreLoadedCompetitionSubtype.STANDARD_LINEFOLLOWER),
    LINEFOLLOWER_ENHANCED("5abc5144-6e0c-4adf-a0f3-a707b377aa06", PreLoadedCompetitionSubtype.LINEFOLLOWER_ENHANCED),
    STANDARD_SUMO("ae322ad9-bb8b-4ea9-8ee0-63b55abc456c", PreLoadedCompetitionSubtype.STANDARD_SUMO),
    MINI_SUMO("087a0c1f-1c99-42c5-a61f-382dcf643bb0", PreLoadedCompetitionSubtype.MINI_SUMO),
    MICRO_SUMO("f3b48d4b-63f8-4104-9a13-62c6badd6214", PreLoadedCompetitionSubtype.MICRO_SUMO),
    FREESTYLE("0ab0b37e-6640-45b7-a152-4c0d5fa6bd24", PreLoadedCompetitionSubtype.FREESTYLE);

    private final String guid;
    private final PreLoadedCompetitionSubtype type;

    PreLoadedCompetition(String guid, PreLoadedCompetitionSubtype type) {
        this.guid = guid;
        this.type = type;
    }

    public String guid() {
        return guid;
    }

    public PreLoadedCompetitionSubtype type() {
        return type;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("type", type).toString();
    }
}
