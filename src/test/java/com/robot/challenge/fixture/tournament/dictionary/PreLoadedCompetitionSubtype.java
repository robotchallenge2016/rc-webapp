package com.robot.challenge.fixture.tournament.dictionary;

import com.google.common.base.MoreObjects;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;

/**
 * Created by mklys on 05/02/16.
 */
public enum PreLoadedCompetitionSubtype {
    LINEFOLLOWER("0a29e734-3176-49cf-b7f3-84bcb5857abf", "LINEFOLLOWER", CompetitionType.LINEFOLLOWER, PreLoadedRobotType.LINEFOLLOWER),
    STANDARD_LINEFOLLOWER("7e89c3b8-5dab-4787-bfce-c482412936a3", "STANDARD LINEFOLLOWER", CompetitionType.LINEFOLLOWER, PreLoadedRobotType.STANDARD_LINEFOLLOWER),
    LINEFOLLOWER_ENHANCED("ce126017-e279-432e-babb-4df96f97d8a7", "LINEFOLLOWER ENHANCED", CompetitionType.LINEFOLLOWER, PreLoadedRobotType.LINEFOLLOWER_ENHANCED),
    STANDARD_SUMO("c9909b80-cb29-4942-b793-9725c93b46d5", "STANDARD SUMO", CompetitionType.SUMO, PreLoadedRobotType.STANDARD_SUMO),
    MINI_SUMO("d17170fb-b2bc-4ca0-89fc-16e58f6bec99", "MINI SUMO", CompetitionType.SUMO, PreLoadedRobotType.MINI_SUMO),
    MICRO_SUMO("44a8da6d-374b-4b9b-92c4-e3ce480722b9", "MICRO SUMO", CompetitionType.SUMO, PreLoadedRobotType.MICRO_SUMO),
    FREESTYLE("eb4f14f8-a082-42be-8331-393c9fd0f87f", "FREESTYLE", CompetitionType.FREESTYLE, PreLoadedRobotType.FREESTYLE);

    private final String guid;
    private final String name;
    private final CompetitionType competitionType;
    private final PreLoadedRobotType robotType;

    PreLoadedCompetitionSubtype(String guid, String name, CompetitionType competitionType, PreLoadedRobotType robotType) {
        this.guid = guid;
        this.name = name;
        this.competitionType = competitionType;
        this.robotType = robotType;
    }

    public String guid() {
        return guid;
    }

    public String typeName() {
        return name;
    }

    public CompetitionType competitionType() {
        return competitionType;
    }

    public PreLoadedRobotType getRobotType() {
        return robotType;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
                .add("guid", guid)
                .add("name", name).toString();
    }
}
