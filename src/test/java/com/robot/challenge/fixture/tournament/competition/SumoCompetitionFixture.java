package com.robot.challenge.fixture.tournament.competition;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.fixture.tournament.dictionary.PreLoadedCompetitionSubtype;
import com.robot.challenge.tournament.competition.rest.model.sumo.ExistingSumoCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.sumo.NewSumoCompetitionModel;

public class SumoCompetitionFixture implements CrudFixture<NewSumoCompetitionModel, ExistingSumoCompetitionModel> {

    @Override
    public NewSumoCompetitionModel newPreloadedModel() {
        return NewSumoCompetitionModel.builder()
                .setName("PreLoadedSumoCompetitionName")
                .setCompetitionSubtypeGuid(PreLoadedCompetitionSubtype.MINI_SUMO.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ENHANCED_ROBOT.guid())
                .requiredNumberOfWins(3)
                .bigWinThreshold(2)
                .bigWinPoints(3)
                .bigLosePoints(0)
                .smallWinPoints(2)
                .smallLosePoints(1)
                .build();
    }

    @Override
    public NewSumoCompetitionModel newModel() {
        return NewSumoCompetitionModel.builder()
                .setName("SumoCompetitionName")
                .setCompetitionSubtypeGuid(PreLoadedCompetitionSubtype.MINI_SUMO.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .addRobot(PreLoadedRobot.LINEFOLLOWER_ENHANCED_ROBOT.guid())
                .requiredNumberOfWins(3)
                .bigWinThreshold(2)
                .bigWinPoints(3)
                .bigLosePoints(0)
                .smallWinPoints(2)
                .smallLosePoints(1)
                .build();
    }

    @Override
    public ExistingSumoCompetitionModel updateModel(ExistingSumoCompetitionModel existing) {
        return ExistingSumoCompetitionModel.builder()
                .setName("UpdatedSumoCompetitionName")
                .setType(PreLoadedCompetitionSubtype.STANDARD_SUMO.guid())
                .addRobot(PreLoadedRobot.MINI_SUMO_ROBOT.guid())
                .addRobot(PreLoadedRobot.STANDARD_SUMO_ROBOT.guid())
                .setVersion(existing.getVersion())
                .requiredNumberOfWins(5)
                .bigWinThreshold(1)
                .bigWinPoints(6)
                .bigLosePoints(1)
                .smallWinPoints(4)
                .smallLosePoints(2)
                .build();
    }
}
