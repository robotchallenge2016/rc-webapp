package com.robot.challenge.functional.website;

import com.robot.challenge.fixture.website.NoticeFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.website.rest.model.ExistingNoticeModel;
import com.robot.challenge.website.rest.model.NewNoticeModel;
import com.robot.challenge.website.rest.resource.NoticeResource;
import com.robot.challenge.website.rest.resource.WebsiteResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class NoticeCrudTest extends ExtendedCrudTest<NewNoticeModel, ExistingNoticeModel, NoticeFixture> {

    public NoticeCrudTest() {
        super(ExistingNoticeModel.class, new NoticeFixture());
    }

    @Override
    protected void assertCreated(NewNoticeModel expected, ExistingNoticeModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getTitle()).isEqualTo(expected.getTitle());
        assertThat(actual.getText()).isEqualTo(expected.getText());
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingNoticeModel actual, NewNoticeModel original, ModelCreated created, ExistingNoticeModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getTitle()).isEqualTo(expected.getTitle());
        assertThat(actual.getText()).isEqualTo(expected.getText());
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    private String context() {
        return WebsiteResource.WEBSITE_CONTEXT;
    }

    @Override
    protected String createEndpoint() {
        return context() + NoticeResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + NoticeResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + NoticeResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + NoticeResource.DELETE;
    }
}
