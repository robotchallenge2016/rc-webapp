package com.robot.challenge.functional.crud;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import com.robot.challenge.infrastructure.rest.resource.CrudResource;

import javax.ws.rs.core.Response;

public abstract class SimpleCrudTest<N, E extends GloballyUniqueModel & VersionedModel, F extends CrudFixture<N, E>>
        extends ArquillianCrudTest<N, E, F> {

    private static String TOURNAMENT_EDITION_GUID = "";
    private CrudResource<N, E> crudResource;

    protected SimpleCrudTest(Class<E> eClass, F crudFixture) {
        super(eClass, crudFixture);
    }

    protected void setCrudResource(CrudResource<N, E> crudResource) {
        this.crudResource = crudResource;
    }

    @Override
    protected Response performCreate(N model) {
        return crudResource.create(TOURNAMENT_EDITION_GUID, model);
    }

    @Override
    protected Response performRetrieve(ModelCreated existing) {
        return crudResource.retrieveByGuid(TOURNAMENT_EDITION_GUID, existing.getGuid());
    }

    @Override
    protected Response performUpdate(ModelCreated existing, E model) {
        return crudResource.update(TOURNAMENT_EDITION_GUID, existing.getGuid(), model);
    }

    @Override
    protected Response performDelete(ModelCreated existing) {
        return crudResource.delete(TOURNAMENT_EDITION_GUID, existing.getGuid());
    }

    protected abstract void assertCreated(N expected, E actual);
    protected abstract void assertUpdated(E actual, N original, ModelCreated created, E expected);
}
