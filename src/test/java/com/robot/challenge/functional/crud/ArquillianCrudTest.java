package com.robot.challenge.functional.crud;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.functional.system.authn.AuthenticationClient;
import com.robot.challenge.infrastructure.exception.Cause;
import com.robot.challenge.infrastructure.exception.model.Problem;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;

import javax.ws.rs.core.Response;

import static org.fest.assertions.Assertions.assertThat;

public abstract class ArquillianCrudTest<N, E extends GloballyUniqueModel & VersionedModel, F extends CrudFixture<N, E>>
        extends CrudTest<N, E, F> {

    private final AuthenticationClient authenticationClient = new AuthenticationClient();
    private final Class<E> eClass;

    protected ArquillianCrudTest(Class<E> eClass, F crudModel) {
        super(crudModel);
        this.eClass = eClass;
    }

    public AuthenticationClient authenticationClient() {
        return authenticationClient;
    }
    public Class<E> existingModelClass() {
        return eClass;
    }

    protected abstract Response performCreate(N model);
    protected abstract Response performRetrieve(ModelCreated existing);
    protected abstract Response performUpdate(ModelCreated existing, E model);
    protected abstract Response performDelete(ModelCreated existing);

    protected ModelCreated create(N model) {
        final Response response = performCreate(model);

        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());

        final ModelCreated createdUser = response.readEntity(ModelCreated.class);

        assertThat(createdUser.getGuid()).isNotNull();
        return createdUser;
    }

    protected E retrieve(ModelCreated existing) {
        final Response response = performRetrieve(existing);

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        final E retrievedSystemUser = response.readEntity(eClass);

        assertThat(retrievedSystemUser).isNotNull();
        assertThat(retrievedSystemUser.getGuid()).isEqualTo(existing.getGuid());
        return retrievedSystemUser;
    }

    protected void update(ModelCreated existing, E model) {
        final Response response = performUpdate(existing, model);
        response.close();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    protected void delete(ModelCreated existing) {
        final Response response = performDelete(existing);

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    protected void expectProblemResponse(Response response, Cause cause) {
        Problem problem = response.readEntity(Problem.class);
        assertThat(problem.getCode()).isEqualTo(cause.toString().toLowerCase());
        assertThat(problem.getTitle()).isEqualTo(cause.getErrorDescription());
    }
}
