package com.robot.challenge.functional.crud;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;

/**
 * Created by mklys on 01/02/16.
 */
public abstract class RestAssuredCrudTest<N, E extends GloballyUniqueModel & VersionedModel, F extends CrudFixture<N, E>>
        extends CrudTest<N, E, F> {

    protected RestAssuredCrudTest(F crudFixture) {
        super(crudFixture);
    }

    @Override
    protected void assertCreated(N expected, E actual) {

    }

    @Override
    protected void assertUpdated(E actual, N original, ModelCreated created, E expected) {

    }
}
