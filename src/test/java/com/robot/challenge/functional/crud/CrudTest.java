package com.robot.challenge.functional.crud;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.After;
import org.junit.Before;

public abstract class CrudTest<N, E extends GloballyUniqueModel & VersionedModel, F extends CrudFixture<N, E>>
        extends DeploymentSetup {

    private final F fixture;

    protected CrudTest(F fixture) {
        this.fixture = fixture;
    }

    protected void crudTest() {
        final N expectedNewModel = fixture.newModel();
        final ModelCreated created = create(expectedNewModel);
        final E existing = retrieve(created);
        assertCreated(expectedNewModel, existing);

        final E expectedUpdatedModel = fixture.updateModel(existing);
        update(created, expectedUpdatedModel);
        final E updated = retrieve(created);
        assertUpdated(updated , expectedNewModel, created, expectedUpdatedModel);
        delete(created);
    }

    public F fixture() {
        return fixture;
    }

    protected abstract void assertCreated(N expected, E actual);
    protected abstract void assertUpdated(E actual, N original, ModelCreated created, E expected);

    protected abstract ModelCreated create(N model);
    protected abstract E retrieve(ModelCreated existing);
    protected abstract void update(ModelCreated existing, E model);
    protected abstract void delete(ModelCreated existing);
}
