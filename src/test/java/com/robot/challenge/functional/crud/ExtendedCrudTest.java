package com.robot.challenge.functional.crud;

import com.robot.challenge.fixture.CrudFixture;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.infrastructure.exception.RequestException;
import com.robot.challenge.infrastructure.rest.model.GloballyUniqueModel;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.infrastructure.rest.model.VersionedModel;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URL;

import static org.fest.assertions.Assertions.assertThat;

public abstract class ExtendedCrudTest<N, E extends GloballyUniqueModel & VersionedModel, F extends CrudFixture<N, E>>
        extends ArquillianCrudTest<N, E, F> {

    @ArquillianResource
    private URL deploymentUrl;

    private ResteasyWebTarget target;
    private ResteasyWebTarget crudResource;
    private ModelCreated createdModel;
    private E existingModel;

    protected ExtendedCrudTest(Class<E> eClass, F crudModel) {
        super(eClass, crudModel);
    }

    @Before
    public void setup() {
        ResteasyClient client = new ResteasyClientBuilder().build();
        target = client.target(deploymentUrl + RestConfig.ROOT_CONTEXT);

        createdModel = createModelBeforeTest();
        existingModel = retrieveModelBeforeTest();
    }

    private ModelCreated createModelBeforeTest() {
        Response createResponse = performCreate(target, fixture().newPreloadedModel());
        assertThat(createResponse.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
        return createResponse.readEntity(ModelCreated.class);
    }

    private E retrieveModelBeforeTest() {
        Response retrieveResponse = performRetrieve(target, createdModel);
        assertThat(retrieveResponse.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        return retrieveResponse.readEntity(existingModelClass());
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void crud(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        this.crudResource = crudResource;
        crudTest();
    }

    protected void createValidationAssertion(ResteasyWebTarget target, N newModel) {
        Response response = performCreate(target, newModel);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        expectProblemResponse(response, RequestException.Cause.VALIDATION_FAILED);
    }

    protected void updateValidationAssertion(ResteasyWebTarget target, E existingModel) {
        Response response = performUpdate(target, modelCreated(), existingModel);
        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
        expectProblemResponse(response, RequestException.Cause.VALIDATION_FAILED);
    }

    @After
    public void after() {
        deleteModelAfterTest();
    }

    private void deleteModelAfterTest() {
        Response deleteResponse = performDelete(target, createdModel);
        assertThat(deleteResponse.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    public ModelCreated modelCreated() {
        return createdModel;
    }

    public E existingModel() {
        return existingModel;
    }

    protected abstract String createEndpoint();
    protected abstract String retrieveEndpoint();
    protected abstract String updateEndpoint();
    protected abstract String deleteEndpoint();

    protected Response performCreate(ResteasyWebTarget target, N model) {
        authenticationClient().addSessionCookie(target, deploymentUrl.toString());
        final Invocation.Builder requestBuilder = target
                .path(createEndpoint())
                .request();
        final Invocation request = requestBuilder.buildPost(Entity.entity(model, MediaType.APPLICATION_JSON_TYPE));
        return request.invoke();
    }

    @Override
    protected Response performCreate(N model) {
        return performCreate(crudResource, model);
    }

    protected Response performRetrieve(ResteasyWebTarget target, ModelCreated existing) {
        authenticationClient().addSessionCookie(target, deploymentUrl.toString());
        final Invocation.Builder requestBuilder = target
                .path(retrieveEndpoint())
                .resolveTemplate("guid", existing.getGuid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        return request.invoke();
    }

    @Override
    protected Response performRetrieve(ModelCreated existing) {
        return performRetrieve(crudResource, existing);
    }

    protected Response performUpdate(ResteasyWebTarget target, ModelCreated existing, E model) {
        authenticationClient().addSessionCookie(target, deploymentUrl.toString());
        final Invocation.Builder requestBuilder = target
                .path(updateEndpoint())
                .resolveTemplate("guid", existing.getGuid())
                .request();
        final Invocation request = requestBuilder.buildPut(Entity.entity(model, MediaType.APPLICATION_JSON_TYPE));
        return request.invoke();
    }

    @Override
    protected Response performUpdate(ModelCreated existing, E model) {
        return performUpdate(crudResource, existing, model);
    }

    protected Response performDelete(ResteasyWebTarget target, ModelCreated existing) {
        authenticationClient().addSessionCookie(target, deploymentUrl.toString());
        final Invocation.Builder requestBuilder = target
                .path(deleteEndpoint())
                .resolveTemplate("guid", existing.getGuid())
                .request();
        final Invocation request = requestBuilder.buildDelete();
        return request.invoke();
    }

    @Override
    protected Response performDelete(ModelCreated existing) {
        return performDelete(crudResource, existing);
    }
}
