package com.robot.challenge.functional.deployment;

import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

@ArquillianSuiteDeployment
public class DeploymentSetup {

    private final static Logger LOG = LoggerFactory.getLogger(DeploymentSetup.class);
    private static final String TEST_WEBAPP_SRC = "src/test/resources/webapp";
    private static final String WEBAPP_SRC = "src/main/webapp";

    public static final String NOT_SECURED_WEBAPP = "NotSecured";
    public static final String SECURED_WEBAPP = "Secured";

    private static File[] dependencies() {
        return Maven.resolver()
                .loadPomFromFile("pom.xml")
                .resolve("org.aeonbits.owner:owner-java8", "org.apache.velocity:velocity")
                .withTransitivity()
                .asFile();
    }

    private static WebArchive webapp(String warName) {
        return ShrinkWrap.create(WebArchive.class, warName)
                .addPackages(Boolean.TRUE, "com.robot.challenge.infrastructure")
                .addPackages(Boolean.TRUE, "com.robot.challenge.system")
                .addPackages(Boolean.TRUE, "com.robot.challenge.tournament")
                .addPackages(Boolean.TRUE, "com.robot.challenge.website")
                .addAsResource("webapp/META-INF/beans.xml", "META-INF/beans.xml")
                .addAsResource("webapp/META-INF/persistence.xml", "META-INF/persistence.xml")
                .addAsResource("dev-imports/TOURNAMENT.sql", "imports/TOURNAMENT.sql")
                .addAsResource("dev-imports/SYSTEM_ROLE.sql", "imports/SYSTEM_ROLE.sql")
                .addAsResource("dev-imports/COMPETITION_SUBTYPE.sql", "imports/COMPETITION_SUBTYPE.sql")
                .addAsResource("dev-imports/COMPETITION_ROBOTS.sql", "imports/COMPETITION_ROBOTS.sql")
                .addAsResource("dev-imports/LINEFOLLOWER_COMPETITION.sql", "imports/LINEFOLLOWER_COMPETITION.sql")
                .addAsResource("dev-imports/SUMO_COMPETITION.sql", "imports/SUMO_COMPETITION.sql")
                .addAsResource("dev-imports/SUMO_MATCH.sql", "imports/SUMO_MATCH.sql")
                .addAsResource("dev-imports/FREESTYLE_COMPETITION.sql", "imports/FREESTYLE_COMPETITION.sql")
                .addAsResource("dev-imports/SYSTEM_USER.sql", "imports/SYSTEM_USER.sql")
                .addAsResource("dev-imports/TEAM.sql", "imports/TEAM.sql")
                .addAsResource("dev-imports/COMPETITOR.sql", "imports/COMPETITOR.sql")
                .addAsResource("dev-imports/ROBOT_SUBTYPE.sql", "imports/ROBOT_SUBTYPE.sql")
                .addAsResource("dev-imports/ROBOT.sql", "imports/ROBOT.sql")
                .addAsResource("dev-imports/NOTICE.sql", "imports/NOTICE.sql")
                .addAsResource("dev-imports/LINEFOLLOWER_RESULT.sql", "imports/LINEFOLLOWER_RESULT.sql")
                .addAsResource("dev-imports/FREESTYLE_RESULT.sql", "imports/FREESTYLE_RESULT.sql")
                .addAsLibraries(dependencies())
                ;
    }

    @Deployment(name = NOT_SECURED_WEBAPP, testable = false)
    public static WebArchive notSecuredWebapp() {
        WebArchive war = webapp("rc-webapp-unsecured.war");

        if(LOG.isDebugEnabled()) {
            LOG.debug(war.toString(true));
        }
        return war;
    }

    @Deployment(name = SECURED_WEBAPP, testable = false)
    public static WebArchive securedWebapp() {
        WebArchive war = webapp("rc-webapp-secured.war")
                .addAsWebInfResource(new File(TEST_WEBAPP_SRC, "WEB-INF/jboss-web.xml"))
                .addAsWebInfResource(new File(TEST_WEBAPP_SRC, "WEB-INF/web.xml"));

        if(LOG.isDebugEnabled()) {
            LOG.debug(war.toString(true));
        }
        return war;
    }
}
