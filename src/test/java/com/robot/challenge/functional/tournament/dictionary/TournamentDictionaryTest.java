package com.robot.challenge.functional.tournament.dictionary;

import com.robot.challenge.fixture.tournament.dictionary.PreLoadedCompetitionSubtype;
import com.robot.challenge.fixture.tournament.dictionary.PreLoadedRobotType;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.tournament.dictionary.domain.CompetitionType;
import com.robot.challenge.tournament.dictionary.rest.model.CompetitionSubtypeModel;
import com.robot.challenge.tournament.dictionary.rest.model.CompetitionTypeModel;
import com.robot.challenge.tournament.dictionary.rest.model.RobotTypeModel;
import com.robot.challenge.tournament.dictionary.rest.resource.TournamentDictionaryResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by mklys on 05/02/16.
 */
@RunWith(Arquillian.class)
public class TournamentDictionaryTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(DeploymentSetup.NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAllCompetitionTypes(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TournamentDictionaryResource.DICTIONARY_PREFIX + TournamentDictionaryResource.COMPETITION_TYPE)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<CompetitionTypeModel> competitionTypes = response.readEntity(new GenericType<List<CompetitionTypeModel>>() {});
        assertThat(competitionTypes.size()).isEqualTo(Arrays.asList(CompetitionType.values()).size());
        Arrays.stream(CompetitionType.values()).forEach(preLoadedCompetitionType -> {
            Optional<CompetitionTypeModel> found = competitionTypes.stream()
                    .filter(competitionType -> assertCompetitionTypeDictionaryItem(competitionType, preLoadedCompetitionType))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertCompetitionTypeDictionaryItem(CompetitionTypeModel actual, CompetitionType expected) {
        return actual.getName().equals(expected.name());
    }

    @Test
    @OperateOnDeployment(DeploymentSetup.NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAllCompetitionSubtypes(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TournamentDictionaryResource.DICTIONARY_PREFIX + TournamentDictionaryResource.COMPETITION_SUBTYPE)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<CompetitionSubtypeModel> competitionTypes = response.readEntity(new GenericType<List<CompetitionSubtypeModel>>() {});
        assertThat(competitionTypes.size()).isEqualTo(Arrays.asList(PreLoadedCompetitionSubtype.values()).size());
        Arrays.stream(PreLoadedCompetitionSubtype.values()).forEach(preLoadedCompetitionSubtype -> {
            Optional<CompetitionSubtypeModel> found = competitionTypes.stream()
                    .filter(competitionType -> assertCompetitionTypeDictionaryItem(competitionType, preLoadedCompetitionSubtype))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertCompetitionTypeDictionaryItem(CompetitionSubtypeModel actual, PreLoadedCompetitionSubtype expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getName().equals(expected.typeName()) &&
                actual.getCompetitionType().getName().equals(expected.competitionType().name()) &&
                actual.getRobotType().getGuid().equals(expected.getRobotType().guid()) &&
                actual.getRobotType().getName().equals(expected.getRobotType().typeName()) &&
                actual.getVersion() != null;
    }

    @Test
    @OperateOnDeployment(DeploymentSetup.NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAllRobotTypes(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TournamentDictionaryResource.DICTIONARY_PREFIX + TournamentDictionaryResource.ROBOT_TYPE)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<RobotTypeModel> robotTypes = response.readEntity(new GenericType<List<RobotTypeModel>>() {});
        assertThat(robotTypes.size()).isEqualTo(Arrays.asList(PreLoadedRobotType.values()).size());
        Arrays.stream(PreLoadedRobotType.values()).forEach(preLoadedRobotType -> {
            Optional<RobotTypeModel> found = robotTypes.stream()
                    .filter(robotType -> assertRobotTypeDictionaryItem(robotType, preLoadedRobotType))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertRobotTypeDictionaryItem(RobotTypeModel actual, PreLoadedRobotType expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getName().equals(expected.typeName()) &&
                actual.getVersion() != null;
    }
}
