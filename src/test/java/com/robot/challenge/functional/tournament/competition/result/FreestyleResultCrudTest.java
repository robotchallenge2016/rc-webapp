package com.robot.challenge.functional.tournament.competition.result;

import com.robot.challenge.fixture.tournament.competition.result.FreestyleResultFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.ExistingFreestyleResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.NewFreestyleResultModel;
import com.robot.challenge.tournament.competition.rest.resource.result.FreestyleResultResource;
import com.robot.challenge.tournament.competition.rest.resource.result.ResultResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class FreestyleResultCrudTest extends ExtendedCrudTest<NewFreestyleResultModel, ExistingFreestyleResultModel, FreestyleResultFixture> {

    public FreestyleResultCrudTest() {
        super(ExistingFreestyleResultModel.class, new FreestyleResultFixture());
    }

    @Override
    protected void assertCreated(NewFreestyleResultModel expected, ExistingFreestyleResultModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getPlace()).isEqualTo(expected.getPlace());
        assertThat(actual.getCompetitionGuid()).isEqualTo(expected.getCompetitionGuid());
        assertThat(actual.getRobotGuid()).isEqualTo(expected.getRobotGuid());
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingFreestyleResultModel actual, NewFreestyleResultModel original, ModelCreated created, ExistingFreestyleResultModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getPlace()).isEqualTo(expected.getPlace());
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX + ResultResource.RESULT_CONTEXT + FreestyleResultResource.DEFAULT_FREESTYLE_RESULT_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + FreestyleResultResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + FreestyleResultResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + FreestyleResultResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + FreestyleResultResource.DELETE;
    }
}
