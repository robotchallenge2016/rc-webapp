package com.robot.challenge.functional.tournament.competition;

import com.robot.challenge.fixture.tournament.competition.SumoCompetitionFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.rest.model.sumo.ExistingSumoCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.sumo.NewSumoCompetitionModel;
import com.robot.challenge.tournament.competition.rest.resource.CompetitionResource;
import com.robot.challenge.tournament.competition.rest.resource.SumoCompetitionResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class SumoCompetitionCrudTest extends ExtendedCrudTest<NewSumoCompetitionModel, ExistingSumoCompetitionModel, SumoCompetitionFixture> {

    public SumoCompetitionCrudTest() {
        super(ExistingSumoCompetitionModel.class, new SumoCompetitionFixture());
    }

    @Override
    protected void assertCreated(NewSumoCompetitionModel expected, ExistingSumoCompetitionModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getCompetitionSubtypeGuid()).isEqualTo(expected.getCompetitionSubtypeGuid());
        expected.getRobotGuids().forEach(expectedRobotGuid -> {
            boolean found = actual.getRobotGuids().stream().filter(expectedRobotGuid::equals).findFirst().isPresent();
            assertThat(found).isTrue();
        });
        assertThat(actual.getRequiredNumberOfWins()).isEqualTo(expected.getRequiredNumberOfWins());
        assertThat(actual.getBigWinPoints()).isEqualTo(expected.getBigWinPoints());
        assertThat(actual.getBigLostPoints()).isEqualTo(expected.getBigLostPoints());
        assertThat(actual.getSmallWinPoints()).isEqualTo(expected.getSmallWinPoints());
        assertThat(actual.getSmallLostPoints()).isEqualTo(expected.getSmallLostPoints());
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingSumoCompetitionModel actual, NewSumoCompetitionModel original, ModelCreated created, ExistingSumoCompetitionModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getCompetitionSubtypeGuid()).isEqualTo(expected.getCompetitionSubtypeGuid());
        expected.getRobotGuids().forEach(expectedRobotGuid -> {
            boolean found = actual.getRobotGuids().stream().filter(expectedRobotGuid::equals).findFirst().isPresent();
            assertThat(found).isTrue();
        });
        assertThat(actual.getRequiredNumberOfWins()).isEqualTo(expected.getRequiredNumberOfWins());
        assertThat(actual.getBigWinPoints()).isEqualTo(expected.getBigWinPoints());
        assertThat(actual.getBigLostPoints()).isEqualTo(expected.getBigLostPoints());
        assertThat(actual.getSmallWinPoints()).isEqualTo(expected.getSmallWinPoints());
        assertThat(actual.getSmallLostPoints()).isEqualTo(expected.getSmallLostPoints());
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX + CompetitionResource.DEFAULT_COMPETITION_PREFIX + SumoCompetitionResource.DEFAULT_SUMO_COMPETITION_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + SumoCompetitionResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + SumoCompetitionResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + SumoCompetitionResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + SumoCompetitionResource.DELETE;
    }    
}
