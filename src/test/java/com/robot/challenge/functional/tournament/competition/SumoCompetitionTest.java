package com.robot.challenge.functional.tournament.competition;

import com.robot.challenge.fixture.tournament.competition.PreLoadedCompetition;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.tournament.competition.rest.resource.CompetitionResource;
import com.robot.challenge.tournament.competition.rest.resource.SumoCompetitionResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class SumoCompetitionTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanStartSumoCompetition(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(context() + SumoCompetitionResource.COMPETITION_START)
                .resolveTemplate("guid", PreLoadedCompetition.MINI_SUMO.guid())
                .request();
        final Invocation request = requestBuilder.buildPost(null);
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanStopSumoCompetition(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(context() + SumoCompetitionResource.COMPETITION_STOP)
                .resolveTemplate("guid", PreLoadedCompetition.MINI_SUMO.guid())
                .request();
        final Invocation request = requestBuilder.buildPost(null);
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanGenerateSumoCompetitionSchedule(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(context() + SumoCompetitionResource.COMPETITION_STOP)
                .resolveTemplate("guid", PreLoadedCompetition.MINI_SUMO.guid())
                .request();
        final Invocation request = requestBuilder.buildPost(null);
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX +
                CompetitionResource.DEFAULT_COMPETITION_PREFIX +
                SumoCompetitionResource.DEFAULT_SUMO_COMPETITION_PREFIX;
    }
}
