package com.robot.challenge.functional.tournament.competitor;

import com.robot.challenge.fixture.tournament.competitor.TeamFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competitor.rest.model.team.ExistingTeamModel;
import com.robot.challenge.tournament.competitor.rest.model.team.NewTeamModel;
import com.robot.challenge.tournament.competitor.rest.resource.TeamResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class TeamCrudTest extends ExtendedCrudTest<NewTeamModel, ExistingTeamModel, TeamFixture> {

    public TeamCrudTest() {
        super(ExistingTeamModel.class, new TeamFixture());
    }

    @Override
    protected void assertCreated(NewTeamModel expected, ExistingTeamModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getCaptainGuid()).isEqualTo(expected.getCaptainGuid());
        assertThat(actual.getCompetitorGuids().size()).isEqualTo(expected.getCompetitorGuids().size() + 1);
        expected.getCompetitorGuids().forEach(competitorGuid -> assertThat(actual.getCompetitorGuids()).contains(competitorGuid));
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingTeamModel actual, NewTeamModel original, ModelCreated created, ExistingTeamModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getCaptainGuid()).isEqualTo(original.getCaptainGuid());
        assertThat(actual.getCompetitorGuids().size()).isEqualTo(expected.getCompetitorGuids().size() + 1);
        expected.getCompetitorGuids().forEach(competitorGuid -> assertThat(actual.getCompetitorGuids()).contains(competitorGuid));
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantCreateTeamWithoutName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        createValidationAssertion(target, fixture().newTeamWithoutName());
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantCreateTeamWithoutCompetitors(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        createValidationAssertion(target, fixture().newTeamWithoutCaptain());
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantUpdateTeamWithoutName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        updateValidationAssertion(target, fixture().existingTeamWithoutName(existingModel()));
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantUpdateTeamWithoutVersion(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        updateValidationAssertion(target, fixture().existingTeamWithoutVersion(existingModel()));
    }    

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + TeamResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + TeamResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + TeamResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + TeamResource.DELETE;
    }    
}
