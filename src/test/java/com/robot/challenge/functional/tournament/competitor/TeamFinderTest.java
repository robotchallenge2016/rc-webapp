package com.robot.challenge.functional.tournament.competitor;

import com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedTeam;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.tournament.competitor.rest.model.team.BriefTeamModel;
import com.robot.challenge.tournament.competitor.rest.model.team.ExistingTeamModel;
import com.robot.challenge.tournament.competitor.rest.resource.TeamResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by mklys on 04/02/16.
 */
@RunWith(Arquillian.class)
public class TeamFinderTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAll(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.RETRIEVE_ALL)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefTeamModel> teams = response.readEntity(new GenericType<List<BriefTeamModel>>() {});
        Arrays.stream(PreLoadedTeam.values()).forEach(preLoadedTeam -> {
            Optional<BriefTeamModel> found = teams.stream()
                    .filter(team -> assertBriefTeamModel(team, preLoadedTeam))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertBriefTeamModel(BriefTeamModel actual, PreLoadedTeam expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getName().equals(expected.teamName());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfExistingNameExists(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.TEAM_NAME_EXISTS)
                .resolveTemplate("name", PreLoadedTeam.LINEFOLLOWER_TEAM.teamName())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantCheckIfNameExistsByEmptyName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.TEAM_NAME_EXISTS)
                .resolveTemplate("name", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfNotExistingNameExists(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.TEAM_NAME_EXISTS)
                .resolveTemplate("name", "nonExistingName")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfExistingNameIsFree(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.TEAM_NAME_IS_FREE)
                .resolveTemplate("name", PreLoadedTeam.FREESTYLE_TEAM.teamName())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfNotExistingNameIsFree(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.TEAM_NAME_IS_FREE)
                .resolveTemplate("name", "nonExistingName")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantCheckIfNameIsFreeByEmptyName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.TEAM_NAME_IS_FREE)
                .resolveTemplate("name", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveTeamByCompetitor(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.RETRIEVE_BY_COMPETITOR)
                .resolveTemplate("guid", PreLoadedCompetitor.MACIEJ_ZURAWSKI.guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        ExistingTeamModel owner = response.readEntity(ExistingTeamModel.class);
        assertExistingTeamModel(owner, PreLoadedCompetitor.MACIEJ_ZURAWSKI.team().get());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatReturnsNotFoundWhenCompetitorIsNotTeamMember(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.RETRIEVE_BY_COMPETITOR)
                .resolveTemplate("guid", PreLoadedCompetitor.RADOSLAW_MAJDAN.guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantSearchRobotOwnerByEmptyOwnerGuid(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.RETRIEVE_BY_COMPETITOR)
                .resolveTemplate("guid", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchTeamByName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.SEARCH_MATCHING_ANY_PARAM)
                .queryParam("search", "UMO")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<ExistingTeamModel> teams = response.readEntity(new GenericType<List<ExistingTeamModel>>() {});
        assertSearchResult(teams, Arrays.asList(PreLoadedTeam.MINI_SUMO_TEAM, PreLoadedTeam.STANDARD_SUMO_TEAM));
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantSearchTeamByEmptySearchParam(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(TournamentResource.TOURNAMENT_PREFIX + TeamResource.SEARCH_MATCHING_ANY_PARAM)
                .queryParam("search", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    private void assertSearchResult(List<ExistingTeamModel> actual, List<PreLoadedTeam> expected) {
        assertThat(actual.size()).isEqualTo(expected.size());
        expected.stream().forEach(preLoadedTeam -> {
            Optional<ExistingTeamModel> found = actual.stream()
                    .filter(team -> assertExistingTeamModel(team, preLoadedTeam))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertExistingTeamModel(ExistingTeamModel actual, PreLoadedTeam expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getName().equals(expected.teamName()) &&
                !actual.getCompetitorGuids().isEmpty() &&
                actual.getVersion() != null;
    }
}
