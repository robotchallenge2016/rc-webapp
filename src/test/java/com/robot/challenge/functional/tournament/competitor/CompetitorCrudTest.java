package com.robot.challenge.functional.tournament.competitor;

import com.robot.challenge.fixture.tournament.competitor.CompetitorFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competitor.rest.model.competitor.ExistingCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.model.competitor.NewCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.resource.CompetitorResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by mklys on 02/02/16.
 */
@RunWith(Arquillian.class)
public class CompetitorCrudTest extends ExtendedCrudTest<NewCompetitorModel, ExistingCompetitorModel, CompetitorFixture> {

    public CompetitorCrudTest() {
        super(ExistingCompetitorModel.class, new CompetitorFixture());
    }

    @Override
    protected void assertCreated(NewCompetitorModel expected, ExistingCompetitorModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getLogin()).isEqualTo(expected.getLogin());
        assertThat(actual.getFirstName()).isEqualTo(expected.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(expected.getLastName());
        assertThat(actual.getTeamGuid()).isEqualTo(expected.getTeamGuid());
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingCompetitorModel actual, NewCompetitorModel original, ModelCreated created, ExistingCompetitorModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getLogin()).isEqualTo(original.getLogin());
        assertThat(actual.getFirstName()).isEqualTo(expected.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(expected.getLastName());
        assertThat(actual.getTeamGuid()).isEqualTo(expected.getTeamGuid());
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + CompetitorResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + CompetitorResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + CompetitorResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + CompetitorResource.DELETE;
    }
}
