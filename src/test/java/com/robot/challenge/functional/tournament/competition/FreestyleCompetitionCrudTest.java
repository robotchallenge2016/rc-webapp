package com.robot.challenge.functional.tournament.competition;

import com.robot.challenge.fixture.tournament.competition.FreestyleCompetitionFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.rest.model.freestyle.ExistingFreestyleCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.freestyle.NewFreestyleCompetitionModel;
import com.robot.challenge.tournament.competition.rest.resource.CompetitionResource;
import com.robot.challenge.tournament.competition.rest.resource.FreestyleCompetitionResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class FreestyleCompetitionCrudTest extends ExtendedCrudTest<NewFreestyleCompetitionModel, ExistingFreestyleCompetitionModel, FreestyleCompetitionFixture> {

    public FreestyleCompetitionCrudTest() {
        super(ExistingFreestyleCompetitionModel.class, new FreestyleCompetitionFixture());
    }

    @Override
    protected void assertCreated(NewFreestyleCompetitionModel expected, ExistingFreestyleCompetitionModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getCompetitionSubtypeGuid()).isEqualTo(expected.getCompetitionSubtypeGuid());
        expected.getRobotGuids().forEach(expectedRobotGuid -> {
            boolean found = actual.getRobotGuids().stream().anyMatch(expectedRobotGuid::equals);
            assertThat(found).isTrue();
        });
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingFreestyleCompetitionModel actual, NewFreestyleCompetitionModel original, ModelCreated created, ExistingFreestyleCompetitionModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getCompetitionSubtypeGuid()).isEqualTo(expected.getCompetitionSubtypeGuid());
        expected.getRobotGuids().forEach(expectedRobotGuid -> {
            boolean found = actual.getRobotGuids().stream().anyMatch(expectedRobotGuid::equals);
            assertThat(found).isTrue();
        });
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX + CompetitionResource.DEFAULT_COMPETITION_PREFIX + FreestyleCompetitionResource.DEFAULT_FREESTYLE_COMPETITION_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + FreestyleCompetitionResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + FreestyleCompetitionResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + FreestyleCompetitionResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + FreestyleCompetitionResource.DELETE;
    }    
}
