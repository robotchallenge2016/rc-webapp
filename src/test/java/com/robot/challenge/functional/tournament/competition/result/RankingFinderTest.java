package com.robot.challenge.functional.tournament.competition.result;

import com.robot.challenge.fixture.tournament.competition.PreLoadedCompetition;
import com.robot.challenge.fixture.tournament.competition.result.PreLoadedLineFollowerResult;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.tournament.competition.rest.model.result.freestyle.FreestyleRankingModel;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.BriefLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.sumo.SumoRankingModel;
import com.robot.challenge.tournament.competition.rest.resource.result.RankingResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class RankingFinderTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveLineFollowerRanking(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(context() + RankingResource.DEFAULT_LINEFOLLOWER_RANKING)
                .resolveTemplate("guid", PreLoadedCompetition.LINEFOLLOWER.guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        List<BriefLineFollowerResultModel> results = response.readEntity(new GenericType<List<BriefLineFollowerResultModel>>() {});

        List<PreLoadedLineFollowerResult> expectedWithOrder = Arrays.asList(PreLoadedLineFollowerResult.LINEFOLLOWER_RESULT_WALL_E_1_BEST,
                PreLoadedLineFollowerResult.LINEFOLLOWER_RESULT_ENERGIZER_BUNNY_3_BEST,
                PreLoadedLineFollowerResult.LINEFOLLOWER_RESULT_LINEFOLLOWER_ROBOT_2_BEST);

        assertThat(results.size()).isEqualTo(expectedWithOrder.size());

        for(int i = 0; i < expectedWithOrder.size(); ++i) {
            BriefLineFollowerResultModel actual = results.get(i);
            PreLoadedLineFollowerResult expected = expectedWithOrder.get(i);

            assertThat(actual.getGuid()).isEqualTo(expected.guid());
            assertThat(actual.getResult()).isEqualTo(expected.result());
        }
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveSumoRanking(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(context() + RankingResource.DEFAULT_SUMO_RANKING)
                .resolveTemplate("guid", PreLoadedCompetition.STANDARD_SUMO.guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<SumoRankingModel> results = response.readEntity(new GenericType<List<SumoRankingModel>>() {});

        List<PreLoadedRobot> robotsInCompetition = Arrays.asList(PreLoadedRobot.R2_D2,
                PreLoadedRobot.ROSIE,
                PreLoadedRobot.THE_IRON_GIANT,
                PreLoadedRobot.PROWL,
                PreLoadedRobot.TRAILBREAKER,
                PreLoadedRobot.HOIST);
        assertThat(robotsInCompetition.size()).isEqualTo(robotsInCompetition.size());

        assertSumoRanking(results.get(0), PreLoadedRobot.MIRAGE, 6, 2, 2, 0, 6);
        assertSumoRanking(results.get(1), PreLoadedRobot.SMOKESCREEN, 6, 2, 2, 0, 5);
        assertSumoRanking(results.get(2), PreLoadedRobot.CLIFFJUMPER, 5, 2, 1, 1, 4);
        assertSumoRanking(results.get(3), PreLoadedRobot.ROOMBA, 3, 2, 1, 1, 3);
        assertSumoRanking(results.get(4), PreLoadedRobot.T_1000, 1, 2, 0, 2, 0);
        assertSumoRanking(results.get(5), PreLoadedRobot.MINI_SUMO_ROBOT, 0, 2, 0, 2, 0);
    }

    private void assertSumoRanking(SumoRankingModel ranking, PreLoadedRobot expectedRobot, int expectedFightsWon,
                                   int expectedPlayed, int expectedWin, int expectedLost, int expectedPoints) {
        assertThat(ranking.getRobot().getGuid()).isEqualTo(expectedRobot.guid());
        assertThat(ranking.getFightsWon()).isEqualTo(expectedFightsWon);
        assertThat(ranking.getPlayed()).isEqualTo(expectedPlayed);
        assertThat(ranking.getWin()).isEqualTo(expectedWin);
        assertThat(ranking.getLost()).isEqualTo(expectedLost);
        assertThat(ranking.getPoints()).isEqualTo(expectedPoints);
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveFreestyleRanking(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(context() + RankingResource.DEFAULT_FREESTYLE_RANKING)
                .resolveTemplate("guid", PreLoadedCompetition.FREESTYLE.guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        List<FreestyleRankingModel> results = response.readEntity(new GenericType<List<FreestyleRankingModel>>() {});

        FreestyleRankingModel first = results.get(0);
        assertThat(first.getRobot().getGuid()).isEqualTo(PreLoadedRobot.ROBOCOP.guid());
        assertThat(first.getPoints()).isEqualTo(9);
        assertThat(first.getFirstPlaceCount()).isEqualTo(3);
        assertThat(first.getSecondPlaceCount()).isEqualTo(0);
        assertThat(first.getThirdPlaceCount()).isEqualTo(0);

        FreestyleRankingModel second = results.get(1);
        assertThat(second.getRobot().getGuid()).isEqualTo(PreLoadedRobot.ASIMO.guid());
        assertThat(second.getPoints()).isEqualTo(5);
        assertThat(second.getFirstPlaceCount()).isEqualTo(0);
        assertThat(second.getSecondPlaceCount()).isEqualTo(2);
        assertThat(second.getThirdPlaceCount()).isEqualTo(1);

        FreestyleRankingModel third = results.get(2);
        assertThat(third.getRobot().getGuid()).isEqualTo(PreLoadedRobot.T_800.guid());
        assertThat(third.getPoints()).isEqualTo(4);
        assertThat(third.getFirstPlaceCount()).isEqualTo(0);
        assertThat(third.getSecondPlaceCount()).isEqualTo(1);
        assertThat(third.getThirdPlaceCount()).isEqualTo(2);
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX + RankingResource.RANKING_CONTEXT;
    }
}
