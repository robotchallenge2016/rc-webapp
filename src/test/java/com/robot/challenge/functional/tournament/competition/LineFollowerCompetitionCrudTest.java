package com.robot.challenge.functional.tournament.competition;

import com.robot.challenge.fixture.tournament.competition.LineFollowerCompetitionFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.rest.model.linefollower.ExistingLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competition.rest.model.linefollower.NewLineFollowerCompetitionModel;
import com.robot.challenge.tournament.competition.rest.resource.CompetitionResource;
import com.robot.challenge.tournament.competition.rest.resource.LineFollowerCompetitionResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class LineFollowerCompetitionCrudTest extends ExtendedCrudTest<NewLineFollowerCompetitionModel, ExistingLineFollowerCompetitionModel, LineFollowerCompetitionFixture> {

    public LineFollowerCompetitionCrudTest() {
        super(ExistingLineFollowerCompetitionModel.class, new LineFollowerCompetitionFixture());
    }

    @Override
    protected void assertCreated(NewLineFollowerCompetitionModel expected, ExistingLineFollowerCompetitionModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getCompetitionSubtypeGuid()).isEqualTo(expected.getCompetitionSubtypeGuid());
        expected.getRobotGuids().forEach(expectedRobotGuid -> {
            boolean found = actual.getRobotGuids().stream().filter(expectedRobotGuid::equals).findFirst().isPresent();
            assertThat(found).isTrue();
        });
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingLineFollowerCompetitionModel actual, NewLineFollowerCompetitionModel original, ModelCreated created, ExistingLineFollowerCompetitionModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getCompetitionSubtypeGuid()).isEqualTo(expected.getCompetitionSubtypeGuid());
        expected.getRobotGuids().forEach(expectedRobotGuid -> {
            boolean found = actual.getRobotGuids().stream().filter(expectedRobotGuid::equals).findFirst().isPresent();
            assertThat(found).isTrue();
        });
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX + CompetitionResource.DEFAULT_COMPETITION_PREFIX + LineFollowerCompetitionResource.DEFAULT_LINEFOLLOWER_COMPETITION_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + LineFollowerCompetitionResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + LineFollowerCompetitionResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + LineFollowerCompetitionResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + LineFollowerCompetitionResource.DELETE;
    }    
}
