package com.robot.challenge.functional.tournament.competitor;

import com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.fixture.tournament.dictionary.PreLoadedRobotType;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.infrastructure.rest.model.GuidsModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.BriefRobotModel;
import com.robot.challenge.tournament.competitor.rest.resource.RobotResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot.HOIST;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot.LINEFOLLOWER_ENHANCED_ROBOT;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot.MINI_SUMO_ROBOT;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot.PROWL;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot.R2_D2;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot.ROSIE;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot.THE_IRON_GIANT;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot.TRAILBREAKER;
import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class RobotFinderTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAll(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.RETRIEVE_ALL_BRIEFED)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefRobotModel> robots = response.readEntity(new GenericType<List<BriefRobotModel>>() {});
        assertThat(robots.size()).isEqualTo(PreLoadedRobot.values().length);
        Arrays.stream(PreLoadedRobot.values()).forEach(preLoadedRobot -> {
            Optional<BriefRobotModel> found = robots.stream()
                    .filter(robot -> assertBriefRobotModel(robot, preLoadedRobot))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertBriefRobotModel(BriefRobotModel actual, PreLoadedRobot expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getName().equals(expected.robotName()) &&
                actual.getOwner().getLogin().equals(expected.owner().login()) &&
                (
                        !expected.owner().team().isPresent() || expected.owner().team().get().teamName().equals(actual.getOwner().getTeamName())
                );
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveByGuids(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final GuidsModel guids = GuidsModel.builder()
                .addGuid(PreLoadedRobot.LINEFOLLOWER_ROBOT.guid())
                .addGuid(PreLoadedRobot.STANDARD_SUMO_ROBOT.guid())
                .build();

        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.RETRIEVE_BY_GUIDS)
                .request();
        final Invocation request = requestBuilder.buildPost(Entity.entity(guids, MediaType.APPLICATION_JSON_TYPE));
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefRobotModel> robots = response.readEntity(new GenericType<List<BriefRobotModel>>() {});
        List<PreLoadedRobot> expectedResult = Arrays.asList(PreLoadedRobot.LINEFOLLOWER_ROBOT, PreLoadedRobot.STANDARD_SUMO_ROBOT);

        assertSearchResult(robots, expectedResult);
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfExistingNameExists(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.ROBOT_NAME_EXISTS)
                .resolveTemplate("name", LINEFOLLOWER_ENHANCED_ROBOT.robotName())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantCheckIfNameExistsByEmptyName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.ROBOT_NAME_EXISTS)
                .resolveTemplate("name", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfNotExistingNameExists(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.ROBOT_NAME_EXISTS)
                .resolveTemplate("name", "nonExistingName")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfExistingNameIsFree(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.ROBOT_NAME_IS_FREE)
                .resolveTemplate("name", MINI_SUMO_ROBOT.robotName())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfNotExistingNameIsFree(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.ROBOT_NAME_IS_FREE)
                .resolveTemplate("name", "nonExistingName")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantCheckIfNameIsFreeByEmptyName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.ROBOT_NAME_IS_FREE)
                .resolveTemplate("name", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchByType(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource){
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.RETRIEVE_ROBOTS_BY_TYPE)
                .resolveTemplate("guid", PreLoadedRobotType.MINI_SUMO.guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefRobotModel> type = response.readEntity(new GenericType<List<BriefRobotModel>>() {});
        assertSearchResult(type, Arrays.asList(R2_D2, ROSIE, THE_IRON_GIANT, PROWL, TRAILBREAKER, HOIST));
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchRobotsByOwner(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.RETRIEVE_ROBOTS_BY_COMPETITOR)
                .resolveTemplate("guid", PreLoadedCompetitor.MACIEJ_ZURAWSKI.guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefRobotModel> owner = response.readEntity(new GenericType<List<BriefRobotModel>>() {});
        assertSearchResult(owner, Arrays.asList(PreLoadedRobot.ENERGIZER_BUNNY, PreLoadedRobot.SKYNET));
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantSearchRobotsByOwnerWithEmptyOwnerGuid(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.RETRIEVE_ROBOTS_BY_COMPETITOR)
                .resolveTemplate("guid", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchRobotByName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.SEARCH_MATCHING_ANY_PARAM)
                .queryParam("search", "sumo")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefRobotModel> competitors = response.readEntity(new GenericType<List<BriefRobotModel>>() {});
        List<PreLoadedRobot> expectedResult = Arrays.asList(PreLoadedRobot.MINI_SUMO_ROBOT, PreLoadedRobot.STANDARD_SUMO_ROBOT);

        assertSearchResult(competitors, expectedResult);
        assertThat(competitors.size()).isEqualTo(expectedResult.size());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchRobotByTypeAndName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX + RobotResource.SEARCH_BY_TYPE_AND_MATCHING_ANY_PARAM)
                .resolveTemplate("guid", PreLoadedRobotType.MINI_SUMO.guid())
                .queryParam("search", "ro")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefRobotModel> competitors = response.readEntity(new GenericType<List<BriefRobotModel>>() {});
        List<PreLoadedRobot> expectedResult = Arrays.asList(ROSIE, THE_IRON_GIANT, PROWL);

        assertSearchResult(competitors, expectedResult);
        assertThat(competitors.size()).isEqualTo(expectedResult.size());
    }

    private void assertSearchResult(List<BriefRobotModel> actual, List<PreLoadedRobot> expected) {
        assertThat(actual.size()).isEqualTo(expected.size());
        expected.forEach(preLoadedCompetitor -> {
            Optional<BriefRobotModel> found = actual.stream()
                    .filter(competitor -> assertRobotResult(competitor, preLoadedCompetitor))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertRobotResult(BriefRobotModel actual, PreLoadedRobot expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getName().equals(expected.robotName()) &&
                actual.getRobotType().getGuid().equals(expected.robotType().guid()) &&
                actual.getRobotType().getName().equals(expected.robotType().typeName()) &&
                actual.getOwner().getGuid().equals(expected.owner().guid());
    }
}
