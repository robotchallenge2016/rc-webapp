package com.robot.challenge.functional.tournament.competitor;

import com.robot.challenge.fixture.tournament.competitor.RobotFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competitor.rest.model.robot.ExistingRobotModel;
import com.robot.challenge.tournament.competitor.rest.model.robot.NewRobotModel;
import com.robot.challenge.tournament.competitor.rest.resource.RobotResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class RobotCrudTest extends ExtendedCrudTest<NewRobotModel, ExistingRobotModel, RobotFixture> {

    public RobotCrudTest() {
        super(ExistingRobotModel.class, new RobotFixture());
    }

    @Override
    protected void assertCreated(NewRobotModel expected, ExistingRobotModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getOwnerGuid()).isEqualTo(expected.getOwnerGuid());
        assertThat(actual.getRobotTypeGuid()).isEqualTo(expected.getRobotTypeGuid());
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingRobotModel actual, NewRobotModel original, ModelCreated created, ExistingRobotModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getName()).isEqualTo(expected.getName());
        assertThat(actual.getOwnerGuid()).isEqualTo(expected.getOwnerGuid());
        assertThat(actual.getRobotTypeGuid()).isEqualTo(expected.getRobotTypeGuid());
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantCreateRobotWithoutName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        createValidationAssertion(target, fixture().newRobotWithoutName());
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantCreateRobotWithoutType(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        createValidationAssertion(target, fixture().newRobotWithoutType());
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantCreateRobotWithoutOwner(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        createValidationAssertion(target, fixture().newRobotWithoutOwner());
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantUpdateRobotWithoutName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        updateValidationAssertion(target, fixture().existingRobotWithoutName(existingModel()));
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantUpdateRobotWithoutType(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        updateValidationAssertion(target, fixture().existingRobotWithoutType(existingModel()));
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantUpdateRobotWithoutOwner(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        updateValidationAssertion(target, fixture().existingRobotWithoutOwner(existingModel()));
    }

    @Test
    @OperateOnDeployment(SECURED_WEBAPP)
    public void thatCantUpdateRobotWithoutVersion(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget target) {
        updateValidationAssertion(target, fixture().existingRobotWithoutVersion(existingModel()));
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX + RobotResource.DEFAULT_ROBOT_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + RobotResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + RobotResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + RobotResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + RobotResource.DELETE;
    }
}
