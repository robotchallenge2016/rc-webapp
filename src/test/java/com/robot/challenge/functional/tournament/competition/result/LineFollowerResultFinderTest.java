package com.robot.challenge.functional.tournament.competition.result;

import com.robot.challenge.fixture.tournament.competition.result.PreLoadedLineFollowerResult;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.BriefLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.resource.result.LineFollowerResultResource;
import com.robot.challenge.tournament.competition.rest.resource.result.ResultResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class LineFollowerResultFinderTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAll(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(context() + LineFollowerResultResource.RETRIEVE_ALL)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefLineFollowerResultModel> results = response.readEntity(new GenericType<List<BriefLineFollowerResultModel>>() {});
        assertThat(results.size()).isEqualTo(PreLoadedLineFollowerResult.values().length);
        Arrays.stream(PreLoadedLineFollowerResult.values()).forEach(preLoadedLineFollowerResult -> {
            Optional<BriefLineFollowerResultModel> found = results.stream()
                    .filter(robot -> assertBriefLineFollowerResult(robot, preLoadedLineFollowerResult))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertBriefLineFollowerResult(BriefLineFollowerResultModel actual, PreLoadedLineFollowerResult expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getResult().equals(expected.result()) &&
                actual.getAuthor().getGuid().equals(expected.author().guid()) &&
                actual.getCompetition().getGuid().equals(expected.competition().guid()) &&
                actual.getRobot().getGuid().equals(expected.robot().guid());
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX + ResultResource.RESULT_CONTEXT + LineFollowerResultResource.DEFAULT_LINEFOLLOWER_RESULT_PREFIX;
    }
}
