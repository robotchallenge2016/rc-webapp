package com.robot.challenge.functional.tournament.competition.result;

import com.robot.challenge.fixture.tournament.competition.result.LineFollowerResultFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.ExistingLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.model.result.linefollower.NewLineFollowerResultModel;
import com.robot.challenge.tournament.competition.rest.resource.result.LineFollowerResultResource;
import com.robot.challenge.tournament.competition.rest.resource.result.ResultResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class LineFollowerResultCrudTest extends ExtendedCrudTest<NewLineFollowerResultModel, ExistingLineFollowerResultModel, LineFollowerResultFixture> {

    public LineFollowerResultCrudTest() {
        super(ExistingLineFollowerResultModel.class, new LineFollowerResultFixture());
    }

    @Override
    protected void assertCreated(NewLineFollowerResultModel expected, ExistingLineFollowerResultModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getResult()).isEqualTo(expected.getResult());
        assertThat(actual.getCompetitionGuid()).isEqualTo(expected.getCompetitionGuid());
        assertThat(actual.getRobotGuid()).isEqualTo(expected.getRobotGuid());
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingLineFollowerResultModel actual, NewLineFollowerResultModel original, ModelCreated created, ExistingLineFollowerResultModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getResult()).isEqualTo(expected.getResult());
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    private String context() {
        return TournamentResource.TOURNAMENT_PREFIX + ResultResource.RESULT_CONTEXT + LineFollowerResultResource.DEFAULT_LINEFOLLOWER_RESULT_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + LineFollowerResultResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + LineFollowerResultResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + LineFollowerResultResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + LineFollowerResultResource.DELETE;
    }
}
