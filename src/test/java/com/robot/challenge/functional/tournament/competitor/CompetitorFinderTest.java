package com.robot.challenge.functional.tournament.competitor;

import com.robot.challenge.fixture.system.PreLoadedSystemUser;
import com.robot.challenge.fixture.system.PreLoadedUser;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor;
import com.robot.challenge.fixture.tournament.competitor.PreLoadedRobot;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.tournament.competitor.rest.model.competitor.BriefCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.model.competitor.ExistingCompetitorModel;
import com.robot.challenge.tournament.competitor.rest.resource.CompetitorResource;
import com.robot.challenge.tournament.rest.resource.TournamentResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.robot.challenge.fixture.system.PreLoadedSystemUser.SLAWOMIR_BANDO;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.ARKADIUSZ_GLOWACKI;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.DAMIAN_GORAWSKI;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.DANIEL_DUBICKI;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.GRZEGORZ_PATER;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.KAZIMIERZ_MOSKAL;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.MARCIN_KUZBA;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.MIROSLAW_SZYMKOWIAK;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.RADOSLAW_MAJDAN;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.TOMASZ_FRANKOWSKI;
import static com.robot.challenge.fixture.tournament.competitor.PreLoadedCompetitor.values;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by mklys on 04/02/16.
 */
@RunWith(Arquillian.class)
public class CompetitorFinderTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAll(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.RETRIEVE_ALL)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefCompetitorModel> competitors = response.readEntity(new GenericType<List<BriefCompetitorModel>>() {});
        Arrays.stream(values()).forEach(preLoadedCompetitor -> {
            Optional<BriefCompetitorModel> found = competitors.stream().filter(competitor ->
                            competitor.getGuid().equals(preLoadedCompetitor.guid()) &&
                            competitor.getLogin().equals(preLoadedCompetitor.login())
            ).findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfExistingLoginExists(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.COMPETITOR_LOGIN_EXISTS)
                .resolveTemplate("login", ARKADIUSZ_GLOWACKI.login())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantCheckIfLoginExistsByEmptyLogin(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.COMPETITOR_LOGIN_EXISTS)
                .resolveTemplate("login", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfNotExistingLoginExists(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.COMPETITOR_LOGIN_EXISTS)
                .resolveTemplate("login", "nonExistingLogin")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfExistingLoginIsFree(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.COMPETITOR_LOGIN_IS_FREE)
                .resolveTemplate("login", ARKADIUSZ_GLOWACKI.login())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfNotExistingLoginIsFree(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.COMPETITOR_LOGIN_IS_FREE)
                .resolveTemplate("login", "nonExistingLogin")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantCheckIfLoginIsFreeByEmptyLogin(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.COMPETITOR_LOGIN_IS_FREE)
                .resolveTemplate("login", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchCompetitorsByLogin(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_MATCHING_ANY_PARAM)
                .queryParam("search", "@gma")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<ExistingCompetitorModel> competitors = response.readEntity(new GenericType<List<ExistingCompetitorModel>>() {});
        List<PreLoadedUser> expectedResult = new ArrayList<>(Arrays.asList(PreLoadedCompetitor.values()));
        expectedResult.addAll(Arrays.asList(PreLoadedSystemUser.values()));

        assertSearchResult(competitors, expectedResult);
        assertThat(competitors.size()).isEqualTo(expectedResult.size());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchCompetitorsByFirstName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_MATCHING_ANY_PARAM)
                .queryParam("search", "OR")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<ExistingCompetitorModel> competitors = response.readEntity(new GenericType<List<ExistingCompetitorModel>>() {});
        assertSearchResult(competitors, Arrays.asList(DAMIAN_GORAWSKI, GRZEGORZ_PATER));
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchCompetitorsByLastName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_MATCHING_ANY_PARAM)
                .queryParam("search", "AN")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<ExistingCompetitorModel> competitors = response.readEntity(new GenericType<List<ExistingCompetitorModel>>() {});
        assertSearchResult(competitors, Arrays.asList(SLAWOMIR_BANDO, TOMASZ_FRANKOWSKI, DAMIAN_GORAWSKI, RADOSLAW_MAJDAN, DANIEL_DUBICKI));
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantSearchCompetitorsByEmptySearchParam(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_MATCHING_ANY_PARAM)
                .queryParam("search", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchCompetitorsWithoutTeamByLogin(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_COMPETITOR_WITHOUT_TEAM_MATCHING_ANY_PARAM)
                .queryParam("search", "@gma")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<ExistingCompetitorModel> competitors = response.readEntity(new GenericType<List<ExistingCompetitorModel>>() {});
        List<PreLoadedUser> expectedResult = PreLoadedSystemUser.teamLessSystemUser();
        expectedResult.addAll(PreLoadedCompetitor.teamLessCompetitors());

        assertSearchResult(competitors, expectedResult);
        assertThat(competitors.size()).isEqualTo(expectedResult.size());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchCompetitorsWithoutTeamByFirstName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_COMPETITOR_WITHOUT_TEAM_MATCHING_ANY_PARAM)
                .queryParam("search", "azi")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<ExistingCompetitorModel> competitors = response.readEntity(new GenericType<List<ExistingCompetitorModel>>() {});
        assertSearchResult(competitors, singletonList(KAZIMIERZ_MOSKAL));
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchCompetitorsWithoutTeamByLastName(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_COMPETITOR_WITHOUT_TEAM_MATCHING_ANY_PARAM)
                .queryParam("search", "AN")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<ExistingCompetitorModel> competitors = response.readEntity(new GenericType<List<ExistingCompetitorModel>>() {});
        assertSearchResult(competitors, Arrays.asList(SLAWOMIR_BANDO, RADOSLAW_MAJDAN, DANIEL_DUBICKI));
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantSearchCompetitorsWithoutTeamByEmptySearchParam(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_COMPETITOR_WITHOUT_TEAM_MATCHING_ANY_PARAM)
                .queryParam("search", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchTeamMembers(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_TEAM_MEMBERS)
                .resolveTemplate("team", TOMASZ_FRANKOWSKI.team().get().guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<ExistingCompetitorModel> competitors = response.readEntity(new GenericType<List<ExistingCompetitorModel>>() {});
        assertSearchResult(competitors, Arrays.asList(MARCIN_KUZBA, MIROSLAW_SZYMKOWIAK, TOMASZ_FRANKOWSKI));
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantSearchTeamMembersByEmptyTeamGuid(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_TEAM_MEMBERS)
                .resolveTemplate("team", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanSearchRobotOwner(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_ROBOT_OWNER)
                .resolveTemplate("robot", PreLoadedRobot.STANDARD_SUMO_ROBOT.guid())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        ExistingCompetitorModel owner = response.readEntity(ExistingCompetitorModel.class);
        assertThat(assertCompetitorResult(owner, PreLoadedSystemUser.JAKUB_PIKON)).isTrue();
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantSearchRobotOwnerByEmptyOwnerGuid(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(TournamentResource.TOURNAMENT_PREFIX + CompetitorResource.SEARCH_ROBOT_OWNER)
                .resolveTemplate("robot", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    private void assertSearchResult(List<ExistingCompetitorModel> actual, List<PreLoadedUser> expected) {
        assertThat(actual.size()).isEqualTo(expected.size());
        expected.stream().forEach(preLoadedCompetitor -> {
            Optional<ExistingCompetitorModel> found = actual.stream()
                    .filter(competitor -> assertCompetitorResult(competitor, preLoadedCompetitor))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertCompetitorResult(ExistingCompetitorModel actual, PreLoadedUser expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getLogin().equals(expected.login()) &&
                actual.getFirstName().equals(expected.firstName()) &&
                actual.getLastName().equals(expected.lastName());
    }
}
