package com.robot.challenge.functional.system.user;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.internal.mapper.ObjectMapperType;
import com.jayway.restassured.response.ResponseBody;
import com.robot.challenge.fixture.system.SystemUserFixture;
import com.robot.challenge.functional.crud.RestAssuredCrudTest;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.infrastructure.utils.JsonDeserializer;
import com.robot.challenge.system.rest.resource.SystemResource;
import com.robot.challenge.system.user.rest.model.user.ExistingSystemUserModel;
import com.robot.challenge.system.user.rest.model.user.NewSystemUserModel;
import com.robot.challenge.system.user.rest.resource.SystemUserResource;
import org.hamcrest.text.IsEmptyString;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.core.Response;
import java.net.URL;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.not;

@RunWith(Arquillian.class)
public class RestAssuredSystemUserTest extends RestAssuredCrudTest<NewSystemUserModel, ExistingSystemUserModel, SystemUserFixture> {

    @ArquillianResource
    private URL deploymentUrl;

    public RestAssuredSystemUserTest() {
        super(new SystemUserFixture());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void crud() {
        crudTest();
    }

    @Override
    protected ModelCreated create(NewSystemUserModel model) {
        ResponseBody response = given()
                .body(model, ObjectMapperType.JACKSON_2)
                    .contentType(ContentType.JSON)
                    .accept(ContentType.JSON)
                .expect()
                    .statusCode(Response.Status.CREATED.getStatusCode())
                    .body("guid", not(IsEmptyString.isEmptyOrNullString()))
                .when()
                    .post(deploymentUrl + RestConfig.ROOT_CONTEXT + SystemResource.SYSTEM_PREFIX + SystemUserResource.CREATE)
                .body();

        return new JsonDeserializer().deserializeToOptional(response.asString(), ModelCreated.class).get();
    }

    @Override
    protected ExistingSystemUserModel retrieve(ModelCreated existing) {
        ResponseBody response = given()
                .body(existing, ObjectMapperType.JACKSON_2)
                    .accept(ContentType.JSON)
                .expect()
                    .statusCode(Response.Status.OK.getStatusCode())
                    .body("guid", not(IsEmptyString.isEmptyOrNullString()))
                .when()
                    .get(deploymentUrl + RestConfig.ROOT_CONTEXT + SystemResource.SYSTEM_PREFIX + SystemUserResource.RETRIEVE_SINGLE, existing.getGuid())
                .body();

        return new JsonDeserializer().deserializeToOptional(response.asString(), ExistingSystemUserModel.class).get();
    }

    @Override
    protected void update(ModelCreated existing, ExistingSystemUserModel model) {
        given()
            .body(model, ObjectMapperType.JACKSON_2)
                .contentType(ContentType.JSON)
            .expect()
                .statusCode(Response.Status.OK.getStatusCode())
            .when()
                .put(deploymentUrl + RestConfig.ROOT_CONTEXT + SystemResource.SYSTEM_PREFIX + SystemUserResource.UPDATE, existing.getGuid());
    }

    @Override
    protected void delete(ModelCreated existing) {
        given()
            .expect()
                .statusCode(Response.Status.OK.getStatusCode())
            .when()
                .delete(deploymentUrl + RestConfig.ROOT_CONTEXT + SystemResource.SYSTEM_PREFIX + SystemUserResource.DELETE, existing.getGuid());
    }
}
