package com.robot.challenge.functional.system.user;

import com.robot.challenge.fixture.system.SystemUserFixture;
import com.robot.challenge.functional.crud.SimpleCrudTest;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.system.rest.resource.SystemResource;
import com.robot.challenge.system.user.rest.model.user.ExistingSystemUserModel;
import com.robot.challenge.system.user.rest.model.user.NewSystemUserModel;
import com.robot.challenge.system.user.rest.resource.SystemUserResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class SimpleSystemUserTest extends SimpleCrudTest<NewSystemUserModel, ExistingSystemUserModel, SystemUserFixture> {

    public SimpleSystemUserTest() {
        super(ExistingSystemUserModel.class, new SystemUserFixture());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void crud(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT + SystemResource.SYSTEM_PREFIX) SystemUserResource systemUserResource) {
//        setCrudResource(systemUserResource);
        crudTest();
    }

    @Override
    protected void assertCreated(NewSystemUserModel expected, ExistingSystemUserModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getLogin()).isEqualTo(expected.getLogin());
        assertThat(actual.getFirstName()).isEqualTo(expected.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(expected.getLastName());
        assertThat(actual.getSystemRoleGuid()).isEqualTo(expected.getSystemRoleGuid());
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingSystemUserModel actual, NewSystemUserModel original, ModelCreated created, ExistingSystemUserModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getLogin()).isEqualTo(original.getLogin());
        assertThat(actual.getFirstName()).isEqualTo(expected.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(expected.getLastName());
        assertThat(actual.getSystemRoleGuid()).isEqualTo(expected.getSystemRoleGuid());
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }
}
