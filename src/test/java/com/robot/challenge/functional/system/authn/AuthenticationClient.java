package com.robot.challenge.functional.system.authn;

import com.robot.challenge.fixture.system.PreLoadedSystemUser;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.system.authn.rest.resource.AuthenticationResource;
import com.robot.challenge.system.rest.resource.SystemResource;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class AuthenticationClient {

    private Cookie maybeSessionCookie;

    private Cookie authenticate(String deploymentUrl) {
        ResteasyClient loginClient = new ResteasyClientBuilder().build();

        ResteasyWebTarget login = loginClient.target(deploymentUrl + RestConfig.ROOT_CONTEXT +
                SystemResource.SYSTEM_PREFIX +
                AuthenticationResource.AUTHN_PREFIX +
                AuthenticationResource.LOGIN_ENDPOINT);

        String body = "j_username=" + PreLoadedSystemUser.MACIEJ_KLYS.login() + "&j_password=admin";

        final Invocation.Builder requestBuilder = login.request();
        final Invocation request = requestBuilder.buildPost(Entity.entity(body, MediaType.APPLICATION_FORM_URLENCODED));
        Response loggedIn = request.invoke();

        assertThat(loggedIn.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        return loggedIn.getCookies().get("JSESSIONID");
    }

    public void addSessionCookie(ResteasyWebTarget target, String deploymentUrl) {
//        if(maybeSessionCookie == null) {
            maybeSessionCookie = authenticate(deploymentUrl);
//        }
        target.register((ClientRequestFilter) requestContext -> {
            List<Object> cookies = new ArrayList<>();
            cookies.add(maybeSessionCookie);
            requestContext.getHeaders().put("Cookie", cookies);
        });
    }
}
