package com.robot.challenge.functional.system.user;

import com.robot.challenge.fixture.system.PreLoadedSystemUser;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.system.rest.resource.SystemResource;
import com.robot.challenge.system.user.rest.model.user.BriefSystemUserModel;
import com.robot.challenge.system.user.rest.resource.SystemUserResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.robot.challenge.fixture.system.PreLoadedSystemUser.MACIEJ_KLYS;
import static com.robot.challenge.fixture.system.PreLoadedSystemUser.SLAWOMIR_BANDO;
import static com.robot.challenge.fixture.system.PreLoadedSystemUser.values;
import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by mklys on 04/02/16.
 */
@RunWith(Arquillian.class)
public class SystemUserFinderTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAll(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(SystemResource.SYSTEM_PREFIX + SystemUserResource.RETRIEVE_ALL)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<BriefSystemUserModel> systemUsers = response.readEntity(new GenericType<List<BriefSystemUserModel>>() {});

        assertThat(systemUsers.size()).isEqualTo(PreLoadedSystemUser.values().length);
        Arrays.stream(values()).forEach(preLoadedSystemUser -> {
            Optional<BriefSystemUserModel> found = systemUsers.stream().filter(systemUser ->
                            systemUser.getGuid().equals(preLoadedSystemUser.guid()) &&
                                    systemUser.getLogin().equals(preLoadedSystemUser.login())
            ).findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfExistingLoginExists(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(SystemResource.SYSTEM_PREFIX + SystemUserResource.USER_LOGIN_EXISTS)
                .resolveTemplate("login", SLAWOMIR_BANDO.login())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantCheckIfLoginExistsByEmptyLogin(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(SystemResource.SYSTEM_PREFIX + SystemUserResource.USER_LOGIN_EXISTS)
                .resolveTemplate("login", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfNotExistingLoginExists(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(SystemResource.SYSTEM_PREFIX + SystemUserResource.USER_LOGIN_EXISTS)
                .resolveTemplate("login", "nonExistingLogin")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfExistingLoginIsFree(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(SystemResource.SYSTEM_PREFIX + SystemUserResource.USER_LOGIN_IS_FREE)
                .resolveTemplate("login", MACIEJ_KLYS.login())
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCanCheckIfNotExistingLoginIsFree(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(SystemResource.SYSTEM_PREFIX + SystemUserResource.USER_LOGIN_IS_FREE)
                .resolveTemplate("login", "nonExistingLogin")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    @OperateOnDeployment(NOT_SECURED_WEBAPP)
    public void thatCantCheckIfLoginIsFreeByEmptyLogin(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget crudResource) {
        final Invocation.Builder requestBuilder = crudResource
                .path(SystemResource.SYSTEM_PREFIX + SystemUserResource.USER_LOGIN_IS_FREE)
                .resolveTemplate("login", "")
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response  = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
    }
}
