package com.robot.challenge.functional.system.dictionary;

import com.robot.challenge.fixture.system.PreLoadedSystemRole;
import com.robot.challenge.functional.deployment.DeploymentSetup;
import com.robot.challenge.infrastructure.config.RestConfig;
import com.robot.challenge.system.dictionary.rest.model.SystemRoleModel;
import com.robot.challenge.system.dictionary.rest.resource.SystemDictionaryResource;
import com.robot.challenge.system.rest.resource.SystemResource;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class SystemDictionaryTest extends DeploymentSetup {

    @Test
    @OperateOnDeployment(DeploymentSetup.NOT_SECURED_WEBAPP)
    public void thatCanRetrieveAllSystemRoles(@ArquillianResteasyResource(RestConfig.ROOT_CONTEXT) ResteasyWebTarget resource) {
        final Invocation.Builder requestBuilder = resource
                .path(SystemResource.SYSTEM_PREFIX + SystemDictionaryResource.SYSTEM_ROLE_ENDPOINT)
                .request();
        final Invocation request = requestBuilder.buildGet();
        final Response response = request.invoke();

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

        List<SystemRoleModel> systemRoles = response.readEntity(new GenericType<List<SystemRoleModel>>() {});
        assertThat(systemRoles.size()).isEqualTo(Arrays.asList(PreLoadedSystemRole.values()).size());
        Arrays.stream(PreLoadedSystemRole.values()).forEach(preLoadedCompetitionType -> {
            Optional<SystemRoleModel> found = systemRoles.stream()
                    .filter(systemRole -> assertCompetitionTypeDictionaryItem(systemRole, preLoadedCompetitionType))
                    .findFirst();
            assertThat(found.isPresent()).isTrue();
        });
    }

    private boolean assertCompetitionTypeDictionaryItem(SystemRoleModel actual, PreLoadedSystemRole expected) {
        return actual.getGuid().equals(expected.guid()) &&
                actual.getName().equals(expected.systemRoleName()) &&
                actual.getVersion() != null;
    }
}