package com.robot.challenge.functional.system.user;

import com.robot.challenge.fixture.system.SystemUserFixture;
import com.robot.challenge.functional.crud.ExtendedCrudTest;
import com.robot.challenge.infrastructure.rest.model.ModelCreated;
import com.robot.challenge.system.rest.resource.SystemResource;
import com.robot.challenge.system.user.rest.model.user.ExistingSystemUserModel;
import com.robot.challenge.system.user.rest.model.user.NewSystemUserModel;
import com.robot.challenge.system.user.rest.resource.SystemUserResource;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class ExtendedSystemUserTest extends ExtendedCrudTest<NewSystemUserModel, ExistingSystemUserModel, SystemUserFixture> {

    public ExtendedSystemUserTest() {
        super(ExistingSystemUserModel.class, new SystemUserFixture());
    }

    @Override
    protected void assertCreated(NewSystemUserModel expected, ExistingSystemUserModel actual) {
        assertThat(actual.getGuid()).isNotNull().isNotEmpty();
        assertThat(actual.getLogin()).isEqualTo(expected.getLogin());
        assertThat(actual.getFirstName()).isEqualTo(expected.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(expected.getLastName());
        assertThat(actual.getSystemRoleGuid()).isEqualTo(expected.getSystemRoleGuid());
        assertThat(actual.getVersion()).isEqualTo(0L);
    }

    @Override
    protected void assertUpdated(ExistingSystemUserModel actual, NewSystemUserModel original, ModelCreated created, ExistingSystemUserModel expected) {
        assertThat(actual.getGuid()).isEqualTo(created.getGuid());
        assertThat(actual.getLogin()).isEqualTo(original.getLogin());
        assertThat(actual.getFirstName()).isEqualTo(expected.getFirstName());
        assertThat(actual.getLastName()).isEqualTo(expected.getLastName());
        assertThat(actual.getSystemRoleGuid()).isEqualTo(expected.getSystemRoleGuid());
        assertThat(actual.getVersion()).isEqualTo(expected.getVersion() + 1);
    }

    private String context() {
        return SystemResource.SYSTEM_PREFIX;
    }

    @Override
    protected String createEndpoint() {
        return context() + SystemUserResource.CREATE;
    }

    @Override
    protected String retrieveEndpoint() {
        return context() + SystemUserResource.RETRIEVE_SINGLE;
    }

    @Override
    protected String updateEndpoint() {
        return context() + SystemUserResource.UPDATE;
    }

    @Override
    protected String deleteEndpoint() {
        return context() + SystemUserResource.DELETE;
    }
}
